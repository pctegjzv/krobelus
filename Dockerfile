FROM golang:1.8

RUN which tar

RUN curl -L --progress-bar http://get.alauda.cn/golang/migrate.linux-amd64.tar.gz | tar xz && \
    mv migrate.linux-amd64 /usr/local/bin/migrate

COPY . $GOPATH/src/krobelus

COPY ./run/json-schema /krobelus/json-schema
COPY ./migrations /krobelus/migrations

WORKDIR $GOPATH/src/krobelus

ENV KROBELUS=$GOPATH/src/krobelus

RUN go install && chmod +x run/scripts/*.sh && mkdir -p /var/log/mathilde


EXPOSE 8080 6070

CMD ["./run/scripts/migrate-and-run.sh"]
