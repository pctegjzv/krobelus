package store

import (
	"encoding/json"
	"fmt"
	"strings"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"
	"krobelus/model"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	"github.com/juju/errors"
	log "github.com/sirupsen/logrus"
)

type ServiceInstanceStore struct {
	ResourceStore
	Request       *model.ServiceInstanceRequest
	Response      *model.ServiceInstanceResponse
	UpdateRequest *model.ServiceInstanceUpdate
}

type ServiceInstanceListStore struct {
	ResourceListStore
	Items []*model.ServiceInstanceListItemResponse
}

func (s *ServiceInstanceStore) LoadUpdateObject() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	return nil
}

func (s *ServiceInstanceStore) PatchUpdateObject() error {
	if s.UpdateRequest.Kubernetes != nil {
		newUpdate, err := json.Marshal(s.UpdateRequest.Kubernetes)
		json.NewDecoder(strings.NewReader(string(newUpdate))).Decode(&s.Object)
		if err != nil {
			panic(err)
		}
		return nil
	}
	return nil
}

// LoadServiceInstance generate ServiceInstanceResponse by uuid from cache
func (s *ServiceInstanceStore) LoadServiceInstance() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	s.Response = s.Object.ToServiceInstanceResponse()
	s.Response.Cluster.UUID = s.UniqueName.ClusterUUID
	o, _ := s.Object.ToServiceInstance()
	plan, broker, err := ClusterServiceBrokerPlanToResponseByInstance(o, s.UniqueName.ClusterUUID)
	s.Response.ClusterServicePlan = plan
	if broker != nil {
		s.Response.Resource.BrokerStatus = model.GetBrokerStatus(broker)
	}
	if err != nil {
		return err
	}
	return nil
}

func (s *ServiceInstanceListStore) LoadServiceInstances() error {
	if err := s.LoadCacheObjects(); err != nil {
		return err
	}
	var items []*model.ServiceInstanceListItemResponse
	for _, object := range s.Objects {
		response := object.ToServiceInstanceListItenResponse()
		si, _ := object.ToServiceInstance()
		response.Cluster.UUID = s.ClusterUUID
		log.Errorf("si.Spec.ClusterServiceClassRef.Name: %s", si.Spec.ClusterServiceClassRef.Name)

		namekey := infra.GetKubernetesNameCacheKey(
			s.ClusterUUID, si.Spec.ClusterServiceClassRef.Name,
			common.ClusterServiceClassType, "")

		log.Infof("ClusterServiceClass NameKey is: %s", si.Spec.ClusterServiceClassRef.Name)
		if si.Spec.ClusterServiceClassRef.Name != "" {
			classStore := ResourceStore{
				NameKey: namekey,
			}
			err := classStore.LoadCacheObject()
			if err != nil {
				log.Errorf("Load Cache classStore error: %s", err.Error())
			} else {
				cs, _ := classStore.Object.ToServiceClass()
				brokerNameKey := infra.GetKubernetesNameCacheKey(s.ClusterUUID, cs.Spec.ClusterServiceBrokerName, common.ClusterServiceBrokerType, "")
				brokerStore := &ResourceStore{
					NameKey: brokerNameKey,
				}
				err = brokerStore.LoadCacheObject()
				if err != nil {
					log.Errorf("servicebroker retrieve from cache error: %s", err.Error())
				}
				br, err := brokerStore.Object.ToBroker()
				if err != nil {
					log.Errorf("object to broker error: %s", err.Error())
				} else {
					response.BrokerStatus = model.GetBrokerStatus(br)
				}
			}
		}
		response.AppNumbers, _ = getBindingNumbersOfInstanceName(object, si.GetNamespace(), s.ClusterUUID)
		items = append(items, response)
	}
	s.Items = items
	return nil
}

func getBindingNumbersOfInstanceName(s *model.KubernetesObject, namespace string, clusterUUID string) (int, error) {
	iName := s.Name
	var appNum int
	pattern := fmt.Sprintf("*%s*%s*%s*", clusterUUID, namespace, common.ServiceBindingType)
	log.Infof("__servicebinding__ pattern: %s", pattern)

	cacheKeys, err := infra.GetMatchCacheKeys(pattern)
	if err != nil {
		log.Errorf("GetMatchCacheKeys error: %s", err.Error())
		return appNum, err
	}
	for _, cacheKey := range cacheKeys {
		cacheKey = strings.TrimPrefix(cacheKey, config.GlobalConfig.Redis.KeyPrefixReader)
		redis, err := infra.GetRedis()
		if err != nil {
			return appNum, err
		}
		data, err := redis.Get(cacheKey)
		if err != nil {
			return appNum, errors.Annotate(err, "get resource from cacheKey cache error")
		}
		if data == "" {
			return appNum, errors.Annotate(common.ErrNotFound,
				fmt.Sprintf("cluster uuid:%s: reource type:%s", clusterUUID, common.ServiceBindingType))
		}
		ks, err := model.BytesToKubernetesObject([]byte(data))
		if err != nil {
			return appNum, errors.Annotate(err, "parse cached resource data error")
		}
		bind, _ := ks.ToServiceBinding()
		if bind.Spec.ServiceInstanceRef.Name == iName && bind.GetNamespace() == namespace {
			appNum++
		}
	}
	return appNum, nil
}

func ClusterServiceBrokerPlanToResponseByInstance(si *scV1beta1.ServiceInstance, clusterId string) (*model.ServicePlanResponse,
	*scV1beta1.ClusterServiceBroker,
	error) {
	if si.Spec.ClusterServicePlanRef == nil {
		return nil, nil, nil
	}
	pattern := fmt.Sprintf("*%s*%s*%s*", clusterId, common.ClusterServicePlanType, si.Spec.ClusterServicePlanRef.Name)
	cacheKeys, err := infra.GetMatchCacheKeys(pattern)
	if err != nil {
		return nil, nil, err
	}
	for _, cacheKey := range cacheKeys {
		cacheKey = strings.TrimPrefix(cacheKey, config.GlobalConfig.Redis.KeyPrefixReader)
		redis, err := infra.GetRedis()
		if err != nil {
			return nil, nil, err
		}
		data, err := redis.Get(cacheKey)
		if err != nil {
			return nil, nil, errors.Annotate(err, "get resource from cacheKey cache error")
		}
		if data == "" {
			return nil, nil, errors.Annotate(common.ErrNotFound,
				fmt.Sprintf("name:%s: reource type:%s", si.Spec.ClusterServicePlanRef.Name, common.ClusterServicePlanType))
		}
		ks, err := model.BytesToKubernetesObject([]byte(data))
		if err != nil {
			return nil, nil, errors.Annotate(err, "parse cached resource data error")
		}
		csPlan, _ := ks.ToServicePlan()
		if si.Spec.ClusterServicePlanRef.Name == csPlan.ObjectMeta.Name {
			brokerStore := &ResourceStore{
				NameKey: infra.GetKubernetesNameCacheKey(
					clusterId, csPlan.Spec.ClusterServiceBrokerName,
					common.ClusterServiceBrokerType, ""),
			}
			err = brokerStore.LoadCacheObject()
			if err != nil {
				log.Errorf("servicebroker retrieve from cache error: %s", err.Error())
			}
			br, err := brokerStore.Object.ToBroker()
			if err != nil {
				log.Errorf("object to broker error: %s", err.Error())
			}

			return &model.ServicePlanResponse{
				Kubernetes: ks.Spec,
			}, br, nil
		}
	}
	return nil, nil, nil
}
