package store

import (
	"fmt"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
)

type PVCStore struct {
	ResourceStore
	Request       *model.PVCRequest
	Response      *model.PVCResponse
	UpdateRequest *model.PVCUpdate
}

type PVCListStore struct {
	ResourceListStore
	Items []*model.PVCResponse
}

// LoadPVC generate PVCResponse by uuid from cache
//TODO: add option to access cluster
func (s *PVCStore) LoadPVC() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	s.Response = s.Object.ToPVCResponse()
	s.Response.Cluster.UUID = s.UniqueName.ClusterUUID
	return nil
}

func (s *PVCListStore) LoadPVCs() error {
	if err := s.LoadCacheObjects(); err != nil {
		return err
	}
	var items []*model.PVCResponse
	for _, object := range s.Objects {
		response := object.ToPVCResponse()
		response.Cluster.UUID = s.ClusterUUID
		response.Kubernetes = nil
		items = append(items, response)
	}

	s.Items = items
	return nil
}

func GetPVCByNamespace(clusterID, namespace, name string) (*model.PVCResponse, error) {
	query := model.Query{
		ClusterUUID: clusterID,
		Namespace:   namespace,
		Name:        name,
		Type:        common.PersistentVolumeClaimK8SResType,
	}
	s := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &query,
			Logger:     common.GetLoggerByRegionID(clusterID),
		},
	}
	if err := s.FetchResource(); err != nil {
		return nil, err
	}
	return s.Object.ToPVCResponse(), nil
}

func AppendPVToPVCResponse(pvcs []*model.PVCResponse) error {

	if 0 == len(pvcs) {
		return nil
	}

	ClusterUUID := pvcs[0].Cluster.UUID

	var PVNames []string

	Map := make(map[string]*model.ResourceNameBase)

	for _, pvc := range pvcs {
		if nil != pvc.PersistentVolume && "" != pvc.PersistentVolume.Name && "Lost" != pvc.Resource.Status {
			PVNames = append(PVNames, pvc.PersistentVolume.Name)
			Map[pvc.PersistentVolume.Name] = pvc.PersistentVolume
		}
	}

	if 0 == len(PVNames) {
		return nil
	}

	pvs, err := GetPVListFromCacheByNames(ClusterUUID, PVNames)
	if err != nil {
		return errors.New(fmt.Sprintf("Append refs to pvc response error: %+v", err))
	}

	for _, name := range PVNames {
		if pv, ok := (*pvs)[name]; ok {
			Map[name].UUID = string(pv.UID)
		}
	}

	return nil

}
