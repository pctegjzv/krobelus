package store

import (
	"krobelus/common"

	"k8s.io/client-go/rest"
)

// HandleRequest helps tarns from v3 to v1 common api. Many v3 resources have special needs,
// put it here. This function handle POST/PUT/DELETE request.
func (s *KubernetesResourceStore) HandleRequest(method string) (*rest.Result, error) {
	if method == common.HttpPost {
		if s.UniqueName.Type == "namespaces" {
			return s.CreateOrGetNamespace()
		}
	}
	if method == common.HttpDelete {
		if s.UniqueName.Type == "namespaces" {
			return s.DeleteNamespace()
		}
	}
	return s.Request(method, nil)
}
