package store

import (
	"encoding/json"
	"fmt"
	"strings"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"
	"krobelus/model"

	"github.com/juju/errors"
)

type BrokerStore struct {
	ResourceStore
	Request       *model.BrokerRequest
	Response      *model.BrokerResponse
	UpdateRequest *model.BrokerUpdate
}

type BrokerListStore struct {
	ResourceListStore
	Items       []*model.BrokerResponse
	ClassNumMap map[string]int
}

func (s *BrokerStore) PatchUpdateObject() error {
	if s.UpdateRequest.Kubernetes != nil {
		newUpdate, err := json.Marshal(s.UpdateRequest.Kubernetes)
		json.NewDecoder(strings.NewReader(string(newUpdate))).Decode(&s.Object)
		if err != nil {
			panic(err)
		}
		return nil
	}
	return nil
}

func (s *BrokerStore) LoadUpdateObject() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	return nil
}

// LoadBroker generate BrokerResponse by uuid from cache
func (s *BrokerStore) LoadBroker() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	s.Response = s.Object.ToBrokerResponse()
	s.Response.Cluster.UUID = s.UniqueName.ClusterUUID
	return nil
}

func (s *BrokerListStore) LoadBrokers() error {
	if err := s.LoadCacheObjects(); err != nil {
		return err
	}
	var items []*model.BrokerResponse

	// need optimize
	classNumMap := make(map[string]int)
	s.ClassNumMap = classNumMap
	// get class
	pattern := fmt.Sprintf("*%s*%s*", s.ClusterUUID, common.ClusterServiceClassType)
	cacheKeys, err := infra.GetMatchCacheKeys(pattern)
	if err != nil {
		return err
	}
	for _, cacheKey := range cacheKeys {
		cacheKey = strings.TrimPrefix(cacheKey, config.GlobalConfig.Redis.KeyPrefixReader)
		redis, err := infra.GetRedis()
		if err != nil {
			return err
		}
		data, err := redis.Get(cacheKey)
		if err != nil {
			return errors.Annotate(err, "get resource from cacheKey cache error")
		}
		if data == "" {
			return errors.Annotate(common.ErrNotFound,
				fmt.Sprintf("cluster uuid:%s: reource type:%s", s.ClusterUUID, common.ClusterServiceClassType))
		}
		ks, err := model.BytesToKubernetesObject([]byte(data))
		if err != nil {
			return errors.Annotate(err, "parse cached resource data error")
		}
		csclass, _ := ks.ToServiceClass()
		classNumMap[csclass.Spec.ClusterServiceBrokerName]++
	}
	for _, object := range s.Objects {
		response := object.ToBrokerResponse()
		response.Cluster.UUID = s.ClusterUUID
		items = append(items, response)
	}
	s.Items = items
	return nil
}
