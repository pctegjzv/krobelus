package store

import (
	"encoding/json"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"

	"k8s.io/apimachinery/pkg/labels"
)

var IgnoreKinds = []string{
	common.KubernetesKindPod,
	common.KubernetesKindEndpoints,
	common.KubernetesKindReplicaSet,
}

type TopologyStore struct {
	Request  *model.TopologyRequest
	Objects  []*model.KubernetesObject
	Response *model.TopologyResponse
	Logger   common.Log
}

func (ts *TopologyStore) LoadObjects() {
	pattern := infra.GetKubernetesCacheKeyMatch(ts.Request.Cluster.UUID, ts.Request.Namespace.Name, "")
	ts.Logger.Debugf("Kubernetes name pattern, %s", pattern)
	items, err := infra.GetMatchData(pattern)
	if err != nil {
		ts.Logger.Errorf("Get kubernetes match data error, %s", err.Error())
		return
	}

	var objects []*model.KubernetesObject
	for _, item := range items {
		var object model.KubernetesObject
		if err = json.Unmarshal([]byte(item), &object); err != nil {
			ts.Logger.Errorf("Json unmarshal error, %s, %s, skip", item, err.Error())
			continue
		}
		if common.StringInSlice(object.Kind, IgnoreKinds) {
			continue
		}
		objects = append(objects, &object)
	}
	ts.Objects = objects
	return
}

func (ts *TopologyStore) ParseTopology() {
	nodes := make(map[string]*model.Node, len(ts.Objects))
	for _, object := range ts.Objects {
		nodes[string(object.UID)] = &model.Node{TypeMeta: object.TypeMeta, ObjectMeta: object.ObjectMeta}
	}
	ts.Response = &model.TopologyResponse{
		Graph: &model.Graph{Nodes: nodes, Edges: []*model.Edge{}},
		Refer: &model.Refer{Reference: []*model.ReferNode{}, ReferencedBy: []*model.ReferNode{}},
	}

	for _, object := range ts.Objects {
		switch object.Kind {
		case common.KubernetesKindDeployment:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindStatefulSet:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindDaemonSet:
			ts.parseReferenceForPodController(object)
		case common.KubernetesKindService:
			ts.parseReferenceForService(object)
		case common.KubernetesKindHPA:
			ts.parseReferenceForHPA(object)
		case common.KubernetesKindRoute:
			ts.parseReferenceForRoute(object)
		}
	}

	name, kind := ts.Request.Name, ts.Request.Kind
	if name == "" && kind == "" {
		return
	}
	target := &model.KubernetesObject{}
	for _, object := range ts.Objects {
		if object.Name == name && object.Kind == kind {
			target = object
			break
		}
	}
	if target == nil {
		ts.Logger.Infof("Can not find kubernetes object %s/%s", kind, name)
		return
	}

	graph, refer := ts.Response.Graph, ts.Response.Refer
	for _, edge := range ts.Response.Graph.Edges {
		if edge.From == string(target.UID) {
			refer.Reference = append(refer.Reference, &model.ReferNode{Type: edge.Type, Node: graph.Nodes[edge.To]})
		}
		if edge.To == string(target.UID) {
			refer.ReferencedBy = append(refer.ReferencedBy, &model.ReferNode{Type: edge.Type, Node: graph.Nodes[edge.From]})
		}
	}
}

func (ts *TopologyStore) parseReferenceForPodController(from *model.KubernetesObject) {
	template, err := from.GetPodTemplateSpec()
	if err != nil {
		ts.Logger.Errorf("Get pod template error, %s", err.Error())
		return
	}
	containers := template.Spec.Containers
	for _, container := range containers {
		for _, env := range container.EnvFrom {
			var kind, name string
			if env.ConfigMapRef != nil {
				kind = common.KubernetesKindConfigmap
				name = env.ConfigMapRef.Name
			}
			if env.SecretRef != nil {
				kind = common.KubernetesKindSecret
				name = env.SecretRef.Name
			}
			ts.addEdge(from, kind, name, common.Reference)
		}
	}

	for _, volume := range template.Spec.Volumes {
		var kind, name string
		if volume.PersistentVolumeClaim != nil {
			kind = common.KubernetesKindPVC
			name = volume.PersistentVolumeClaim.ClaimName
		}
		if volume.ConfigMap != nil {
			kind = common.KubernetesKindConfigmap
			name = volume.ConfigMap.Name
		}
		if volume.Secret != nil {
			kind = common.KubernetesKindSecret
			name = volume.Secret.SecretName
		}
		ts.addEdge(from, kind, name, common.Reference)
	}

	accountName := template.Spec.ServiceAccountName
	if accountName != "" {
		ts.addEdge(from, common.KubernetesKindServiceAccount, accountName, common.Reference)
	}
}

func (ts *TopologyStore) parseReferenceForService(from *model.KubernetesObject) {
	spec, err := from.GetServiceSpec()
	if err != nil {
		ts.Logger.Errorf("Get service spec error, %s", err.Error())
		return
	}
	if len(spec.Selector) == 0 {
		ts.Logger.Debugf("Service %s has no selector, skip", from.Name)
		return
	}
	selector := labels.SelectorFromSet(spec.Selector)
	for _, object := range ts.Objects {
		if exist, _ := common.KubernetesDeployModeKind[object.Kind]; !exist {
			continue
		}
		template, _ := object.GetPodTemplateSpec()
		if selector.Matches(labels.Set(template.Labels)) {
			ts.addEdge(from, object.Kind, object.Name, common.Selector)
		}
	}
}

func (ts *TopologyStore) parseReferenceForHPA(from *model.KubernetesObject) {
	spec, err := from.GetHPASpec()
	if err != nil {
		ts.Logger.Errorf("Get HPA spec error, %s", err.Error())
		return
	}
	reference := spec.ScaleTargetRef
	ts.addEdge(from, reference.Kind, reference.Name, common.Reference)
}

func (ts *TopologyStore) parseReferenceForRoute(from *model.KubernetesObject) {
	reference, ok := from.Spec["to"]
	if !ok {
		ts.Logger.Errorf("Can not find to field for Route")
		return
	}
	meta, ok := reference.(map[string]interface{})
	if !ok {
		ts.Logger.Errorf("Assert reference to map failed")
		return
	}
	ts.addEdge(from, meta["kind"].(string), meta["name"].(string), common.Reference)
}

func (ts *TopologyStore) addEdge(from *model.KubernetesObject, kind string, name string, edgeType string) {
	var to *model.KubernetesObject
	for _, object := range ts.Objects {
		if object.Kind == kind && object.Name == name {
			to = object
		}
	}
	if to == nil {
		ts.Logger.Errorf("Object %s/%s not found", kind, name)
		return
	}

	edge := model.Edge{
		From: string(from.UID),
		To:   string(to.UID),
		Type: edgeType,
	}
	ts.Response.Graph.Edges = append(ts.Response.Graph.Edges, &edge)
}
