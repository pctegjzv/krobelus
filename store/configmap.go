package store

import (
	"krobelus/common"
	"krobelus/model"
)

type ConfigMapStore struct {
	ResourceStore
	Request       *model.ConfigMapRequest
	Response      *model.ConfigMapResponse
	UpdateRequest *model.ConfigMapUpdate
}

type ConfigMapListStore struct {
	ResourceListStore
	Items []*model.ConfigMapResponse
}

func (s *ConfigMapStore) LoadUpdateObject() error {
	if s.UpdateRequest.Kubernetes != nil {
		s.Object = s.UpdateRequest.Kubernetes
		return nil
	}
	if s.UpdateRequest.Resource.Description != nil {
		if err := s.LoadCacheObjectByUID(); err != nil {
			return err
		}
		model.UpdateAnnotations(
			s.Object,
			common.AnnotationsDescription,
			*s.UpdateRequest.Resource.Description,
		)
	}
	return nil
}

// LoadConfigMap generate ConfigMapResponse by uuid from cache
//TODO: add option to access cluster
func (s *ConfigMapStore) LoadConfigMap() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	s.Response = s.Object.ToConfigMapResponse()
	s.Response.Cluster.UUID = s.UniqueName.ClusterUUID
	return nil
}

func (s *ConfigMapListStore) LoadConfigMaps() error {
	if err := s.LoadCacheObjects(); err != nil {
		return err
	}
	var items []*model.ConfigMapResponse
	for _, object := range s.Objects {
		response := object.ToConfigMapResponse()
		response.Cluster.UUID = s.ClusterUUID
		items = append(items, response)
	}
	s.Items = items
	return nil
}

func GetConfigMapByNamespace(clusterID, namespace, name string) (*model.ConfigMapResponse, error) {
	query := model.Query{
		ClusterUUID: clusterID,
		Namespace:   namespace,
		Name:        name,
		Type:        "configmaps",
	}
	s := KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: &query,
			Logger:     common.GetLoggerByRegionID(clusterID),
		},
	}
	if err := s.FetchResource(); err != nil {
		return nil, err
	}
	return s.Object.ToConfigMapResponse(), nil
}
