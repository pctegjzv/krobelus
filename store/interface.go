package store

import (
	"fmt"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"
	"krobelus/infra/kubernetes"
	"krobelus/model"

	"github.com/gin-gonic/gin/json"
	"github.com/juju/errors"
	apiV1beta1 "k8s.io/api/extensions/v1beta1"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

// KubernetesStore interface to interact with kubernetes cluster.
type KubernetesStore interface {
	GetChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) (*rest.Result, error)
	CreateChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error
	UpdateChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error
	RollbackChildResource(parent model.AlaudaResource, resource *model.KubernetesResource, revision int64) error
	DeleteChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error

	ListChildResourcesWithSelector(parent model.AlaudaResource, kind string) (*rest.Result, error)
	DeleteChildResourcesWithSelector(parent model.AlaudaResource, kind string) error
}

type KubernetesStoreForWorker struct {
	KubernetesResourceStore
}

func GenerateKubernetesStore() KubernetesStore {
	if config.GlobalConfig.Worker.Mode == "test" {
		return &KubernetesStoreForTest{}
	} else {
		return &KubernetesStoreForWorker{}
	}
}

func CreateApplicationObjects(parent model.AlaudaResource, objects []*model.KubernetesObject) error {
	store := GenerateKubernetesStore()
	for _, object := range objects {
		resource := object.ToResource()
		if err := store.CreateChildResource(parent, resource); err != nil {
			if err == common.ErrKubernetesResourceExists {
				if err := store.UpdateChildResource(parent, resource); err != nil {
					return err
				}
			} else {
				return err
			}
		}
	}
	return nil
}

func DeleteApplicationObjects(parent model.AlaudaResource, objects []*model.KubernetesObject) error {
	store := GenerateKubernetesStore()
	for _, object := range objects {
		resource := object.ToResource()
		if err := store.DeleteChildResource(parent, resource); err != nil {
			return err
		}
	}
	return nil
}

func DeleteApplication(app model.AlaudaResource, object *model.KubernetesObject) error {
	store := GenerateKubernetesStore()
	if err := store.DeleteChildResource(app, object.ToResource()); err != nil {
		return err
	}
	return nil
}

func CreateOtherResources(parent model.AlaudaResource, resources []*model.KubernetesResource) error {
	store := GenerateKubernetesStore()
	for _, resource := range resources {
		if err := store.CreateChildResource(parent, resource); err != nil {
			if err == common.ErrKubernetesResourceExists {
				if err := store.UpdateChildResource(parent, resource); err != nil {
					return err
				}
			} else {
				return err
			}
		}
	}
	return nil
}

func DeleteOtherResources(parent model.AlaudaResource, resources []*model.KubernetesResource) error {
	store := GenerateKubernetesStore()
	for _, resource := range resources {
		if err := store.DeleteChildResource(parent, resource); err != nil {
			return err
		}
	}
	return nil
}

// GetChildResource trying to get a kubernetes resource with it's parent info.
func (s *KubernetesStoreForWorker) GetChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) (*rest.Result, error) {
	s.LoadResource(parent, resource)
	s.Logger.Infof("Trying to retrieve %s in %s", resource.Name, resource.Namespace)
	return s.Request(common.HttpGet, nil)
}

// CreateChildResource trying to create a kubernetes resource with it's parent info.
func (s *KubernetesStoreForWorker) CreateChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error {
	s.LoadResource(parent, resource)
	// Get kubernetes resource before create.
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	result, err := s.Request(common.HttpGet, nil)
	// If resource not found, create it.
	if err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof(prompt + "not found, create it")
			if _, err := s.Request(common.HttpPost, nil); err != nil {
				return errors.Annotate(err, prompt+"create error")
			}
			return nil
		} else {
			return errors.Annotate(err, prompt+"get error")
		}
	}

	// If resource exists, conflict error occurs.
	object, err := model.RestResultToResource(result)
	if err != nil {
		return errors.Annotate(err, prompt+"result to resource error")
	}
	if !parent.IsFamily(object) {
		return errors.New(prompt + "already exists, but it does not belong to the same alauda resource")
	}
	return common.ErrKubernetesResourceExists
}

// UpdateChildResource trying to update a kubernetes resource with it's parent info.
func (s *KubernetesStoreForWorker) UpdateChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error {
	s.LoadResource(parent, resource)
	// If resource not found, error occurs.
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	result, err := s.Request(common.HttpGet, nil)
	if err != nil {
		return err
	}

	// If resource does not belong to this service, skip.
	// Otherwise merge origin data if necessary.
	old, err := model.RestResultToResource(result)
	if err != nil {
		return errors.Annotate(err, prompt+"result to resource error")
	}
	if !parent.IsFamily(old) {
		return errors.New(prompt + "already exists, but it does not belong to this alauda resource")
	}
	mergeOriginData(old, resource)

	// If resource already exists, update it.
	s.Object = resource.ToObject()
	if _, err := s.Request(common.HttpPut, nil); err != nil {
		return errors.Annotate(err, prompt+"update error")
	}
	return nil
}

// RollbackChildResource trying to roll back a kubernetes resource with it's parent info.
func (s *KubernetesStoreForWorker) RollbackChildResource(parent model.AlaudaResource, resource *model.KubernetesResource, revision int64) error {
	switch resource.Kind {
	case common.KubernetesKindDeployment:
		rollback := apiV1beta1.DeploymentRollback{
			TypeMeta: metaV1.TypeMeta{
				Kind:       "DeploymentRollback",
				APIVersion: common.KubernetesAPIVersionExtensionsV1beta1,
			},
			Name:       resource.Name,
			RollbackTo: apiV1beta1.RollbackConfig{Revision: revision},
		}
		data, err := json.Marshal(rollback)
		if err != nil {
			return errors.Annotate(err, "Rollback marshal")
		}
		object, err := model.BytesToKubernetesObject(data)
		if err != nil {
			return errors.Annotate(err, "To kubernetes object")
		}
		object.Name = resource.Name
		s.LoadResource(parent, resource)
		s.UniqueName.SubType = "rollback"
		s.Object = object
		if _, err = s.Request(common.HttpPost, nil); err != nil {
			return errors.Annotate(err, "Post request for deployment")
		}
		return nil
	case common.KubernetesKindDaemonSet:
		s.LoadResource(parent, resource)
		s.Object = nil
		s.UniqueName.Name = ""
		s.UniqueName.Type = common.GetKubernetesTypeFromKind(common.KubernetesControllerRevision)
		selector := map[string]string{"labelSelector": parent.GetLabelSelector()}
		result, err := s.Request(common.HttpGet, selector)
		if err != nil {
			return errors.Annotate(err, "Get revision list")
		}
		data, err := model.FindControllerRevisionFromRestResult(result, revision)
		if err != nil {
			return errors.Annotate(err, "Find controller revision")
		}
		object, err := model.BytesToKubernetesObject(data)
		if err != nil {
			return errors.Annotate(err, "To kubernetes object")
		}
		s.LoadResource(parent, resource)
		object.APIVersion = resource.APIVersion
		s.Object = object
		s.UniqueName.PatchType = string(types.StrategicMergePatchType)
		if _, err = s.Request(common.HttpPatch, nil); err != nil {
			return errors.Annotate(err, "Patch request for daemonset")
		}
		return nil
	default:
		return fmt.Errorf("%v does not support rollback now", resource.Kind)
	}
}

// DeleteChildResource will delete a kubernetes resource if it really exists.
// If resource exists in cluster but does not belongs to same service, then skip delete.
func (s *KubernetesStoreForWorker) DeleteChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error {
	s.LoadResource(parent, resource)
	// Get kubernetes resource before delete.
	prompt := fmt.Sprintf("%v %v in %v ", resource.Kind, resource.Name, resource.Namespace)
	result, err := s.Request(common.HttpGet, nil)
	// If resource not found in cluster, skip.
	if err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof(prompt + "not found, skip")
			return nil
		} else if err == kubernetes.ErrorResourceKindNotFound {
			s.Logger.Infof(prompt + " kind not found, skip")
			return nil
		} else {
			return errors.Annotate(err, prompt+"get error when delete resource")
		}
	}

	// If resource does not belong to this service, skip.
	object, err := model.RestResultToResource(result)
	if err != nil {
		return errors.Annotate(err, prompt+"result to resource error")
	}
	uid := model.GetAlaudaUID(object)
	if !parent.IsFamily(object) {
		s.Logger.Infof(prompt + "does not belong to the same alauda resource, skip")
		return nil
	}

	// If resource belongs to this service, delete.
	if _, err := s.Request(common.HttpDelete, nil); err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof(prompt + "not found, skip")
		} else {
			return errors.Annotate(err, prompt+"delete error")
		}
	}
	infra.DeleteResourceFromJakiro(uid)
	return nil
}

// DeleteChildResourcesWithSelector will delete kubernetes resources with the same label and kind.
func (s *KubernetesStoreForWorker) DeleteChildResourcesWithSelector(parent model.AlaudaResource, kind string) error {
	s.LoadResource(parent, nil)
	s.ResourceStore.UniqueName.Type = common.GetKubernetesTypeFromKind(kind)
	s.ResourceStore.UniqueName.Name = ""
	s.Logger.Infof("Trying to delete %s with selector: %s", kind, parent.GetLabelSelector())

	selector := map[string]string{"labelSelector": parent.GetLabelSelector()}
	if _, err := s.Request(common.HttpDelete, selector); err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof("Api version %s not supported or resource not found? skip delete")
			return nil
		}
		return errors.Annotate(err, "delete resources error")
	}
	return nil
}

// ListChildResourcesWithSelector will list kubernetes resources with the same label and kind.
func (s *KubernetesStoreForWorker) ListChildResourcesWithSelector(parent model.AlaudaResource, kind string) (*rest.Result, error) {
	s.LoadResource(parent, nil)
	s.ResourceStore.UniqueName.Type = common.GetKubernetesTypeFromKind(kind)
	s.ResourceStore.UniqueName.Name = ""
	s.Logger.Infof("Trying to list %s with selector: %s", kind, parent.GetLabelSelector())

	selector := map[string]string{"labelSelector": parent.GetLabelSelector()}
	return s.Request(common.HttpGet, selector)
}

// mergeOriginData will merge exist data with the new one. for some resources, this is needed.
// such as PVC, when bounded, it's spec will changed(bad design).
func mergeOriginData(old *model.KubernetesResource, now *model.KubernetesResource) error {
	switch old.Kind {
	case common.KubernetesKindPVC:
		now.Spec = old.Spec
	case common.KubernetesKindService:
		oldService, err := old.ToV1Service()
		if err != nil {
			return err
		}
		nowService, err := now.ToV1Service()
		if err != nil {
			return err
		}
		nowService.Spec.ClusterIP = oldService.Spec.ClusterIP
		nowService.SetResourceVersion(oldService.ResourceVersion)
		return now.ParseV1Service(nowService)
	default:
		now.SetResourceVersion(old.ResourceVersion)
	}
	return nil
}

func SendKubernetesApplicationEvent(parent model.AlaudaResource, ev *model.Event) error {
	ev.Name = parent.GetName()
	ev.Namespace = parent.GetNamespace()
	// allow caller to custom uid
	if ev.UID == "" {
		ev.UID = parent.GetUUID()
	}

	event := model.GenKubernetesApplicationEvent(ev)
	resource, err := model.ToKubernetesResource(event)
	if err != nil {
		return err
	}
	return GenerateKubernetesStore().CreateChildResource(parent, resource)
}
