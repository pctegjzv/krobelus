package store

import (
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"

	"github.com/juju/errors"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

// KubernetesResourceStore combine the ability to retrieve kubernetes resource from cache and cluster
type KubernetesResourceStore struct {
	ResourceStore
	Client rest.Interface
}

func (s *KubernetesResourceStore) String() string {
	return fmt.Sprintf("%s/%s/%s", s.UniqueName.Namespace, s.UniqueName.Type, s.UniqueName.Name)
}

// GetKubernetesResourceStore generate a KubernetesResourceStore from query/log/object
func GetKubernetesResourceStore(query *model.Query, logger common.Log, object *model.KubernetesObject) KubernetesResourceStore {
	return KubernetesResourceStore{
		ResourceStore: ResourceStore{
			UniqueName: query,
			Logger:     logger,
			Object:     object,
		},
	}
}

func (s *KubernetesResourceStore) LoadResource(parent model.AlaudaResource, resource *model.KubernetesResource) {
	s.UniqueName = &model.Query{
		ClusterUUID: parent.GetClusterUUID(),
		Namespace:   parent.GetNamespace(),
		Type:        "",
		Name:        "",
	}
	s.Object = nil
	s.Logger = parent.GetLogger()

	if resource != nil {
		s.UniqueName.Type = common.GetKubernetesTypeFromKind(resource.Kind)
		s.UniqueName.Name = resource.Name
		s.Object = resource.ToObject()
	}
}

func (s *KubernetesResourceStore) ResourceTypeForObject(obj *model.KubernetesObject) (string, error) {
	gv, err := schema.ParseGroupVersion(obj.APIVersion)
	if err != nil {
		return "", err
	}

	return s.ResourceTypeForGVK(schema.GroupVersionKind{
		Group: gv.Group,
		Kind:  obj.Kind,
	})
}

func (s *KubernetesResourceStore) ResourceTypeForGVK(gvk schema.GroupVersionKind) (string, error) {
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return "", err
	}
	return client.ResourceTypeForGVK(gvk)
}

func (s *KubernetesResourceStore) LoadClient() error {
	s.Logger.Debugf("Load resource store client with query: %+v", s.UniqueName)
	client, err := infra.NewCachedKubeClient(s.UniqueName.ClusterUUID, s.Logger)
	if err != nil {
		return err
	}

	groupVersion := ""
	if s.Object != nil {
		groupVersion = s.Object.APIVersion
	}

	s.Client, err = client.ClientForGroupVersionResource(groupVersion, s.UniqueName.Type)
	if err != nil {
		return err
	}

	return nil
}

// SetRaw sets data bytes to 'Raw', and clear the 'Object' value.
func (s *KubernetesResourceStore) SetRaw(raw []byte) *KubernetesResourceStore {
	s.Raw = raw
	// Set Object to nil to avoid use Object as request body.
	s.Object = nil
	return s
}

func (s *KubernetesResourceStore) Request(method string, params map[string]string) (*rest.Result, error) {
	if err := s.LoadClient(); err != nil {
		return nil, err
	}
	var request *rest.Request
	switch method {
	case common.HttpPost:
		request = s.PostRequest()
	case common.HttpDelete:
		request = s.DeleteRequest()
	case common.HttpPut:
		request = s.PutRequest()
	case common.HttpGet:
		request = s.GetRequest()
	case common.HttpPatch:
		request = s.PatchRequest()
	default:
		return nil, errors.New(fmt.Sprintf("Unsupported request method: %s", method))
	}

	if params != nil {
		for key, value := range params {
			request = request.Param(key, value)
		}
	}

	request.NamespaceIfScoped(s.UniqueName.Namespace, s.NamespaceScoped())

	s.Logger.Debugf("Request kubernetes: %s %s", method, request.URL().String())
	t1 := time.Now()
	result := request.Do()
	s.Logger.Info("Request kubernetes cost: ", time.Since(t1))
	if result.Error() != nil {
		s.Logger.Infof("Request kubernetes result error: %s", result.Error().Error())
	} else {
		var code int
		result.StatusCode(&code)
		s.Logger.Debugf("Request kubernetes result code: %d", code)
	}
	return &result, result.Error()
}

func (s *KubernetesResourceStore) getBody() []byte {
	body := s.Raw
	if s.Object != nil {
		body, _ = s.Object.ToBytes()
	}
	s.Logger.Infof("Trying to create/update resource: %s", string(body))
	return body
}

func (s *KubernetesResourceStore) NamespaceScoped() bool {
	if s.UniqueName.Namespace == "" {
		return false
	} else {
		return true
	}
}

func (s *KubernetesResourceStore) GetRequest() *rest.Request {
	request := s.Client.Get().Resource(s.UniqueName.Type).SubResource(s.UniqueName.SubType)
	if s.UniqueName.Name != "" {
		request.Name(s.UniqueName.Name)
	}
	return request
}

func (s *KubernetesResourceStore) DeleteRequest() *rest.Request {
	request := s.Client.Delete().
		Resource(s.UniqueName.Type).
		SubResource(s.UniqueName.SubType)
	if s.UniqueName.Name != "" {
		request.Name(s.UniqueName.Name)
	}
	return request
}

func (s *KubernetesResourceStore) PostRequest() *rest.Request {
	request := s.Client.Post().
		Resource(s.UniqueName.Type).
		SubResource(s.UniqueName.SubType).
		Body(s.getBody())
	if s.UniqueName.Name != "" && s.UniqueName.SubType == "rollback" {
		request.Name(s.UniqueName.Name)
	}
	return request
}

func (s *KubernetesResourceStore) PutRequest() *rest.Request {
	return s.Client.Put().
		Resource(s.UniqueName.Type).
		Name(s.UniqueName.Name).
		SubResource(s.UniqueName.SubType).
		Body(s.getBody())
}

func (s *KubernetesResourceStore) PatchRequest() *rest.Request {
	return s.Client.Patch(types.PatchType(s.UniqueName.PatchType)).
		Resource(s.UniqueName.Type).
		Name(s.UniqueName.Name).
		SubResource(s.UniqueName.SubType).
		Body(s.getBody())
}

// AddResourceToCache manually add resource to cache
func (s *KubernetesResourceStore) AddResourceToCache() error {
	key := infra.ParseQueryToNameKey(s.UniqueName)
	redis, err := infra.GetRedis()
	if err != nil {
		return err
	}
	s.Logger.Debug("add resource to cache ", s.String())
	return redis.Set(key, s.Raw, time.Second*time.Duration(300))
}

// FetchResource fetches a resource from cache or cluster.
func (s *KubernetesResourceStore) FetchResource() error {
	// Get from the cache first then from the cluster
	if err := s.LoadCacheObject(); err == nil {
		s.Logger.Debugf("Get resource %s in %s from cache", s.UniqueName.Type, s.UniqueName.ClusterUUID)
		return nil
	}

	result, err := s.Request("GET", nil)
	if result == nil && err != nil {
		s.Logger.Errorf("Get resource %s in %s failed: %s", s.UniqueName.Type, s.UniqueName.ClusterUUID, err.Error())
		return err
	}

	var code int
	result.StatusCode(&code)

	if result.Error() != nil {
		s.Logger.Infof("Request failed: %v %v ", code, result.Error())
		return ParseResultError(result)
	}

	s.Logger.Debugf("Request get status code: %d", code)
	return s.ParseResultData(result)
}

func ParseResultError(result *rest.Result) error {
	var code int
	result.StatusCode(&code)
	if code == 404 {
		return errors.Annotate(common.ErrResourceNotExist, result.Error().Error())
	}
	return result.Error()
}

func (s *KubernetesResourceStore) ParseResultData(result *rest.Result) error {
	raw, err := result.Raw()
	if err != nil {
		return err
	}
	s.Raw = raw
	s.Object, err = model.BytesToKubernetesObject(raw)
	return err
}

// FetchResourceList fetches resources from cache or cluster.
func (s *KubernetesResourceStore) FetchResourceList(params map[string]string) error {

	// Get from the cache first then from the cluster
	if err := s.LoadCacheObjects(params); err == nil {
		s.Logger.Debugf("Get resource list %s in %s from cache", s.UniqueName.Type, s.UniqueName.ClusterUUID)
		return nil
	}

	result, err := s.Request("GET", params)
	if result == nil && err != nil {
		s.Logger.Errorf("Get resource list %s in %s failed: %s", s.UniqueName.Type, s.UniqueName.ClusterUUID, err.Error())
		return err
	}

	if result.Error() != nil {
		s.Logger.Infof("Request failed: %v", result.Error())
		return result.Error()
	}

	raw, err := result.Raw()
	if err != nil {
		return err
	}
	s.Raw = raw
	s.Objects, err = model.BytesToKubernetesObjectList(raw)
	if err != nil {
		return err
	}
	return nil
}

func (s *KubernetesResourceStore) DeleteChildResources(resourcetype string, labelSelector string) error {
	krs := GetKubernetesResourceStore(&model.Query{
		ClusterUUID: s.UniqueName.ClusterUUID,
		Name:        "",
		Namespace:   s.UniqueName.Namespace,
		Type:        resourcetype,
	}, s.Logger, nil)
	s.Logger.Infof("Trying to delete %s with selector: %s", resourcetype, labelSelector)

	selector := map[string]string{"labelSelector": labelSelector}
	if _, err := krs.Request(common.HttpDelete, selector); err != nil {
		if apiErrors.IsNotFound(err) {
			s.Logger.Infof("Api version %s not supported or resource not found? skip delete")
			return nil
		}
		return errors.Annotate(err, "delete resources error")
	}
	return nil
}

func (s *KubernetesResourceStore) UpdateResourceCache(result *rest.Result) error {
	if err := s.ParseResultData(result); err != nil {
		return err
	}
	if s.UniqueName.Name == "" {
		s.UniqueName.Name = s.Object.Name
	}
	return s.AddResourceToCache()
}
