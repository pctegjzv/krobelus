package store

import (
	"encoding/json"
	"fmt"
	"strings"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"
	"krobelus/model"

	"github.com/alecthomas/jsonschema"
	"github.com/juju/errors"
	log "github.com/sirupsen/logrus"
	"k8s.io/apimachinery/pkg/runtime"
)

type ServiceClassStore struct {
	ResourceStore
	Plans    *ServicePlanListStore
	Broker   *BrokerStore
	Response *model.ServiceClassResponse
}

type ServiceClassListStore struct {
	ResourceListStore
	// Items []*model.ServiceClassItemResponse
}

type ServicePlanListStore struct {
	ResourceListStore
	Items []*model.ServicePlanResponse
}

type Empty struct {
}

type InputParameters struct {
	Parameters interface{} `json:"parameters,omitempty"`
}

// LoadServiceClass generate ServiceClassResponse by uuid from cache
func (s *ServiceClassStore) LoadServiceClass() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	s.Response = s.Object.ToServiceClassResponse()
	scclass, err := s.Object.ToServiceClass()
	if err := s.GetServicesPlansByClassName(scclass.Spec.ClusterServiceBrokerName); err != nil {
		return err
	}
	if err != nil {
		return err
	}
	s.Response.ClusterServiceClass.Cluster.UUID = s.UniqueName.ClusterUUID
	s.Response.ServiceClassPlans = make([]*model.ServicePlanResponse, 0)

	for _, planObj := range s.Plans.ResourceListStore.Objects {
		planResp := &model.ServicePlanResponse{
			Kubernetes: planObj.Spec,
		}
		s.Response.ServiceClassPlans = append(s.Response.ServiceClassPlans, planResp)
	}

	brokerStore := &ResourceStore{
		NameKey: infra.GetKubernetesNameCacheKey(
			s.UniqueName.ClusterUUID, scclass.Spec.ClusterServiceBrokerName,
			common.ClusterServiceBrokerType, ""),
	}
	err = brokerStore.LoadCacheObject()
	if err != nil {
		return err
	}
	cm, err := brokerStore.Object.ToBroker()
	if err != nil {
		return err
	}
	s.Response.ServiceClassBroker = &model.BrokerResource{
		ResourceBasic: model.ResourceBasic{
			Name:        cm.Name,
			UUID:        string(cm.UID),
			Description: cm.GetAnnotations()[common.AnnotationsDescription],
			CreatedAt:   cm.GetCreationTimestamp().Time,
			UpdatedAt:   cm.GetCreationTimestamp().Time,
		},
		Status: model.GetBrokerStatus(cm),
	}
	return nil
}

func (s *ServiceClassStore) GetServicesPlansByClassName(brokerName string) error {
	className := s.UniqueName.Name
	pattern := fmt.Sprintf("*%s*%s*", s.UniqueName.ClusterUUID, common.ClusterServicePlanType)
	cacheKeys, err := infra.GetMatchCacheKeys(pattern)
	if err != nil {
		log.Errorf("GetMatchCacheKeys error: %s", err.Error())
		return err
	}
	csplans := &ServicePlanListStore{}
	for _, cacheKey := range cacheKeys {
		cacheKey = strings.TrimPrefix(cacheKey, config.GlobalConfig.Redis.KeyPrefixReader)
		redis, err := infra.GetRedis()
		if err != nil {
			return err
		}
		data, err := redis.Get(cacheKey)
		if err != nil {
			return errors.Annotate(err, "get resource from cacheKey cache error")
		}
		if data == "" {
			return errors.Annotate(common.ErrNotFound,
				fmt.Sprintf("cluster uuid:%s: reource type:%s", s.UniqueName.ClusterUUID, common.ClusterServicePlanType))
		}
		ks, err := model.BytesToKubernetesObject([]byte(data))
		if err != nil {
			return errors.Annotate(err, "parse cached resource data error")
		}
		csplan, _ := ks.ToServicePlan()
		if csplan.Spec.ClusterServiceClassRef.Name == className && csplan.Spec.ClusterServiceBrokerName == brokerName {
			r := jsonschema.Reflector{
				ExpandedStruct: true,
			}

			emptySchema := InputParameters{
				Parameters: r.Reflect(&Empty{}),
			}
			emptySchemaJson, err := json.Marshal(emptySchema.Parameters)

			if err != nil {
				return errors.Annotate(err, "InputParameters TestUser marshal err")
			}
			if csplan.Spec.ServiceInstanceCreateParameterSchema == nil {
				csplan.Spec.ServiceInstanceCreateParameterSchema = &runtime.RawExtension{Raw: emptySchemaJson}
			}
			if csplan.Spec.ServiceInstanceUpdateParameterSchema == nil {
				csplan.Spec.ServiceInstanceUpdateParameterSchema = &runtime.RawExtension{Raw: emptySchemaJson}
			}
			if csplan.Spec.ServiceBindingCreateParameterSchema == nil {
				csplan.Spec.ServiceBindingCreateParameterSchema = &runtime.RawExtension{Raw: emptySchemaJson}
			}
			ks.ParseServicePlan(csplan)
			csplans.ResourceListStore.Objects = append(csplans.ResourceListStore.Objects, ks)
		}
	}
	s.Plans = csplans
	return nil
}

// GetServicesClasses will retrieves brokers'class list from cache.
func GetServicesClassesByViewableBrokerUUIDs(uuids []string, clusterUUID string, brokerFilter string, classFilter string) ([]*model.ServiceClassItemResponse, error) {
	result := make([]*model.ServiceClassItemResponse, 0)
	brokerNames := make([]string, 0)
	brokerNameResourceMap := make(map[string]*model.BrokerResource)

	for _, uuid := range uuids {
		s := &BrokerStore{
			ResourceStore: ResourceStore{
				UUID: uuid,
			},
		}
		err := s.LoadBroker()
		if err != nil {
			log.Errorf("LoadBroker error: %s", err.Error())
			continue
		}
		brokerNames = append(brokerNames, s.UniqueName.Name)
		brokerNameResourceMap[s.UniqueName.Name] = &s.Response.Resource
		if clusterUUID == "" {
			clusterUUID = s.UniqueName.ClusterUUID
		}
	}
	// load all classes
	pattern := fmt.Sprintf("*%s*%s*", clusterUUID, common.ClusterServiceClassType)

	cacheKeys, err := infra.GetMatchCacheKeys(pattern)
	if err != nil {
		return nil, err
	}
	for _, cacheKey := range cacheKeys {
		cacheKey = strings.TrimPrefix(cacheKey, config.GlobalConfig.Redis.KeyPrefixReader)
		redis, err := infra.GetRedis()
		if err != nil {
			return nil, err
		}
		data, err := redis.Get(cacheKey)
		if err != nil {
			return nil, errors.Annotate(err, "get resource from cacheKey cache error")
		}
		if data == "" {
			return nil, errors.Annotate(common.ErrNotFound,
				fmt.Sprintf("cluster uuid:%s: reource type:%s", clusterUUID, common.ClusterServiceClassType))
		}
		ks, err := model.BytesToKubernetesObject([]byte(data))
		if err != nil {
			return nil, errors.Annotate(err, "parse cached resource data error")
		}
		csclass, _ := ks.ToServiceClass()
		if brokerFilter != "" && brokerFilter != csclass.Spec.ClusterServiceBrokerName {
			continue
		}
		if classFilter != "" && classFilter != csclass.Name {
			continue
		}
		if common.StringInSlice(csclass.Spec.ClusterServiceBrokerName, brokerNames) {
			classListItem := &model.ServiceClassItemResponse{
				ClusterServiceClassSpec: csclass.Spec,
				UUID:   csclass.ObjectMeta.UID,
				Broker: brokerNameResourceMap[csclass.Spec.ClusterServiceBrokerName],
			}
			result = append(result, classListItem)
		}
	}
	return result, nil
}
