package store

import (
	"krobelus/common"
	"krobelus/db"
	"krobelus/model"

	"k8s.io/api/core/v1"
)

func GetServiceInstances(ss *db.ServiceStore) ([]*model.KubernetesObject, error) {
	service := ss.Response
	s := GetKubernetesResourceStore(&model.Query{
		Namespace:   service.Namespace.Name,
		ClusterUUID: service.Cluster.UUID,
		Type:        "pods",
	}, common.ToLog(ss.Logger.WithField("service_id", service.Resource.UUID)), nil)

	if err := s.FetchResourceList(map[string]string{"labelSelector": service.GetLabelSelector()}); err != nil {
		return nil, err
	} else {
		return s.Objects.Items, nil
	}
}

func GetAppPods(ss *model.ServiceListStore) (result []*v1.Pod, err error) {
	s := GetKubernetesResourceStore(&model.Query{
		Namespace:   ss.Namespace,
		Type:        "pods",
		ClusterUUID: ss.Services[0].Cluster.UUID,
	}, ss.Logger, nil)
	if err := s.FetchResourceList(map[string]string{"labelSelector": ss.PodSelector}); err != nil {
		return nil, err
	} else {
		for _, object := range s.Objects.Items {
			pod, err := object.ToPod()
			if err != nil {
				return nil, err
			}
			result = append(result, pod)
		}
	}
	return
}

func GetServicePods(ss *db.ServiceStore) (result []*v1.Pod, err error) {
	objects, err := GetServiceInstances(ss)
	if err != nil {
		return
	}
	for _, object := range objects {
		pod, err := object.ToPod()
		if err != nil {
			return nil, err
		}
		result = append(result, pod)
	}
	return
}

// GetServicesInstances will retrieves services'pod list from cache.
func GetServicesPods(ss *model.ServiceListStore) ([]*v1.Pod, error) {
	if ss.PodSelector != "" {
		return GetAppPods(ss)
	}

	var result []*v1.Pod
	for _, uuid := range ss.GetServiceUUIDs() {
		service := model.ServiceResponse{
			Resource: model.ServiceResource{
				UUID: uuid,
			},
			Namespace: model.Namespace{
				Name: ss.Namespace,
			},
			Cluster: model.Cluster{
				UUID: ss.Services[0].Cluster.UUID,
			},
		}
		pods, err := GetServicePods(&db.ServiceStore{Response: &service, Logger: ss.Logger})
		if err != nil {
			return result, err
		} else {
			result = append(result, pods...)
		}
	}
	return result, nil
}
