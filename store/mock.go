package store

import (
	"krobelus/model"

	"k8s.io/client-go/rest"
)

type KubernetesStoreForTest struct {
}

func (s *KubernetesStoreForTest) GetChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) (*rest.Result, error) {
	return nil, nil
}

func (s *KubernetesStoreForTest) CreateChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error {
	return nil
}

func (s *KubernetesStoreForTest) UpdateChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error {
	return nil
}

func (s *KubernetesStoreForTest) RollbackChildResource(parent model.AlaudaResource, resource *model.KubernetesResource, revision int64) error {
	return nil
}

func (s *KubernetesStoreForTest) DeleteChildResource(parent model.AlaudaResource, resource *model.KubernetesResource) error {
	return nil
}

func (s *KubernetesStoreForTest) ListChildResourcesWithSelector(parent model.AlaudaResource, kind string) (*rest.Result, error) {
	return nil, nil
}

func (s *KubernetesStoreForTest) DeleteChildResourcesWithSelector(parent model.AlaudaResource, kind string) error {
	return nil
}
