package store

import (
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	log "github.com/sirupsen/logrus"
)

type ServiceBindingStore struct {
	ResourceStore
	ServiceStore  *db.ServiceStore
	Request       *model.ServiceBindingRequest
	Response      *model.ServiceBindingResponse
	UpdateRequest *model.ServiceBindingUpdate
}

type ServiceBindingListStore struct {
	ResourceListStore
	Items []*model.ServiceBindingResponse
}

func (s *ServiceBindingStore) LoadUpdateObject() error {
	if s.UpdateRequest.Kubernetes != nil {
		s.Object = s.UpdateRequest.Kubernetes
		return nil
	}
	return nil
}

func (s *ServiceBindingStore) LoadServiceBinding() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	s.Response = s.Object.ToServiceBindingResponse()
	s.Response.Cluster.UUID = s.UniqueName.ClusterUUID

	return nil
}

func (s *ServiceBindingListStore) LoadServiceBindingsbyFilter(namespace string, serviceUUID string, filterInstanceName string) error {

	if err := s.LoadCacheObjects(); err != nil {
		return err
	}
	ins := &scV1beta1.ServiceInstance{}
	var items []*model.ServiceBindingResponse
	for _, object := range s.Objects {
		response := object.ToServiceBindingResponse()
		sb, err := object.ToServiceBinding()
		uuid := sb.ObjectMeta.Annotations[common.SvcUidKey()]
		if sb.Spec.Parameters == nil {
			response.Kubernetes.Spec["parameters"] = make(map[string]string)
		}
		if uuid == "" {
			continue
		}
		if serviceUUID != "" && uuid != serviceUUID {
			continue
		}
		store := &db.ServiceStore{
			UUID: uuid,
		}
		response.Cluster.UUID = s.ClusterUUID
		client, err := infra.NewKubeClient(s.ClusterUUID)
		if err != nil {
			return err
		}

		err = store.GetService(false)
		if err != nil {
			log.Errorf("Service retrieve in db error: %s", err.Error())
			err = client.DeleteV1Secret(namespace, sb.Spec.SecretName)
			if err != nil {
				log.Errorf("Secret delete in db error: %s", err.Error())
			}
			err = client.DeleteServiceBinding(namespace, sb.Name)
			if err != nil {
				log.Errorf("Binding delete in db error: %s", err.Error())
			}
			continue
		}
		secret, err := client.GetV1Secret(sb.Spec.SecretName, namespace)
		if err == nil && secret.Data != nil {
			for k, byteArray := range secret.Data {
				response.Credential[k] = string(byteArray[:len(byteArray)])
			}
		}
		response.ServiceResource = store.Response.Resource

		// else not found

		instanceName := sb.Spec.ServiceInstanceRef.Name
		if filterInstanceName != "" && filterInstanceName != instanceName {
			continue
		}

		instanceStore := &ResourceStore{
			NameKey: infra.GetKubernetesNameCacheKey(s.ClusterUUID, instanceName, common.ServiceInstanceType, namespace),
		}

		err = instanceStore.LoadCacheObject()
		if err != nil {
			log.Errorf("serviceInstance retrieve from cache error: %s", err.Error())
		} else {
			ins, err = instanceStore.Object.ToServiceInstance()
			if err != nil {
				log.Errorf("object to serviceInstance error: %s", err.Error())
				return err
			}
		}

		if ins != nil {
			response.ServiceInstanceRef = model.ServiceInstanceRef{
				ResourceBasic: model.ResourceBasic{
					UUID:        string(ins.UID),
					Name:        ins.Name,
					Description: ins.GetAnnotations()[common.AnnotationsDescription],
					CreatedAt:   ins.GetCreationTimestamp().Time,
					UpdatedAt:   ins.GetCreationTimestamp().Time,
				},
				ClusterServicePlan:  ins.Spec.ClusterServicePlanExternalName,
				ClusterServiceClass: ins.Spec.ClusterServiceClassExternalName,
			}
			items = append(items, response)
		}
	}
	s.Items = items
	return nil
}
