package store

import (
	"encoding/json"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"

	"github.com/juju/errors"
	log "github.com/sirupsen/logrus"
	"k8s.io/api/core/v1"
)

// PVStore ...
type PVStore struct {
	ResourceStore
	Request       *model.PVRequest
	Response      *model.PVResponse
	UpdateRequest *model.PVUpdateRequest
	Status        *model.AlaudaResourceStatus
	PV            *v1.PersistentVolume
}

// PVListStore ..
type PVListStore struct {
	ResourceListStore
	Items []*model.PVResponse
}

// LoadPV generate PVResponse by uuid from cache
func (s *PVStore) LoadPV() error {
	if err := s.LoadCacheObjectByUID(); err != nil {
		return err
	}
	s.Response = s.Object.ToPVResponse()
	s.Response.Cluster.Name = s.Object.ClusterName
	s.Response.Cluster.UUID = s.UniqueName.ClusterUUID
	return nil
}

func (s *PVListStore) LoadPVs() error {
	if err := s.LoadCacheObjects(); err != nil {
		return err
	}
	var items []*model.PVResponse
	for _, object := range s.Objects {
		response := object.ToPVResponse()
		response.Cluster.UUID = s.ClusterUUID
		items = append(items, response)
	}
	s.Items = items
	return nil
}

func GetPVListFromCacheByNames(clusterUUID string, names []string) (*model.CacheResponse, error) {
	keys := make([]string, len(names))
	for i, n := range names {
		keys[i] = infra.GetKubernetesNameCacheKey(clusterUUID, n, common.PersistentVolumeK8SResType, "")
	}

	return getPVListFromCacheByKeys(keys)
}

// GetPVListFromCache will get PersistentVolumes from cache if possible.
func GetPVListFromCache(store *PVListStore) (*model.CacheResponse, error) {

	logger := log.WithField("action", "GetPVListFromCache").WithField("region_id", store.ClusterUUID)

	keys := make([]string, len(store.UUIDs))
	for i, uuid := range store.UUIDs {
		keys[i] = infra.GetKubernetesResourceCacheKey(uuid)
	}

	r, err := infra.GetRedis()
	if err != nil {
		return nil, err
	}
	vals, err := r.MGet(keys...)
	if err != nil {
		logger.WithError(err).Error("mget %s error", keys)
		return nil, err
	}

	results, err := getPVListFromCacheByKeys(vals)
	if err != nil {
		logger.WithError(err).Error("Get pv list from cache by keys: %s", keys)
		return nil, err
	}

	return results, nil
}

func getPVListFromCacheByKeys(keys []string) (*model.CacheResponse, error) {
	r, err := infra.GetRedis()
	if err != nil {
		return nil, err
	}
	vals, err := r.MGet(keys...)

	if err != nil {
		return nil, errors.Trace(err)
	}
	if 0 == len(vals) {
		return nil, common.ErrCacheMiss
	}

	result := make(model.CacheResponse)

	for _, pvString := range vals {
		pv := &v1.PersistentVolume{}
		err := json.Unmarshal([]byte(pvString), pv)
		if nil != err {
			return nil, err
		}
		pvName := string(pv.ObjectMeta.Name)
		result[pvName] = pv
	}
	return &result, nil
}

// GetPVFromCache ...
func GetPVFromCache(pvUUID string) (*v1.PersistentVolume, error) {

	logger := log.WithField("action", "GetPVFromCache").WithField("pv", pvUUID)

	r, err := infra.GetRedis()
	if err != nil {
		return nil, err
	}

	key := infra.GetKubernetesResourceCacheKey(pvUUID)

	pvNameKey, err := r.Get(key)
	if err != nil {
		logger.WithError(err).Error("get pv name key failed!")
		return nil, errors.Trace(err)
	}

	exist, err := r.Exists(pvNameKey)
	if err != nil {
		logger.WithError(err).Error("verify exist failed!")
		return nil, errors.Trace(err)
	}
	if !exist {
		logger.WithError(err).Error("cache miss")
		return nil, common.ErrCacheMiss
	}

	pvString, err := r.Get(pvNameKey)
	logger.Debugf("--- %s", pvString)

	if err != nil {
		logger.WithError(err).Error("get pv info in cache failed")
		return nil, err
	}

	pv := &v1.PersistentVolume{}

	err = json.Unmarshal([]byte(pvString), pv)

	if nil != err {
		logger.WithError(err).Error("unable to unmarshal")
		return nil, err
	}
	return pv, nil
}
