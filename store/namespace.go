package store

import (
	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/rest"
)

type NamespaceStore struct {
	Request *model.NamespaceRequest
	ResourceStore
}

//TODO: remove
type NamespaceListStore struct {
	ResourceListStore
}

// CreateOrGetNamespace will create a namespace or get it (builtin namespace)
// This is a special case for common resource create api, before call this function,
// s.Object/s.UniqueName should be present. Since namespace is a very import resource,
// try to make it successfully as possible
func (s *KubernetesResourceStore) CreateOrGetNamespace() (*rest.Result, error) {
	if !model.IsBuiltInNamespace(s.Object.Name) {
		client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
		if err != nil {
			return nil, err
		}
		result, err := s.Request("POST", nil)

		if err == nil {
			err = client.SyncFromDefault(s.Object.Name)
			return result, err
		}

		if !apiErrors.IsAlreadyExists(err) {
			return result, err
		}
	}
	s.UniqueName.Name = s.Object.Name
	result, err := s.Request("GET", nil)
	return result, err
}

// DeleteNamespace will skip builtin namespace and only do a GET request
func (s *KubernetesResourceStore) DeleteNamespace() (*rest.Result, error) {
	method := common.HttpDelete
	if model.IsBuiltInNamespace(s.UniqueName.Name) {
		s.Logger.Infof("This is a builtin namespace %s, skip delete", s.UniqueName.Name)
		method = common.HttpGet
	}
	return s.Request(method, nil)
}
