package main

import (
	"os"

	"krobelus/cmd"
	"krobelus/config"
	"krobelus/db"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
)

var (
	app        *cli.App
	configPath string
)

func init() {
	// Initialise a CLI app
	app = cli.NewApp()
	app.Name = "krobelus"
	app.Usage = "Alauda Kubernetes Resource Manager"
	app.Author = "Alauda"
	app.Email = "info@alauda.io"
	app.Version = "1.0.0"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:        "c",
			Value:       "",
			Destination: &configPath,
			Usage:       "Path to a configuration file",
		},
	}
}

func main() {
	config.InitConfig()
	config.SetLogger()

	app.Commands = []cli.Command{
		{
			Name:  "api",
			Usage: "launch krobelus api",
			Action: func(c *cli.Context) {
				cmd.API()
			},
		},
		{
			Name:  "worker",
			Usage: "launch krobelus worker",
			Action: func(c *cli.Context) {
				cmd.Worker()
			},
		},
		{
			Name:  "websocket",
			Usage: "launch krobelus websocket",
			Action: func(c *cli.Context) {
				cmd.Websocket()
			},
		},
	}

	db.InitDB()

	// Run the CLI app
	if err := app.Run(os.Args); err != nil {
		log.Infof(err.Error())
	}
}
