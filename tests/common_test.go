package tests

import (
	"testing"

	"krobelus/common"
	"krobelus/config"
)

func TestGetKubernetesTypes(t *testing.T) {
	if common.GetKubernetesTypeFromKind("Secret") != "secrets" {
		t.Errorf("Trans type secret error")
	}
	if common.GetKubernetesTypeFromKind("StorageClass") != "storageclasses" {
		t.Errorf("Trans type storageclass error")
	}
}

func TestGetLabel(t *testing.T) {
	config.GlobalConfig.Label.BaseDomain = "alauda.io"
	label := common.SvcNameKey()
	if label != "service.alauda.io/name" {
		t.Errorf("error label: %s", label)
	}
}
