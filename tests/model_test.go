package tests

import (
	"testing"

	"krobelus/model"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func TestSetApplicationNamespace(t *testing.T) {
	object := model.KubernetesObject{
		TypeMeta: metaV1.TypeMeta{
			Kind:       "Service",
			APIVersion: "v1",
		},
		ObjectMeta: metaV1.ObjectMeta{
			Namespace: "default",
			Name:      "test",
		},
	}
	object.SetApplicationNamespace("f1")

	if object.GetNamespace() != "f1" {
		t.Errorf("Set application namespace error")
	}
}
