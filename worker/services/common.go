package services

import (
	"time"

	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"

	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

func updateStatus(service *db.ServiceStore, state common.KubernetesResourceStatus, message string) error {
	statusStore := db.StatusStore{
		Logger: service.Logger,
		Status: &model.AlaudaResourceStatus{
			UUID:         service.GetServiceUUID(),
			CurrentState: state,
			Output:       message,
			Type:         model.AlaudaServiceType,
		},
	}
	service.Logger.Infof("Change current state to %s, output %s", state, message)
	if err := statusStore.UpdateServiceStatus(); err != nil {
		service.Logger.Errorf("Save status to db error, %s", err.Error())
		return err
	}
	if !statusStore.Status.CurrentState.IsDeployErrorStatus() {
		service.Logger.Debugf("Change service status to %s over", statusStore.Status.CurrentState)
		return nil
	}

	if err := infra.SendAlaudaEvent(&statusStore); err != nil {
		service.Logger.Errorf("Send alauda event error, %s", err.Error())
		return err
	}
	if err := sendKubernetesEvent(service, message); err != nil {
		service.Logger.Errorf("Send kubernetes event for service error, %v", err.Error())
		return err
	}
	service.Logger.Debugf("Change service status to %s over", state)
	return nil
}

func checkResource(uuid string) error {
	var err error
	for i := 0; i < 3; i++ {
		_, err = infra.GetResourceFromJakiro(uuid)
		if err != nil {
			time.Sleep(time.Second * 5)
			continue
		}
		return nil
	}
	return err
}

func sendKubernetesEvent(service *db.ServiceStore, message string) error {
	name := service.Response.Parent.Name
	uuid := service.Response.Parent.UUID
	if name == "" {
		name = service.Response.Resource.Name
		uuid = service.Response.Resource.UUID
	}
	event := v1.Event{
		TypeMeta: metaV1.TypeMeta{
			Kind:       common.KubernetesKindEvent,
			APIVersion: common.KubernetesAPIVersionV1,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      name + "-" + common.GenerateRandString(5),
			Namespace: service.Response.Namespace.Name,
		},
		InvolvedObject: v1.ObjectReference{
			Kind:      common.KubernetesKindApplication,
			Name:      name,
			Namespace: service.Response.Namespace.Name,
			UID:       types.UID(uuid),
		},
		Message: message,
		Reason:  common.FailedSync,
		Source: v1.EventSource{
			Component: common.Component,
		},
		Count:          1,
		FirstTimestamp: metaV1.Time{Time: time.Now().UTC()},
		LastTimestamp:  metaV1.Time{Time: time.Now().UTC()},
	}
	resource, err := model.ToKubernetesResource(event)
	if err != nil {
		return err
	}
	return store.GenerateKubernetesStore().CreateChildResource(service.Response, resource)
}
