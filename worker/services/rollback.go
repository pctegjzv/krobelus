package services

import (
	"strconv"

	"krobelus/common"
	"krobelus/db"
	"krobelus/model"
	"krobelus/store"

	"github.com/juju/errors"
)

// Rollback will call kubernetes api to roll back kubernetes controller for AlaudaService.
func Rollback(service *model.ServiceResponse, status *model.AlaudaResourceStatus) error {
	controller := model.GetKubernetesPodController(service.Kubernetes)
	revision, err := strconv.ParseInt(status.Input["rollbackTo"], 10, 64)
	if err != nil {
		return errors.Annotate(err, "Parse revision")
	}
	kubernetesStore := store.GenerateKubernetesStore()
	if err := kubernetesStore.RollbackChildResource(service, controller, revision); err != nil {
		return errors.Annotate(err, "Rollback failed")
	}

	result, err := kubernetesStore.GetChildResource(service, controller)
	if err != nil {
		return errors.Annotate(err, "Get request to cluster")
	}
	resource, err := model.RestResultToResource(result)
	if err != nil {
		return errors.Annotate(err, "Rest result to resource")
	}
	resource.SetResourceVersion("")
	resource.SetUID("")
	resource.SetSelfLink("")
	resource.SetGeneration(0)
	current := make([]*model.KubernetesResource, len(service.Kubernetes))
	for i, k := range service.Kubernetes {
		if !common.IsPodController(k.Kind) {
			current[i] = k
		} else {
			current[i] = resource
		}
	}
	if _, err := db.UpdateServiceYaml(service.Resource.UUID, current, service.Kubernetes); err != nil {
		return errors.Annotate(err, "Update service yaml")
	}
	return nil
}
