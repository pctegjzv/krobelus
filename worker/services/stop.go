package services

import (
	"fmt"

	"krobelus/common"
	"krobelus/db"
	"krobelus/model"
	"krobelus/store"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

// Stop will call kubernetes api to stop kubernetes resources for AlaudaService.
func Stop(uuid string) error {
	serviceStore := &db.ServiceStore{UUID: uuid, Logger: common.GetLoggerByServiceID(uuid)}
	if err := serviceStore.GetService(true); err != nil {
		message := fmt.Sprintf("Get serviceStore from db error %v", err.Error())
		return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, message)
	}

	// Replicas for pod controller will be set to zero except daemonset.
	for _, resource := range serviceStore.Response.Kubernetes {
		// Resources except pod controller have no changes.
		if !common.IsPodController(resource.Kind) {
			continue
		}
		// Daemonset will be deleted from cluster as it has no replicas.
		if resource.Kind == common.KubernetesKindDaemonSet {
			if err := deletePodController(serviceStore.Response, resource); err != nil {
				return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, err.Error())
			}
			continue
		}
		// Replicas for deployment/statefulset will be set to zero.
		if err := stopPodController(serviceStore.Response, resource); err != nil {
			return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, err.Error())
		}
	}
	return updateStatus(serviceStore, common.CurrentDeletedStatus, "Stop Done")
}

func stopPodController(service *model.ServiceResponse, resource *model.KubernetesResource) error {
	if err := resource.SetPodReplicas(0); err != nil {
		return err
	}
	kubernetesStore := store.GenerateKubernetesStore()
	if err := kubernetesStore.UpdateChildResource(service, resource); err != nil {
		if apiErrors.IsNotFound(err) {
			return nil
		}
		return err
	}
	return nil
}
