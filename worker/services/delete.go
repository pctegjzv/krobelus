package services

import (
	"fmt"

	"krobelus/common"
	"krobelus/model"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/store"

	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

// Delete will delete resources both in cluster and alauda platform for AlaudaService.
func Delete(uuid string) error {
	serviceStore := &db.ServiceStore{UUID: uuid, Logger: common.GetLoggerByServiceID(uuid)}
	if err := serviceStore.GetService(false); err != nil {
		message := fmt.Sprintf("Get serviceStore from db error %v", err.Error())
		return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, message)
	}

	// To avoid serviceStore status misjudgment, pod controller will be deleted at last.
	model.SortKubernetesObjects(serviceStore.Response.Kubernetes, model.AppKindOrder, false)

	// Delete child resources in cluster for AlaudaService.
	kubernetesStore := store.GenerateKubernetesStore()
	for _, resource := range serviceStore.Response.Kubernetes {
		if !common.IsPodController(resource.Kind) {
			if err := kubernetesStore.DeleteChildResource(serviceStore.Response, resource); err != nil {
				if apiErrors.IsNotFound(err) {
					return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, err.Error())
				}
			}
		} else {
			if err := deletePodController(serviceStore.Response, resource); err != nil {
				return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, err.Error())
			}
		}
	}

	// Delete pvc resources in cluster for AlaudaService.
	if err := kubernetesStore.DeleteChildResourcesWithSelector(serviceStore.Response, common.KubernetesKindPVC); err != nil {
		return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, err.Error())
	}

	// Delete references resources in cluster for AlaudaService.
	if err := serviceStore.DeleteReferences(); err != nil {
		message := fmt.Sprintf("Delete serviceStore references error: %s", err.Error())
		return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, message)
	}

	// Delete jakiro resources in cluster for AlaudaService.
	if err := infra.DeleteResourceFromJakiro(serviceStore.Response.Resource.UUID); err != nil {
		message := fmt.Sprintf("Delete jakiro resource error: %v", err.Error())
		return updateStatus(serviceStore, common.CurrentDeleteErrorStatus, message)
	}
	return updateStatus(serviceStore, common.CurrentDeletedStatus, "Delete Done")
}

func deletePodController(service *model.ServiceResponse, resource *model.KubernetesResource) error {
	kubernetesStore := store.GenerateKubernetesStore()
	if err := kubernetesStore.DeleteChildResource(service, resource); err != nil {
		if apiErrors.IsNotFound(err) {
			return nil
		}
		return err
	}
	if err := kubernetesStore.DeleteChildResourcesWithSelector(service, common.KubernetesKindReplicaSet); err != nil {
		return err
	}
	if err := kubernetesStore.DeleteChildResourcesWithSelector(service, common.KubernetesKindPod); err != nil {
		return err
	}
	return nil
}
