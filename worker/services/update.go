package services

import (
	"fmt"

	"krobelus/common"
	"krobelus/db"
	"krobelus/model"
	"krobelus/store"

	"github.com/juju/errors"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

// Update will call kubernetes api to update kubernetes resources for AlaudaService.
func Update(uuid string) error {
	serviceStore := &db.ServiceStore{UUID: uuid, Logger: common.GetLoggerByServiceID(uuid)}
	if err := serviceStore.GetService(true); err != nil {
		message := fmt.Sprintf("Get serviceStore from db error %v", err.Error())
		return updateStatus(serviceStore, common.CurrentUpdateErrorStatus, message)
	}
	status, err := db.GetStatus(uuid)
	if err != nil {
		message := fmt.Sprintf("Get status from db error %v", err.Error())
		return updateStatus(serviceStore, common.CurrentUpdateErrorStatus, message)
	}

	// If rollback parameters has been set, rollback.
	if rollbackTo, ok := status.Input["rollbackTo"]; ok && rollbackTo != "" {
		if err := Rollback(serviceStore.Response, status); err != nil {
			return updateStatus(serviceStore, common.CurrentUpdateErrorStatus, err.Error())
		} else {
			return updateStatus(serviceStore, common.CurrentUpdatedStatus, "Rollback Done")
		}
	}

	// To avoid serviceStore status misjudgment, pod controller will be updated at last.
	model.SortKubernetesObjects(serviceStore.Response.Kubernetes, model.AppKindOrder, false)

	kubernetesStore := store.GenerateKubernetesStore()
	for _, resource := range serviceStore.Response.Kubernetes {
		if err := kubernetesStore.UpdateChildResource(serviceStore.Response, resource); err != nil {
			if apiErrors.IsNotFound(err) {
				if err := kubernetesStore.CreateChildResource(serviceStore.Response, resource); err != nil {
					return updateStatus(serviceStore, common.CurrentUpdateErrorStatus, err.Error())
				}
			} else {
				return updateStatus(serviceStore, common.CurrentUpdateErrorStatus, err.Error())
			}
		}
	}

	// Try to delete resources which are no longer needed.
	if err = deleteUnusedKubernetesResources(serviceStore.Response, common.KubernetesKindService); err != nil {
		return updateStatus(serviceStore, common.CurrentUpdateErrorStatus, err.Error())
	}
	return updateStatus(serviceStore, common.CurrentUpdatedStatus, "Update Done")
}

// deleteUnusedKubernetesResources will delete unused kubernetes resources for this kind.
// resources created by other components will be ignored.
func deleteUnusedKubernetesResources(service *model.ServiceResponse, kind string) error {
	kubernetesStore := store.GenerateKubernetesStore()
	result, err := kubernetesStore.ListChildResourcesWithSelector(service, kind)
	if err != nil {
		return errors.Annotate(err, "List resources")
	}
	resources, err := model.RestResultToServiceResources(result)
	if err != nil {
		return errors.Annotate(err, "Result to services")
	}
	for _, r := range resources {
		r.Kind = kind
		if _, ok := r.Labels[common.SvcCreateByKey()]; ok {
			continue
		}
		exist := false
		for _, k := range service.Kubernetes {
			if k.Name == r.Name && k.Kind == r.Kind {
				exist = true
				break
			}
		}
		if !exist {
			if err := kubernetesStore.DeleteChildResource(service, r); err != nil {
				return errors.Annotate(err, "Delete service")
			}
		}
	}
	return nil
}
