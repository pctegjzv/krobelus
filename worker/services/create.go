package services

import (
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/model"
	"krobelus/db"
	"krobelus/store"

	"github.com/juju/errors"
)

// Create will call kubernetes api to create kubernetes resources for AlaudaService.
func Create(uuid string) error {
	serviceStore := &db.ServiceStore{UUID: uuid, Logger: common.GetLoggerByServiceID(uuid)}
	if err := serviceStore.GetService(true); err != nil {
		message := fmt.Sprintf("Get serviceStore from db error: %v", err.Error())
		return updateStatus(serviceStore, common.CurrentCreateErrorStatus, message)
	}
	// To avoid resource data inconsistency, jakiro resource will be checked
	// to make sure this serviceStore has really existed in alauda platform.
	if err := checkResource(uuid); err != nil {
		message := fmt.Sprintf("Check resource from jakiro error: %v", err.Error())
		return updateStatus(serviceStore, common.CurrentCreateErrorStatus, message)
	}

	// To avoid serviceStore status misjudgments, pod controller will be created at last.
	model.SortKubernetesObjects(serviceStore.Response.Kubernetes, model.AppKindOrder, false)
	if err := waitForAppCreated(serviceStore); err != nil {
		return updateStatus(serviceStore, common.CurrentCreateErrorStatus, err.Error())
	}

	// Update will be executed to keep it up-to-date if resource has already existed in cluster.
	kubernetesStore := store.GenerateKubernetesStore()
	for _, resource := range serviceStore.Response.Kubernetes {
		if err := kubernetesStore.CreateChildResource(serviceStore.Response, resource); err != nil {
			if err == common.ErrKubernetesResourceExists {
				if err := kubernetesStore.UpdateChildResource(serviceStore.Response, resource); err != nil {
					return updateStatus(serviceStore, common.CurrentCreateErrorStatus, err.Error())
				}
			} else {
				return updateStatus(serviceStore, common.CurrentCreateErrorStatus, err.Error())
			}
		}
	}
	return updateStatus(serviceStore, common.CurrentCreatedStatus, "Create Done")
}

func waitForAppCreated(service *db.ServiceStore) error {
	for i := 0; i < 20; i++ {
		service.Logger.Debugf("Wait for app created")
		if service.Response.Parent.UUID == "" {
			return nil
		}
		app := &db.AppStore{UUID: service.Response.Parent.UUID, Logger: service.Logger}
		if err := app.Get(); err != nil {
			return err
		}
		status := common.KubernetesResourceStatus(app.Response.Resource.Status)
		if status.IsDeployErrorStatus() {
			return errors.New("App create failed")
		}
		if status.IsDeployedStatus() {
			return nil
		}
		time.Sleep(time.Second * 5)
	}
	return errors.New("Create app timeout after wait for 100s")
}
