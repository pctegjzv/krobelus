package apps

import (
	"krobelus/common"
	"krobelus/model"
	"krobelus/db"
	"krobelus/store"
)

// AppCreate create app. Since app has been divided to services, the services will do the
// actual create job. AppCreate only need to care about kubernetes resources that are not
// belong to any services.
func AppCreate(uuid string) error {
	app := &db.AppStore{UUID: uuid, Logger: common.GetLoggerByAppID(uuid)}
	if err := app.Get(); err != nil {
		return updateStatus(app, common.CurrentCreateErrorStatus, "App not found in db")
	}

	model.SortKubernetesObjects(app.Others.Response, model.AppKindOrder, false)
	if len(app.Others.Response) > 0 {
		if err := store.CreateOtherResources(app.Response, app.Others.Response); err != nil {
			return updateStatus(app, common.CurrentCreateErrorStatus, err.Error())
		}
	}
	return updateStatus(app, common.CurrentCreatedStatus, "Create app other resources done")
}
