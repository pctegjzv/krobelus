package apps

import (
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"
)

func updateStatus(app *db.AppStore, state common.KubernetesResourceStatus, message string) error {
	uuid := app.GetAppUUID()
	statusStore := db.StatusStore{
		Logger: app.Logger,
		Status: &model.AlaudaResourceStatus{
			UUID:         uuid,
			CurrentState: state,
			Output:       message,
			Type:         model.AppType,
		},
	}
	app.Logger.Infof("Change app status to %s", statusStore.Status.CurrentState)
	if err := statusStore.UpdateAppStatus(); err != nil {
		app.Logger.Errorf("Save app status to db error, %v", err.Error())
		return err
	}
	if !statusStore.Status.CurrentState.IsDeployErrorStatus() {
		app.Logger.Debugf("Change app status to %s over", statusStore.Status.CurrentState)
		return nil
	}

	if err := infra.SendAlaudaEvent(&statusStore); err != nil {
		app.Logger.Errorf("Send alauda event for app error, %v", err.Error())
		return err
	}
	if err := store.SendKubernetesApplicationEvent(app.Response, &model.Event{
		Message: message,
		Type:    common.EventTypeWarning,
		Reason:  common.FailedSync,
	}); err != nil {
		app.Logger.Errorf("Send kubernetes event for app error, %v", err.Error())
		return err
	}
	app.Logger.Debugf("Change app status to %s over", statusStore.Status.CurrentState)
	return nil
}
