package apps

import (
	"krobelus/common"
	"krobelus/model"
	"krobelus/db"
	"krobelus/store"
)

func AppUpdate(uuid string) error {
	app := &db.AppStore{UUID: uuid, Logger: common.GetLoggerByAppID(uuid)}
	if err := app.Get(); err != nil {
		return updateStatus(app, common.CurrentUpdateErrorStatus, "App not found in db")
	}

	model.SortKubernetesObjects(app.Others.Response, model.AppKindOrder, false)
	if err := store.CreateOtherResources(app.Response, app.Others.Response); err != nil {
		return updateStatus(app, common.CurrentUpdateErrorStatus, err.Error())

	}
	return updateStatus(app, common.CurrentUpdatedStatus, "Update app other resources done")
}
