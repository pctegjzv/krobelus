package apps

import (
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/model"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/store"
)

// AppDelete will change app's current state to Deleted if all services in app has been deleted within 5 minutes.
// Otherwise, it will be changed to DeleteError. Kubernetes resources will be sorted in order to make sure all
// resources can be deleted correctly. Jakiro resource for this app will be deleted at last.
func AppDelete(uuid string) error {
	app := &db.AppStore{UUID: uuid, Logger: common.GetLoggerByAppID(uuid)}

	// Wait for all services in app has been deleted correctly
	succeed := true
	for i := 0; i < 30; i++ {
		succeed = true
		if err := app.Get(); err != nil {
			if common.IsNotExistError(err) {
				message := fmt.Sprintf("App not found in db")
				return updateStatus(app, common.CurrentDeletedStatus, message)
			}
			message := fmt.Sprintf("App retrieve in db error, %s", err.Error())
			return updateStatus(app, common.CurrentDeleteErrorStatus, message)
		}

		app.SetServices()
		for _, service := range app.Services {
			r := service.Response.Resource
			switch common.KubernetesResourceStatus(r.Status) {
			case common.CurrentDeletedStatus:
				continue
			case common.CurrentDeletingStatus:
				succeed = false
				break
			case common.CurrentDeleteErrorStatus:
				message := fmt.Sprintf("Service %v in app enter an unexpected status %v", r.Name, r.Status)
				return updateStatus(app, common.CurrentDeleteErrorStatus, message)
			}
		}
		if succeed {
			break
		}
		time.Sleep(time.Second * 10)
	}
	if !succeed {
		return updateStatus(app, common.CurrentDeleteErrorStatus, "Delete app timeout after wait for 300s")
	}

	// Delete kubernetes resources belongs to app
	model.SortKubernetesObjects(app.Others.Response, model.AppKindOrder, true)
	if len(app.Others.Response) > 0 {
		if err := store.DeleteOtherResources(app.Response, app.Others.Response); err != nil {
			return updateStatus(app, common.CurrentDeleteErrorStatus, err.Error())
		}
	}

	// Delete jakiro resource for this app
	if err := infra.DeleteResourceFromJakiro(uuid); err != nil {
		message := fmt.Sprintf("Delete jakiro resource error, %v", err.Error())
		return updateStatus(app, common.CurrentDeleteErrorStatus, message)
	}
	return updateStatus(app, common.CurrentDeletedStatus, "Delete Done")
}
