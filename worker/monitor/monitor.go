package monitor

import (
	"strings"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"

	"github.com/liticer/machinery/v1/tasks"
)

var DBTime time.Time
var Logger common.Log

// Start a monitor to watch status of alauda app/service resources. If a resource stays in deploying status
// for a long time, it's operation message may be lost, a new message will be send to message queue to resume
// this operation.
func Start() {
	if !config.GlobalConfig.Worker.MonitorEnabled {
		return
	}
	Logger = common.GetLoggerByKV("role", "monitor")
	Logger.Infof("Start monitor")
	for {
		var err error
		// Get date
		DBTime, err = db.GetDBTime()
		if err != nil {
			Logger.Errorf("Get database time error, %s, sleep 30s", err.Error())
			time.Sleep(time.Second * 30)
			continue
		}
		before := DBTime.Add(-time.Second * 30)

		Logger.Debugf("Database time, %v", DBTime)
		// List status
		statusList, err := db.ListStatus(&before, common.DeployingStatusList)
		if err != nil {
			Logger.Errorf("List status error, %s, sleep 30s", err.Error())
			time.Sleep(time.Second * 30)
			continue
		}

		// Check status
		for _, status := range statusList {
			if status.Type != model.ApplicationType {
				Logger.Infof("Check %+v", status)
				check(status)
			}
		}
		time.Sleep(time.Second * 60)
	}
}

func check(status *model.AlaudaResourceStatus) {
	backend := infra.MQServer.GetBackend()
	retry := false
	state, err := backend.GetState(status.QueueID)
	if err != nil {
		if common.IsNotExistError(err) {
			Logger.Infof("Task not found, retry")
			retry = true
		} else {
			Logger.Errorf("Get task state error, %v", err.Error())
		}
	} else {
		Logger.Infof("Task state, %+v", state)
		if DBTime.UTC().Sub(status.UpdatedAt.UTC()) >= time.Duration(120)*time.Second {
			Logger.Infof("Task Lost, retry")
			retry = true
		} else if state.State == tasks.StateReceived || state.State == tasks.StatePending || state.State == tasks.StateStarted {
			Logger.Infof("Task %v, wait", strings.ToLower(state.State))
			retry = false
		} else {
			Logger.Infof("Task %v, retry", strings.ToLower(state.State))
			retry = true
		}
	}
	if retry {
		status.QueueID = common.NewTaskUUID()
		statusStore := &db.StatusStore{Status: status, Logger: Logger}
		if _, err := statusStore.Set(nil); err != nil {
			Logger.Errorf("Save status to db error, %v", err.Error())
			return
		}
		if err := infra.SendTask(statusStore); err != nil {
			Logger.Errorf("Send task error, %v", err.Error())
			return
		}
	}
}
