package validation

import (
	"fmt"

	"krobelus/api/schema"
	"krobelus/common"
	"krobelus/db"
	"krobelus/model"
)

func ValidateServiceCreate(store *db.ServiceStore) error {
	return validateServiceKubernetesResources(store.Request.Cluster.UUID, store.Request.Kubernetes)
}

func ValidateServiceUpdate(store *db.ServiceStore) error {
	if err := validateServiceKubernetesResources(store.GetRegionUUID(), store.Request.Kubernetes); err != nil {
		return err
	}
	if err := validateServiceTargetState(store.Response.Resource.State.Target); err != nil {
		return err
	}
	if err := validateServiceCurrentState(store.Response.Resource.Status); err != nil {
		return err
	}
	return validateServiceUpdate(store)
}

func ValidateServiceStart(store *db.ServiceStore) error {
	if err := validateServiceTargetState(store.Response.Resource.State.Target); err != nil {
		return err
	}
	return validateServiceCurrentState(store.Response.Resource.State.Current)
}

func ValidateServiceStop(store *db.ServiceStore) error {
	if err := validateServiceTargetState(store.Response.Resource.State.Target); err != nil {
		return err
	}
	return validateServiceCurrentState(store.Response.Resource.State.Current)
}

func ValidateServiceDelete(store *db.ServiceStore) error {
	return validateServiceCurrentState(store.Response.Resource.State.Current)
}

func ValidateServiceRetry(store *db.ServiceStore) error {
	current := common.KubernetesResourceStatus(store.Response.Resource.State.Current)
	if !current.IsDeployErrorStatus() {
		return fmt.Errorf("only service with error status can retry, current state %v", current)
	}
	return nil
}

func ValidateServiceRollback(store *db.ServiceStore) error {
	if err := validateServiceTargetState(store.Response.Resource.State.Target); err != nil {
		return err
	}
	return validateServiceCurrentState(store.Response.Resource.State.Current)
}

func validateServiceCurrentState(current string) error {
	if common.KubernetesResourceStatus(current).IsDeployingStatus() {
		return fmt.Errorf("deploying service can't do any operations, current state %v", current)
	}
	return nil
}

func validateServiceTargetState(target string) error {
	if common.ResourceTargetState(target).IsDeleted() {
		return fmt.Errorf("deleted service can't do any operations except retry and delete")
	}
	return nil
}

// validateServiceKubernetesResources will check if the resources list contains some kind that we
// don't support for now.
func validateServiceKubernetesResources(region string, resources []*model.KubernetesResource) error {
	counts := map[string]int{
		common.KubernetesPodController: 0,
	}
	for _, v := range resources {
		if !common.KubernetesResourceTypesMap[v.Kind] {
			return fmt.Errorf("invalid kubernetes resource kind: %s", v.Kind)
		}
		if v.Name == "" {
			return fmt.Errorf("invalid kubernetes resource name: %s", v.Name)
		}
		if err := schema.CheckKubernetesResourceSchema(region, v.Kind, v); err != nil {
			return err
		}
		if common.IsPodController(v.Kind) {
			counts[common.KubernetesPodController] += 1
		}
	}
	if counts[common.KubernetesPodController] != 1 {
		return fmt.Errorf("one kubernetes pod controller is required, found %d", counts[common.KubernetesPodController])
	}
	return nil
}

// TODO: validate selector update
func validateServiceUpdate(store *db.ServiceStore) error {
	current := model.GetKubernetesPodController(store.Request.Kubernetes)
	previous := model.GetKubernetesPodController(store.Response.Kubernetes)
	if current.Name != previous.Name || current.Kind != previous.Kind || current.APIVersion != previous.APIVersion {
		store.Logger.Errorf(
			"Forbidden field to update: origin:%s/%s/%s, target:%s/%s/%s",
			previous.Kind, previous.APIVersion, previous.Name,
			current.Kind, current.APIVersion, current.Name,
		)
		return fmt.Errorf("kubernetes resource name/kind/apiVersion can not be updated")
	}
	return nil
}
