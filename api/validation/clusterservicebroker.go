package validation

import (
	"krobelus/model"

	"github.com/juju/errors"
)

func ValidateClusterServiceBrokerCommon(broker *model.BrokerRequest) error {
	if broker.Kubernetes == nil {
		return errors.New("Missing ClusterServiceBroker yaml")
	}
	return nil
}

func ValidateClusterServiceBrokerUpdate(cm *model.BrokerUpdate) error {
	if cm.Kubernetes == nil {
		return errors.New("Invalid update request for ClusterServiceBroker")
	}
	return nil
}
