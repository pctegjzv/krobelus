package validation

import (
	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
)

func ValidateServiceBindingCommon(ServiceBinding *model.ServiceBindingRequest) error {
	if ServiceBinding.Kubernetes == nil {
		return errors.New("Missing ServiceBinding yaml")
	}
	if ServiceBinding.Kubernetes.Annotations[common.SvcUidKey()] == "" {
		return errors.New("Missing service uuid annotation")
	}
	return nil
	//return ValidateServiceBindingSchema(ServiceBinding.Cluster.UUID, ServiceBinding.Kubernetes)
}

func ValidateServiceBindingUpdate(cm *model.ServiceBindingUpdate) error {
	desc := cm.Resource.Description
	if desc == nil && cm.Kubernetes == nil {
		return errors.New("Invalid update request for ServiceBinding")
	}
	return nil
}
