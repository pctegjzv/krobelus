package validation

import (
	"fmt"

	"krobelus/common"
	"krobelus/db"
)

func ValidateAppCreate(app *db.AppStore) error {
	if len(app.Services) < 1 {
		return fmt.Errorf("one or more service is required for an app, now %v", len(app.Services))
	}
	for _, sr := range app.Services {
		err := ValidateServiceCreate(sr)
		if err != nil {
			return err
		}
	}
	return validateOtherResources(app)
}

func ValidateAppStart(store *db.AppStore) error {
	for _, sr := range store.Services {
		err := ValidateServiceStart(sr)
		if err != nil {
			return err
		}
	}
	return nil
}

func ValidateAppStop(store *db.AppStore) error {
	for _, sr := range store.Services {
		err := ValidateServiceStop(sr)
		if err != nil {
			return err
		}
	}
	return nil
}

func ValidateAppDelete(store *db.AppStore) error {
	for _, sr := range store.Services {
		err := ValidateServiceDelete(sr)
		if err != nil {
			return err
		}
	}
	return nil
}

func ValidateAppRetry(store *db.AppStore) error {
	retry := false
	for _, sr := range store.Services {
		if err := ValidateServiceRetry(sr); err == nil {
			retry = true
			break
		}
	}
	if retry {
		return nil
	} else {
		return fmt.Errorf("no service can be retry for this app")
	}
}

func ValidateAppUpdate(store *db.AppStore) error {
	for _, sr := range store.Services {
		switch {
		case sr.Response == nil:
			if err := ValidateServiceCreate(sr); err != nil {
				return err
			}
		case sr.Request == nil:
			if err := ValidateServiceDelete(sr); err != nil {
				return err
			}
		default:
			if err := ValidateServiceUpdate(sr); err != nil {
				return err
			}
		}
	}
	return validateOtherResources(store)
}

// validateOtherResources will validate the kind of the request resources. current the rules are:
// 1. Namespace / PV are not supported
// TODO: forbidden cluster scope resource someday?
func validateOtherResources(store *db.AppStore) error {
	for _, item := range store.Others.Request {
		if common.StringInSlice(item.Kind, common.AppUnsupportedResourceKind) {
			return fmt.Errorf("unsupported resource kind: %s", item.Kind)
		}
	}
	return nil
}
