package validation

import (
	"encoding/json"
	"fmt"

	"krobelus/api/schema"
	"krobelus/common"
	"krobelus/model"

	"krobelus/infra"
	"krobelus/store"

	"github.com/sirupsen/logrus"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
)

func checkVolumeSatisfyPV(volumeInfo *model.VolumeResponse, pv *v1.PersistentVolume) error {

	volumeInfoSize := fmt.Sprintf("%dG", volumeInfo.Size)

	volumeQty, err := resource.ParseQuantity(volumeInfoSize)
	if nil != err {
		return err
	}
	volumeSize := volumeQty.Value()

	requestedQty := pv.Spec.Capacity[v1.ResourceStorage]

	requestedSize := requestedQty.Value()

	logger := logrus.WithField("action", "checkVolumeSatisfyPV")

	logger.Infof("checkVolumeSatisfyPV: %d %d", volumeSize, requestedSize)

	if volumeSize < requestedSize {
		return fmt.Errorf("storage capacity of volume[%s](%s) requested by persistentvolume[%v](%s) is not enough", volumeInfo.DriverName, volumeQty.String(), pv.ObjectMeta.Name, requestedQty.String())
	}

	return nil
}

func validateCapacity(pv *v1.PersistentVolume) error {

	if len(pv.Spec.Capacity) == 0 {
		return fmt.Errorf("pv [%s] is invalid: spec.capacity is required", pv.ObjectMeta.Name)
	}

	if _, ok := pv.Spec.Capacity[v1.ResourceStorage]; !ok || len(pv.Spec.Capacity) > 1 {
		return fmt.Errorf("pv [%s] is invalid: spec.capacity: Unsupported value: [%v]: supported values: storage", pv.ObjectMeta.Name, pv.Spec.Capacity)
	}
	return nil
}

func validateAccessModes(pv *v1.PersistentVolume) error {
	if len(pv.Spec.AccessModes) <= 0 {
		return fmt.Errorf("[%s]: spec.accessModes field is required", pv.ObjectMeta.Name)
	}
	return nil
}

// ValidateV1PVCreate validates PV create request from v1 api.
func ValidateV1PVCreate(s *store.KubernetesResourceStore, ctxt *model.PVContext) error {
	volumeUUID, volumeUUIDFound := s.Object.Annotations[common.PVVolumeUidKey()]
	driver, driverFound := s.Object.Annotations[common.PVVolumeDriverKey()]
	// alauda resource will be checked only when both of paramaters are found.
	//  if both parameters are found, verify parameters

	if volumeUUIDFound != driverFound {
		return common.BuildInvalidArgsError("volume UUID or volume driver is missing")
	}

	pv, err := s.Object.ToV1PV()
	if err != nil {
		return common.BuildResourceTransError("Trans pv request error")
	}

	ctxt.PV = pv

	if err := validateCapacity(pv); nil != err {
		return err
	}

	//  if both parameters are missing, bypass the verification
	if volumeUUIDFound {

		err = getVolumeInfo(volumeUUID, ctxt)
		if err != nil {
			s.Logger.WithError(err).Error("GetRikiVolume Error")
			return err
		}

		volumeInfo := ctxt.VolumeInfo

		if common.VolumeStateAvailable != volumeInfo.State {
			message := fmt.Sprintf("volume[%s] is in state [%s], which is not available for now", volumeInfo.Name, volumeInfo.State)
			s.Logger.Errorf("CheckKubernetesResourceSchema error: %s", message)
			return fmt.Errorf("validation failed: %s", message)
		}

		if err := validateAccessModes(pv); nil != err {
			s.Logger.Errorf("validateAccessModes error: %v", err)
			return err
		}

		if driver != common.Glusterfs && driver != common.EBS {
			return fmt.Errorf("driver type [%s]: not in \"ebs\" and \"glusterfs\"", driver)
		}

		if driver != volumeInfo.DriverName {
			return fmt.Errorf("driver type [%s] is not the same with volume(%s) type [%s]", driver, volumeInfo.DriverVolumeID, volumeInfo.DriverName)
		}

		if err := checkVolumeSatisfyPV(volumeInfo, pv); nil != err {
			return err
		}
	}

	return nil
}

// ValidatePVCreate will not change any value
// validation on Kubernetes persistentvolume object will use store.PV
// store.PV must be the same with store.Request.Kubernetes
func ValidatePVCreate(store *store.PVStore, volumeInfo *model.VolumeResponse) error {

	pv := store.Request

	err := schema.CheckKubernetesResourceSchema(pv.Cluster.UUID, "PersistentVolume", pv.Kubernetes)

	if err != nil {
		store.Logger.Errorf("CheckKubernetesResourceSchema error: %v", err)
		return err
	}

	if err := validateAccessModes(store.PV); nil != err {
		store.Logger.Errorf("validateAccessModes error: %v", err)
		return err
	}
	if volumeInfo != nil {
		if common.VolumeStateAvailable != volumeInfo.State {
			message := fmt.Sprintf("volume[%s] is in state [%s], which is not available for now", volumeInfo.Name, volumeInfo.State)
			store.Logger.Errorf("CheckKubernetesResourceSchema error: %s", message)
			return fmt.Errorf("validation failed: %s", message)
		}

		driver := pv.Driver.Name

		if driver != common.Glusterfs && driver != common.EBS {
			return fmt.Errorf("driver type [%s]: not in \"ebs\" and \"glusterfs\"", driver)
		}

		if pv.Driver.Name != volumeInfo.DriverName {
			return fmt.Errorf("driver type [%s] is not the same with volume(%s) type [%s]", pv.Driver.Name, pv.Volume.UUID, volumeInfo.DriverName)
		}

		if err := checkVolumeSatisfyPV(volumeInfo, store.PV); nil != err {
			return err
		}

	}
	return nil
}

// ValidatePVUpdate ...
func ValidatePVUpdate(store *store.PVStore, volumeInfo *model.VolumeResponse) error {

	// Validate capacity

	pv := store.UpdateRequest

	clusterID := store.Response.Cluster.UUID

	bytes, err := json.Marshal(pv)

	if nil != err {
		return err
	}

	if store.Response.Resource.Name != pv.Kubernetes.ObjectMeta.Name {
		return fmt.Errorf("persistentvolume name [%s] can't be updated", store.Response.Resource.Name)
	}

	var res model.KubernetesResource

	err = json.Unmarshal([]byte(bytes), &res)

	if nil != err {
		return err
	}

	err = schema.CheckKubernetesResourceSchema(clusterID, "PersistentVolume", &res)

	if err != nil {
		return err
	}

	if err := validateAccessModes(pv.Kubernetes); nil != err {
		return err
	}

	if err := validateCapacity(pv.Kubernetes); nil != err {
		return err
	}

	if volumeInfo != nil {
		if err := checkVolumeSatisfyPV(volumeInfo, pv.Kubernetes); nil != err {
			return err
		}
	}
	return nil
}

func getVolumeInfo(volumeUUID string, ctxt *model.PVContext) error {
	volumeInfo, httpError := infra.GetRikiVolume(volumeUUID)
	if httpError != nil {
		return common.BuildHttpError(httpError)
	}

	ctxt.VolumeInfo = volumeInfo
	return nil
}

// ValidateV1PVUpdate validates PV update request from v1 api.
func ValidateV1PVUpdate(s *store.KubernetesResourceStore, sOld *store.KubernetesResourceStore, ctxt *model.PVContext) error {

	volumeUUID, ok := sOld.Object.Annotations[common.PVVolumeUidKey()]
	if ok {
		err := getVolumeInfo(volumeUUID, ctxt)
		if err != nil {
			s.Logger.WithError(err).Error("GetRikiVolume Error")
			return err
		}
	}
	pv, err := s.Object.ToV1PV()
	if err != nil {
		return common.BuildResourceTransError("Trans pv request error")
	}

	ctxt.PV = pv

	if sOld.UniqueName.Name != ctxt.PV.Name {
		return fmt.Errorf("persistentvolume name [%s] can't be updated", sOld.UniqueName.Name)
	}

	if err := validateAccessModes(ctxt.PV); nil != err {
		return err
	}

	if err := validateCapacity(ctxt.PV); nil != err {
		return err
	}
	if ctxt.VolumeInfo != nil {
		if err := checkVolumeSatisfyPV(ctxt.VolumeInfo, ctxt.PV); nil != err {
			return err
		}
	}
	return nil
}
