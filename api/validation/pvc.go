package validation

import (
	"fmt"

	"krobelus/api/schema"
	"krobelus/model"
	"krobelus/store"

	"github.com/juju/errors"
)

func ValidatePVCCommon(pvc *model.PVCRequest) error {
	if pvc.Kubernetes == nil {
		return errors.New("Missing persistentvolumeclaim yaml")
	}

	pvcRes, err := pvc.Kubernetes.ToKubernetesResource()

	if nil != err {
		return err
	}
	return ValidatePVCSchema(pvc.Cluster.UUID, pvcRes)
}

func ValidatePVCSchema(clusterUUID string, kubernetes *model.KubernetesResource) error {
	return schema.CheckKubernetesResourceSchema(clusterUUID, string(model.PersistentVolumeClaimType), kubernetes)
}

func ValidatePVCUpdate(store *store.PVCStore) error {

	updateReq := store.UpdateRequest
	if updateReq.Kubernetes == nil {
		return errors.New("Invalid update request for pvc")
	}

	if updateReq.Kubernetes.Name != store.UniqueName.Name {
		return errors.New(fmt.Sprintf("pvc name [%s] can't be updated", store.UniqueName.Name))
	}
	return nil
}
