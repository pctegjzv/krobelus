package validation

import (
	"krobelus/model"

	"github.com/juju/errors"
)

func ValidateConfigMapCommon(configmap *model.ConfigMapRequest) error {
	if configmap.Kubernetes == nil {
		return errors.New("Missing configmap yaml")
	}
	return nil
	//return ValidateConfigMapSchema(configmap.Cluster.UUID, configmap.Kubernetes)
}

func ValidateConfigMapUpdate(cm *model.ConfigMapUpdate) error {
	desc := cm.Resource.Description
	if desc == nil && cm.Kubernetes == nil {
		return errors.New("Invalid update request for configmap")
	}
	return nil
}
