package validation

import (
	"krobelus/model"

	"github.com/juju/errors"
)

func ValidateServiceInstanceCommon(ServiceInstance *model.ServiceInstanceRequest) error {
	if ServiceInstance.Kubernetes == nil {
		return errors.New("Missing ServiceInstance yaml")
	}
	return nil
	//return ValidateServiceInstanceSchema(ServiceInstance.Cluster.UUID, ServiceInstance.Kubernetes)
}

func ValidateServiceInstanceUpdate(cm *model.ServiceInstanceUpdate) error {
	desc := cm.Resource.Description
	if desc == nil && cm.Kubernetes == nil {
		return errors.New("Invalid update request for ServiceInstance")
	}
	return nil
}
