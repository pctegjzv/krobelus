package transition

import (
	"fmt"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
)

// GetKubernetesResourceSelector will get selector from kubernetes resource.
// Now only support kubernetes service.
func GetKubernetesResourceSelector(resource *model.KubernetesResource) (map[string]string, error) {
	switch resource.Kind {
	case common.KubernetesKindService:
		switch resource.APIVersion {
		case common.KubernetesAPIVersionV1:
			object, err := resource.ToV1Service()
			if err != nil {
				return nil, err
			}
			return object.Spec.Selector, nil
		default:
			return nil, errors.New(fmt.Sprintf("Unsupported version %v for service", resource.APIVersion))
		}
	case common.KubernetesKindPDB:
		object, err := resource.ToPDB()
		if err != nil {
			return nil, err
		}
		if object.Spec.Selector != nil {
			if object.Spec.Selector.MatchLabels != nil {
				return object.Spec.Selector.MatchLabels, nil
			}
		}
		return nil, fmt.Errorf("cannot find selector from pdb resource: %s", resource.Name)

	default:
		return map[string]string{}, nil
	}
}
