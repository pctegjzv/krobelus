package transition

import (
	"krobelus/model"
)

func TransServiceBindingRequest(binding *model.ServiceBindingRequest) {
	binding.Kubernetes.Name = binding.Resource.Name
	binding.Kubernetes.Namespace = binding.Namespace.Name
}
