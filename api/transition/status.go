package transition

import (
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/model"
)

func GetTimeDiff() time.Duration {
	return time.Duration(config.GlobalConfig.DB.TimeZoneDiff) * time.Hour
}

func isTimesUp(updatedAt time.Time) bool {
	dbUTCTime := updatedAt.Add(GetTimeDiff())
	return time.Now().UTC().Sub(dbUTCTime) >= time.Duration(config.GlobalConfig.API.DeployTimeout)*time.Second
}

func getStartedCurrent(r *model.ServiceResponse) common.ResourceStatus {

	i := r.Resource.Instances
	var s common.ResourceStatus
	switch {
	case i == nil:
		s = common.StatusUnknown
	case i.Desired == 0 || i.Current == 0:
		s = common.StatusError
	case i.Desired == i.Current:
		s = common.StatusRunning
	case i.Desired != i.Current:
		s = common.StatusWarning
	}
	if !isTimesUp(r.Resource.UpdatedAt) {
		if s != common.StatusRunning {
			s = common.StatusDeploying
		}
	}
	return s
}

func getStoppedCurrent(r *model.ServiceResponse) common.ResourceStatus {
	current := r.Resource.Status
	switch current {
	case string(common.CurrentDeletedStatus):
		i := r.Resource.Instances
		if i.Current > 0 {
			return common.StatusDeploying
		} else {
			return common.StatusStopped
		}
	default:
		return common.StatusUnknown
	}
}

func getNewStatus(r *model.ServiceResponse) common.ResourceStatus {
	c := r.Resource.State.Current
	t := r.Resource.State.Target
	if r.Resource.Status != string(common.StatusDeploying) {
		return common.ResourceStatus(r.Resource.Status)
	}
	switch t {
	case string(common.TargetStartedState):
		if c == string(common.CurrentCreatingStatus) || c == string(common.CurrentCreatedStatus) {
			return common.StatusStarting
		}
		if c == string(common.CurrentUpdatingStatus) || c == string(common.CurrentUpdatedStatus) {
			return common.StatusUpdating
		}
	case string(common.TargetStoppedState):
		if c == string(common.CurrentDeletingStatus) || c == string(common.CurrentDeletedStatus) {
			return common.StatusStopping
		}
	case string(common.TargetDeletedState):
		if c == string(common.CurrentDeletingStatus) || c == string(common.CurrentDeletedStatus) {
			return common.StatusDeleting
		}
	}
	return common.ResourceStatus(r.Resource.Status)
}

func SetFinalStatus(response *model.ServiceResponse) {
	setServiceActions(response)
	current := response.Resource.Status
	if common.KubernetesResourceStatus(current).IsDeployingStatus() {
		response.Resource.Status = string(common.StatusDeploying)
		response.Resource.NewStatus = string(getNewStatus(response))
		return
	}
	if common.KubernetesResourceStatus(current).IsDeployErrorStatus() {
		response.Resource.Status = string(common.StatusError)
		response.Resource.NewStatus = string(common.StatusError)
		return
	}
	target := response.Resource.TargetStatus
	instances := response.Resource.Instances
	switch target {
	case string(common.TargetStartedState):
		response.Resource.Status = string(getStartedCurrent(response))
	case string(common.TargetStoppedState):
		instances.Desired = 0
		response.Resource.Status = string(getStoppedCurrent(response))
	}
	response.Resource.NewStatus = response.Resource.Status
}

func translateAppStatusForDetail(response *model.AppResponse) {
	setAppActions(response)
	r := response
	if r.Resource.Services == nil {
		r.Resource.Services = &model.AppServices{}
	}
	if common.KubernetesResourceStatus(r.Resource.Status).IsDeployErrorStatus() {
		r.Resource.Status = string(common.StatusError)
		return
	}

	services := r.Resource.Services
	for _, service := range r.Services {
		switch common.ResourceStatus(service.Resource.Status) {
		case common.StatusRunning:
			r.Resource.Services.Running += 1
		case common.StatusError:
			r.Resource.Services.Error += 1
		case common.StatusWarning:
			r.Resource.Services.Warning += 1
		case common.StatusStopped:
			r.Resource.Services.Stopped += 1
		case common.StatusDeploying:
			r.Resource.Services.Deploying += 1
		case common.StatusUnknown:
			r.Resource.Services.Unknown += 1
		}
		r.Resource.Services.Total += 1
	}

	status := common.StatusRunning
	switch {
	case services.Total == 0:
		status = common.StatusStopped
	case services.Total == services.Running:
		status = common.StatusRunning
	case services.Stopped == services.Total:
		status = common.StatusStopped
	case services.Error == services.Total:
		status = common.StatusError
	case services.Deploying == services.Total:
		status = common.StatusDeploying
	case services.Unknown == services.Total:
		status = common.StatusUnknown

	case services.Deploying > 0:
		status = common.StatusDeploying
	case services.Total > services.Running && services.Running > 0:
		status = common.StatusWarning
	case services.Unknown > 0:
		status = common.StatusUnknown
	case services.Error > 0:
		status = common.StatusError
	case services.Warning > 0:
		status = common.StatusWarning
	}
	r.Resource.Status = string(status)

	r.Resource.CreatedAt = r.Resource.CreatedAt.Add(GetTimeDiff())
	r.Resource.UpdatedAt = r.Resource.UpdatedAt.Add(GetTimeDiff())
}

func setServiceActions(response *model.ServiceResponse) {
	state := response.Resource.State
	actions := response.Resource.Actions
	roll, retry := true, true
	if len(response.KubernetesBackup) == 0 {
		roll = false
	}
	if common.KubernetesResourceStatus(state.Current).IsDeployingStatus() {
		roll, retry = false, false
	}
	if !common.KubernetesResourceStatus(state.Current).IsDeployErrorStatus() {
		retry = false
	}
	if common.ResourceTargetState(state.Target).IsDeleted() {
		roll = false
	}
	if common.ResourceTargetState(state.Target).IsStopped() {
		roll = false
	}
	actions.Rollback, actions.Retry = roll, retry
	response.Resource.Actions = actions
}

func setAppActions(response *model.AppResponse) {
	actions := response.Resource.Actions
	roll, retry := true, false
	if len(response.KubernetesBackup) == 0 {
		roll = false
	}
	for _, service := range response.Services {
		if common.KubernetesResourceStatus(service.Resource.State.Current).IsDeployingStatus() {
			roll, retry = false, false
			break
		}
		if common.KubernetesResourceStatus(service.Resource.State.Current).IsDeployErrorStatus() {
			retry = true
		}
	}
	actions.Rollback, actions.Retry = roll, retry
	response.Resource.Actions = actions
}
