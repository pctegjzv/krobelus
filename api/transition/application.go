package transition

import (
	"fmt"

	"krobelus/common"
	"krobelus/db"
	"krobelus/model"
	"krobelus/pkg/application/v1alpha1"

	"github.com/gin-gonic/gin/json"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

func GetApplicationCreateWorkflow(request *model.ApplicationRequest, cluster string) *model.AlaudaResourceStatus {
	bt, _ := json.Marshal(request.Kubernetes)
	crd, _ := json.Marshal(request.Crd)

	status := &model.AlaudaResourceStatus{
		UUID:         request.Kubernetes[0].Labels[common.AppUidKey()],
		Type:         model.ApplicationType,
		CurrentState: common.CurrentCreatingStatus,
		TargetState:  common.TargetStartedState,
		QueueID:      common.NewGroupUUID(),
		Input: map[string]string{
			"kubernetes": string(bt),
			"cluster":    cluster,
			"crd":        string(crd),
		},
	}
	return status
}

func GetApplicationUpdateWorkflow(request *model.ApplicationUpdateRequest, cluster string, crd *v1alpha1.Application) (*model.AlaudaResourceStatus, error) {
	bt, _ := json.Marshal(request.Kubernetes)
	crdJson, _ := json.Marshal(crd)
	deleted, _ := json.Marshal(&request.Deleted)

	status := &model.AlaudaResourceStatus{
		UUID:        model.GetAppUUID(crd),
		Type:        model.ApplicationType,
		TargetState: common.TargetStartedState,
		QueueID:     common.NewGroupUUID(),
	}
	status.CurrentState = common.CurrentUpdatingStatus
	status.Input = map[string]string{
		"kubernetes": string(bt),
		"cluster":    cluster,
		"crd":        string(crdJson),
		"deleted":    string(deleted),
	}
	return status, nil
}

func GetApplicationDeleteWorkflow(cluster string, app *model.Application) (*model.AlaudaResourceStatus, error) {
	bt, _ := json.Marshal(app.Kubernetes)
	crd, _ := json.Marshal(app.Crd)

	status, err := db.GetStatus(app.Crd.GetLabels()[common.AppUidKey()])
	if err != nil {
		return nil, err
	}
	status.CurrentState = common.CurrentDeletingStatus
	status.TargetState = common.TargetDeletedState
	status.Input = map[string]string{
		"kubernetes": string(bt),
		"cluster":    cluster,
		"crd":        string(crd),
	}
	return status, nil
}

// Translate app payload for create
func TranslateApplicationCreatePayload(request *model.ApplicationRequest) error {
	uuid := common.NewUUID()
	crd := request.Resource.ToAppCrd()
	var results []*model.KubernetesObject
	appSelectorName := model.GetAppSelectorName(request.Resource.Name, request.Resource.Namespace)

	for _, object := range request.Kubernetes {
		if object == nil {
			continue
		}

		if common.StringInSlice(object.Kind, common.AppUnsupportedResourceKind) {
			return fmt.Errorf("unsupported resource kind in application: %s", object.Kind)
		}

		groupVersion, err := schema.ParseGroupVersion(object.APIVersion)
		if err != nil {
			return err
		}

		object.SetResourceVersion("")
		object.SetUID("")
		object.SetSelfLink("")
		object.SetGeneration(0)
		object.SetApplicationNamespace(request.Resource.Namespace)

		object.SetLabel(common.AppNameKey(), appSelectorName)
		object.SetLabel(common.AppUidKey(), uuid)

		//Note: The origin user of service name/uuid label is dd-agent/alb. In the feature, both will be replaced
		// or discarded. dd-agent will be modified to support new app name label, and alb will use kubernetes service
		// directly
		if common.IsPodController(object.Kind) {
			// object.SetLabel(common.LabelServiceName, object.Name)
			//object.SetLabel(common.GetSvcUidLabel(), common.NewUUID())
			object.UpdatePodTemplateLabels(map[string]string{common.AppNameKey(): appSelectorName})
			object.InjectApplicationEnv(request.Resource.Name)
			object.AddResourceLabelIfNeeded()
		}

		results = append(results, object)

		crd.Spec.ComponentGroupKinds = append(crd.Spec.ComponentGroupKinds, metav1.GroupKind{
			Kind:  object.Kind,
			Group: groupVersion.Group,
		})

	}

	request.Kubernetes = results
	request.Crd = model.AppCrdToObject(crd)
	request.Crd.SetLabel(common.AppUidKey(), uuid)
	return nil
}

// Translate app payload for update
//TODO(hangyan): add pod resource label?
func TranslateApplicationUpdatePayload(request *model.ApplicationUpdateRequest, oldApp *model.Application) (*v1alpha1.Application, error) {
	uuid := oldApp.Crd.GetLabels()[common.AppUidKey()]

	newCrd := oldApp.Crd.ToAppCrd()
	newCrd.Spec.ComponentGroupKinds = []metav1.GroupKind{}
	newCrd.Spec.AssemblyPhase = v1alpha1.Pending
	var results []*model.KubernetesObject
	cks := make(map[string]bool)
	appSelectorName := model.GetAppSelectorName(newCrd.Name, newCrd.Namespace)

	for _, object := range request.Kubernetes {
		if object == nil {
			continue
		}

		if common.StringInSlice(object.Kind, common.AppUnsupportedResourceKind) {
			return nil, fmt.Errorf("unsupported resource kind in application: %s", object.Kind)
		}

		groupVersion, err := schema.ParseGroupVersion(object.APIVersion)
		if err != nil {
			return nil, err
		}

		object.SetApplicationNamespace(newCrd.Namespace)

		//TODO: use get label selector from app crd
		object.SetLabel(common.AppNameKey(), appSelectorName)
		object.SetLabel(common.AppUidKey(), uuid)
		if common.IsPodController(object.Kind) {
			object.AddResourceLabelIfNeeded()
			object.InjectApplicationEnv(newCrd.Name)
		}

		results = append(results, object)

		if _, exist := cks[groupVersion.Group+"."+object.Kind]; !exist {
			cks[groupVersion.Group+"."+object.Kind] = true
			newCrd.Spec.ComponentGroupKinds = append(newCrd.Spec.ComponentGroupKinds, metav1.GroupKind{
				Kind:  object.Kind,
				Group: groupVersion.Group,
			})
		}
	}

	request.Kubernetes = results
	return newCrd, nil
}

// Translate app payload for import
//TODO: update env for pod controller
// TODO: check namespace
func TranslateApplicationImportPayload(request *model.ApplicationImportResourcesRequest, app *model.Application) error {
	appCrd := app.Crd.ToAppCrd()

	cks := make(map[string]bool)
	ckKeyFunc := func(group, kind string) string {
		return group + "." + kind
	}
	for _, ck := range appCrd.Spec.ComponentGroupKinds {
		cks[ckKeyFunc(ck.Group, ck.Kind)] = true
	}

	for _, obj := range request.Kubernetes {
		if obj == nil {
			continue
		}

		if common.StringInSlice(obj.Kind, common.AppUnsupportedResourceKind) {
			return fmt.Errorf("unsupported resource kind in application: %s", obj.Kind)
		}

		groupVersion, err := schema.ParseGroupVersion(obj.APIVersion)
		if err != nil {
			return err
		}

		ckKey := ckKeyFunc(groupVersion.Group, obj.Kind)
		if _, exist := cks[ckKey]; !exist {
			cks[ckKey] = true
			appCrd.Spec.ComponentGroupKinds = append(appCrd.Spec.ComponentGroupKinds, metav1.GroupKind{
				Kind:  obj.Kind,
				Group: groupVersion.Group,
			})
		}
	}
	app.Crd = *model.AppCrdToObject(appCrd)
	return nil
}

// Translate app payload for export
// TODO: update pod env for pod controller
func TranslateApplicationExportPayload(request *model.ApplicationExportResourcesRequest, app *model.Application) error {
	appCrd := app.Crd.ToAppCrd()

	cks := make(map[string]bool)
	ckKeyFunc := func(group, kind string) string {
		return group + "." + kind
	}

	getKey := func(obj *model.KubernetesObject) string {
		return fmt.Sprintf("%s:%s:%s", obj.Kind, obj.Namespace, obj.Name)
	}
	// Make a map using gvk as key for fast searching the object.
	removeResourcesMap := make(map[string]*model.KubernetesObject)
	for _, object := range request.Kubernetes {
		if _, exist := common.ClusterScopeResourceKind[object.Kind]; exist {
			object.SetNamespace("")
		} else {
			object.SetNamespace(app.Crd.Namespace)
		}
		removeResourcesMap[getKey(object)] = object
	}

	appCrd.Spec.ComponentGroupKinds = []metav1.GroupKind{}
	for _, obj := range app.Kubernetes {
		if _, exist := removeResourcesMap[getKey(obj)]; exist {
			continue
		}
		groupVersion, err := schema.ParseGroupVersion(obj.APIVersion)
		if err != nil {
			return err
		}

		ckKey := ckKeyFunc(groupVersion.Group, obj.Kind)
		if _, exist := cks[ckKey]; !exist {
			cks[ckKey] = true
			appCrd.Spec.ComponentGroupKinds = append(appCrd.Spec.ComponentGroupKinds, metav1.GroupKind{
				Kind:  obj.Kind,
				Group: groupVersion.Group,
			})
		}
	}
	app.Crd = *model.AppCrdToObject(appCrd)
	return nil
}
