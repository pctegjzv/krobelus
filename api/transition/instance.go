package transition

import (
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	k8sStore "krobelus/store"

	"krobelus/kubernetes"

	"github.com/juju/errors"
	"k8s.io/api/core/v1"
)

// Set containers for service.
// Container list will be used for service list only.
func setServiceContainerList(store *db.ServiceStore) error {
	ratio, err := infra.GetResourceRatio(store.Response.Cluster.UUID)
	if err != nil {
		return err
	}

	for _, ks := range store.Response.Kubernetes {
		if common.KubernetesDeployModeKind[ks.Kind] {
			containers, err := ks.GetContainers(ratio.IsDefault())
			if err != nil {
				return err
			}
			store.Response.Resource.Containers = containers
			break
		}
	}
	return nil
}

// setServiceInstanceDesired Set service desired instance for service.
// Desired instance count is equal to desired count for kubernetes pods.
func setServiceInstanceDesired(store *db.ServiceStore, parseYaml bool) (err error) {
	if store.Response.Resource.Instances == nil {
		store.Response.Resource.Instances = &model.ServiceInstancesCount{
			Desired: 1,
			Current: 0,
		}
	}
	replicas := 1
	if parseYaml {
		controller := model.GetKubernetesPodController(store.Response.Kubernetes)
		if replicas, err = controller.GetPodReplicas(); err != nil {
			store.Logger.Warn("Get pod replicas error from kubernetes resource")
		}
	}
	store.Response.Resource.Instances.Desired = replicas
	store.Logger.Infof("Get service desired count from kubernetes resource: %+v", store.Response.Resource.Instances)
	return nil
}

// setServiceInstancesCurrent set service current instances.
// Current instance count is equal to current count for kubernetes pods.
func setServiceInstancesCurrent(store *db.ServiceStore, pods []*v1.Pod, accessCluster bool) error {
	logger := store.Logger

	if accessCluster {
		err := updateServiceInstancesCountFromCluster(store)
		if err != nil {
			logger.WithError(err).Error("Retrieve service instances counts from cluster error!")
		} else {
			logger.Debug("Retrieve service instances count from cluster")
		}
	}

	if err := updateServiceInstancesCountFromPods(store, pods); err != nil {
		logger.WithError(err).Info("Retrieve services pods from pods cache error")
	} else {
		logger.Debug("Retrieve services instances count from pods cache")
	}
	return nil
}

// Generate pods list for this service.
// Get pods for this service from pods list.
//TODO: remove
func filterPodList(uuid string, pods []*v1.Pod) []*v1.Pod {
	var result []*v1.Pod
	for _, pod := range pods {
		find := false
		for key, value := range pod.Labels {
			if key == common.SvcUidKey() {
				if value == uuid {
					find = true
					break
				}
			}
		}
		if find {
			result = append(result, pod)
		}
	}
	return result
}

// updateServiceInstancesCountFromCluster Update current instance count from cluster.
// also update pod controller resource from cluster
func updateServiceInstancesCountFromCluster(store *db.ServiceStore) error {
	response := store.Response
	controller := store.FindPodController()
	s := k8sStore.KubernetesResourceStore{
		ResourceStore: k8sStore.ResourceStore{
			UniqueName: &model.Query{
				Type:        common.GetKubernetesTypeFromKind(controller.Kind),
				Name:        controller.Name,
				ClusterUUID: store.Response.Cluster.UUID,
				Namespace:   store.Response.Namespace.Name,
			},
			Logger: store.Logger,
		},
	}
	if err := s.FetchResource(); err != nil {
		return err
	} else {
		if s.Object.GetServiceUUID() != response.Resource.UUID {
			return errors.New("Fetched pod controller does not belong to current service")
		}

		controller.ObjectMeta = s.Object.ObjectMeta
		// preserve real replicas
		replicas, _ := controller.GetPodReplicas()
		controller.Spec = s.Object.Spec
		controller.SetPodReplicas(float64(replicas))
		controller.Status = s.Object.Status

		instances, err := s.Object.GetServicesInstancesCount()
		if err != nil {
			return err
		}
		response.Resource.Instances = instances

	}
	return nil
}

// updateServiceInstancesCountFromPods update current instance from pods list.
func updateServiceInstancesCountFromPods(store *db.ServiceStore, pods []*v1.Pod) (err error) {
	response := store.Response
	instances := response.Resource.Instances
	if pods == nil {
		if pods, err = k8sStore.GetServicePods(store); err != nil {
			return err
		}
	}
	pods = filterPodList(response.Resource.UUID, pods)
	// pods = model.SetVersionForServicePods(pods)
	controller := model.GetKubernetesPodController(store.Response.Kubernetes)
	desired, current := kubernetes.GetCountFromPodList(pods, controller)
	if instances.Desired == 0 {
		instances.Desired = desired
		store.Logger.Infof("Updated service instances desired count from pods cache: %+v", instances.Desired)
	}
	instances.Current = current
	store.Logger.Infof("Updated instances info: %+v", store.Response.Resource.Instances)
	return nil
}
