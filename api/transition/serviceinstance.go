package transition

import (
	"krobelus/model"
	"krobelus/store"
)

func TransServiceInstanceRequest(ServiceInstance *model.ServiceInstanceRequest) {
	ServiceInstance.Kubernetes.Name = ServiceInstance.Resource.Name
	ServiceInstance.Kubernetes.Namespace = ServiceInstance.Namespace.Name
}

func TransServiceInstanceUpdate(s *store.ServiceInstanceStore) error {
	ks := s.UpdateRequest.Kubernetes
	ks.Name = s.UniqueName.Name
	ks.Namespace = s.UniqueName.Namespace
	return nil
}
