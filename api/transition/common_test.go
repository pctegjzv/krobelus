package transition

import (
	"testing"

	"krobelus/model"
)

func TestResourceRatio(t *testing.T) {
	ratio := model.ResourceRatio{
		Cpu: 2,
		Mem: 2,
	}
	cpu, mem, err := GetNewRequests("190m", "128Mi", &ratio)
	if cpu != "95m" || mem != "64Mi" || err != nil {
		t.Errorf("error result: %s %s %v", cpu, mem, err)
	}
}
