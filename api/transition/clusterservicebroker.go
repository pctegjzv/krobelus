package transition

import (
	"encoding/json"
	"fmt"
	"reflect"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"
	"krobelus/store"

	"github.com/juju/errors"
	log "github.com/sirupsen/logrus"
	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func UnknownInterfaceToMap(i interface{}) map[string]string {
	if i != nil {
		strMap := make(map[string]string)
		v := reflect.ValueOf(i)
		switch reflect.TypeOf(i).Kind() {
		case reflect.Map:
			for _, key := range v.MapKeys() {
				strct := v.MapIndex(key)
				k, kok := key.Interface().(string)
				v, vok := strct.Interface().(string)
				if kok && vok {
					strMap[k] = v
				}
			}
		}
		return strMap
	}
	return nil
}

func TransClusterServiceBrokerRequest(broker *model.BrokerRequest, client *infra.KubeClient) error {
	if broker.Resource.Name != "" {
		broker.Kubernetes.Name = broker.Resource.Name
	}
	anno := make(map[string]string)

	if broker.Resource.DisplayName != "" {
		anno[common.ResourceDisplayNameKey()] = broker.Resource.DisplayName
	} else {
		anno[common.ResourceDisplayNameKey()] = broker.Resource.Name
	}
	broker.Kubernetes.SetAnnotations(anno)
	authInfo := broker.Kubernetes.Spec["authInfo"]
	if authInfo == nil {
		return nil
	}
	authInfoMap := UnknownInterfaceToMap(authInfo)
	if authInfoMap["basicAuthUsernameKey"] != "" && authInfoMap["basicAuthPasswordKey"] != "" {
		client.CreateV1Secret(&v1.Secret{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: "v1",
				Kind:       "Secret",
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name: broker.Resource.Name,
			},
			Type: "Opaque",
			Data: map[string][]byte{
				"username": []byte(authInfoMap["basicAuthUsernameKey"]),
				"password": []byte(authInfoMap["basicAuthPasswordKey"]),
			},
		}, common.NamespaceDefault)
		broker.Kubernetes.Spec["authInfo"] = scV1beta1.ServiceBrokerAuthInfo{
			Basic: &scV1beta1.BasicAuthConfig{
				SecretRef: &scV1beta1.ObjectReference{
					Namespace: common.NamespaceDefault,
					Name:      broker.Resource.Name,
				},
			},
		}
	} else {
		broker.Kubernetes.Spec["authInfo"] = nil
	}
	return nil
}

func TransClusterServiceBrokerUpdate(broker *model.BrokerUpdate, bstore *store.BrokerStore, client *infra.KubeClient) error {
	// display name
	anno := make(map[string]string)
	if broker.Resource.DisplayName != "" {
		anno[common.ResourceDisplayNameKey()] = broker.Resource.DisplayName
	}
	broker.Kubernetes.SetAnnotations(anno)
	authInfo := broker.Kubernetes.Spec["authInfo"]
	// auth
	log.Infof("authInfot", authInfo)
	if authInfo == nil {
		return nil
	}
	authInfoMap := UnknownInterfaceToMap(authInfo)

	if authInfoMap["basicAuthUsernameKey"] != "" && authInfoMap["basicAuthPasswordKey"] != "" {
		result, err := client.UpdateV1Secret(&v1.Secret{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: "v1",
				Kind:       "Secret",
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name: bstore.UniqueName.Name,
			},
			Type: "Opaque",
			Data: map[string][]byte{
				"username": []byte(authInfoMap["basicAuthUsernameKey"]),
				"password": []byte(authInfoMap["basicAuthPasswordKey"]),
			},
		}, common.NamespaceDefault)
		if err != nil {
			return err
		}
		broker.Kubernetes.Spec["authInfo"] = scV1beta1.ServiceBrokerAuthInfo{
			Basic: &scV1beta1.BasicAuthConfig{
				SecretRef: &scV1beta1.ObjectReference{
					Namespace: common.NamespaceDefault,
					Name:      bstore.UniqueName.Name,
				},
			},
		}
		log.Infof("update secret result", result)
	}
	return nil
}

func TransClusterServiceBrokerGet(broker *model.BrokerResponse, client *infra.KubeClient) error {
	brokerObj, err := broker.Kubernetes.ToBroker()
	if err != nil {
		return err
	}
	if brokerObj.Spec.AuthInfo == nil {
		return nil
	}
	secretRef := brokerObj.Spec.AuthInfo.Basic.SecretRef
	secret, err := client.GetV1Secret(secretRef.Name, secretRef.Namespace)
	if err != nil {
		return err
	}
	authInfoMap := make(map[string]string)
	authInfoMap["basicAuthUsernameKey"] = fmt.Sprintf("%s", secret.Data["username"])
	authInfoMap["basicAuthPasswordKey"] = fmt.Sprintf("%s", secret.Data["password"])
	broker.Kubernetes.Spec["authInfo"] = authInfoMap
	return nil
}

func TransClusterServiceBrokerDelete(broker *model.BrokerResponse, client *infra.KubeClient) error {
	brokerObj, err := broker.Kubernetes.ToBroker()
	if err != nil {
		return err
	}
	secretRef := brokerObj.Spec.AuthInfo.Basic.SecretRef
	secret, err := client.GetV1Secret(secretRef.Name, secretRef.Namespace)
	if err != nil {
		return err
	}
	authInfoMap := make(map[string]string)
	authInfoMap["basicAuthUsernameKey"] = fmt.Sprintf("%s", secret.Data["username"])
	authInfoMap["basicAuthPasswordKey"] = fmt.Sprintf("%s", secret.Data[""])
	broker.Kubernetes.Spec["authInfo"] = authInfoMap
	return nil
}

func TranslateBrokerListResponse(store *store.BrokerListStore) ([]*model.BrokerListItemResponse, error) {

	brokerResponses := store.Items
	var brokers []*model.BrokerListItemResponse
	if len(brokerResponses) == 0 {
		return nil, errors.New("TransBrokerListResponse: no items passing in.")
	}
	for _, brokerResponse := range brokerResponses {
		broker := &model.BrokerListItemResponse{
			BrokerResource: brokerResponse.Resource,
			Cluster:        brokerResponse.Cluster,
		}
		status, err := json.Marshal(brokerResponse.Kubernetes.Status)
		if nil != err {
			return nil, err
		}
		var statusData map[string][]scV1beta1.ServiceBrokerCondition
		err = json.Unmarshal(status, &statusData)
		if len(statusData["conditions"]) > 0 {
			broker.Status = statusData["conditions"][0]
		}
		broker.ClassNum = store.ClassNumMap[broker.Name]
		brokers = append(brokers, broker)
	}
	return brokers, nil
}
