package transition

import (
	"krobelus/model"
	"krobelus/store"
)

//TODO: use TransPVCUpdate
func TransPVCRequest(pvc *model.PVCRequest) {
	pvc.Kubernetes.Namespace = pvc.Namespace.Name
	pvc.Kubernetes.Name = pvc.Resource.Name
	if "" != pvc.Resource.Name {
		pvc.Kubernetes.ObjectMeta.Name = pvc.Resource.Name
	}
}

func TransPVCUpdate(store *store.PVCStore) {
	ks := store.UpdateRequest.Kubernetes
	ks.Name = store.UniqueName.Name
	ks.Namespace = store.UniqueName.Namespace
}
