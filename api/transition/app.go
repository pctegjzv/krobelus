package transition

import (
	"fmt"

	"krobelus/common"
	"krobelus/db"
	"krobelus/model"
)

// Translate app payload for create pre.
func TranslateAppCreatePayloadPre(store *db.AppStore) error {
	if store.Request.Resource.UUID == "" {
		store.Request.Resource.UUID = common.NewUUID()
	}
	store.Request.Kubernetes = ignoreEmptyKubernetesResources(store.Request.Kubernetes)
	return ParseServicesFromApp(store)
}

// Translate app payload for create.
func TranslateAppCreatePayload(store *db.AppStore) error {
	store.Status = &model.AlaudaResourceStatus{
		UUID:         store.GetAppUUID(),
		Type:         model.AppType,
		CurrentState: common.CurrentCreatingStatus,
		TargetState:  common.TargetStartedState,
		QueueID:      common.NewGroupUUID(),
	}

	// This should be done before translate service. when parse configmap/pvc ref
	// uuid annotation is needed.
	for _, resource := range store.Others.Request {
		transOtherResource(resource, store, "")
	}

	for _, sr := range store.Services {
		if err := TranslateServiceCreatePayload(sr); err != nil {
			return err
		}
	}

	return nil
}

// transOtherResource will do:
// 1. set other resource's namespace ( if this a namespaced resource)
// 2. set app id labels
// 3. set uuid annotation, if this is a new resource, a new uuid will be generated, uid in
// 		args should be '', otherwise pass the already exist uuid
func transOtherResource(resource *model.KubernetesResource, store *db.AppStore, uid string) {
	if _, exist := common.ClusterScopeResourceKind[resource.Kind]; !exist {
		resource.SetNamespace(store.Request.Namespace.Name)
	} else {
		resource.SetNamespace("")
	}

	resource.SetAppIdLabel(store.Request.Resource.UUID)
	resource.SetResourceUidAnnotation(uid)
}

// Translate app payload for start.
func TranslateAppStartPayload(store *db.AppStore) {
	store.Status = &model.AlaudaResourceStatus{
		UUID:         store.GetAppUUID(),
		Type:         model.AppType,
		CurrentState: common.CurrentCreatingStatus,
		TargetState:  common.TargetStartedState,
		QueueID:      common.NewTaskUUID(),
	}
	for _, service := range store.Services {
		TranslateServiceStartPayload(service)
	}
}

// Translate app payload for stop.
func TranslateAppStopPayload(store *db.AppStore) {
	store.Status = &model.AlaudaResourceStatus{
		UUID:         store.GetAppUUID(),
		Type:         model.AppType,
		CurrentState: common.CurrentDeletingStatus,
		TargetState:  common.TargetStoppedState,
		QueueID:      common.NewTaskUUID(),
	}
	for _, service := range store.Services {
		TranslateServiceStopPayload(service)
	}
}

// Translate app payload for delete.
func TranslateAppDeletePayload(store *db.AppStore, force bool) {
	store.Status = &model.AlaudaResourceStatus{
		UUID:         store.GetAppUUID(),
		Type:         model.AppType,
		CurrentState: common.CurrentDeletingStatus,
		TargetState:  common.TargetDeletedState,
		QueueID:      common.NewTaskUUID(),
	}
	if force {
		store.Status.CurrentState = common.CurrentDeletedStatus
	}
	for _, service := range store.Services {
		TranslateServiceDeletePayload(service, force)
	}
}

// Translate app payload for retry.
func TranslateAppRetryPayload(store *db.AppStore) {
	resource := store.Response.Resource
	current := common.DeployErrorToDeployingStatus(resource.Status)
	if current != "" {
		store.Status = &model.AlaudaResourceStatus{
			UUID:         store.GetAppUUID(),
			Type:         model.AppType,
			CurrentState: common.KubernetesResourceStatus(current),
			TargetState:  common.ResourceTargetState(resource.TargetStatus),
			QueueID:      common.NewTaskUUID(),
		}
	}
	for _, service := range store.Services {
		TranslateServiceRetryPayload(service)
	}
}

// Translate app payload for update pre.
func TranslateAppUpdatePayloadPre(store *db.AppStore) error {
	store.Request.Kubernetes = ignoreEmptyKubernetesResources(store.Request.Kubernetes)
	if err := ParseServicesFromApp(store); err != nil {
		return err
	} else {
		mergeRequestAndResponse(store)
		return nil
	}
}

// Translate app payload for update.
func TranslateAppUpdatePayload(store *db.AppStore) error {
	store.Status = &model.AlaudaResourceStatus{
		UUID:         store.GetAppUUID(),
		Type:         model.AppType,
		CurrentState: common.CurrentUpdatingStatus,
		TargetState:  common.TargetStartedState,
		QueueID:      common.NewTaskUUID(),
	}
	for _, sr := range store.Services {
		switch {
		case sr.Response == nil:
			if err := TranslateServiceCreatePayload(sr); err != nil {
				return err
			}
		case sr.Request == nil:
			TranslateServiceDeletePayload(sr, false)
		default:
			TranslateServiceUpdatePayloadPre(sr)
			if err := TranslateServiceUpdatePayload(sr); err != nil {
				return err
			}
			if need, _ := needUpdate(sr); !need {
				sr.Status.CurrentState = common.CurrentUpdatedStatus
			}
		}
	}
	for _, resource := range store.Others.Request {
		exist := model.GetSpecificResource(store.Others.Response, resource.Name, resource.Kind)
		uid := ""
		if exist != nil {
			uid = model.GetResourceUidAnnotation(exist)
		}
		transOtherResource(resource, store, uid)
	}

	var oldResources []*model.KubernetesResource
	for _, sr := range store.Services {
		if sr.Response != nil {
			oldResources = append(oldResources, sr.Response.Kubernetes...)
		}
	}
	for _, rs := range store.Response.Kubernetes {
		if !common.StringInSlice(rs.Kind, common.SubResourcesForAlaudaService) {
			oldResources = append(oldResources, rs)
		}
	}
	store.Response.Kubernetes = oldResources

	return nil
}

// Translate service response for service list.
func TranslateAppListResponse(store *model.AppListStore) error {
	for _, app := range store.Apps {
		serviceListStore := &model.ServiceListStore{
			Services:    app.Services,
			Logger:      store.Logger,
			Namespace:   app.Namespace.Name,
			PodSelector: app.GetLabelSelector(),
		}
		if err := TranslateServiceListResponse(serviceListStore, true, nil); err != nil {
			return err
		}
		translateAppStatusForDetail(app)
		app.Kubernetes = nil
	}
	return nil
}

// Translate app response.
func TranslateAppDetailResponse(store *db.AppStore) *model.AppResponse {
	translateAppResponseForDetail(store)
	translateAppServicesForDetail(store, false)
	translateAppStatusForDetail(store.Response)
	return store.Response
}

// Translate app yaml response.
func TranslateAppYamlResponse(store *db.AppStore) *model.AppResponse {
	translateAppServicesForDetail(store, false)
	return store.Response
}

func translateAppServicesForDetail(store *db.AppStore, ignoreYaml bool) {
	listStore := &model.ServiceListStore{
		Services:    store.Response.Services,
		Logger:      store.Logger,
		Namespace:   store.Response.Namespace.Name,
		PodSelector: store.Response.GetLabelSelector(),
	}
	if err := TranslateServiceListResponse(listStore, ignoreYaml, nil); err != nil {
		panic(err.Error())
	}
	store.Response.Kubernetes = []*model.KubernetesResource{}
	for _, service := range store.Response.Services {
		store.Response.Kubernetes = append(store.Response.Kubernetes, service.Kubernetes...)
	}
	store.Response.Kubernetes = append(store.Response.Kubernetes, store.Others.Response...)
}

func translateAppResponseForDetail(store *db.AppStore) {
	if store.Others.Response == nil {
		store.Others.Response = store.Others.Request
	}
	if store.Response == nil {
		store.Response = &model.AppResponse{
			Resource: model.AppResource{
				UUID:     store.Request.Resource.UUID,
				Name:     store.Request.Resource.Name,
				Status:   "",
				Services: &model.AppServices{},
			},
			Namespace:  store.Request.Namespace,
			Cluster:    store.Request.Cluster,
			Services:   []*model.ServiceResponse{},
			Type:       model.AppType,
			Kubernetes: store.Request.Kubernetes,
		}
	}
	store.Response.Services = []*model.ServiceResponse{}
	for _, service := range store.Services {
		if service.Response == nil {
			service.Response = &model.ServiceResponse{
				Resource: model.ServiceResource{
					UUID:         service.Request.Resource.UUID,
					Name:         service.Request.Resource.Name,
					Status:       string(store.Status.CurrentState),
					TargetStatus: string(store.Status.TargetState),
				},
				Namespace:  service.Request.Namespace,
				Cluster:    service.Request.Cluster,
				Type:       model.AlaudaServiceType,
				Kubernetes: service.Request.Kubernetes,
			}
		}
		store.Response.Services = append(store.Response.Services, service.Response)
	}
	store.Response.Others = []*model.OtherResourceResponse{}
	for _, resource := range store.Others.Response {
		store.Response.Others = append(store.Response.Others, resource.ToOtherResourceResponse())
	}

}

// Fetch services from app.
func ParseServicesFromApp(store *db.AppStore) error {
	labelMap := map[string]map[string]string{}
	serviceMap := map[string]*db.ServiceStore{}
	store.Services = []*db.ServiceStore{}
	// eg: nginx:Deployment
	controllerMap := map[string]string{}
	// Statefulset-ServiceName map. service name is the key, statefulset name is the value
	statefulSetSvcNameMap := map[string]string{}
	// others.
	var others []*model.KubernetesResource

	for _, ks := range store.Request.Kubernetes {
		if !common.KubernetesDeployModeKind[ks.Kind] {
			continue
		}
		name := ks.Name
		kubernetes := []*model.KubernetesResource{ks}
		service := &db.ServiceStore{
			Logger: store.Logger,
			Parent: store,
			Request: &model.ServiceRequest{
				Resource: model.ServiceResource{
					UUID:         common.NewUUID(),
					Name:         ks.Name,
					CreateMethod: store.Request.Resource.CreateMethod,
				},
				Namespace: model.Namespace{
					UUID: store.Request.Namespace.UUID,
					Name: store.Request.Namespace.Name,
				},
				Cluster: model.Cluster{
					UUID: store.Request.Cluster.UUID,
					Name: store.Request.Cluster.Name,
				},
				Kubernetes: kubernetes,
				Parent: model.Parent{
					UUID: store.Request.Resource.UUID,
					Name: store.Request.Resource.Name,
				},
			},
		}
		if _, ok := serviceMap[name]; ok {
			return fmt.Errorf("duplicate kubernetes resource names found: %s", ks.Name)
		}

		if ks.Kind == common.KubernetesKindStatefulSet {
			svcName, err := ks.GetStatefulsetServiceName()
			if err != nil {
				return err
			}
			store.Logger.Infof("Parse StatefulSet %s Svc Name: %s", ks.Name, svcName)
			statefulSetSvcNameMap[svcName] = name
		}

		serviceMap[name] = service
		labels, err := ks.GetPodTemplatesLabels()
		if err != nil {
			return err
		}
		labelMap[name] = labels
		controllerMap[name] = ks.Kind
		store.Services = append(store.Services, service)
	}

	for _, ks := range store.Request.Kubernetes {
		if common.KubernetesDeployModeKind[ks.Kind] {
			continue
		}
		if common.KubernetesKindHPA == ks.Kind {
			ref, err := ks.GetScaleTargetRef()
			if err != nil {
				return err
			}
			if kind, ok := controllerMap[ref.Name]; ok && kind == ref.Kind {
				store.Logger.Infof("Pair HPA %s with %s %s", ks.Name, ref.Kind, ref.Name)
				service := serviceMap[ref.Name]
				service.Request.Kubernetes = append(service.Request.Kubernetes, ks)
			} else {
				return fmt.Errorf("unmatched HPA resource: %s", ks.Name)
			}
			continue
		}

		if common.KubernetesKindPDB == ks.Kind {
			paired := false
			selector, err := GetKubernetesResourceSelector(ks)
			if err != nil {
				store.Logger.WithError(err).Warn("Skip match PDB resource")
			} else {
				for name, labels := range labelMap {
					if common.MapSubSet(selector, labels) {
						store.Logger.Infof("Pair PDB %s with service %s ", ks.Name, name)
						service := serviceMap[name]
						service.Request.Kubernetes = append(service.Request.Kubernetes, ks)
						paired = true
					}
				}
			}
			if paired {
				continue
			}
		}

		if ks.Kind != common.KubernetesKindService {
			others = append(others, ks)
			continue
		}

		if name, ok := statefulSetSvcNameMap[ks.Name]; ok {
			service := serviceMap[name]
			service.Request.Kubernetes = append(service.Request.Kubernetes, ks)
			continue
		}

		selectors, err := GetKubernetesResourceSelector(ks)
		if err != nil {
			return err
		}
		added := false
		//TODO: use MapSubset
		for name, labels := range labelMap {
			found := true
			for key, value := range selectors {
				if dest, ok := labels[key]; !ok || dest != value {
					found = false
					break
				}
			}
			if found {
				added = true
				service := serviceMap[name]
				service.Request.Kubernetes = append(service.Request.Kubernetes, ks)
				break
			}
		}
		if !added {
			return fmt.Errorf("can not find related deployment/daemonset/statefulset for %s: %s", ks.Kind, ks.Name)

		}
	}

	if store.Others == nil {
		store.Others = &model.AppOtherResources{
			Request: others,
		}
	} else {
		store.Others.Request = others
	}

	return nil
}

func mergeRequestAndResponse(store *db.AppStore) {
	var toAdd []*db.ServiceStore
	for _, service := range store.Response.Services {
		if serviceStore := findStoreForResponse(store.Services, service.Resource.Name); serviceStore != nil {
			serviceStore.Response = service
		} else {
			toAdd = append(toAdd, &db.ServiceStore{Response: service, Logger: store.Logger})
		}
	}
	store.Services = append(store.Services, toAdd...)
}

func findStoreForResponse(services []*db.ServiceStore, name string) *db.ServiceStore {
	for _, service := range services {
		if service.Request.Resource.Name == name {
			return service
		}
	}
	return nil
}

func needUpdate(store *db.ServiceStore) (bool, error) {
	equal, err := model.IsKubernetesResourceListEqual(store.Request.Kubernetes, store.Response.Kubernetes)
	return !equal, err
}
