package transition

import (
	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"k8s.io/api/core/v1"
)

// GetReadOnlyFromAccessModes ...
func GetReadOnlyFromAccessModes(pv *v1.PersistentVolume) bool {
	switch pv.Spec.AccessModes[0] {
	case "ROX", "ReadOnlyMany":
		return true
	default:
		return false
	}
	return true
}

// TransCreateV1PVRequest transforms the PV create request from v1 api.
func TransCreateV1PVRequest(s *store.KubernetesResourceStore, ctxt *model.PVContext) error {
	cluster := ctxt.Cluster
	pv := ctxt.PV
	volumeInfo := ctxt.VolumeInfo

	// bypass yaml which is not processed by xin
	if volumeInfo != nil {

		pv.SetLabels(map[string]string{
			common.ClusterUidKey(): cluster.UUID,
		})
		annotations := pv.GetAnnotations()
		if annotations == nil {
			annotations = make(map[string]string)
		}
		if cluster.Name != "" {
			annotations[common.ClusterNameKey()] = cluster.Name
		}
		annotations[common.PVVolumeDriverKey()] = volumeInfo.DriverName
		annotations[common.PVVolumeNameKey()] = volumeInfo.Name
		annotations[common.PVVolumeUidKey()] = volumeInfo.UUID
		pv.SetAnnotations(annotations)

		readOnly := GetReadOnlyFromAccessModes(pv)

		switch volumeInfo.DriverName {
		case common.Glusterfs:
			pv.Spec.Glusterfs = &v1.GlusterfsVolumeSource{
				Path:          volumeInfo.Name,
				EndpointsName: common.GetGlusterFSEndpoint(),
				ReadOnly:      readOnly,
			}
		case common.EBS:
			pv.Spec.AWSElasticBlockStore = &v1.AWSElasticBlockStoreVolumeSource{
				VolumeID:  volumeInfo.DriverVolumeID,
				FSType:    "ext4",
				Partition: 0,
				ReadOnly:  readOnly,
			}
		}
	}
	return s.Object.ParsePV(ctxt.PV)
}

// TransPVRequest ..
// 1. call riki to get volume name
// 2. generate volume driver config to kubernetes json
func TransCreatePVRequest(store *store.PVStore, volumeInfo *model.VolumeResponse) {

	pv := store.Request

	store.PV.ObjectMeta.Name = pv.Resource.Name

	store.PV.ObjectMeta.SetLabels(pv.GeneratePVLabels())

	annotations := store.PV.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
	}
	annotations[common.ClusterNameKey()] = store.Request.Cluster.Name
	annotations[common.PVVolumeDriverKey()] = store.Request.Driver.Name
	annotations[common.PVVolumeNameKey()] = store.Request.Volume.Name
	annotations[common.PVVolumeUidKey()] = store.Request.Volume.UUID
	store.PV.SetAnnotations(annotations)

	driver := pv.Driver.Name

	readOnly := GetReadOnlyFromAccessModes(store.PV)

	switch driver {
	case common.Glusterfs:
		store.PV.Spec.Glusterfs = &v1.GlusterfsVolumeSource{
			Path:          volumeInfo.Name,
			EndpointsName: common.GetGlusterFSEndpoint(),
			ReadOnly:      readOnly,
		}
	case common.EBS:
		store.PV.Spec.AWSElasticBlockStore = &v1.AWSElasticBlockStoreVolumeSource{
			VolumeID:  volumeInfo.DriverVolumeID,
			FSType:    "ext4",
			Partition: 0,
			ReadOnly:  readOnly,
		}
	default:
		return
	}
}

// TransV1PVUpdateRequest transforms the PV update request from v1 api.
func TransV1PVUpdateRequest(sOld *store.KubernetesResourceStore, sNew *store.KubernetesResourceStore) error {
	pvOld, err := sOld.Object.ToV1PV()
	if err != nil {
		return common.BuildResourceTransError("Trans pv request error")
	}

	pvNew, err := sNew.Object.ToV1PV()
	if err != nil {
		return common.BuildResourceTransError("Trans pv request error")
	}

	TransPVUpdateRequest(pvOld, pvNew)
	err = sNew.Object.ParsePV(pvNew)
	if err != nil {
		return common.BuildResourceTransError("Parse pv error")
	}

	return nil
}

// TransPVUpdateRequest handle kubernetes spec volume type(ebs, glusterfs) update
func TransPVUpdateRequest(old *v1.PersistentVolume, new *v1.PersistentVolume) error {

	oldLabels := old.GetLabels()
	oldAnnotations := old.GetAnnotations()
	newLabels := new.GetLabels()
	newAnnotations := new.GetAnnotations()
	if newLabels == nil {
		newLabels = make(map[string]string)
	}
	if newAnnotations == nil {
		newAnnotations = make(map[string]string)
	}

	for k, v := range oldLabels {
		newLabels[k] = v
	}
	for k, v := range oldAnnotations {
		newAnnotations[k] = v
	}

	new.SetLabels(newLabels)
	new.SetAnnotations(newAnnotations)

	if nil != old.Spec.Glusterfs {
		new.Spec.Glusterfs = &v1.GlusterfsVolumeSource{
			Path:          old.Spec.Glusterfs.Path,
			EndpointsName: old.Spec.Glusterfs.EndpointsName,
			ReadOnly:      old.Spec.Glusterfs.ReadOnly,
		}
	} else if nil != old.Spec.AWSElasticBlockStore {
		new.Spec.AWSElasticBlockStore = &v1.AWSElasticBlockStoreVolumeSource{
			VolumeID:  old.Spec.AWSElasticBlockStore.VolumeID,
			FSType:    old.Spec.AWSElasticBlockStore.FSType,
			Partition: old.Spec.AWSElasticBlockStore.Partition,
			ReadOnly:  old.Spec.AWSElasticBlockStore.ReadOnly,
		}

	}

	return nil
}

// TransPVListResponse ...
func TransPVListResponse(store *store.PVListStore, cacheResponse *model.CacheResponse) error {

	for _, resp := range *cacheResponse {
		if pvResponse, err := TransPVResponse(resp); err != nil {
			continue
		} else {
			store.Items = append(store.Items, pvResponse)
		}
	}
	return nil
}

// TransPVResponse ...
func TransPVResponse(resp *v1.PersistentVolume) (*model.PVResponse, error) {
	kubernetesResource := &model.KubernetesResource{}
	err := kubernetesResource.ParseV1PV(resp)
	if err != nil {
		return nil, err
	}

	annotations := resp.GetAnnotations()
	if annotations == nil {
		annotations = make(map[string]string)
	}
	labels := resp.GetLabels()
	if labels == nil {
		labels = make(map[string]string)
	}
	pvResponse := &model.PVResponse{
		Resource: model.PVResource{
			UUID:         string(resp.ObjectMeta.UID),
			Name:         resp.ObjectMeta.Name,
			Status:       string(resp.Status.Phase),
			TargetStatus: string(common.CurrentCreatedStatus),
			CreatedAt:    resp.ObjectMeta.GetCreationTimestamp().Time,
			UpdatedAt:    resp.ObjectMeta.GetCreationTimestamp().Time,
		},
		Cluster: model.Cluster{
			UUID: labels[common.ClusterUidKey()],
			Name: annotations[common.ClusterNameKey()],
		},
		Type: model.PersistentVolumeType,
		Driver: model.Driver{
			Name: annotations[common.PVVolumeDriverKey()],
		},
		Volume: model.Volume{
			Name: annotations[common.PVVolumeNameKey()],
			UUID: annotations[common.PVVolumeUidKey()],
		},
		ReferencedBy: model.ResourceBaseWithType{},
		Kubernetes:   kubernetesResource,
	}
	for _, accessMode := range resp.Spec.AccessModes {
		pvResponse.Resource.AccessModes = append(pvResponse.Resource.AccessModes, string(accessMode))
	}
	pvResponse.Resource.PVReclaimPolicy = string(resp.Spec.PersistentVolumeReclaimPolicy)
	storage := resp.Spec.Capacity["storage"]
	pvResponse.Resource.Capacity = storage.String()
	claimRef := resp.Spec.ClaimRef
	if nil != claimRef {
		pvResponse.ReferencedBy.UUID = string(claimRef.UID)
		pvResponse.ReferencedBy.Name = claimRef.Name
		pvResponse.ReferencedBy.Type = claimRef.Kind
	}

	return pvResponse, nil
}

func TransToPVResponse(store *store.PVStore, resp *v1.PersistentVolume) (*model.PVResponse, error) {
	kubernetesResource := &model.KubernetesResource{}
	err := kubernetesResource.ParseV1PV(resp)
	if err != nil {
		return nil, err
	}

	response := &model.PVResponse{
		Resource: model.PVResource{
			UUID:         string(resp.ObjectMeta.UID),
			Name:         resp.ObjectMeta.Name,
			Status:       string(common.CurrentCreatedStatus),
			TargetStatus: string(common.CurrentCreatedStatus),
			CreatedAt:    resp.ObjectMeta.GetCreationTimestamp().Time,
			UpdatedAt:    resp.ObjectMeta.GetCreationTimestamp().Time,
		},
		Cluster: model.Cluster{
			UUID: store.Request.Cluster.UUID,
			Name: store.Request.Cluster.Name,
		},
		Type: model.PersistentVolumeType,
		Driver: model.Driver{
			Name: store.Request.Driver.Name,
		},
		Volume: model.Volume{
			Name: store.Request.Volume.Name,
			UUID: store.Request.Volume.UUID,
		},
		ReferencedBy: model.ResourceBaseWithType{},
		Kubernetes:   kubernetesResource,
	}
	return response, nil
}
