package transition

import (
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/kubernetes"
	"krobelus/model"
	resourceStore "krobelus/store"

	"k8s.io/api/core/v1"
)

// TransServiceRequest update the kubernetes yaml of the origin request, include name and namespace
// correction, labels, envs...
func TranslateServiceCreatePayload(store *db.ServiceStore) error {
	err := translateServicePayload(store)
	setServiceStatus(store, common.CurrentCreatingStatus, common.TargetStartedState)
	return err
}

// Translate service store for service delete.
func TranslateServiceDeletePayload(store *db.ServiceStore, force bool) {
	if force {
		setServiceStatus(store, common.CurrentDeletedStatus, common.TargetDeletedState)
	} else {
		setServiceStatus(store, common.CurrentDeletingStatus, common.TargetDeletedState)
	}
	if store.Response == nil {
		store.Response = &model.ServiceResponse{}
	}
}

// TranslateServiceUpdatePayloadPre translate service store for update before validate.
func TranslateServiceUpdatePayloadPre(store *db.ServiceStore) {
	resources := store.Request.Kubernetes
	store.Request = store.Response.ToServiceRequest()
	store.Request.Kubernetes = resources
	target := common.ResourceTargetState(store.Response.Resource.TargetStatus)
	current := common.CurrentUpdatingStatus
	if target.IsStopped() {
		current = common.CurrentDeletedStatus
	}
	setServiceStatus(store, current, target)
}

// TranslateServiceUpdatePayload translate service store for update after validate.
func TranslateServiceUpdatePayload(store *db.ServiceStore) error {
	return translateServicePayload(store)
}

// Translate service store for service start.
func TranslateServiceStartPayload(store *db.ServiceStore) {
	setServiceStatus(store, common.CurrentCreatingStatus, common.TargetStartedState)
}

// Translate service store for service stop.
func TranslateServiceStopPayload(store *db.ServiceStore) {
	setServiceStatus(store, common.CurrentDeletingStatus, common.TargetStoppedState)
}

// Translate service store for service retry.
func TranslateServiceRetryPayload(store *db.ServiceStore) {
	current := common.DeployErrorToDeployingStatus(store.Response.Resource.Status)
	if current != "" {
		setServiceStatus(store, current, common.ResourceTargetState(store.Response.Resource.TargetStatus))
	}
}

func TranslateServiceRollbackPayload(store *db.ServiceStore) {
	setServiceStatus(store, common.CurrentUpdatingStatus, common.TargetStartedState)
}

// TranslateServiceListResponse for service list. Pods for these services will be fetched from
// redis one-time. Then each service will be translated as a single service except containers list.
func TranslateServiceListResponse(store *model.ServiceListStore, ignoreYaml bool, filters map[string]string) error {
	var pods []*v1.Pod
	if len(store.Services) > 0 {
		result, err := resourceStore.GetServicesPods(store)
		if err != nil {
			store.Logger.Errorf("Get service instances from redis error: %v", err.Error())
			return err
		}
		pods = result
	}

	if ip, ok := filters["host_ip"]; ok && ip != "" {
		store.Logger.Debugf("Filter services by host ip: %v", ip)
		filterServiceListByHostIP(store, pods, ip)
	}

	for _, item := range store.Services {
		service := &db.ServiceStore{
			Logger:   common.ToLog(store.Logger.WithField("service_id", item.Resource.UUID)),
			Response: item,
		}
		if err := setServiceContainerList(service); err != nil {
			store.Logger.Errorf("Set service containers error: %v", err.Error())
		}
		if err := TranslateServiceDetailResponse(service, false, true, pods); err != nil {
			store.Logger.Errorf("Translate service response error : %v", err.Error())
		}
		if ignoreYaml {
			item.Kubernetes = nil
		}
	}
	return nil
}

// TranslateServiceDetailResponse for service both list and detail. Please note if pods is nil,
// it will get pods from redis. Only set pods to nil if you really want.
// accessCluster: access cluster to get service info, if cache data not working
// parseYaml: parse kubernetes yaml to get service instances count
func TranslateServiceDetailResponse(store *db.ServiceStore, accessCluster bool, parseYaml bool, pods []*v1.Pod) error {
	if err := setServiceInstanceDesired(store, parseYaml); err != nil {
		store.Logger.Errorf("Set desired instances for service error: %v", err.Error())
		return err
	}

	if err := setServiceInstancesCurrent(store, pods, accessCluster); err != nil {
		store.Logger.Errorf("Set current instances for service error: %v", err.Error())
		// Set current instances error will be ignored, be careful.
	}

	store.Logger.Debugf("Origin current status: %s", store.Response.Resource.Status)
	SetFinalStatus(store.Response)

	if accessCluster {
		if err := updateServicePorts(store); err != nil {
			store.Logger.WithError(err).Warn("Updated service ports for service error")
		}
	}

	store.Response.Resource.CreatedAt = store.Response.Resource.CreatedAt.Add(GetTimeDiff())
	store.Response.Resource.UpdatedAt = store.Response.Resource.UpdatedAt.Add(GetTimeDiff())

	return nil
}

// Set status for service.
// Status will be used for sending operation message to queue for worker.
func setServiceStatus(store *db.ServiceStore, current common.KubernetesResourceStatus, target common.ResourceTargetState) {
	input := map[string]string{"rollbackTo": ""}
	if store.Request != nil {
		input["rollbackTo"] = store.Request.RollbackTo
	}
	store.Status = &model.AlaudaResourceStatus{
		UUID:         store.GetServiceUUID(),
		Type:         model.AlaudaServiceType,
		CurrentState: current,
		TargetState:  target,
		QueueID:      common.NewTaskUUID(),
		Input:        input,
	}
}

// Translate service payload.
// 1. If no uuid set, set it
// 2. Remove auto-generated field in kubernetes resource
// 3. Reset namespace
// 4. add labels (svc_id, svc_name)
// 5. update pod selector and template
func translateServicePayload(store *db.ServiceStore) error {
	request := store.Request
	if store.GetServiceUUID() == "" {
		request.Resource.UUID = common.NewUUID()
	}
	store.UUID = store.GetServiceUUID()
	store.Request.Kubernetes = ignoreEmptyKubernetesResources(store.Request.Kubernetes)
	for _, ks := range request.Kubernetes {
		ks.SetResourceVersion("")
		ks.SetUID("")
		ks.SetSelfLink("")
		ks.SetGeneration(0)
		switch {
		case ks.Kind == common.KubernetesKindService:
			ks.SetNamespace(request.Namespace.Name)
			ks.SetLabels(request.GenServiceLabels(ks, true))
		case common.IsPodController(ks.Kind):
			if err := translatePodController(ks, store); err != nil {
				return err
			}
			ref, err := kubernetes.ParseConfigMapRefFromKubernetesResource(ks, store)
			if err != nil {
				return err
			} else {
				store.Logger.Debugf("Parse service configmap refs: %+v", ref)
				store.ReferenceObjectsList = append(store.ReferenceObjectsList, ref)
			}
			pvcRef, err := kubernetes.ParsePVCRefFromKubernetesResource(ks, store)
			if err != nil {
				return err
			} else {
				store.Logger.Debugf("Parse service pvc refs: %+v", pvcRef)
				store.ReferenceObjectsList = append(store.ReferenceObjectsList, pvcRef)
			}
		default:
			ks.SetLabels(request.GenServiceLabels(ks, false))
			ks.SetNamespace(request.Namespace.Name)

		}
	}
	return nil
}

// Translate kubernetes pod controller.
// 1. Add labels for kubernetes pod controller(deployment/daemonset/statefulset).
// 2. update pod templates(labels, annotations, envs...)
func translatePodController(resource *model.KubernetesResource, store *db.ServiceStore) error {
	request := store.Request
	response := store.Response
	resource.SetNamespace(request.Namespace.Name)
	resource.SetLabels(request.GenServiceLabels(resource, false))
	if err := resource.UpdatePodTemplateAffinity(request.Resource.UUID); err != nil {
		return err
	}
	if err := resource.UpdatePodTemplateLabels(request); err != nil {
		return err
	}
	if err := resource.UpdateSelector(request); err != nil {
		return err
	}
	if err := resource.UpdatePodEnv(request); err != nil {
		return err
	}

	if store.Request.Resource.CreateMethod == "UI" || (response != nil && response.Resource.CreateMethod == "UI") {
		ratio, err := infra.GetResourceRatio(store.Request.Cluster.UUID)
		if err != nil {
			return err
		}

		store.Logger.Infof("Get region resource ratio: %v", ratio)
		if !ratio.IsDefault() {
			if err = UpdateResourceRequest(ratio, resource); err != nil {
				return err
			}
		}
	}

	if response != nil && response.Kubernetes != nil {
		controller := model.GetKubernetesPodController(response.Kubernetes)
		version, err := controller.GetPodVersion()
		if err != nil {
			return err
		}
		if version > 0 {
			resource.UpdatePodVersionAnnotation(version)
		}
		equal, err := model.IsPodTemplateEqual(resource, controller)
		if err != nil {
			return err
		}
		if !equal {
			store.Logger.Infof("Pod template has been updated, new version: %d", version+1)
			return resource.UpdatePodVersionAnnotation(version + 1)
		}
	}

	return nil
}

// Ignore empty ones in kubernetes resource arrays.
// For compatibility mainly.
func ignoreEmptyKubernetesResources(resources []*model.KubernetesResource) []*model.KubernetesResource {
	var result []*model.KubernetesResource
	for _, rs := range resources {
		if rs != nil {
			result = append(result, rs)
		}
	}
	return result
}

// Filter services list by host ip.
// Service has no pods on this host will be filtered.
func filterServiceListByHostIP(store *model.ServiceListStore, pods []*v1.Pod, ip string) {
	uuids := map[string]bool{}
	for _, pod := range pods {
		if pod.Status.HostIP != ip {
			continue
		}
		for key, value := range pod.Labels {
			if key == common.SvcUidKey() {
				uuids[value] = true
				break
			}
		}
	}
	var services []*model.ServiceResponse
	for _, service := range store.Services {
		if uuids[service.Resource.UUID] {
			services = append(services, service)
		}
	}
	store.Services = services
}

func updateServicePorts(store *db.ServiceStore) error {
	rs := &resourceStore.KubernetesResourceStore{
		ResourceStore: resourceStore.ResourceStore{
			UniqueName: &model.Query{
				ClusterUUID: store.Response.Cluster.UUID,
				Namespace:   store.Response.Namespace.Name,
				Type:        "services",
			},
			Logger: store.Logger,
		},
	}

	for _, k := range store.Response.Kubernetes {
		if k.Kind == common.KubernetesKindService {
			rs.ResourceStore.UniqueName.Name = k.Name
			rs.ResourceStore.NameKey = ""
			err := rs.FetchResource()
			if err != nil {
				store.Logger.Errorf("Service retrieve from cluster error: %s", err.Error())
				return err
			}
			store.Logger.Debugf("Update service '%s' ports with: %+v ", k.Name, rs.Object.Spec["ports"])

			k.Spec["ports"] = rs.Object.Spec["ports"]
		}
	}

	return nil
}
