package transition

import (
	"krobelus/common"
	"krobelus/model"
)

//TODO: use TransConfigMapUpdate
func TransConfigMapRequest(configmap *model.ConfigMapRequest) {
	configmap.Kubernetes.Namespace = configmap.Namespace.Name
	configmap.Kubernetes.Name = configmap.Resource.Name
	model.UpdateAnnotations(configmap.Kubernetes, common.AnnotationsDescription, configmap.Resource.Description)
}
