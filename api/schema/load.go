package schema

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/xeipuuv/gojsonschema"
)

var (
	KubernetesSchemaPath = "/krobelus/json-schema"
)

var (
	KubernetesSchemas = map[string]map[string]*gojsonschema.Schema{}
)

func loadSchemas() error {
	dirs, err := ioutil.ReadDir(KubernetesSchemaPath)
	if err != nil {
		return err
	}
	for _, dir := range dirs {
		version := dir.Name()
		if !dir.IsDir() {
			logrus.Warnf("expect dir with version as it's name: %s", version)
			continue
		}
		path := fmt.Sprintf("%s/%s", KubernetesSchemaPath, version)
		files, err := ioutil.ReadDir(path)
		if err != nil {
			return err
		}
		if KubernetesSchemas[version] == nil {
			KubernetesSchemas[version] = make(map[string]*gojsonschema.Schema)
		}
		for _, file := range files {
			fileName := file.Name()
			if !strings.Contains(fileName, ".json") {
				logrus.Warnf("Not a json schema , skip: %s", fileName)
				continue
			}
			name := fmt.Sprintf("file://%s/%s", path, fileName)
			logrus.Debugf("Loading schema %s/%s", version, name)
			loader := gojsonschema.NewReferenceLoader(name)
			schema, err := gojsonschema.NewSchema(loader)
			if err != nil {
				return err
			}
			var extension = filepath.Ext(fileName)
			KubernetesSchemas[version][fileName[0:len(fileName)-len(extension)]] = schema
		}

	}
	return nil
}

// LoadSchemas will read schemas files on disk and load them into memory for reuse
func LoadSchemas() {
	if err := loadSchemas(); err != nil {
		panic(err)
	}
}
