package schema

import (
	"strings"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"

	"github.com/juju/errors"
	"github.com/sirupsen/logrus"
	"github.com/xeipuuv/gojsonschema"
)

func CheckKubernetesResourceSchema(regionID string, kind string, resource *model.KubernetesResource) error {
	version, err := infra.GetKubernetesVersion(regionID)
	if err != nil {
		return errors.Annotate(common.ErrInvalidRegionInfo, "parse kubernetes version info")
	}
	return checkSchema(version, kind, resource)
}

func checkSchema(version, kind string, resource *model.KubernetesResource) error {
	logrus.Debugf("Run schema check for: %s/%s", version, kind)
	versionSchema, ok := KubernetesSchemas[version]
	if !ok {
		logrus.Warnf("Missing schema for version %s", version)
		return nil
	}
	s, ok := versionSchema[strings.ToLower(kind)]
	if !ok {
		logrus.Warningf("Missing schema for %s/%s", version, kind)
		return nil
	}
	loader := gojsonschema.NewGoLoader(resource)
	result, err := s.Validate(loader)
	if err != nil {
		return err
	}
	if !result.Valid() {
		for _, err := range result.Errors() {
			return errors.Annotatef(errors.New(err.String()), "%s:", kind)
		}
	}
	return nil
}
