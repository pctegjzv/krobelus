package handler

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
)

// SessionExpireTime ...
const SessionExpireTime = 60

// SessionValue ...
type SessionValue struct {
	Cluster   string `json:"cluster"`
	Namespace string `json:"namespace"`
	Pod       string `json:"pod"`
	Container string `json:"container"`
}

// genTerminalSessionId generates a unique session ID string. The format is not really interesting.
// This ID is used to identify the session when the client opens the WebSocket connection.
func genTerminalSessionID() string {
	bytes := []byte(common.NewUUID())
	id := make([]byte, hex.EncodedLen(len(bytes)))
	hex.Encode(id, bytes)
	return string(id)
}

func getSessionValue(c *gin.Context) (*SessionValue, error) {

	contentType := c.Request.Header.Get("Content-Type")

	// Remove "; charset=" if included in header.
	if idx := strings.Index(contentType, ";"); idx > 0 {
		contentType = contentType[:idx]
	}
	patchType := types.PatchType(contentType)

	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	podName := c.Param("pod")

	query := &model.Query{
		ClusterUUID: cluster,
		Type:        "pods",
		Namespace:   namespace,
		Name:        podName,
		PatchType:   string(patchType),
	}

	s := store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: query,
			Logger:     common.GetLoggerWithAction(c, "validPodStatus"),
		},
	}

	result, err := s.Request("GET", nil)
	if result == nil && err != nil {
		s.Logger.Errorf("Get resource %s in %s failed: %s", query.Type, query.ClusterUUID, err.Error())
		return nil, err
	}

	pod := v1.Pod{}

	err = result.Into(&pod)

	s.Logger.Debugf("pod info: container length=%d", len(pod.Spec.Containers))

	if err != nil {
		return nil, err
	}

	if pod.Status.Phase == v1.PodSucceeded || pod.Status.Phase == v1.PodFailed {
		return nil, fmt.Errorf("Cannot exec into a container in a completed pod; current phase is %s", pod.Status.Phase)
	}

	containerName := c.Param("container")

	s.Logger.Debugf("Container name: %s", containerName)

	if strings.HasPrefix(containerName, "/") {
		containerName = containerName[1:]
	}

	if len(containerName) == 0 || "/" == containerName {
		if len(pod.Spec.Containers) > 0 {
			containerName = pod.Spec.Containers[0].Name
			s.Logger.Infof("Defaulting container name to %s", pod.Spec.Containers[0].Name)
		} else {
			return nil, fmt.Errorf("Cannot execinto a pod with no container specified")
		}
	}

	for _, c := range pod.Status.ContainerStatuses {
		if c.Name == containerName && nil == c.State.Running {
			return nil, fmt.Errorf("Cannot exec into a container which is not running; current phase is %v", c.State)
		}
	}

	v := SessionValue{
		Cluster:   cluster,
		Pod:       podName,
		Namespace: namespace,
		Container: containerName,
	}

	return &v, nil
}

// session id will be set to redis and expire in 60s
func setSessionIDToCache(sessionID string, v *SessionValue) error {
	redis, err := infra.GetRedis()
	if err != nil {
		return err
	}

	key := fmt.Sprintf("ws-session-id:%s", sessionID)

	var value []byte

	value, err = json.Marshal(v)

	if err != nil {
		return err
	}

	err = redis.Set(key, string(value), common.GetTimeDuration(SessionExpireTime))
	if err != nil {
		return err
	}

	return nil
}

// HandleExecShell handles exec requires for a terminal,
// it validate pod and container status and return a session id
func HandleExecShell(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "HandleExecShell")

	logger.Info("Start")

	v, err := getSessionValue(c)

	if err != nil {
		setBadRequestResponse(c, err)
		return
	}

	logger.Infof("Validate pod status pass: value=%+v", v)

	sessionID := genTerminalSessionID()

	logger.Infof("Generate new session id: id=%s", sessionID)

	err = setSessionIDToCache(sessionID, v)

	if err != nil {
		setErrorResponse(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{"id": sessionID, "selfLink": fmt.Sprintf("/api/sock")})
}
