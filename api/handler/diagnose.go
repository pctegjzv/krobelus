package handler

import (
	"net/http"

	"krobelus/db"
	"krobelus/infra"

	"github.com/alauda/bergamot/diagnose"
	"github.com/gin-gonic/gin"
)

func Ping(c *gin.Context) {
	c.String(http.StatusOK, "krobelus:What I've seen goes far beyond death!")
}

func Diagnose(c *gin.Context) {
	reporter, _ := diagnose.New()
	reporter.Add(db.NewDBChecker(db.GoQuDB))
	redis, err := infra.GetRedis()
	if err == nil {
		reporter.Add(redis)
		c.JSON(http.StatusOK, reporter.Check())
	} else {
		result := reporter.Check()
		redisReport := diagnose.NewReport("redis")
		redisReport.Message = err.Error()
		redisReport.Status = diagnose.StatusError
		result.Add(*redisReport)
		result.Status = diagnose.StatusError
		c.JSON(http.StatusOK, result)
	}

}
