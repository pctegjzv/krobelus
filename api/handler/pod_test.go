package handler

import (
	"testing"

	"k8s.io/api/core/v1"
)

func TestPagePodList(t *testing.T) {
	pod := &v1.Pod{}
	pods := []*v1.Pod{pod, pod, pod}
	result := getPagedResult(pods, 20, 1)
	if result["count"] != 3 {
		t.Errorf("Error page pods list")
	}
}
