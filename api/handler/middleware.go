package handler

import (
	"strings"

	"krobelus/common"

	"github.com/gin-gonic/gin"
)

func AppMigration() gin.HandlerFunc {
	return func(c *gin.Context) {

		if !strings.Contains(c.Request.URL.Path, "/applications") {
			return
		}

		query := getAppQuery(c)
		logger := common.GetLogger(c)
		logger.Info("Trying to check app crd in cluster")
		if err := InstallCrd(query, logger); err != nil {
			c.AbortWithError(500, err)
			return
		}

		// update actions
		if c.Request.Method != "GET" && query.Name != "" {
			logger.Info("trying to migrate old app ")
			if err := Migrate(query, logger); err != nil {
				setErrorResponse(c, err)
				c.Abort()
				return
			}
		}

	}

}
