package handler

import (
	"fmt"
	"net/http"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"

	"krobelus/common"
)

// CreatePersistentVolumeClaim ...
func CreatePersistentVolumeClaim(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Create PVC")

	logger.Infof("Start")

	var pvcReq model.PVCRequest

	if err := c.Bind(&pvcReq); err != nil {
		setBadRequestResponse(c, err)
		return
	} else {
		if err := validation.ValidatePVCCommon(&pvcReq); err != nil {
			setBadRequestResponse(c, err)
			return
		}
		logger.Debugf("Received create pvc request: %+v", pvcReq)
		transition.TransPVCRequest(&pvcReq)
	}

	client, err := infra.NewKubeClient(pvcReq.Cluster.UUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	pvc, err := client.CreateV1PVC(pvcReq.Kubernetes)
	if nil != err {
		setK8SErrorResponse(c, err)
		return
	}
	response := model.PVCToResponse(pvc)
	response.Cluster = pvcReq.Cluster
	response.Namespace = pvcReq.Namespace
	c.JSON(http.StatusCreated, response)
}

// ListPersistentVolumeClaim ...
func ListPersistentVolumeClaim(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "List PVC")

	logger.Info("Start")

	request, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for pvc list api.")
		setBadRequestResponse(c, err)
		return
	}

	logger.Debugf("Received list pvc request: %+v", request)

	s := store.PVCListStore{
		ResourceListStore: store.ResourceListStore{
			UUIDs:       request.UUIDS,
			ClusterUUID: request.ClusterUUID,
			Logger:      logger,
		},
	}

	if err = s.LoadPVCs(); err != nil {
		s.Logger.WithError(err).Error("list pvc error")
		setServerUnknownError(c, err)
	} else {
		if len(s.Items) > 0 {
			result, err := db.AppendRefsToPVCs(s.Items)
			s.Logger.WithError(err).Warnf("list pvc errors")
			if result == nil {
				setServerUnknownError(c, err)
			} else {
				if err := store.AppendPVToPVCResponse(result); nil != err {
					setErrorResponse(c, err)
				} else {
					c.JSON(http.StatusOK, result)
				}
			}
		} else {
			setEmptyListResponse(c)
		}
	}
}

func getPVCStore(c *gin.Context, action string) *store.PVCStore {
	return &store.PVCStore{
		ResourceStore: store.ResourceStore{
			UUID:   getUUID(c),
			Logger: common.GetLoggerWithAction(c, fmt.Sprintf("%s PVC", action)),
		},
	}
}

//GetPersistentVolumeClaim ...
func GetPersistentVolumeClaim(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Get PVC")
	logger.Infof("Start")
	s := getPVCStore(c, "Get")
	if err := s.LoadPVC(); err != nil {
		setErrorResponse(c, err)
	} else {
		ds := db.PVCStore{Response: s.Response, UUID: s.UUID}
		if err := ds.AppendRefsToResponse(); err != nil {
			setServerUnknownError(c, err)
			return
		}
		if err := store.AppendPVToPVCResponse([]*model.PVCResponse{ds.Response}); nil != err {
			setErrorResponse(c, err)
			return
		}
		c.JSON(http.StatusOK, ds.Response)
	}
}

// UpdatePersistentVolumeClaim ...
func UpdatePersistentVolumeClaim(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Update PVC")
	logger.Infof("Start")
	s := getPVCStore(c, "Update")
	var update model.PVCUpdate
	if err := c.Bind(&update); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	s.UpdateRequest = &update

	s.Logger.Debugf("Get update pvc request: %+v", s.UpdateRequest.Kubernetes)

	// validate exists
	if err := s.LoadCacheObjectByUID(); err != nil {
		setErrorResponse(c, err)
		return
	}

	if err := validation.ValidatePVCUpdate(s); err != nil {
		setBadRequestResponse(c, err)
		return
	}

	transition.TransPVCUpdate(s)

	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}

	if _, err = client.UpdateV1PVC(s.UpdateRequest.Kubernetes); err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	setEmptyResponse(c)
}

// DeletePersistentVolumeClaim ...
func DeletePersistentVolumeClaim(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Delete PVC")
	logger.Infof("Start")
	s := getPVCStore(c, "Delete")
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	if err = client.DeleteV1PVC(s.UniqueName.Namespace, s.UniqueName.Name); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}
