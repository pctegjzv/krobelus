package handler

import (
	"net/http"

	"krobelus/common"
	"krobelus/db"
	"krobelus/model"

	"github.com/gin-gonic/gin"
)

// GetResourceRefs get resource ref info
func GetRefs(c *gin.Context) {
	rs := db.RefStore{UUID: getUUID(c)}

	if err := rs.AppendRefsToResponse(); err != nil {
		setServerUnknownError(c, err)
		return
	}
	c.JSON(http.StatusOK, rs.Reference)
}

func ListRefs(c *gin.Context) {
	logger := common.GetLogger(c)
	request, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for namespace list api.")
		setBadRequestResponse(c, err)
		return
	}
	logger.Debugf("Received list namespace request: %+v", request)
	result := make(map[string]*model.Reference)
	for _, uuid := range request.UUIDS {
		rs := db.RefStore{UUID: uuid}
		if err := rs.AppendRefsToResponse(); err != nil {
			logger.WithError(err).Error("get ref data error")
			continue
		} else {
			result[uuid] = rs.Reference
		}
	}
	c.JSON(http.StatusOK, result)
}
