package handler

import (
	"net/http"

	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
)

func GetTopology(c *gin.Context) {
	name := c.Query("name")
	kind := c.Query("kind")

	topologyStore := &store.TopologyStore{
		Logger:   common.GetLogger(c),
		Response: &model.TopologyResponse{},
		Request: &model.TopologyRequest{
			Name:      name,
			Kind:      kind,
			Namespace: model.Namespace{Name: c.Param("namespace")},
			Cluster:   model.Cluster{UUID: c.Param("cluster")},
		},
	}

	topologyStore.LoadObjects()
	topologyStore.ParseTopology()

	if name == "" && kind == "" {
		c.JSON(http.StatusOK, topologyStore.Response.Graph)
	} else {
		c.JSON(http.StatusOK, topologyStore.Response.Refer)
	}
}
