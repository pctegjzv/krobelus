package handler

import (
	"strconv"

	"krobelus/common"
	"krobelus/store"

	"github.com/gin-gonic/gin"
)

func getLogSelector(c *gin.Context) *store.Selection {

	refTimestamp := c.Query("referenceTimestamp")
	if refTimestamp == "" {
		refTimestamp = store.NewestTimestamp
	}

	refLineNum, err := strconv.Atoi(c.Query("referenceLineNum"))
	if err != nil {
		refLineNum = 0
	}
	offsetFrom, err1 := strconv.Atoi(c.Query("offsetFrom"))
	offsetTo, err2 := strconv.Atoi(c.Query("offsetTo"))
	logFilePosition := c.Query("logFilePosition")

	logSelector := store.DefaultSelection

	if err1 == nil && err2 == nil {
		logSelector = &store.Selection{
			ReferencePoint: store.LogLineId{
				LogTimestamp: store.LogTimestamp(refTimestamp),
				LineNum:      refLineNum,
			},
			OffsetFrom:      offsetFrom,
			OffsetTo:        offsetTo,
			LogFilePosition: logFilePosition,
		}
	}

	return logSelector
}

func GetPodLogs(c *gin.Context) {

	logger := common.ToLog(common.GetLogger(c).WithField("action", "GetPodLogs"))

	logger.Info("Start")

	query, err := getQuery(c)
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}

	s := store.LogsStore{
		KubernetesResourceStore: store.KubernetesResourceStore{
			ResourceStore: store.ResourceStore{
				UniqueName: query,
				Logger:     logger,
			},
		},
	}

	params := map[string]string{
		"follow":     "false",
		"timestamps": "true",
	}

	previous := c.Query("previous")

	if previous != "" {
		params["previous"] = previous
	}

	container := c.Query("container")

	if container != "" {
		params["container"] = container
	}

	logSelector := getLogSelector(c)

	if logSelector.LogFilePosition == store.Beginning {
		params["LimitBytes"] = strconv.FormatInt(store.ByteReadLimit, 10)
	} else {
		params["tailLines"] = strconv.FormatInt(store.LineReadLimit, 10)

	}

	result, err := s.Request("GET", params)

	if result == nil && err != nil {
		setServerUnknownError(c, err)
	} else {
		var code int
		result.StatusCode(&code)
		if code == 0 {
			code = 500
		}
		rawLogs, err := result.Raw()
		if err != nil {
			c.JSON(code, common.BuildKubernetesError(result.Error().Error()))
		} else {
			raw := s.ConstructLogDetails(s.UniqueName.Name, string(rawLogs[:]), container, logSelector)
			c.Data(code, "application/json", raw)
		}
	}

}
