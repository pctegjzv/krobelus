package handler

import (
	"net/http"
	"strings"

	"krobelus/api/transition"
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/infra/kubernetes"
	"krobelus/model"
	"krobelus/pkg/application/v1alpha1"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
	"github.com/spf13/cast"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/client-go/rest"
)

func getAppQuery(c *gin.Context) *model.Query {
	query := model.Query{
		ClusterUUID: c.Param("cluster"),
		Type:        "applications",
		Namespace:   c.Param("namespace"),
		Name:        c.Param("name"),
	}
	return &query

}

// CreateAppCrdResource will create application resource first, it the crd doest not exist, install
// the crd and retry
func CreateAppCrdResource(cluster string, app *model.KubernetesObject, logger common.Log) (*rest.Result, error) {

	query := model.Query{
		ClusterUUID: cluster,
		Namespace:   app.Namespace,
		Type:        "applications",
	}
	s := store.GetKubernetesResourceStore(&query, logger, app)
	result, err := s.Request("POST", nil)
	if err != nil {
		if err == kubernetes.ErrorResourceKindNotFound {
			if err := s.InstallAppCrd(); err != nil {
				return result, err
			} else {
				return s.Request("POST", nil)
			}
		}
	}
	return result, err
}

func InstallCrd(query *model.Query, logger common.Log) error {
	s := store.GetKubernetesResourceStore(query, logger, nil)
	if err := s.InstallAppCrd(); err != nil {
		if !apiErrors.IsAlreadyExists(err) {
			logger.WithError(err).Error("install app crd error")
			return err
		} else {
			logger.Debug("application crd already exist")
		}
	}
	return nil
}

// MigrateOldApp will migrate old app in db to cluster when needed(like update, start,stop....)
// after the install success, the old data in db should be deleted
// To prevent normal create handler, we have to add an annotation to mark this app is a migration
// If the app was stopped before, we have to annotate the workloads in cluster two
func MigrateOldApp(resp *model.AppResponse, log common.Log) (*rest.Result, error) {
	app := resp.ToNewApplicationCrd()
	app.Spec.AssemblyPhase = v1alpha1.Succeeded
	object := model.AppCrdToObject(app)
	object.SetAno(common.AppMigrationKey(), "true")
	result, err := CreateAppCrdResource(resp.Cluster.UUID, object, log)

	if object.GetAnnotations()[common.AppActionKey()] == "stop" {
		for _, item := range resp.Kubernetes {
			if common.IsPodController(item.Kind) {
				replicas, _ := item.GetPodReplicas()
				data := model.GenAnnotaionPatchData(common.AppLastReplicasKey(), cast.ToString(replicas))
				query := model.GetQuery(resp.Cluster.UUID, item.Kind, item)
				s := store.GetKubernetesResourceStore(query, log, nil)
				s.Raw = data
				result, err := s.Request(common.HttpPatch, nil)
				if err != nil {
					return result, err
				}

			}
		}
	}
	return result, err
}

func CreateApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	cluster := c.Param("cluster")

	var request model.ApplicationRequest
	if err := c.Bind(&request); err != nil {
		logger.WithError(err).Error("parse request application data error")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	logger.Infof("Create app, request: %s", common.ToString(request))

	// Check if old app exist in db, it not , proceed to crd
	exist, err := IsAppExist(cluster, request.Resource.Namespace, request.Resource.Name, logger)
	if err != nil {
		setDbErrorResponse(c, err, model.ApplicationType)
		return
	}
	if exist {
		setBadRequestResponse(c, errors.New("app with this name already exist"))
		return
	}

	if err := transition.TranslateApplicationCreatePayload(&request); err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	// TODO: move it to common
	result, err := CreateAppCrdResource(cluster, request.Crd, logger)
	if result == nil {
		setK8SErrorResponse(c, err)
		return
	} else {
		var isCreated bool
		result.WasCreated(&isCreated)
		if !isCreated {
			SetKubernetesResult(c, model.ParseResult(result))
			return
		}
	}

	status := transition.GetApplicationCreateWorkflow(&request, cluster)
	logger.Infof("Get app status data: %+v", status)

	if err := db.InsertStatus(status); err != nil {
		logger.Errorf("Save app status to db error: %s", err.Error())
		setDbErrorResponse(c, err, "app")
		return
	}

	SetKubernetesResult(c, model.ParseResult(result))
}

func UpdateApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	var request model.ApplicationUpdateRequest
	if err := c.Bind(&request); err != nil {
		logger.WithError(err).Error("parse request application data error")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	logger.Infof("Update app, request: %s", common.ToString(request))

	s := store.GetKubernetesResourceStore(query, logger, nil)

	oldApp, err := s.GetApplication()
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	newCrd, err := transition.TranslateApplicationUpdatePayload(&request, oldApp)
	if err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	s.Object = model.AppCrdToObject(newCrd)
	result, err := s.UpdateApplication(&request, oldApp)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	status, err := transition.GetApplicationUpdateWorkflow(&request, query.ClusterUUID, newCrd)
	if err != nil {
		logger.Errorf("Update app status data error: %+s", err.Error())
		setDbErrorResponse(c, err, "app")
		return
	}

	logger.Infof("Update app status data: %+v", status)

	ss := db.StatusStore{Status: status, Logger: logger}
	if err := ss.InsertOrSet(nil); err != nil {
		logger.Errorf("Save app status to db error: %s", err.Error())
		setDbErrorResponse(c, err, "app")
		return
	}

	SetKubernetesResult(c, model.ParseResult(result))
}

func GetApplication(c *gin.Context) {
	// Check old app in db first
	query := getAppQuery(c)
	logger := common.GetLogger(c)

	app, err := GetAppByNameAndSync(query, logger)
	if app != nil {
		c.JSON(http.StatusOK, app.ToNewCommonObjectList())
		return
	}

	objects, err := getApplication(query, logger)
	if err != nil {
		setErrorResponse(c, err)
	} else {
		c.JSON(http.StatusOK, objects)
	}
	return
}

// ListAndSyncOldApps retrieve old format app from db and remove the invalid ones
func ListAndSyncOldApps(query *model.Query, log common.Log) ([]*model.AppResponse, error) {
	oldApps, err := db.ListAppsInCluster(query.ClusterUUID, query.Namespace)
	if err != nil {
		return nil, err
	}
	internal, err := infra.ListResourceFromJakiro(query.ClusterUUID, "APPLICATION")
	if err != nil {
		return nil, err
	}
	for _, resp := range oldApps {
		if !resp.IsInInternalResources(internal) {
			if err := db.DeleteApp(resp.Resource.UUID); err != nil {
				log.WithError(err).Error("delete old invalid app error ")
			} else {
				log.Info("delete old invalid app:", resp.Resource.UUID)
			}
		}
	}
	return db.ListAppsInCluster(query.ClusterUUID, query.Namespace)
}

// ListApplication will get application from cluster and db
// If old db presents, remove the invalid data in db
func ListApplication(c *gin.Context) {
	query := getAppQuery(c)
	logger := common.GetLogger(c)

	oldApps, err := ListAndSyncOldApps(query, logger)
	if err != nil {
		logger.WithError(err).Error("list old apps in db error")
		setDbErrorResponse(c, err, model.ApplicationType)
		return
	} else {
		logger.Infof("get %d old apps from db", len(oldApps))
	}

	s := store.GetKubernetesResourceStore(query, logger, nil)

	results, err := s.ListApplication()
	if err != nil {
		setK8SErrorResponse(c, err)
	} else {
		c.JSON(http.StatusOK, mergeOldAndNewApplications(oldApps, results))
	}
	return
}

// mergeOldAndNewApplications convert old and new app to the same response format
func mergeOldAndNewApplications(old []*model.AppResponse, new []*model.ApplicationResponse) []model.ApplicationKubernetesObjectList {
	var result []model.ApplicationKubernetesObjectList
	for _, item := range old {
		result = append(result, model.ApplicationKubernetesObjectList{
			Kubernetes: item.ToNewCommonObjectList(),
		})
	}

	for _, item := range new {
		result = append(result, item.ToCommonKubernetesObjectList())
	}
	return result
}

func getApplication(query *model.Query, logger common.Log) (objects []*model.KubernetesObject, err error) {
	s := store.GetKubernetesResourceStore(query, logger, nil)
	if err = s.FetchResource(); err != nil {
		return
	} else {
		app := s.Object.ToAppCrd()
		objects = []*model.KubernetesObject{model.AppCrdToObject(app)}

		result, err := s.GetAppSubResources(app)
		if err != nil {
			s.Logger.WithError(err).Error("get app resources error")
			return objects, err
		}
		objects = append(objects, result...)
	}
	return
}

func IsAppExist(cluster, namespace, name string, log common.Log) (bool, error) {
	query := model.Query{
		ClusterUUID: cluster,
		Namespace:   namespace,
		Name:        name,
	}
	_, err := GetAppByNameAndSync(&query, log)
	if err != nil {
		if errors.Cause(err) == common.ErrResourceNotExist {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// GetAppByNameAndSync sync krobelus db and jakiro db to find the real app
// If nothing found, remove the junk data and return 404
// If found, return the real app and remove the junk data
func GetAppByNameAndSync(query *model.Query, logger common.Log) (*model.AppResponse, error) {
	resp, err := db.GetAppsByName(query.ClusterUUID, query.Namespace, query.Name)
	if err != nil {
		return nil, err
	}
	internal, err := infra.ListResourceFromJakiro(query.ClusterUUID, "APPLICATION")
	if err != nil {
		return nil, err
	}
	result, others := model.FilterSingleByInternalResources(internal, resp)
	if others != nil && len(others) > 0 {
		logger.Info("remove junk apps in db: ", len(others))
		for _, item := range others {
			if err := db.DeleteApp(item.Resource.UUID); err != nil {
				logger.WithError(err).Error("delete junk app error")
			}
		}
	}

	if result == nil {
		return nil, common.ErrResourceNotExist
	}
	return result, nil
}

func Migrate(query *model.Query, logger common.Log) error {
	resp, err := GetAppByNameAndSync(query, logger)
	if err != nil && common.IsNotExistError(err) {
		return nil
	}
	if resp != nil {
		result, err := MigrateOldApp(resp, logger)
		if err != nil {
			logger.WithError(err).Error("migrate old app error ", result)
			return err
		} else {
			logger.Info("migrate old app done")
			return db.DeleteApp(resp.Resource.UUID)
		}
	}
	return err
}

// StopApplication stop/start a application
// When consider old apps: things are a little complicated:
// 1. If the app was running before, we just need to install the crd
// 2. If the app was stopped before, we need to install the crd first, annotate the workload in cluster
// and then let the handler to do the  regular `start` action
// TODO: check before do action
func StartStopApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)
	isStop := strings.Contains(c.Request.URL.Path, "stop")
	action := "stopping"
	if !isStop {
		action = "starting"
	}

	s := store.GetKubernetesResourceStore(query, logger, nil)
	if err := s.FetchResource(); err != nil {
		setErrorResponse(c, err)
		return
	} else {
		if err = store.PatchApplication(&model.Application{
			Cluster: query.ClusterUUID,
			Crd:     *s.Object,
		}, model.GenSimpleApplicationUpdateData("Pending", common.AppActionKey(), action)); err != nil {
			setErrorResponse(c, err)
			return
		}
	}
	setEmptyResponse(c)
}

func GetApplicationPods(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	s := store.GetKubernetesResourceStore(query, logger, nil)
	if err := s.FetchResource(); err != nil {
		setErrorResponse(c, err)
	} else {
		app := s.Object.ToAppCrd()
		s.UniqueName.Type = "pods"
		s.UniqueName.Name = ""
		s.Object = nil
		err := s.FetchResourceList(map[string]string{"labelSelector": model.GetAppLabelSelector(app)})
		if err != nil {
			setK8SErrorResponse(c, err)

		} else {
			c.JSON(http.StatusOK, s.Objects.Items)
		}
	}
	return
}

func GetApplicationYAML(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	objects, err := getApplication(query, logger)
	if objects != nil {
		for _, object := range objects {
			object.SetResourceVersion("")
			object.SetGeneration(0)
			object.SetSelfLink("")
			object.SetUID("")
		}
		bt, err := transition.KubernetesJsonToYamlBytes(model.ObjectsToResources(objects))
		if err != nil {
			logger.WithError(err).Error("parse app resources to yaml error")
			setServerUnknownError(c, err)
			return
		} else {
			c.Writer.Header()["Content-Type"] = transition.YamlContentType
			c.Writer.Write(bt)
			c.Status(http.StatusOK)
			return
		}
	} else {
		setK8SErrorResponse(c, err)
		return
	}
}

func DeleteApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	logger.Infof("Delete app %+v", query)

	s := store.GetKubernetesResourceStore(query, logger, nil)

	app, err := s.GetApplication()
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	status, err := transition.GetApplicationDeleteWorkflow(query.ClusterUUID, app)
	if err != nil {
		logger.Errorf("Delete app status data error: %+s", err.Error())
		setDbErrorResponse(c, err, "app")
		return
	}

	logger.Infof("Delete app status data: %+v", status)

	ss := db.StatusStore{Status: status, Logger: logger}
	if err := ss.InsertOrSet(nil); err != nil {
		logger.Errorf("Save app status to db error: %s", err.Error())
		setDbErrorResponse(c, err, "app")
		return
	}

	err = s.DeleteApplication(app)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	setEmptyResponse(c)
}

func ImportResourcesToApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	var request model.ApplicationImportResourcesRequest
	if err := c.Bind(&request); err != nil {
		logger.WithError(err).Error("parse request application data error")
		setBadRequestResponse(c, err)
		return
	}
	logger.Infof("Import app, request: %s", common.ToString(request))

	s := store.GetKubernetesResourceStore(query, logger, nil)

	app, err := s.GetApplication()
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	err = transition.TranslateApplicationImportPayload(&request, app)
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}

	s.Object = &app.Crd
	_, err = s.ImportResourcesToApplication(&request, app)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	setEmptyResponse(c)
}

func ExportResourcesFromApplication(c *gin.Context) {
	logger := common.GetLogger(c)
	query := getAppQuery(c)

	var request model.ApplicationExportResourcesRequest
	if err := c.Bind(&request); err != nil {
		logger.WithError(err).Error("parse request application data error")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	logger.Infof("Export app, request: %s", common.ToString(request))

	s := store.GetKubernetesResourceStore(query, logger, nil)

	app, err := s.GetApplication()
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	err = transition.TranslateApplicationExportPayload(&request, app)
	if err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	s.Object = &app.Crd
	_, err = s.ExportResourcesFromApplication(&request, app)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	setEmptyResponse(c)
}
