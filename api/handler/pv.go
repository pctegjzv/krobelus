package handler

import (
	"fmt"
	"net/http"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"k8s.io/api/core/v1"
	"k8s.io/client-go/rest"
)

// CreatePersistentVolume will handle of db data and kubernetes object separately
// 1. validate input
// 2. generate volume driver
// 3. send messages to kubernetes
func CreatePersistentVolume(c *gin.Context) {

	logger := common.GetLoggerWithAction(c, "Create PV")

	logger.Info("Start")

	var pvReq model.PVRequest
	var err error

	if err := c.Bind(&pvReq); err != nil {
		logger.WithError(err).Error("Parse Request Body Error")
		setBadRequestResponse(c, err)
		return
	}

	pv := &pvReq
	var volumeInfo *model.VolumeResponse
	if len(pv.Volume.UUID) > 0 {
		var httpError *common.HttpErrorResponse
		volumeInfo, httpError = infra.GetRikiVolume(pv.Volume.UUID)
		if httpError != nil {
			logger.WithError(httpError).Error("GetRikiVolume Error")
			setHttpErrorResponse(c, httpError)
			return
		}
		logger.Info("GetRikiVolume finish")
		pv.Volume.Name = volumeInfo.Name

	}
	s := getPVStore(c, "Create")
	s.Request = pv
	s.Status = &model.AlaudaResourceStatus{CurrentState: common.CurrentCreatedStatus, TargetState: common.TargetCreatedState}

	s.PV, err = pv.Kubernetes.ToV1PV()

	if nil != err {
		setBadRequestResponse(c, err)
		return
	}

	if err := validation.ValidatePVCreate(s, volumeInfo); err != nil {
		setBadRequestResponse(c, err)
		return
	}

	pv.Kubernetes = nil

	logger.Info("ValidatePVCreate finish")

	transition.TransCreatePVRequest(s, volumeInfo)

	client, err := infra.NewKubeClient(pv.Cluster.UUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	resp, err := client.CreateV1PV(s.PV)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	logger.Info("CreateV1PV finish")

	s.PV = resp

	if len(pv.Volume.UUID) > 0 {
		if err := infra.UpdateRikiVolume(pv.Volume.UUID, resp.ObjectMeta.Name, string(resp.ObjectMeta.UID)); nil != err {
			if e := client.DeleteV1PV(s.PV.ObjectMeta.Name); nil != e {
				setK8SErrorResponse(c, e)
				return
			}
			setHttpErrorResponse(c, err)
			return
		}
	}

	response, err := transition.TransToPVResponse(s, resp)

	if err != nil {
		c.JSON(http.StatusInternalServerError, common.BuildResourceTransError(err.Error()))
		return
	}

	logger.Info("Finish")

	c.JSON(http.StatusCreated, response)
}

// ListPersistentVolume ...
func ListPersistentVolume(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "List PV")

	logger.Info("Start")

	request, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for pv list api.")
		setBadRequestResponse(c, err)
		return
	}

	if len(request.UUIDS) == 0 {
		c.JSON(http.StatusOK, []*model.PVResponse{})
		return
	}

	if request.ClusterUUID != "" {
		infra.AddRegionForWatch(request.ClusterUUID)
		logger.Debugf("Add region %s for watch", request.ClusterUUID)
	}

	s := store.PVListStore{
		ResourceListStore: store.ResourceListStore{
			UUIDs:       request.UUIDS,
			ClusterUUID: request.ClusterUUID,
			Logger:      logger,
		},
	}

	logger.Debugf("Received list PV request: %+v", request.UUIDS)

	cacheResp, err := store.GetPVListFromCache(&s)

	//Set current status
	if nil == err {
		transition.TransPVListResponse(&s, cacheResp)
	} else {
		s.Logger.WithError(err).Error("GetPVListFromCache Error")
	}

	if len(s.Items) > 0 {
		c.JSON(http.StatusOK, s.Items)
	} else {
		setEmptyListResponse(c)
	}
}

//GetPersistentVolume ...
func GetPersistentVolume(c *gin.Context) {
	logger := common.GetLogger(c)
	logger.Infof("Start")

	s := getPVStore(c, "Get")
	if cacheResponse, err := store.GetPVFromCache(s.UUID); err != nil {
		logger.WithError(err).Error("GetPVFromCache error")
		c.JSON(http.StatusNotFound, common.BuildResourceNotExistError(err.Error()))
		return
	} else {
		logger.Info("load from cache finish")
		if s.Response, err = transition.TransPVResponse(cacheResponse); err != nil {
			logger.WithError(err).Error("TransPVResponse error")
			setErrorResponse(c, err)
			return
		}
		logger.Info("trans response finish")
	}
	// TODO: Since cluster uuid is not required in query parameters, if the cache missed,
	// the region of which the pv belongs to won't be watched util other API access trigger
	// the watch. May need to make cluster uuid to be required in future.
	infra.AddRegionForWatch(s.Response.Cluster.UUID)
	c.JSON(http.StatusOK, s.Response)
}

// UpdatePersistentVolume ...
func UpdatePersistentVolume(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Update PV")

	logger.Info("Start")

	var update model.PVUpdateRequest

	var err error

	if err := c.Bind(&update); err != nil {
		setBadRequestResponse(c, err)
		return
	}

	s := getPVStore(c, "Update")
	s.UpdateRequest = &update

	if err := s.LoadPV(); err != nil {
		setErrorResponse(c, err)
		return
	}

	logger.Info("Load from cache")
	var volumeInfo *model.VolumeResponse
	if len(s.Response.Volume.UUID) > 0 {
		var httpError *common.HttpErrorResponse
		volumeInfo, httpError = infra.GetRikiVolume(s.Response.Volume.UUID)
		if httpError != nil {
			setHttpErrorResponse(c, httpError)
			return
		}
		logger.Info("Get volume from Riki finish")
	}
	if err := validation.ValidatePVUpdate(s, volumeInfo); err != nil {
		setBadRequestResponse(c, err)
		return
	}

	logger.Info("Validation finish")

	if s.PV, err = s.Response.Kubernetes.ToV1PV(); nil != err {
		setBadRequestResponse(c, err)
		return
	}

	if err := transition.TransPVUpdateRequest(s.PV, update.Kubernetes); nil != err {
		setBadRequestResponse(c, err)
		return
	}

	client, err := infra.NewKubeClient(s.Response.Cluster.UUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}

	_, err = client.UpdateV1PV(update.Kubernetes)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	logger.Info("Update kubernetes finish")

	setEmptyResponse(c)
}

// DeletePersistentVolume ...
func DeletePersistentVolume(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Delete PV")

	logger.Infof("Start")

	s := getPVStore(c, "Delete")

	if err := s.LoadPV(); err != nil {
		if common.IsNotExistError(err) {
			logger.Warnf("no pv %s found, ignore it", s.UUID)
			setEmptyResponse(c)
		} else {
			c.JSON(http.StatusInternalServerError, common.BuildServerUnknownError(err.Error()))
		}
		return
	}

	response := s.Response
	client, err := infra.NewKubeClient(response.Cluster.UUID)

	if err != nil {
		s.Logger.Errorf("get k8s client error: %s", err.Error())
		setRegionErrorResponse(c, err)
		return
	}

	if len(response.Volume.UUID) > 0 {
		if err := infra.UpdateRikiVolume(response.Volume.UUID, "", ""); err != nil {
			setHttpErrorResponse(c, err)
			c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
			return
		}
	}
	if err := client.DeleteV1PV(response.Resource.Name); err != nil {
		s.Logger.Errorf("delete pv in cluster error: %s", err.Error())
		setK8SErrorResponse(c, err)
		return
	}

	setEmptyResponse(c)
}

func getPVStore(c *gin.Context, action string) *store.PVStore {
	return &store.PVStore{
		ResourceStore: store.ResourceStore{
			UUID:   getUUID(c),
			Logger: common.GetLoggerWithAction(c, fmt.Sprintf("%s PV", action)),
		},
	}
}

// HandleBeforePVRequest does some works before executing PV request.
func HandleBeforePVRequest(method string, s *store.KubernetesResourceStore) error {
	var err error
	context := &model.PVContext{
		Resource: model.ResourceNameBase{
			Name: s.UniqueName.Name,
		},
		Cluster: model.Cluster{
			UUID: s.UniqueName.ClusterUUID,
		},
	}
	switch method {
	case common.HttpPost:
		err = handleBeforePVCreate(s, context)
	case common.HttpPut:
		err = handleBeforePVUpdate(s, context)
	case common.HttpDelete:
		err = handleBeforePVDelete(s, context)
	}
	return err
}

func handleBeforePVCreate(s *store.KubernetesResourceStore, ctxt *model.PVContext) error {
	err := validation.ValidateV1PVCreate(s, ctxt)
	if err != nil {
		return err
	}

	err = transition.TransCreateV1PVRequest(s, ctxt)
	if err != nil {
		return err
	}

	return nil
}

func handleBeforePVUpdate(s *store.KubernetesResourceStore, ctxt *model.PVContext) error {
	sOld := &store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: s.UniqueName,
			Logger:     s.Logger,
		},
	}
	err := sOld.FetchResource()
	if err != nil {
		return err
	}

	ctxt.Resource.UUID = string(sOld.Object.UID)
	err = validation.ValidateV1PVUpdate(s, sOld, ctxt)
	if err != nil {
		return err
	}

	err = transition.TransV1PVUpdateRequest(sOld, s)
	if err != nil {
		return err
	}

	return nil
}

func handleBeforePVDelete(s *store.KubernetesResourceStore, ctxt *model.PVContext) error {
	sQuery := &store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: s.UniqueName,
			Logger:     s.Logger,
		},
	}
	err := sQuery.FetchResource()
	if err != nil {
		return err
	}
	if sQuery.Object.GetAnnotations() != nil {
		volumeUUID, ok := sQuery.Object.Annotations[common.PVVolumeUidKey()]
		if ok {
			if err := infra.UpdateRikiVolume(volumeUUID, "", ""); nil != err {
				return err
			}
		}
	}

	return nil
}

// HandlePVRequestDone does some works after the PV request is done.
func HandlePVRequestDone(method string, s *store.KubernetesResourceStore, result *rest.Result) error {
	switch method {
	case common.HttpPost:
		if result != nil {
			return handlePVCreateDone(s, result)
		}
	}
	return nil
}

func handlePVCreateDone(s *store.KubernetesResourceStore, result *rest.Result) error {
	if result.Error() != nil {
		return result.Error()
	}

	pv := &v1.PersistentVolume{}
	err := result.Into(pv)
	if err != nil {
		return err
	}

	volumeUUID, ok := s.Object.Annotations[common.PVVolumeUidKey()]
	if ok {
		if err := infra.UpdateRikiVolume(volumeUUID, pv.Name, string(pv.UID)); nil != err {
			return err
		}
	}
	return nil
}
