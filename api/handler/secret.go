package handler

import (
	"fmt"
	"net/http"

	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/json"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
)

// CreateSecret create kubernetes secret with human friendly api interface
func CreateSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "CreateSecret")
	logger.Info("Start")
	req := model.SecretRequest{Log: logger}
	if err := c.Bind(&req); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	secret, err := req.GenerateSecret()
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}
	bytes, _ := json.Marshal(secret)
	cluster := c.Param("cluster")
	query := model.Query{
		Namespace:   secret.Namespace,
		Type:        "Secrets",
		ClusterUUID: cluster,
	}
	s := store.GetKubernetesResourceStore(&query, logger, nil)
	s.SetRaw(bytes)
	result, err := s.Request(common.HttpPost, nil)
	if err != nil {
		SetKubernetesResultWithMixErr(c, result, err)
		return
	}
	if req.SAName != "" {
		patchBytes := []byte(fmt.Sprintf(`{"imagePullSecrets": [{"name": "%s"}]}`, secret.Name))
		query := model.Query{
			Type:        "Serviceaccounts",
			ClusterUUID: cluster,
			Namespace:   secret.Namespace,
			Name:        req.SAName,
			PatchType:   string(types.StrategicMergePatchType),
		}
		sas := store.GetKubernetesResourceStore(&query, logger, nil)
		sas.SetRaw(patchBytes)
		result, err := sas.Request(common.HttpPatch, nil)
		if err != nil {
			SetKubernetesResultWithMixErr(c, result, err)
			return
		}
	}
	SetKubernetesResult(c, model.ParseResult(result))
}

func UpdateSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "GetSecret")
	logger.Info("Start")
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	name := c.Param("name")
	req := map[string]string{}
	if err := c.Bind(&req); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	query := model.Query{
		Name:        name,
		Namespace:   namespace,
		ClusterUUID: cluster,
		Type:        "secrets",
	}
	s := store.GetKubernetesResourceStore(&query, logger, nil)
	if err := s.FetchResource(); err != nil {
		setErrorResponse(c, err)
	}
	result, err := s.Request(common.HttpGet, nil)
	if err != nil {
		SetKubernetesResultWithMixErr(c, result, err)
		return
	}
	secret := &v1.Secret{}
	if err := result.Into(secret); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	secret.Data = nil
	if err := model.AddKeyToSecret(secret, req); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	if bytes, err := json.Marshal(secret); err != nil {
		setErrorResponse(c, err)
		return
	} else {
		s.SetRaw(bytes)
	}
	result, err = s.Request(common.HttpPut, nil)
	if err != nil {
		SetKubernetesResultWithMixErr(c, result, err)
		return
	}

	if err := s.UpdateResourceCache(result); err != nil {
		logger.WithError(err).Warn("update source cache error")
	}
	if err := result.Into(secret); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	c.JSON(http.StatusOK, secret)
}

func PatchSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "GetSecret")
	logger.Info("Start")
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	name := c.Param("name")
	req := model.Origin{}
	if err := c.Bind(&req); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	if 0 == len(req) {
		setEmptyResponse(c)
		return
	}
	patchBytes, err := json.Marshal(map[string]interface{}{
		"data": req.Encode(),
	})
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}
	query := model.Query{
		Name:        name,
		Namespace:   namespace,
		ClusterUUID: cluster,
		Type:        "Secrets",
		PatchType:   string(types.StrategicMergePatchType),
	}
	s := store.GetKubernetesResourceStore(&query, logger, nil)
	s.SetRaw(patchBytes)
	result, err := s.Request(common.HttpPatch, nil)
	if err != nil {
		SetKubernetesResultWithMixErr(c, result, err)
		return
	}

	if err := s.UpdateResourceCache(result); err != nil {
		logger.WithError(err).Warn("update source cache error")
	}
	secret := &v1.Secret{}
	if err := result.Into(secret); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	c.JSON(http.StatusOK, secret)
}

func GetSecret(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "GetSecret")
	logger.Info("Start")
	cluster := c.Param("cluster")
	namespace := c.Param("namespace")
	name := c.Param("name")
	query := &model.Query{
		Type:        "Secrets",
		Name:        name,
		Namespace:   namespace,
		ClusterUUID: cluster,
	}
	s := store.GetKubernetesResourceStore(query, logger, nil)
	if err := s.FetchResource(); err != nil {
		setErrorResponse(c, err)
	}
	secret := model.Origin{}
	secret.Parse(s.Object.Data)
	secretData := secret.GetDecodedSecret()
	logger.Debugf("get secret data decoded : data=%s", secretData)
	c.JSON(http.StatusOK, secretData)
}
