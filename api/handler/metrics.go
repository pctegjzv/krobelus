package handler

import (
	"net/http"

	"krobelus/common"
	"krobelus/model"

	"krobelus/infra"

	"github.com/gin-gonic/gin"
)

func UpdateRegionInstancesCache(c *gin.Context) {
	var instancesCache model.RegionInstancesCache
	err := c.Bind(&instancesCache)
	if err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	if len(instancesCache.Apps.Items) > 0 {
		infra.PushTinyMetrics(&instancesCache)
	}
	c.JSON(http.StatusNoContent, gin.H{})
}
