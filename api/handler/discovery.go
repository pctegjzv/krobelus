package handler

import (
	"net/http"

	"krobelus/common"
	"krobelus/infra"

	"github.com/gin-gonic/gin"
)

// UpdateAPIDiscoveryCache updates the api discovery cache from the k8s cluster.
func UpdateAPIDiscoveryCache(c *gin.Context) {
	err := infra.InvalidateApiDiscoveryCache()
	if err != nil {
		setErrorResponse(c, err)
		return
	}
	c.JSON(http.StatusNoContent, gin.H{})
}

// GetClusterResourceTypes gets resource types from cluster.
func GetClusterResourceTypes(c *gin.Context) {
	logger := common.GetLogger(c)
	cluster := c.Param("cluster")
	logger.Infof("Get resource types from %s", cluster)
	client, err := infra.NewKubeClient(cluster)
	if err != nil {
		setErrorResponse(c, err)
		return
	}

	rl, err := client.ServerResources()
	if err != nil {
		setErrorResponse(c, err)
		return
	}

	c.JSON(http.StatusOK, rl)
}
