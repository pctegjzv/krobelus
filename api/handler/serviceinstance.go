package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

func CreateServiceInstance(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Create ServiceInstances")

	logger.Info("Start")

	var ServiceInstance model.ServiceInstanceRequest
	var err error

	if err := c.Bind(&ServiceInstance); err != nil {
		logger.WithError(err).Error("Parse Request Body Error")
		setBadRequestResponse(c, err)
		return
	} else {
		if err := validation.ValidateServiceInstanceCommon(&ServiceInstance); err != nil {
			setBadRequestResponse(c, err)
			return
		}
		logger.Debugf("Received create ServiceInstance request: %+v", ServiceInstance)
		transition.TransServiceInstanceRequest(&ServiceInstance)
	}

	client, err := infra.NewKubeClient(ServiceInstance.Cluster.UUID)
	if err != nil {
		logger.WithError(err).Error("Create client err")
		setRegionErrorResponse(c, err)
		return
	}
	cm, err := client.CreateServiceInstance(ServiceInstance.Kubernetes)
	if err != nil {
		logger.WithError(err).Error("Create service instance err")
		setK8SErrorResponse(c, err)
		return
	}
	// wait for create ready
	var s *store.ServiceInstanceStore
	var reb *scV1beta1.ServiceInstance
	var needDeleteIns bool
	for retryCount := 0; retryCount <= scResultRetryTimes; retryCount++ {
		if retryCount == scResultRetryTimes {
			needDeleteIns = true
			setBrokerTimeoutResponse(c, "Broker Url Timeout")
		}
		scRetrySleep()
		s = getServiceInstanceStoreByUid(string(cm.UID))
		if err := s.LoadServiceInstance(); err == nil {
			reb, err = s.Object.ToServiceInstance()
			if err != nil {
				continue
			}
		} else {
			reb, err = client.GetServiceInstance(ServiceInstance.Resource.Name, ServiceInstance.Namespace.Name)
			s.Response = model.ServiceInstanceToResponse(reb)
		}
		if len(reb.Status.Conditions) == 0 {
			continue
		}
		if reb.Status.Conditions[0].Status == scV1beta1.ConditionTrue {
			break
		}
		if reb.Status.Conditions[0].Status == scV1beta1.ConditionFalse {
			switch reb.Status.Conditions[0].Reason {
			case provisioningInFlightReason:
				continue
			case errorWithParameters:
				setServiceCatalogClientErrResponse(c, common.ServiceInstanceParamError, reb.Status.Conditions[0].Message)
				break
			case errorErrorCallingProvisionReason:
				setServiceCatalogServerErrResponse(c, common.ServiceInstanceBrokerError, reb.Status.Conditions[0].Message)
				break
			default:
				setServiceCatalogServerErrResponse(c, reb.Status.Conditions[0].Reason, reb.Status.Conditions[0].Message)
			}
			needDeleteIns = true
			break
		}
	}
	if needDeleteIns {
		if err = client.DeleteServiceInstance(s.UniqueName.Namespace, s.UniqueName.Name); err != nil {
			logger.Debugf(" delete service instance err: %+v", err)
		}
		return
	}

	response := model.ServiceInstanceToResponse(cm)
	response.Cluster = ServiceInstance.Cluster
	response.Namespace = ServiceInstance.Namespace
	c.JSON(http.StatusCreated, response)
}

func GetServiceInstance(c *gin.Context) {
	s := getServiceInstanceStore(c, "Get")
	if err := s.LoadServiceInstance(); err != nil {
		setErrorResponse(c, err)
	} else {
		c.JSON(http.StatusOK, s.Response)
	}
}

func ListSCServiceInstances(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "List ServiceInstances")
	request, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for ServiceInstance list api.")
		setBadRequestResponse(c, err)
		return
	}
	logger.Debugf("Received list ServiceInstance request: %+v", request)
	s := store.ServiceInstanceListStore{
		ResourceListStore: store.ResourceListStore{
			UUIDs:       request.UUIDS,
			ClusterUUID: request.ClusterUUID,
			Logger:      logger,
		},
	}
	if err = s.LoadServiceInstances(); err != nil {
		s.Logger.WithError(err).Error("list ServiceInstance error")
		setServerUnknownError(c, err)
	} else {
		if err != nil {
			setServerUnknownError(c, err)
		}
		if len(s.Items) > 0 {
			c.JSON(http.StatusOK, s.Items)
		} else {
			setEmptyListResponse(c)
		}
	}
}

func DeleteServiceInstance(c *gin.Context) {
	s := getServiceInstanceStore(c, "Delete")
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	if err = client.DeleteServiceInstance(s.UniqueName.Namespace, s.UniqueName.Name); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}

func UpdateServiceInstance(c *gin.Context) {
	s := getServiceInstanceStore(c, "Update")
	var update model.ServiceInstanceUpdate
	if err := c.Bind(&update); err != nil {
		setBadRequestResponse(c, err)
		return
	}

	s.UpdateRequest = &update
	s.Logger.Debugf("Get update ServiceInstance request: %+v", s.UpdateRequest.Kubernetes)
	if err := validation.ValidateServiceInstanceUpdate(s.UpdateRequest); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	if err := s.LoadUpdateObject(); err != nil {
		setErrorResponse(c, err)
		return
	}
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	err = transition.TransServiceInstanceUpdate(s)
	if err != nil {
		setErrorResponse(c, err)
		return
	}
	if err := s.PatchUpdateObject(); err != nil {
		setErrorResponse(c, err)
		return
	}
	s.Logger.Debugf("Get update serviceinstance request: %+v", s.UpdateRequest.Kubernetes)
	if _, err = client.UpdateServiceInstance(s.Object); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}

func getServiceInstanceStore(c *gin.Context, action string) *store.ServiceInstanceStore {
	return &store.ServiceInstanceStore{
		ResourceStore: store.ResourceStore{
			UUID:   getUUID(c),
			Logger: common.GetLoggerWithAction(c, fmt.Sprintf("%s ServiceInstance", action)),
		},
	}
}

func getServiceInstanceStoreByUid(uid string) *store.ServiceInstanceStore {
	return &store.ServiceInstanceStore{
		ResourceStore: store.ResourceStore{
			UUID: uid,
		},
	}
}

func HandleSIRequestDone(method string, s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	s.Logger.Debugf("HandleSIRequestDone")
	var err error
	var body []byte
	switch method {
	case common.HttpPost:
		err = handleSICreateDone(s)
	case common.HttpGet:
		if s.UniqueName.Name == "" {
			body, err = handleSIListDone(s, result)
		} else {
			body, err = handleSIGetDone(s, result)
		}
	}
	return body, err
}

func handleSICreateDone(s *store.KubernetesResourceStore) error {
	return nil
}

func handleSIListDone(s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	var body []byte
	var bindingsNum int
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return nil, err
	}
	siList := &scV1beta1.ServiceInstanceList{}
	siList.Items = make([]scV1beta1.ServiceInstance, 0)

	if result != nil {
		result.Into(siList)
		for index, item := range siList.Items {
			bindingsNum = 0
			bindings, err := client.GetServiceBindings(&metaV1.ListOptions{}, item.ObjectMeta.Namespace)
			if err != nil {
				return nil, err
			}
			for idx, _ := range bindings {
				if bindings[idx].Spec.ServiceInstanceRef.Name == item.ObjectMeta.Name {
					bindingsNum++
				}
			}
			anno := make(map[string]string)
			if item.Spec.ClusterServiceClassRef != nil {
				class, _ := client.GetClusterServiceClass(item.Spec.ClusterServiceClassRef.Name)
				if class != nil {
					anno[common.SCCPlanUpdatableKey()] = strconv.FormatBool(class.Spec.PlanUpdatable)
					brokerName := class.Spec.ClusterServiceBrokerName
					broker, _ := client.GetClusterServiceBroker(brokerName)
					if broker != nil {
						anno[common.SCBStatusKey()] = string(broker.Status.Conditions[0].Status)
					}
				}
			}
			anno[common.SCIAppNumKey()] = fmt.Sprintf("%d", bindingsNum)
			siList.Items[index].SetAnnotations(anno)
		}
	} else if s.Objects != nil && len(s.Objects.Items) > 0 {
		for _, item := range s.Objects.Items {
			si, err := item.ToServiceInstance()
			if err != nil {
				return nil, err
			}

			bindingsNum = 0
			bindings, err := client.GetServiceBindings(&metaV1.ListOptions{}, item.ObjectMeta.Namespace)
			if err != nil {
				return nil, err
			}
			for idx, _ := range bindings {
				if bindings[idx].Spec.ServiceInstanceRef.Name == si.ObjectMeta.Name {
					bindingsNum++
				}
			}
			anno := make(map[string]string)

			if si.Spec.ClusterServiceClassRef != nil {
				class, _ := client.GetClusterServiceClass(si.Spec.ClusterServiceClassRef.Name)
				if class != nil {
					anno[common.SCCPlanUpdatableKey()] = strconv.FormatBool(class.Spec.PlanUpdatable)
					brokerName := class.Spec.ClusterServiceBrokerName
					broker, _ := client.GetClusterServiceBroker(brokerName)
					if broker != nil {
						anno[common.SCBStatusKey()] = string(broker.Status.Conditions[0].Status)
					}
				}
			}
			anno[common.SCIAppNumKey()] = fmt.Sprintf("%d", bindingsNum)
			si.SetAnnotations(anno)
			siList.Items = append(siList.Items, *si)
		}
	}

	body, err = json.Marshal(siList)
	if err != nil {
		return nil, err
	}
	return body, err
}

func handleSIGetDone(s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	var body []byte
	var rawPlans []byte
	var instance *scV1beta1.ServiceInstance
	var err error

	if s.Object != nil {
		instance, err = s.Object.ToServiceInstance()
		if err != nil {
			return nil, err
		}
	} else if result.Error() != nil {
		result.Into(instance)
	} else {
		return nil, result.Error()
	}
	anno := make(map[string]string)
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return nil, err
	}
	if instance.Spec.ClusterServiceClassRef != nil {
		class, _ := client.GetClusterServiceClass(instance.Spec.ClusterServiceClassRef.Name)
		if class != nil {
			anno[common.SCCPlanUpdatableKey()] = strconv.FormatBool(class.Spec.PlanUpdatable)
			brokerName := class.Spec.ClusterServiceBrokerName
			broker, _ := client.GetClusterServiceBroker(brokerName)
			if broker != nil {
				anno[common.SCBStatusKey()] = string(broker.Status.Conditions[0].Status)
			}
		}
	}
	if instance.Spec.ClusterServicePlanRef != nil {
		plan, err := client.GetClusterServicePlan(instance.Spec.ClusterServicePlanRef.Name)
		if err != nil {
			return nil, err
		}
		rawPlans, err = json.Marshal(map[string]interface{}{
			common.ClusterServicePlanType: []*scV1beta1.ClusterServicePlan{
				plan,
			},
		})
		if err != nil {
			return nil, err
		}
	} else {
		plans, err := client.GetClusterServicePlans(&metaV1.ListOptions{
			FieldSelector: fmt.Sprintf("spec.externalName=%s", instance.Spec.ClusterServicePlanExternalName),
		})
		if err != nil {
			return nil, err
		}
		rawPlans, err = json.Marshal(map[string]interface{}{
			common.ClusterServicePlanType: plans,
		})
	}
	anno[common.ObjRefListKey()] = string(rawPlans)
	instance.SetAnnotations(anno)

	body, err = json.Marshal(instance)
	if err != nil {
		return nil, err
	}
	return body, err
}
