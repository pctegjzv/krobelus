package handler

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
)

func getUUID(c *gin.Context) string {
	return c.Param("uuid")
}

// IsParseResourcesRequest checks if this is a parse resources request.
// It accept the same request as create/update request, but does not
// actually do the action, but only parse the related resources items
// and return them to api portal to do RBAC check
func IsParseResourcesRequest(c *gin.Context) bool {
	return c.Query("parse_resources") == "true"
}

func getResourceStore(c *gin.Context, action string) store.ResourceStore {
	return store.ResourceStore{
		UUID:   getUUID(c),
		Logger: common.GetLoggerWithAction(c, action),
	}
}

func getRequestData(c *gin.Context) (*model.GetUUIDS, error) {
	body := c.Request.Body
	bytes, err := ioutil.ReadAll(body)
	if err != nil {
		return nil, err
	}
	var data model.GetUUIDS
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		return nil, err
	}
	return &data, nil
}

func setDbErrorResponse(c *gin.Context, err error, resourceType model.ResourceType) {
	if common.IsNotExistError(err) {
		c.JSON(http.StatusNotFound, common.BuildDBNotFoundError(fmt.Sprintf("%s not exists", resourceType)))
		return
	}
	if db.IsUniqueViolationError(err) {
		c.JSON(http.StatusBadRequest, common.BuildResourceAlreadyExistError(
			fmt.Sprintf("%s with current parameters already exist!", resourceType)))
		return
	}
	c.JSON(http.StatusInternalServerError, common.BuildServerUnknownError(err.Error()))
}

func setCacheErrorResponse(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, common.BuildError(common.CacheError, err.Error()))
}

// setErrorResponse will parse err and set the correspond http status code and error code
//TODO: use for all error handler
func setErrorResponse(c *gin.Context, err error) {
	// k8s errors
	if apiErrors.IsAlreadyExists(err) {
		c.JSON(http.StatusBadRequest, common.BuildResourceAlreadyExistError(
			fmt.Sprintf("resource already exist: %s", err.Error())))
		return
	}

	// other errors
	if errors.Cause(err) == common.ErrNotFound || common.IsNotExistError(err) {
		c.JSON(http.StatusNotFound, common.BuildResourceNotExistError(err.Error()))
	} else if errors.Cause(err) == common.ErrKubernetesConnect {
		c.JSON(http.StatusInternalServerError, common.BuildError(common.CodeKubernetesConnectError, err.Error()))
	} else {
		setServerUnknownError(c, err)
	}
}

// SetErrorResponse generate error code with resource info
// resource: eg: namespace/configmap/deployment...
func SetErrorResponse(c *gin.Context, err error, resource string) {
	if apiErrors.IsAlreadyExists(err) {
		c.JSON(http.StatusBadRequest,
			common.BuildError(fmt.Sprintf("%s_already_exist", resource), err.Error()))
		return
	}
	setErrorResponse(c, err)
}

// setBadRequestResponse: 400 / invalid_args
func setBadRequestResponse(c *gin.Context, err error) {
	c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
}

// setRegionErrorResponse: 500 / region_error
func setRegionErrorResponse(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, common.BuildRegionError(err.Error()))
}

// setServerUnknownError 500 / unknown_issue
func setServerUnknownError(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, common.BuildServerUnknownError(err.Error()))
}

func setK8SErrorResponse(c *gin.Context, err error) {
	c.JSON(http.StatusInternalServerError, common.BuildKubernetesError(err.Error()))
}

// setEmptyResponse: 204 / {}
func setEmptyResponse(c *gin.Context) {
	c.JSON(http.StatusNoContent, gin.H{})
}

// setEmptyListResponse: 200 / []
func setEmptyListResponse(c *gin.Context) {
	c.JSON(http.StatusOK, []string{})
}

func setHttpErrorResponse(c *gin.Context, err *common.HttpErrorResponse) {
	c.JSON(err.StatusCode, common.BuildHttpError(err))
}

func setBrokerTimeoutResponse(c *gin.Context, message string) {
	c.JSON(http.StatusInternalServerError, common.BuildBrokerTimeoutError(message))
}

func setServiceCatalogClientErrResponse(c *gin.Context, code string, message string) {
	c.JSON(http.StatusBadRequest, common.BuildServiceCatalogError(code, message))
}

func setServiceCatalogServerErrResponse(c *gin.Context, code string, message string) {
	c.JSON(http.StatusInternalServerError, common.BuildServiceCatalogError(code, message))
}

func setNamespaceResourceCreateErrResponse(c *gin.Context, errs []error) {
	c.JSON(http.StatusInternalServerError, common.BuildKrobelusErrors(errs))
}

func sendServiceTask(store *db.ServiceStore) error {
	return infra.SendTask(&db.StatusStore{Status: store.Status, Logger: store.Logger})
}

func sendAppTask(store *db.AppStore) (err error) {
	if store.Status != nil && !store.Status.TargetState.IsStopped() {
		err = infra.SendTask(&db.StatusStore{Status: store.Status, Logger: store.Logger})
	}
	for _, service := range store.Services {
		err = infra.SendTask(&db.StatusStore{Status: service.Status, Logger: store.Logger})
	}
	return
}
