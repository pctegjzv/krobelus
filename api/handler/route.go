package handler

import (
	"github.com/gin-gonic/gin"
)

func AddRoutes(r *gin.Engine) {

	// General kubernetes resource api group.
	// Support format:
	// /v1/clusters/:cluster/:type  			[GET]
	//
	// For namespaces scope resources
	// /v1/clusters/:cluster/:type/:namespace/      [GET, POST]
	// /v1/clusters/:cluster/:type/:namespace/:name [GET, PUT, DELETE]
	//
	// For cluster scope resources
	// /v1/clusters/:cluster/:type [POST]
	// /v1/clusters/:cluster/:type/:name [GET, PUT, DELETE]

	v1 := r.Group("/v1/clusters")
	{
		v1.POST("/:cluster/:type", ProcessResource)
		v1.POST("/:cluster/:type/*name", ProcessResource)
		v1.GET("/:cluster/:type", ListResource)
		v1.GET("/:cluster/:type/*name", GetResource)
		v1.PUT("/:cluster/:type/*name", ProcessResource)
		v1.PATCH("/:cluster/:type/*name", ProcessResource)
		v1.DELETE("/:cluster/:type/*name", ProcessResource)
	}

	v2 := r.Group("/v2/clusters").Use(AppMigration())
	{
		v2.POST("/:cluster/applications", CreateApplication)
		v2.PUT("/:cluster/applications/:namespace/:name", UpdateApplication)
		v2.PUT("/:cluster/applications/:namespace/:name/stop", StartStopApplication)
		v2.PUT("/:cluster/applications/:namespace/:name/start", StartStopApplication)
		v2.GET("/:cluster/applications/:namespace/:name", GetApplication)
		v2.GET("/:cluster/applications", ListApplication)
		v2.GET("/:cluster/applications/:namespace", ListApplication)
		v2.GET("/:cluster/applications/:namespace/:name/yaml", GetApplicationYAML)
		v2.DELETE("/:cluster/applications/:namespace/:name", DeleteApplication)
		v2.PUT("/:cluster/applications/:namespace/:name/import", ImportResourcesToApplication)
		v2.PUT("/:cluster/applications/:namespace/:name/export", ExportResourcesFromApplication)
		v2.GET("/:cluster/applications/:namespace/:name/pods", GetApplicationPods)

		v2.GET("/:cluster/topology/:namespace", GetTopology)
		v2.GET("/:cluster/resourcetypes", GetClusterResourceTypes)

		v2.POST("/:cluster/secrets", CreateSecret)
		v2.GET("/:cluster/secrets/:namespace/:name", GetSecret)
		v2.PUT("/:cluster/secrets/:namespace/:name", UpdateSecret)
		v2.PATCH("/:cluster/secrets/:namespace/:name", PatchSecret)
	}

	custom := r.Group("/v1/custom/clusters")
	{
		custom.POST("/:cluster/namespaces", CreateCMBNamespace)
		custom.PUT("/:cluster/namespaces/:name", UpdateCMBNamespace)
	}

	misc := r.Group("/v1/misc/clusters")
	{
		misc.POST("/:cluster/exec/:namespace/:pod/*container", HandleExecShell)
		misc.POST("/:cluster/resources", BatchCreateResource)
	}

	v3 := r.Group("/v3")
	{

		// Apps
		v3.POST("/apps", CreateApp)
		v3.GET("/apps", ListApp)
		v3.GET("/apps/:uuid", GetApp)
		v3.PATCH("/apps/:uuid", UpdateApp)
		v3.DELETE("/apps/:uuid", DeleteApp)
		v3.GET("/apps/:uuid/yaml", GetAppYaml)
		v3.PUT("/apps/:uuid/start", StartApp)
		v3.PUT("/apps/:uuid/stop", StopApp)
		v3.PUT("/apps/:uuid/retry", RetryApp)

		// Services
		v3.POST("/services", CreateService)
		v3.GET("/services", ListServices)
		v3.GET("/services/:uuid", GetService)
		v3.PATCH("/services/:uuid", UpdateService)
		v3.DELETE("/services/:uuid", DeleteService)
		v3.GET("/services/:uuid/yaml", GetServiceYaml)
		v3.GET("/services/:uuid/revisions", GetServiceRevisions)
		v3.PUT("/services/:uuid/rollback", RollbackService)
		v3.PUT("/services/:uuid/scaleup", ServiceScale)
		v3.PUT("/services/:uuid/scaledown", ServiceScale)
		v3.PUT("/services/:uuid/start", StartService)
		v3.PUT("/services/:uuid/stop", StopService)
		v3.PUT("/services/:uuid/retry", RetryService)

		v3.GET("/services/:uuid/instances", ListServiceInstances)

		// Pods
		v3.GET("/pods", ListPods)

		// Namespaces
		// TODO: impl list and remove all v3
		v3.POST("/namespaces", CreateNamespace)
		v3.GET("/namespaces", ListNamespaces)
		v3.DELETE("/namespaces/:uuid", DeleteNamespace)

		// ConfigMaps
		v3.POST("/configmaps", CreateConfigMap)
		v3.GET("/configmaps/:uuid", GetConfigMap)
		v3.DELETE("/configmaps/:uuid", DeleteConfigMap)
		v3.GET("/configmaps", ListConfigMaps)
		v3.PATCH("/configmaps/:uuid", UpdateConfigMap)

		v3.GET("/refs/:uuid", GetRefs)
		v3.GET("/refs", ListRefs)

		//PVs
		v3.POST("/persistentvolumes", CreatePersistentVolume)
		v3.GET("/persistentvolumes", ListPersistentVolume)
		v3.GET("/persistentvolumes/:uuid", GetPersistentVolume)
		v3.PATCH("/persistentvolumes/:uuid", UpdatePersistentVolume)
		v3.DELETE("/persistentvolumes/:uuid", DeletePersistentVolume)

		//PVCs
		v3.POST("/persistentvolumeclaims", CreatePersistentVolumeClaim)
		v3.GET("/persistentvolumeclaims", ListPersistentVolumeClaim)
		v3.GET("/persistentvolumeclaims/:uuid", GetPersistentVolumeClaim)
		v3.PATCH("/persistentvolumeclaims/:uuid", UpdatePersistentVolumeClaim)
		v3.DELETE("/persistentvolumeclaims/:uuid", DeletePersistentVolumeClaim)

		//service catalogs
		// clusterservicebroker
		v3.POST("/clusterservicebrokers", CreateBrokers)
		v3.GET("/clusterservicebrokers", ListBrokers)
		v3.GET("/clusterservicebrokers/:uuid", GetBroker)
		v3.PATCH("/clusterservicebrokers/:uuid", UpdateBroker)
		v3.DELETE("/clusterservicebrokers/:uuid", DeleteBroker)
		// clusterserviceclasses
		v3.GET("/clusterserviceclasses", ListClusterServiceClasses)
		v3.GET("/clusterserviceclasses/:uuid", GetClusterServiceClass)
		// serviceinstance
		v3.POST("/serviceinstances", CreateServiceInstance)
		v3.GET("/serviceinstances", ListSCServiceInstances)
		v3.GET("/serviceinstances/:uuid", GetServiceInstance)
		v3.PATCH("/serviceinstances/:uuid", UpdateServiceInstance)
		v3.DELETE("/serviceinstances/:uuid", DeleteServiceInstance)

		// servicebindings
		v3.POST("/servicebindings", CreateServiceBinding)
		v3.GET("/servicebindings", ListServiceBindings)
		// v3.GET("/servicebindings/:uuid", GetServiceInstance)
		v3.DELETE("/servicebindings/:uuid", DeleteServiceBinding)
	}

	internal := r.Group("/internal")
	{

		// service instances cache
		internal.PUT("/instances/metrics", UpdateRegionInstancesCache)

		// update api discovery cache
		internal.PUT("/apidiscovery", UpdateAPIDiscoveryCache)
	}

	r.GET("/_ping", Ping)
	r.GET("/", Ping)
	r.GET("/_diagnose", Diagnose)

}
