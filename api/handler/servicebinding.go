package handler

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"k8s.io/client-go/rest"
)

func CreateServiceBinding(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Create ServiceBindings")

	logger.Info("Start")
	var needDeleteBind bool
	var ServiceBinding model.ServiceBindingRequest
	var err error

	if err := c.Bind(&ServiceBinding); err != nil {
		logger.WithError(err).Error("Parse Request Body Error")
		setBadRequestResponse(c, err)
		return
	} else {
		if err := validation.ValidateServiceBindingCommon(&ServiceBinding); err != nil {
			setBadRequestResponse(c, err)
			return
		}
		logger.Debugf("Received create ServiceBinding request: %+v", ServiceBinding)
		transition.TransServiceBindingRequest(&ServiceBinding)
	}
	binding, err := ServiceBinding.Kubernetes.ToServiceBinding()
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}
	r, err := infra.GetRedis()
	if err != nil {
		setCacheErrorResponse(c, err)
		return
	}
	BindingServiceCacheKey := fmt.Sprintf("%s:%s:%s:%s",
		ServiceBinding.Cluster.UUID,
		ServiceBinding.Kubernetes.Namespace,
		binding.Spec.ServiceInstanceRef.Name,
		ServiceBinding.Kubernetes.Annotations[common.SvcUidKey()],
	)

	if str, err := r.Get(BindingServiceCacheKey); err != nil || str != "" {
		setServiceCatalogServerErrResponse(c, common.BindingRepeatError, "binding of instance and service repeat")
		return
	}

	r.Set(BindingServiceCacheKey, "true", common.GetTimeDuration(1))
	r.Persist(BindingServiceCacheKey)

	client, err := infra.NewKubeClient(ServiceBinding.Cluster.UUID)
	if err != nil {
		logger.WithError(err).Error("NewKubeClient Error")
		setRegionErrorResponse(c, err)
		return
	}
	cm, err := client.CreateServiceBinding(ServiceBinding.Kubernetes)
	if err != nil {
		logger.WithError(err).Error("CreateServiceBinding Error")
		setK8SErrorResponse(c, err)
		return
	}

	// wait for create ready
	var s *store.ServiceBindingStore
	var reb *scV1beta1.ServiceBinding
	for retryCount := 0; retryCount <= scResultRetryTimes; retryCount++ {
		if retryCount == scResultRetryTimes {
			needDeleteBind = true
			setBrokerTimeoutResponse(c, "Broker Url Timeout")
		}
		scRetrySleep()
		s = getServiceBindingStoreByUid(string(cm.UID))
		if err := s.LoadServiceBinding(); err == nil {
			reb, err = s.Object.ToServiceBinding()
			if err != nil {
				continue
			}
		} else {
			reb, err = client.GetServiceBinding(ServiceBinding.Resource.Name, ServiceBinding.Namespace.Name)
			s.Response = model.ServiceBindingToResponse(reb)
		}
		if len(reb.Status.Conditions) == 0 {
			continue
		}
		if reb.Status.Conditions[0].Status == scV1beta1.ConditionTrue {
			break
		}
		if reb.Status.Conditions[0].Status == scV1beta1.ConditionFalse {
			switch reb.Status.Conditions[0].Reason {
			case bindingInFlightReason:
				continue
			default:
				setServiceCatalogServerErrResponse(c, reb.Status.Conditions[0].Reason, reb.Status.Conditions[0].Message)
				break
			}
			needDeleteBind = true
			break
		}
	}
	if needDeleteBind {
		if err = client.DeleteServiceBinding(ServiceBinding.Namespace.Name, ServiceBinding.Resource.Name); err != nil {
			logger.Debugf(" delete service binding err: %+v", err)
		}
		r.Del(BindingServiceCacheKey)
		return
	}
	response := model.ServiceBindingToResponse(cm)
	response.Cluster = ServiceBinding.Cluster
	response.Namespace = ServiceBinding.Namespace
	c.JSON(http.StatusCreated, response)
}

func ListServiceBindings(c *gin.Context) {
	var must model.ServiceBindingMustQuery
	if err := c.BindQuery(&must); err != nil {
		setBadRequestResponse(c, err)
		return
	} else {
		if must.Namespace == "" || must.Cluster == "" {
			setBadRequestResponse(c, errors.New("namespace or cluster is requried"))
			return
		}
	}
	logger := common.GetLoggerWithAction(c, "List ServiceBindings")
	serviceUuid := c.Query("service_uuid")
	instanceName := c.Query("instance_name")
	request, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for ServiceBinding list api.")
		setBadRequestResponse(c, err)
		return
	}
	logger.Debugf("Received list ServiceBinding request: %+v", request)
	s := store.ServiceBindingListStore{
		ResourceListStore: store.ResourceListStore{
			UUIDs:  request.UUIDS,
			Logger: logger,
		},
	}
	if err = s.LoadServiceBindingsbyFilter(must.Namespace, serviceUuid, instanceName); err != nil {
		s.Logger.WithError(err).Error("list ServiceBinding error")
		setServerUnknownError(c, err)
	} else {
		if err != nil {
			setServerUnknownError(c, err)
			return
		}
		if len(s.Items) > 0 {
			for _, item := range s.Items {
				serviceStore := &db.ServiceStore{
					UUID:   item.ServiceResource.UUID,
					Logger: common.GetLogger(c),
				}
				err = serviceStore.GetService(false)
				if err != nil {
					setErrorResponse(c, err)
					return
				}
				transition.TranslateServiceDetailResponse(serviceStore, true, true, nil)
				item.ServiceResource = serviceStore.Response.Resource
			}
			c.JSON(http.StatusOK, s.Items)
		} else {
			setEmptyListResponse(c)
			return
		}
	}
}

func DeleteServiceBinding(c *gin.Context) {
	s := getServiceBindingStore(c, "Delete")
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	if err := s.LoadServiceBinding(); err != nil {
		setErrorResponse(c, err)
		return
	}
	binding, err := s.Response.Kubernetes.ToServiceBinding()
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}
	r, err := infra.GetRedis()
	if err != nil {
		setCacheErrorResponse(c, err)
		return
	}
	BindingServiceCacheKey := fmt.Sprintf("%s:%s:%s:%s",
		s.Response.Cluster.UUID,
		s.Response.Namespace.Name,
		binding.Spec.ServiceInstanceRef.Name,
		s.Response.Kubernetes.Annotations[common.SvcUidKey()],
	)
	_, err = r.Get(BindingServiceCacheKey)
	if err != nil {
		setBadRequestResponse(c, errors.New("binding of instance and service repeat"))
	} else {
		r.Del(BindingServiceCacheKey)
	}

	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	if err = client.DeleteServiceBinding(s.UniqueName.Namespace, s.UniqueName.Name); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}

func getServiceBindingStore(c *gin.Context, action string) *store.ServiceBindingStore {
	return &store.ServiceBindingStore{
		ResourceStore: store.ResourceStore{
			UUID:   getUUID(c),
			Logger: common.GetLoggerWithAction(c, fmt.Sprintf("%s ServiceBinding", action)),
		},
	}
}

func getServiceBindingStoreByUid(uid string) *store.ServiceBindingStore {
	return &store.ServiceBindingStore{
		ResourceStore: store.ResourceStore{
			UUID: uid,
		},
	}
}

// HandleBeforeSBRequest does some works before executing SB request.
func HandleBeforeSBRequest(method string, s *store.KubernetesResourceStore) error {
	var err error
	switch method {
	case common.HttpPost:
		err = handleBeforeSBCreate(s)
	case common.HttpDelete:
		err = handleBeforeSBDelete(s)
	}
	return err
}

func handleBeforeSBCreate(s *store.KubernetesResourceStore) error {
	if err := s.LoadClient(); err != nil {
		return err
	}
	bindingName := common.NewUUID()
	s.NameKey = bindingName
	s.Object.ObjectMeta.SetName(bindingName)

	binding, err := s.Object.ToServiceBinding()
	if err != nil {
		return err
	}
	req := s.Client.Get()
	bindingList := &scV1beta1.ServiceBindingList{}
	err = req.Resource(s.UniqueName.Type).Namespace(s.UniqueName.Namespace).Do().Into(bindingList)
	if err != nil {
		return err
	}
	for _, item := range bindingList.Items {
		labels := item.ObjectMeta.GetLabels()
		if labels[common.SvcUidKey()] == binding.ObjectMeta.Labels[common.SvcUidKey()] &&
			item.Spec.ServiceInstanceRef.Name == binding.Spec.ServiceInstanceRef.Name {
			return common.BuildServiceCatalogError(common.BindingRepeatError, "binding of instance and service repeat")
		}
	}
	return nil
}

func handleBeforeSBDelete(s *store.KubernetesResourceStore) error {
	return nil
}

func HandleSBRequestDone(method string, s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	var err error
	var body []byte
	switch method {
	case common.HttpGet:
		if s.UniqueName.Name == "" {
			body, err = handleSBListDone(s, result)
		}
	}
	return body, err
}

func handleSBListDone(s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	instanceName := s.Context.Query("instance_name")
	if err != nil {
		return nil, err
	}
	allbindingList := &scV1beta1.ServiceBindingList{}
	bindingList := &scV1beta1.ServiceBindingList{}
	bindingList.Items = make([]scV1beta1.ServiceBinding, 0)

	var body []byte
	if result != nil {
		result.Into(allbindingList)
		for _, item := range allbindingList.Items {
			anno := make(map[string]string)

			if instanceName == "" || instanceName == item.Spec.ServiceInstanceRef.Name {
				instance, _ := client.GetServiceInstance(item.Spec.ServiceInstanceRef.Name, item.ObjectMeta.Namespace)
				if instance != nil {
					anno[common.SCCNameKey()] = instance.Spec.ClusterServiceClassExternalName
					anno[common.SCPNameKey()] = instance.Spec.ClusterServicePlanExternalName
					anno[common.SCINameKey()] = item.Spec.ServiceInstanceRef.Name
				}
			} else {
				continue
			}

			var serviceStore *db.ServiceStore
			var serviceRes model.ServiceResource
			// credential
			uuid := item.GetLabels()[common.SvcUidKey()]
			if uuid != "" {
				serviceStore = &db.ServiceStore{
					UUID:   uuid,
					Logger: common.GetLogger(s.Context),
				}
				err = serviceStore.GetService(false)
				if err != nil {
					continue
				}
				transition.TranslateServiceDetailResponse(serviceStore, true, true, nil)
				serviceRes = serviceStore.Response.Resource
			}
			// get credentials
			secret, err := client.GetV1Secret(item.Spec.SecretName, item.ObjectMeta.Namespace)
			credential := map[string]string{}
			s.Logger.Debugf("Get secret: %+v", secret)

			if err == nil || secret != nil {
				if secret.Data != nil {
					for k, byteArray := range secret.Data {
						credential[k] = string(byteArray[:len(byteArray)])
					}
				}
			}
			reflist := map[string]interface{}{
				"credential": credential,
				"service":    serviceRes,
			}

			rawRef, err := json.Marshal(reflist)
			if err != nil {
				return nil, err
			}
			anno[common.ObjRefListKey()] = string(rawRef)

			item.SetAnnotations(anno)
			bindingList.Items = append(bindingList.Items, item)
		}
	} else if s.Objects != nil && len(s.Objects.Items) > 0 {
		for _, item := range s.Objects.Items {
			binding, err := item.ToServiceBinding()
			if err != nil {
				return nil, err
			}
			anno := make(map[string]string)

			if instanceName == "" || instanceName == binding.Spec.ServiceInstanceRef.Name {
				instance, _ := client.GetServiceInstance(binding.Spec.ServiceInstanceRef.Name, binding.ObjectMeta.Namespace)
				if instance != nil {
					anno[common.SCCNameKey()] = instance.Spec.ClusterServiceClassExternalName
					anno[common.SCPNameKey()] = instance.Spec.ClusterServicePlanExternalName
					anno[common.SCINameKey()] = binding.Spec.ServiceInstanceRef.Name
				}
			} else {
				continue
			}
			var serviceStore *db.ServiceStore
			var serviceRes model.ServiceResource
			uuid := item.GetLabels()[common.SvcUidKey()]
			// get bind service
			if uuid != "" {
				serviceStore = &db.ServiceStore{
					UUID:   uuid,
					Logger: common.GetLogger(s.Context),
				}
				err = serviceStore.GetService(false)
				if err != nil {
					continue
				}
				transition.TranslateServiceDetailResponse(serviceStore, true, true, nil)
				serviceRes = serviceStore.Response.Resource
			}
			// get credentials

			secret, err := client.GetV1Secret(binding.Spec.SecretName, binding.ObjectMeta.Namespace)
			credential := map[string]string{}

			if err == nil || secret != nil {
				if secret.Data != nil {
					for k, byteArray := range secret.Data {
						credential[k] = string(byteArray[:len(byteArray)])
					}
				}
			}

			reflist := map[string]interface{}{
				"credential": credential,
				"service":    serviceRes,
			}

			rawRef, err := json.Marshal(reflist)
			if err != nil {
				return nil, err
			}
			anno[common.ObjRefListKey()] = string(rawRef)
			binding.SetAnnotations(anno)
			bindingList.Items = append(bindingList.Items, *binding)
		}
	}

	body, err = json.Marshal(bindingList)
	if err != nil {
		return nil, err
	}
	return body, err
}
