package handler

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	kubeStore "krobelus/store"
)

// Create an app.
func CreateApp(c *gin.Context) {
	logger := common.GetLogger(c)

	var store = &db.AppStore{
		Request: &model.AppRequest{},
		Logger:  logger,
	}

	if err := c.Bind(store.Request); err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	logger.Infof("Create app | request: %s.", common.ToString(store.Request))

	if err := transition.TranslateAppCreatePayloadPre(store); err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	logger.Infof("Translate app | uuid %v.", store.Request.Resource.UUID)

	if err := validation.ValidateAppCreate(store); err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	if err := transition.TranslateAppCreatePayload(store); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	if IsParseResourcesRequest(c) {
		c.JSON(http.StatusCreated, store.ParseResourceItems())
		return
	}

	if err := store.Create(); err != nil {
		logger.Errorf("Save app to db error: %s", err.Error())
		setDbErrorResponse(c, err, "app")
		return
	}

	logger.Infof("Send app create message | uuid %v.", store.Request.Resource.UUID)

	if err := sendAppTask(store); err != nil {
		logger.Errorf("Send app create message error: %s", err.Error())
	}
	c.JSON(http.StatusCreated, transition.TranslateAppDetailResponse(store))
}

// Get app.
func GetApp(c *gin.Context) {
	uuid := c.Param("uuid")
	logger := common.GetLogger(c)
	store := &db.AppStore{
		Request: &model.AppRequest{Resource: model.AppResource{UUID: uuid}},
		Logger:  logger,
	}

	logger.Infof("Get app | uuid %v.", uuid)
	onlyYaml := strings.ToLower(c.Query("only_yaml")) == "true"
	if err := store.Get(); err != nil {
		store.Logger.Errorf("Service retrieve in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	if onlyYaml {
		c.JSON(http.StatusOK, model.KubernetesYaml{Kubernetes: store.Response.Kubernetes, KubernetesBackup: store.Response.KubernetesBackup})
		return
	}

	store.SetServices()
	c.JSON(http.StatusOK, transition.TranslateAppDetailResponse(store))
}

// Get app yaml
func GetAppYaml(c *gin.Context) {
	uuid := c.Param("uuid")
	logger := common.GetLogger(c)
	store := &db.AppStore{
		Request: &model.AppRequest{Resource: model.AppResource{UUID: uuid}},
		Logger:  logger,
	}

	logger.Infof("Get app | uuid %v.", uuid)
	err := store.Get()
	if err != nil {
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.SetServices()
	transition.TranslateAppYamlResponse(store)

	bytes, err := transition.KubernetesJsonToYamlBytes(store.Response.Kubernetes)
	c.Writer.Header()["Content-Type"] = transition.YamlContentType
	c.Writer.Write(bytes)
	c.Status(http.StatusOK)
}

// List app.
func ListApp(c *gin.Context) {
	data, err := getRequestData(c)
	logger := common.GetLogger(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	list := data.UUIDS
	logger.Debugf("List app | uuids %s.", common.ToString(list))

	items, err := db.ListApps(list)
	if err != nil {
		logger.Errorf("App list retrieve in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AppType)
		return
	}

	err = transition.TranslateAppListResponse(&model.AppListStore{Apps: items, Logger: logger})
	if err != nil {
		logger.Errorf("App list translate error: %s", err.Error())
		setDbErrorResponse(c, err, model.AppType)
		return
	}
	if len(items) > 0 {
		logger.Debug("Add region for watch when list apps.")
		infra.AddRegionForWatch(items[0].Cluster.UUID)
		c.JSON(http.StatusOK, items)
	} else {
		setEmptyListResponse(c)
	}
}

// Start app.
func StartApp(c *gin.Context) {
	uuid := c.Param("uuid")
	logger := common.GetLogger(c)
	store := &db.AppStore{
		Request: &model.AppRequest{Resource: model.AppResource{UUID: uuid}},
		Logger:  logger,
	}

	logger.Infof("Start app | uuid %v.", uuid)
	err := store.Get()
	if err != nil {
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.SetServices()

	logger.Infof("Validate app | uuid %v.", uuid)
	err = validation.ValidateAppStart(store)
	if err != nil {
		logger.Errorf("Validate app error: %s", err.Error())
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	transition.TranslateAppStartPayload(store)

	logger.Infof("Save app | uuid %v.", uuid)
	err = store.Start()
	if err != nil {
		logger.Errorf("Get app error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}

	logger.Infof("Send app start message | uuid %v.", uuid)
	if err = sendAppTask(store); err != nil {
		logger.Errorf("Send app start message error: %s", err.Error())
	}
	c.JSON(http.StatusAccepted, transition.TranslateAppDetailResponse(store))
}

// Stop app.
func StopApp(c *gin.Context) {
	uuid := c.Param("uuid")
	logger := common.GetLogger(c)
	store := &db.AppStore{
		Request: &model.AppRequest{Resource: model.AppResource{UUID: uuid}},
		Logger:  logger,
	}

	logger.Infof("Stop app | uuid %v.", uuid)
	err := store.Get()
	if err != nil {
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.SetServices()

	logger.Infof("Validate app | uuid %v.", uuid)
	err = validation.ValidateAppStop(store)
	if err != nil {
		logger.Errorf("Validate app error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}
	transition.TranslateAppStopPayload(store)

	logger.Infof("Save app | uuid %v.", uuid)
	err = store.Stop()
	if err != nil {
		logger.Errorf("Save app to db error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}

	logger.Infof("Send app stop message | uuid %v.", uuid)
	if err = sendAppTask(store); err != nil {
		logger.Errorf("Send app stop message error: %s", err.Error())
	}
	c.JSON(http.StatusAccepted, transition.TranslateAppDetailResponse(store))
}

// Delete app.
func DeleteApp(c *gin.Context) {
	uuid := c.Param("uuid")
	force := strings.ToLower(c.Query("force")) == "true"
	logger := common.GetLogger(c)
	store := &db.AppStore{
		Request: &model.AppRequest{Resource: model.AppResource{UUID: uuid}},
		Logger:  logger,
	}

	logger.Infof("Delete app | uuid %v.", uuid)
	err := store.Get()
	if err != nil {
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.SetServices()

	logger.Infof("Validate app | uuid %v.", uuid)
	err = validation.ValidateAppDelete(store)
	if err != nil {
		logger.Errorf("Validate app error: %s", err.Error())
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	transition.TranslateAppDeletePayload(store, force)

	logger.Infof("Save app | uuid %v.", uuid)
	err = store.Delete()
	if err != nil {
		logger.Errorf("Save app to db error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}

	logger.Infof("Send app delete message | uuid %v.", uuid)
	if err = sendAppTask(store); err != nil {
		logger.Errorf("Send app delete message error: %s", err.Error())
	}

	if force {
		// Set status code to 404, let jakiro delete it in resource table
		c.JSON(http.StatusNotFound, common.BuildDBNotFoundError(fmt.Sprintf("%s not exists", model.AppType)))
	} else {
		c.JSON(http.StatusNoContent, nil)
	}
}

// Retry app.
func RetryApp(c *gin.Context) {
	uuid := c.Param("uuid")
	logger := common.GetLogger(c)
	store := &db.AppStore{
		Request: &model.AppRequest{Resource: model.AppResource{UUID: uuid}},
		Logger:  logger,
	}

	logger.Infof("Retry app | uuid %v.", uuid)
	err := store.Get()
	if err != nil {
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.SetServices()

	logger.Infof("Validate app | uuid %v.", uuid)
	err = validation.ValidateAppRetry(store)
	if err != nil {
		logger.Errorf("Validate app error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}
	transition.TranslateAppRetryPayload(store)

	logger.Infof("Save app | uuid %v.", uuid)
	err = store.Retry()
	if err != nil {
		logger.Errorf("Save app to db error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}

	logger.Infof("Send app retry message | uuid %v.", uuid)
	if err = sendAppTask(store); err != nil {
		logger.Errorf("Send app retry message error: %s", err.Error())
	}
	c.JSON(http.StatusAccepted, transition.TranslateAppDetailResponse(store))
}

// Update app.
func UpdateApp(c *gin.Context) {
	uuid := getUUID(c)
	parseResources := IsParseResourcesRequest(c)
	var update model.CommonUpdateRequest
	if err := c.Bind(&update); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	store := db.AppStore{
		UUID:          uuid,
		Logger:        common.GetLoggerWithAction(c, "Update App"),
		Request:       &model.AppRequest{Resource: model.AppResource{UUID: uuid}, Kubernetes: update.Kubernetes},
		UpdateRequest: &update,
	}
	if update.Resource.Description != nil && !parseResources {
		store.Logger.Infof("Trying to update app description: %s", store.UUID)
		if err := store.UpdateDescription(*update.Resource.Description); err != nil {
			setDbErrorResponse(c, err, model.AppType)
			return
		} else {
			store.Logger.Infof("App description update to %s", *update.Resource.Description)
		}
	}

	store.Logger.Infof("Update app | uuid %v.", uuid)
	if err := store.Get(); err != nil {
		setDbErrorResponse(c, err, model.AppType)
		return
	}

	store.Logger.Infof("Translate app pre | uuid %v.", store.Request.Resource.UUID)
	if err := transition.TranslateAppUpdatePayloadPre(&store); err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	store.Logger.Infof("Validate app | uuid %v.", uuid)
	if err := validation.ValidateAppUpdate(&store); err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	store.Logger.Infof("Translate app | uuid %v.", store.Request.Resource.UUID)
	if err := transition.TranslateAppUpdatePayload(&store); err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	store.ParseOtherResourcesUpdate()

	if parseResources {
		items := store.ParseResourceUpdateItems()
		if len(items) == 0 {
			setEmptyListResponse(c)
		} else {
			c.JSON(http.StatusOK, items)
		}
		return
	}

	if err := kubeStore.DeleteOtherResources(store.Response, store.Others.Update.DeleteList); err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	store.Logger.Infof("Save app | uuid %v.", uuid)
	if err := store.Update(); err != nil {
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}

	store.Logger.Infof("Send app update message | uuid %v.", uuid)
	if err := sendAppTask(&store); err != nil {
		store.Logger.Errorf("Send app update message error: %s", err.Error())
	}
	c.JSON(http.StatusOK, transition.TranslateAppDetailResponse(&store))
}
