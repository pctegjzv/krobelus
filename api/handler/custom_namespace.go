package handler

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"
	"krobelus/infra/kubernetes"
	"krobelus/model"
	"krobelus/store"

	"github.com/Rican7/retry"
	"github.com/Rican7/retry/backoff"
	"github.com/Rican7/retry/strategy"
	"github.com/gin-gonic/gin"
	"k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
)

type createOtherResourceFunc func(*infra.KubeClient, *model.CMBNamespaceRequest) error

// CreateCMBNamespace creates a namespace used for CMB.
// It will also create a resource quota and several secrets.
func CreateCMBNamespace(c *gin.Context) {
	var namespace model.CMBNamespaceRequest
	cluster := c.Param("cluster")

	if err := c.Bind(&namespace); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	logger := common.GetLoggerWithAction(c, "Create CMB Namespace")
	logger.Debugf("Create CMB namespace: %+v", &namespace)
	client, err := infra.NewKubeClient(cluster)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}

	// Create namespace
	ns := &v1.Namespace{}
	model.UpdateAnnotations(namespace.Namespace, common.ResourceStatusKey(), common.ResourceStatusInitializing)
	reqNS, err := namespace.Namespace.ToNamespace()
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}

	err = client.CreateResource("namespaces", reqNS, nil, ns)
	if err != nil {
		SetErrorResponse(c, err, "namespace")
		return
	}

	errs := createCMBNamespaceOtherResources(client, &namespace)
	if len(errs) == 0 {
		ns = &v1.Namespace{}
		err = client.GetResource("namespaces", namespace.Namespace.Name,
			&metaV1.GetOptions{}, nil, ns)
		if err != nil {
			logger.Errorf("Update namespace state error: %v", err)
		} else {
			delete(ns.Annotations, common.ResourceStatusKey())
			err = client.UpdateResource("namespaces", ns.Name, ns, nil, ns)
			if err != nil {
				logger.Errorf("Update namespace state error: %v", err)
			}
		}
	} else {
		logger.Errorf("Create other resources error: %+v", errs)
		setNamespaceResourceCreateErrResponse(c, errs)
		return
	}
	c.JSON(http.StatusCreated, ns)
}

func createCMBNamespaceOtherResources(client *infra.KubeClient, namespace *model.CMBNamespaceRequest) []error {
	var errs []error
	lock := &sync.Mutex{}
	wg := sync.WaitGroup{}

	createFuncs := []createOtherResourceFunc{
		createResourceQuota,
		createSecretData,
		createJFrogNamespace,
		createLimitRange,
		addDefaultSAToSCC,
	}

	for _, fn := range createFuncs {
		wg.Add(1)
		go func(fn createOtherResourceFunc) {
			defer wg.Done()
			err := fn(client, namespace)
			if err != nil {
				lock.Lock()
				defer lock.Unlock()
				errs = append(errs, err)
			}
		}(fn)
	}

	wg.Wait()
	return errs
}

func createResourceQuota(client *infra.KubeClient, ns *model.CMBNamespaceRequest) error {
	rqReq, err := ns.ResourceQuota.ToResourceQuota()
	if err != nil {
		return common.BuildResourceQuotaCreateError(err.Error())
	}

	rq := &v1.ResourceQuota{}
	err = client.CreateResource("resourcequotas", rqReq,
		map[string]interface{}{kubernetes.ResourceNamespaceParam: ns.Namespace.Name}, rq)
	if err != nil {
		return common.BuildResourceQuotaCreateError(err.Error())
	}

	return nil
}

func createLimitRange(client *infra.KubeClient, ns *model.CMBNamespaceRequest) error {
	cpuQuantityRequest, err := resource.ParseQuantity(config.GlobalConfig.CMB.DefaultContainerCPURequest)
	if err != nil {
		return err
	}

	memQuantityRequest, err := resource.ParseQuantity(config.GlobalConfig.CMB.DefaultContainerMEMRequest)
	if err != nil {
		return err
	}

	cpuQuantityLimit, err := resource.ParseQuantity(config.GlobalConfig.CMB.DefaultContainerCPULimit)
	if err != nil {
		return err
	}

	memQuantityLimit, err := resource.ParseQuantity(config.GlobalConfig.CMB.DefaultContainerMEMLimit)
	if err != nil {
		return err
	}

	lrReq := &v1.LimitRange{}
	lrReq.APIVersion = common.KubernetesAPIVersionV1
	lrReq.Kind = string(model.LimitRange)
	lrReq.Namespace = ns.Namespace.Name
	lrReq.Spec.Limits = []v1.LimitRangeItem{
		{
			Type: v1.LimitTypeContainer,
			DefaultRequest: v1.ResourceList{
				v1.ResourceCPU:    cpuQuantityRequest,
				v1.ResourceMemory: memQuantityRequest,
			},
			Default: v1.ResourceList{
				v1.ResourceCPU:    cpuQuantityLimit,
				v1.ResourceMemory: memQuantityLimit,
			},
		},
	}

	lr := &v1.LimitRange{}
	err = client.CreateResource("limitranges", lrReq,
		map[string]interface{}{kubernetes.ResourceNamespaceParam: ns.Namespace.Name}, lr)
	if err != nil {
		return common.BuildLimitRangeCreateError(err.Error())
	}

	return nil
}

func createSecretData(client *infra.KubeClient, ns *model.CMBNamespaceRequest) error {
	var secretResp *v1.Secret
	secretResp, err := createCMBImagePullSecret(client, ns.Namespace.Name, ns.SecretData)
	if err != nil {
		return common.BuildSecretCreateError(err.Error())
	}

	if err := addSecretToDefaultSA(client, secretResp); err != nil {
		return common.BuildSAAddSecretCreateError(err.Error())
	}

	return nil
}

func createJFrogNamespace(client *infra.KubeClient, ns *model.CMBNamespaceRequest) error {
	secret := ns.SecretData
	err := infra.CreateJFrogNamespace(&infra.JFrogNamespace{
		Name:      secret.Name,
		Email:     secret.Email,
		Tenant:    secret.Tenant,
		Namespace: secret.Namespace,
		Password:  secret.Password,
		Account:   secret.Account,
		Endpoint:  secret.Endpoint,
	})
	if err != nil {
		return common.BuildJFrogNamespaceCreateError(err)
	}

	return nil
}

func createCMBImagePullSecret(client *infra.KubeClient, namespace string, secret *model.Secret) (*v1.Secret, error) {
	dockerConfig := &model.DockerConfigs{
		Auths: map[string]*model.DockerConfig{
			secret.Endpoint: {
				Username: secret.Name,
				Password: secret.Password,
				Email:    secret.Email,
				Auth:     base64.StdEncoding.EncodeToString([]byte(secret.Name + ":" + secret.Password)),
			},
		},
	}
	dcj, err := json.Marshal(dockerConfig)
	if err != nil {
		return nil, err
	}
	k8sSecret := &v1.Secret{
		ObjectMeta: metaV1.ObjectMeta{
			Name:      secret.SecretName,
			Namespace: namespace,
		},
		Type: v1.SecretTypeDockerConfigJson,
		StringData: map[string]string{
			v1.DockerConfigJsonKey: string(dcj),
		},
	}

	result := &v1.Secret{}
	err = client.CreateResource("secrets", k8sSecret,
		map[string]interface{}{kubernetes.ResourceNamespaceParam: namespace}, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func addSecretToDefaultSA(client *infra.KubeClient, secret *v1.Secret) error {
	options := metaV1.GetOptions{}

	// Wait until default service account created or retry failed.
	sa := &v1.ServiceAccount{}
	action := func(attempt uint) error {
		err := client.GetResource("serviceaccounts", "default", &options,
			map[string]interface{}{kubernetes.ResourceNamespaceParam: secret.Namespace}, sa)
		return err
	}

	err := retry.Retry(
		action,
		strategy.Backoff(backoff.BinaryExponential(2*time.Millisecond)),
		strategy.Limit(10),
	)

	if err != nil {
		return err
	}

	updateSASecret := func(sa *v1.ServiceAccount) error {
		for _, s := range sa.ImagePullSecrets {
			if s.Name == secret.Name {
				return nil
			}
		}
		sa.ImagePullSecrets = append(sa.ImagePullSecrets, v1.LocalObjectReference{Name: secret.Name})
		return client.UpdateResource("serviceaccounts", sa.Name, sa,
			map[string]interface{}{kubernetes.ResourceNamespaceParam: secret.Namespace}, sa)
	}
	err = updateSASecret(sa)
	if k8sErrors.IsConflict(err) {
		// Conflict due to invalid resource revision, upadte again.
		sa = &v1.ServiceAccount{}
		err := client.GetResource("serviceaccounts", "default", &options,
			map[string]interface{}{kubernetes.ResourceNamespaceParam: secret.Namespace}, sa)
		if err != nil {
			return err
		}
		return updateSASecret(sa)
	}

	return err
}

func addDefaultSAToSCC(client *infra.KubeClient, ns *model.CMBNamespaceRequest) error {
	logger := common.GetLoggerByRegionID(client.RegionID)
	ss := store.GetKubernetesResourceStore(&model.Query{
		ClusterUUID: client.RegionID,
		Name:        "anyuid",
		Type:        "securitycontextconstraints",
		PatchType:   string(types.JSONPatchType),
	}, logger, nil)
	if err := ss.LoadClient(); err != nil && err == kubernetes.ErrorResourceKindNotFound {
		logger.Warnf("Resources kind 'securitycontextconstraints' not found, skip adding default sa to scc.")
		return nil
	}

	ss.Raw = []byte(fmt.Sprintf(`[{"op": "add", "path": "/users/1", "value": "system:serviceaccount:%s:default" }]`,
		ns.Namespace.Name))
	result, err := processResource(&ss, common.HttpPatch)
	if err != nil {
		return common.BuildAddSAToSCCError(err.Error())
	}

	if result.Error != nil {
		return common.BuildAddSAToSCCError(result.Error.Error())
	}
	return nil
}

// UpdateCMBNamespace updates a namespace used for CMB.
// Now supports updating following data:
// * resource quota
func UpdateCMBNamespace(c *gin.Context) {
	var namespace model.CMBNamespaceUpdateRequest
	cluster := c.Param("cluster")
	name := c.Param("name")

	if err := c.Bind(&namespace); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	logger := common.GetLoggerWithAction(c, "Update CMB Namespace")
	logger.Debugf("Update CMB namespace: %+v", &namespace)

	s := store.GetKubernetesResourceStore(&model.Query{
		ClusterUUID: cluster,
		Type:        "resourcequotas",
		Namespace:   name,
		Name:        namespace.ResourceQuota.Name,
		PatchType:   string(types.MergePatchType),
	}, logger, nil)
	s.Raw = model.GetResourceQuotaUpdateJson(namespace.ResourceQuota.Spec)
	result, err := s.Request(common.HttpPatch, nil)
	if result == nil && err != nil {
		setServerUnknownError(c, err)
	} else {
		if err := s.UpdateResourceCache(result); err != nil {
			logger.WithError(err).Warn("update resource cache error")
		}
		SetKubernetesResult(c, model.ParseResult(result))
	}
}
