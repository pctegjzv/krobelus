package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

func GetClusterServiceClass(c *gin.Context) {
	s := getServiceClassStore(c, "Get")
	if err := s.LoadServiceClass(); err != nil {
		setErrorResponse(c, err)
	} else {
		c.JSON(http.StatusOK, s.Response)
	}
}

func ListClusterServiceClasses(c *gin.Context) {
	brokerFilter := c.Query("broker_name")
	classFilter := c.Query("name")
	logger := common.GetLogger(c)
	data, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for classes list api.")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	uuids := data.UUIDS
	pageSize, err := cast.ToIntE(c.DefaultQuery("page_size", common.DefaultPageSize))
	if err != nil {
		logger.WithError(err).Error("Invalid page_size query params!")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	page, err := cast.ToIntE(c.DefaultQuery("page", common.DefaultPage))
	if err != nil {
		logger.WithError(err).Error("Invalid page query params!")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	classes, err := store.GetServicesClassesByViewableBrokerUUIDs(uuids, data.ClusterUUID, brokerFilter, classFilter)
	if err != nil {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	logger.Debugf("Trying to retrieve classes for broker uuids: %+v", uuids)
	result := getClassPagedResult(classes, pageSize, page)
	c.JSON(http.StatusOK, result)
}
func getServiceClassStore(c *gin.Context, action string) *store.ServiceClassStore {
	return &store.ServiceClassStore{
		ResourceStore: store.ResourceStore{
			UUID:   getUUID(c),
			Logger: common.GetLoggerWithAction(c, fmt.Sprintf("%s ServiceClass", action)),
		},
	}
}
func getClassPagedResult(classes []*model.ServiceClassItemResponse, pageSize int, page int) gin.H {
	count := len(classes)
	start := pageSize * (page - 1)
	end := pageSize * page
	result := classes
	if count > start {
		if end > count {
			end = count
		}
		result = classes[start:end]
	} else {
		result = result[0:0]
	}
	return gin.H{
		"num_pages": page,
		"page_size": pageSize,
		"count":     count,
		"results":   result,
	}
}

func HandleCSCRequestDone(method string, s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	var err error
	var body []byte
	switch method {
	case common.HttpGet:
		if s.UniqueName.Name != "" {
			body, err = handleCSCGetDone(s, result)
		} else {
			body, err = handleCSCListDone(s, result)
		}
	}
	return body, err
}

func handleCSCGetDone(s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return nil, err
	}
	var class *scV1beta1.ClusterServiceClass
	if s.Object != nil {
		class, err = s.Object.ToServiceClass()
		if err != nil {
			return nil, err
		}
	} else if result.Error() != nil {
		result.Into(class)
	} else {
		return nil, result.Error()
	}

	plans, _ := client.GetClusterServicePlans(&metaV1.ListOptions{
		FieldSelector: fmt.Sprintf("spec.clusterServiceClassRef.name=%s", s.UniqueName.Name),
	})

	anno := make(map[string]string)
	rawPlans, err := json.Marshal(map[string]interface{}{common.ClusterServicePlanType: plans})
	if err != nil {
		return nil, err
	}
	anno[common.ObjRefListKey()] = string(rawPlans)
	broker, err := client.GetClusterServiceBroker(class.Spec.ClusterServiceBrokerName)
	if err != nil {
		return nil, err
	}
	anno[common.SCBDisplayNameKey()] = broker.GetAnnotations()[common.SCBDisplayNameKey()]
	anno[common.SCBStatusKey()] = string(broker.Status.Conditions[0].Status)
	s.Object.SetAnnotations(anno)
	body, err := json.Marshal(s.Object)
	if err != nil {
		return nil, err
	}
	return body, err
}

func handleCSCListDone(s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return nil, err
	}
	classList := &scV1beta1.ClusterServiceClassList{}
	classList.Items = make([]scV1beta1.ClusterServiceClass, 0)

	var body []byte
	if result != nil {
		result.Into(classList)
		for index, item := range classList.Items {
			broker, err := client.GetClusterServiceBroker(item.Spec.ClusterServiceBrokerName)
			if err == nil {
				anno := make(map[string]string)
				anno[common.SCBDisplayNameKey()] = broker.GetAnnotations()[common.SCBDisplayNameKey()]
				anno[common.SCBStatusKey()] = string(broker.Status.Conditions[0].Status)
				classList.Items[index].SetAnnotations(anno)
			}
		}
	} else if s.Objects != nil && len(s.Objects.Items) > 0 {
		for _, item := range s.Objects.Items {
			class, err := item.ToServiceClass()
			if err != nil {
				return nil, err
			}
			broker, err := client.GetClusterServiceBroker(class.Spec.ClusterServiceBrokerName)
			if err == nil {
				anno := make(map[string]string)
				anno[common.SCBDisplayNameKey()] = broker.GetAnnotations()[common.SCBDisplayNameKey()]
				anno[common.SCBStatusKey()] = string(broker.Status.Conditions[0].Status)
				class.SetAnnotations(anno)
				classList.Items = append(classList.Items, *class)
			}
		}
	}

	body, err = json.Marshal(classList)
	if err != nil {
		return nil, err
	}
	return body, err
}
