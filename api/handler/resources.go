package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

func getParams(c *gin.Context, keys []string) map[string]string {
	params := map[string]string{}
	for _, key := range keys {
		if value := c.Query(key); value != "" {
			params[key] = value
		}
	}
	return params
}

// getQuery generate a Query struct from query string, if any param missing, return error
// name format list:
//		/{name}
//		/{namespace}
//		/{namespace}/{name}
//		/{namespace}/{name}/{subtype}
func getQuery(c *gin.Context) (*model.Query, error) {
	logger := common.GetLogger(c)
	name := c.Param("name")

	if len(name) > 0 && strings.HasPrefix(name, "/") {
		name = name[1:]
	}

	var namespace string
	var subType string

	result := strings.SplitN(name, "/", 3) // split up to 3 elements

	switch len(result) {
	case 3:
		namespace = result[0]
		name = result[1]
		subType = result[2]
	case 2:
		namespace = result[0]
		name = result[1]
	default:
		name = result[0]
	}

	contentType := c.Request.Header.Get("Content-Type")

	// Remove "; charset=" if included in header.
	if idx := strings.Index(contentType, ";"); idx > 0 {
		contentType = contentType[:idx]
	}
	patchType := types.PatchType(contentType)

	// Ensure the patchType is one we support

	switch patchType {
	case types.JSONPatchType:
	case types.MergePatchType:
	case types.StrategicMergePatchType:
	default:
		//TODO: remove default
		patchType = types.StrategicMergePatchType
	}

	query := &model.Query{
		ClusterUUID: c.Param("cluster"),
		Type:        c.Param("type"),
		Namespace:   namespace,
		Name:        name,
		PatchType:   string(patchType),
		SubType:     subType,
	}
	logger.Debugf("getQuery: %+v", query)
	return query, nil
}

func SetKubernetesResultWithMixErr(c *gin.Context, result *rest.Result, err error) {
	if result != nil && result.Error() != nil {
		SetKubernetesResult(c, model.ParseResult(result))
		return
	}
	setErrorResponse(c, err)
	return
}

func SetKubernetesResult(c *gin.Context, result *model.Result) {
	logger := common.GetLogger(c)
	if result.Code == 0 {
		result.Code = 500
	}
	if result.Error != nil {
		logger.Infof("Request %s failed: %v", c.Request.URL, result.Error)
		c.JSON(result.Code, common.BuildKubernetesError(result.Error.Error()))
	} else {
		logger.Debugf("Request %s get status code: %d", c.Request.URL, result.Code)
		c.Data(result.Code, "application/json", result.Body)
	}
}

func SetKubernetesCreateResults(c *gin.Context, results []*model.Result) {
	logger := common.GetLogger(c)
	var resErrors []error
	var resBodys []string
	for _, r := range results {
		if r.Code == 0 {
			r.Code = 500
		}
		if r.Error != nil {
			logger.Errorf("Request failed for %v", r.Extra)
			resErrors = append(resErrors, common.BuildKubernetesError(
				fmt.Sprintf("create %s-%s-%s error: %s", r.Extra["resource"],
					r.Extra["namespace"], r.Extra["name"], r.Error.Error())))
		} else {
			resBodys = append(resBodys, string(r.Body))
		}
	}
	if len(resErrors) > 0 {
		c.JSON(http.StatusInternalServerError, common.BuildKrobelusErrors(resErrors))
	} else {
		c.Data(http.StatusCreated, "application/json",
			[]byte(fmt.Sprintf("[%s]", strings.Join(resBodys, ","))))
	}
}

// GetResource get a single resource or list a resource in a single namespace
func GetResource(c *gin.Context) {
	logger := common.GetLogger(c)
	query, err := getQuery(c)
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}
	switch {
	case query.Namespace != "" && query.Name == "":
		ListResource(c)
		return
	case query.SubType == "log" && query.Type == "pods":
		GetPodLogs(c)
		return
	}

	s := store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: query,
			Logger:     logger,
		},
	}

	infra.AddRegionForWatch(s.UniqueName.ClusterUUID)

	// Get from the cache first then from the cluster
	if err := s.LoadCacheObject(); err == nil {
		logger.Debugf("Get resource %s in %s from cache", query.Type, query.ClusterUUID)
		SetKubernetesResult(c, HandleRequestDone("GET", &s, nil))
		return
	}

	result, err := s.Request("GET", nil)
	if result == nil && err != nil {
		logger.Errorf("Get resource %s in %s failed: %s", query.Type, query.ClusterUUID, err.Error())
		setServerUnknownError(c, err)
		return
	}
	SetKubernetesResult(c, HandleRequestDone("GET", &s, result))
}

func BatchCreateResource(c *gin.Context) {
	logger := common.GetLogger(c)
	var objList []*model.KubernetesObject
	if err := c.Bind(&objList); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	model.SortKubernetesObjects(objList, model.ResourceListKindOrder, false)
	rs := store.GetKubernetesResourceStore(
		&model.Query{
			ClusterUUID: c.Param("cluster"),
		},
		logger, nil)
	var results []*model.Result
	for _, obj := range objList {
		resource, err := rs.ResourceTypeForGVK(obj.GroupVersionKind())
		if err != nil {
			setBadRequestResponse(c, err)
			return
		}
		s := store.GetKubernetesResourceStore(
			&model.Query{
				ClusterUUID: c.Param("cluster"),
				Name:        obj.Name,
				Namespace:   obj.Namespace,
				Type:        resource,
			}, logger, obj)
		res, httpErr := processResource(&s, common.HttpPost)
		if httpErr != nil {
			c.JSON(httpErr.StatusCode, httpErr.Cause)
			return
		}

		if err := s.UpdateResourceCache(res.Origin); err != nil {
			logger.WithError(err).Warn("update resource cache error")
		}
		res.Extra = map[string]interface{}{
			"resource":  resource,
			"name":      obj.Name,
			"namespace": obj.Namespace,
		}
		results = append(results, res)
	}
	SetKubernetesCreateResults(c, results)
}

// ProcessResource update a resource eg: create/update/delete...
func ProcessResource(c *gin.Context) {
	method := c.Request.Method
	logger := common.GetLogger(c)
	cacheNeedUpdate := false

	logger.Debugf("process resource url params: %s %s", method, c.Request.URL)
	query, err := getQuery(c)
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}

	s := store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: query,
			Logger:     logger,
		},
	}

	if method == common.HttpPost || method == common.HttpPut {
		var object model.KubernetesObject
		if err := c.Bind(&object); err != nil {
			setBadRequestResponse(c, err)
			return
		}
		s.Object = &object
		cacheNeedUpdate = true
	}

	if method == common.HttpPatch {
		raw, err := ioutil.ReadAll(c.Request.Body)
		if err != nil {
			setBadRequestResponse(c, err)
			return
		}
		s.SetRaw(raw)
		cacheNeedUpdate = true
	}

	res, httpErr := processResource(&s, method)
	if httpErr != nil {
		c.JSON(httpErr.StatusCode, httpErr.Cause)
	} else {
		if cacheNeedUpdate {
			if err := s.UpdateResourceCache(res.Origin); err != nil {
				logger.WithError(err).Warn("update resource cache error")
			}
		}
		SetKubernetesResult(c, res)
	}
}

func processResource(s *store.KubernetesResourceStore, method string) (*model.Result, *common.HttpResponseError) {
	err := HandleBeforeRequest(method, s)
	if err != nil {
		return nil, &common.HttpResponseError{
			Cause:      common.BuildInvalidArgsError(err.Error()),
			StatusCode: http.StatusBadRequest}
	}

	result, err := s.HandleRequest(method)

	if result == nil && err != nil {
		return nil, &common.HttpResponseError{
			Cause:      common.BuildServerUnknownError(err.Error()),
			StatusCode: http.StatusBadGateway}
	}
	res := HandleRequestDone(method, s, result)
	return res, nil
}

func ListResource(c *gin.Context) {
	//  /:cluster/:uuid/:type
	logger := common.GetLogger(c)
	query, err := getQuery(c)
	params := getParams(c, []string{"fieldSelector", "labelSelector"})

	if err != nil {
		logger.Errorf("List resource %s in %s failed: %s", query.Type, query.ClusterUUID, err.Error())
		setServerUnknownError(c, err)
		return
	}

	logger.Debugf("List resource %s on cluster %s.", query.Type, query.ClusterUUID)

	s := store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: query,
			Logger:     logger,
			Context:    c,
		},
	}

	infra.AddRegionForWatch(s.UniqueName.ClusterUUID)

	// Get from the cache first  then from the cluster
	if err := s.LoadCacheObjects(params); err == nil {
		logger.Debugf("List resource %s in %s from cache", query.Type, query.ClusterUUID)
		res := HandleRequestDone("GET", &s, nil)
		SetKubernetesResult(c, res)
		return
	} else {
		logger.Debugf("List resource from cache error: %v", err)
	}

	result, err := s.Request("GET", params)
	if result == nil && err != nil {
		logger.Errorf("List resource %s in %s failed: %s", query.Type, query.ClusterUUID, err.Error())
		setServerUnknownError(c, err)
		return
	}
	SetKubernetesResult(c, HandleRequestDone("GET", &s, result))
}

func HandleBeforeRequest(method string, s *store.KubernetesResourceStore) error {
	switch s.UniqueName.Type {
	case common.PersistentVolumeK8SResType:
		return HandleBeforePVRequest(method, s)
	case common.ClusterServiceBrokerType:
		return HandleBeforeCSBRequest(method, s)
	case common.ServiceBindingType:
		return HandleBeforeSBRequest(method, s)
	}
	return nil
}

func HandleRequestDone(method string, s *store.KubernetesResourceStore, result *rest.Result) *model.Result {
	res := model.ParseResult(result)

	// Get/List from cache
	if result == nil && s.Raw != nil && len(s.Raw) > 0 {
		res.Body = s.Raw
	}

	var err error
	var customBody []byte
	switch s.UniqueName.Type {
	case common.PersistentVolumeK8SResType:
		err = HandlePVRequestDone(method, s, result)
	case common.ClusterServiceBrokerType:
		customBody, err = HandleCSBRequestDone(method, s, result)
	case common.ClusterServiceClassType:
		customBody, err = HandleCSCRequestDone(method, s, result)
	case common.ServiceInstanceType:
		customBody, err = HandleSIRequestDone(method, s, result)
	case common.ServiceBindingType:
		customBody, err = HandleSBRequestDone(method, s, result)
	}

	if err != nil && method == common.HttpPost {
		// Delete created resource if error occurs.
		s.HandleRequest(common.HttpDelete)
	}

	// Delete cache object directly in cache watcher is not working
	// Also ignore 404 when delete
	if method == common.HttpDelete {
		if res.Code == 404 {
			s.Logger.Warnf("ignore resource 404 when delete: %s", s.String())
			res.Code = 204
			res.Error = nil
		}
		tempErr := s.DeleteCacheObject()
		if tempErr != nil {
			s.Logger.WithError(tempErr).Warn("HandleRequestDone error")
		}
	}

	if len(customBody) > 0 {
		res.Body = customBody
	}
	if err != nil {
		res.Error = err
	}
	return res

}
