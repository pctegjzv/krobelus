package handler

import (
	"fmt"
	"net/http"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
)

func UpdateConfigMap(c *gin.Context) {
	s := getStore(c, "Update")
	var update model.ConfigMapUpdate
	if err := c.Bind(&update); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	s.UpdateRequest = &update
	s.Logger.Debugf("Get update configmap request: %+v", s.UpdateRequest.Kubernetes)
	if err := validation.ValidateConfigMapUpdate(s.UpdateRequest); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	if err := s.LoadUpdateObject(); err != nil {
		setErrorResponse(c, err)
		return
	}
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	if _, err = client.UpdateV1ConfigMap(s.Object); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}

func CreateConfigMap(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Create ConfigMap")
	var configmap model.ConfigMapRequest
	if err := c.Bind(&configmap); err != nil {
		setBadRequestResponse(c, err)
		return
	} else {
		if err := validation.ValidateConfigMapCommon(&configmap); err != nil {
			setBadRequestResponse(c, err)
			return
		}
		logger.Debugf("Received create configmap request: %+v", configmap)
		transition.TransConfigMapRequest(&configmap)
	}

	client, err := infra.NewKubeClient(configmap.Cluster.UUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	cm, err := client.CreateV1ConfigMap(configmap.Kubernetes)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	response := model.ConfigMapToResponse(cm)
	response.Cluster = configmap.Cluster
	response.Namespace = configmap.Namespace
	c.JSON(http.StatusCreated, response)
}

func GetConfigMap(c *gin.Context) {
	s := getStore(c, "Get")
	if err := s.LoadConfigMap(); err != nil {
		setErrorResponse(c, err)
	} else {
		ds := db.ConfigMapStore{Response: s.Response, UUID: s.UUID}
		if err := ds.AppendRefsToResponse(); err != nil {
			setServerUnknownError(c, err)
			return
		}
		c.JSON(http.StatusOK, ds.Response)
	}
}

func ListConfigMaps(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "List ConfigMaps")
	request, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for configmap list api.")
		setBadRequestResponse(c, err)
		return
	}
	logger.Debugf("Received list configmap request: %+v", request)
	s := store.ConfigMapListStore{
		ResourceListStore: store.ResourceListStore{
			UUIDs:       request.UUIDS,
			ClusterUUID: request.ClusterUUID,
			Logger:      logger,
		},
	}

	if err = s.LoadConfigMaps(); err != nil {
		s.Logger.WithError(err).Error("list configmap error")
		setServerUnknownError(c, err)
	} else {
		if len(s.Items) > 0 {
			result, err := db.AppendRefsToConfigMaps(s.Items)
			if err != nil {
				s.Logger.WithError(err).Warnf("List configmap errors")
			}
			if result == nil {
				setServerUnknownError(c, err)
			} else {
				c.JSON(http.StatusOK, result)
			}
		} else {
			setEmptyListResponse(c)
		}
	}
}

func getStore(c *gin.Context, action string) *store.ConfigMapStore {
	return &store.ConfigMapStore{
		ResourceStore: store.ResourceStore{
			UUID:   getUUID(c),
			Logger: common.GetLoggerWithAction(c, fmt.Sprintf("%s Configmap", action)),
		},
	}
}

func DeleteConfigMap(c *gin.Context) {
	s := getStore(c, "Delete")
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	if err = client.DeleteV1ConfigMap(s.UniqueName.Namespace, s.UniqueName.Name); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}
