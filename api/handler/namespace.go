package handler

import (
	"net/http"

	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"krobelus/infra"

	"github.com/gin-gonic/gin"
	"k8s.io/api/core/v1"
)

func DeleteNamespace(c *gin.Context) {
	s := store.NamespaceStore{ResourceStore: getResourceStore(c, "Delete Namespace")}
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	s.Logger.Debugf("Namespace unique name is: %+v", s.UniqueName)
	if model.IsBuiltInNamespace(s.UniqueName.Name) {
		s.Logger.Infof("This is a builtin namespace %s, skip delete", s.UniqueName.Name)
		setEmptyResponse(c)
		return
	}

	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}

	if err = client.DeleteNamespace(s.UniqueName.Name); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}

func ListNamespaces(c *gin.Context) {
	logger := common.GetLogger(c)
	request, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for namespace list api.")
		setBadRequestResponse(c, err)
		return
	}
	logger.Debugf("Received list namespace request: %+v", request)

	s := &store.NamespaceListStore{
		ResourceListStore: store.ResourceListStore{
			UUIDs:       request.UUIDS,
			Logger:      logger,
			ClusterUUID: request.ClusterUUID,
		},
	}
	if err := s.LoadCacheObjects(); err != nil {
		setServerUnknownError(c, err)
		return
	}

	if len(s.Objects) == 0 {
		setEmptyListResponse(c)
	} else {
		c.JSON(http.StatusOK, s.Objects)
	}
}

// CreateNamespace create a namespace in region. If this is a default namespace,
// which should be already exist in cluster, just get and return. otherwise, create and return.
// TODO:remove
// 1. use cache to response
func CreateNamespace(c *gin.Context) {
	var namespace model.NamespaceRequest

	if err := c.Bind(&namespace); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	logger := common.GetLoggerWithAction(c, "Create Namespace")
	logger.Debugf("Create namespace: %+v", namespace)
	client, err := infra.NewKubeClient(namespace.Cluster.UUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	var ns *v1.Namespace
	if !model.IsBuiltInNamespace(namespace.Resource.Name) {
		ns, err = client.CreateNamespaceAndSyncSecrets(&namespace)
	} else {
		ns, err = client.GetNamespace(namespace.Resource.Name)
	}
	if err != nil {
		setK8SErrorResponse(c, err)
	} else {
		c.JSON(http.StatusCreated, *ns)
	}
}
