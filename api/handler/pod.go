package handler

import (
	"net/http"

	"krobelus/common"
	"krobelus/db"
	"krobelus/kubernetes"
	"krobelus/model"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"k8s.io/api/core/v1"
)

func ListPods(c *gin.Context) {
	logger := common.GetLogger(c)
	data, err := getRequestData(c)
	if err != nil {
		logger.WithError(err).Error("Parse request body error for pods list api.")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	uuids := data.UUIDS
	if len(uuids) <= 0 {
		c.JSON(http.StatusOK, []string{})
		return
	}
	pageSize, err := cast.ToIntE(c.DefaultQuery("page_size", common.DefaultPageSize))
	if err != nil {
		logger.WithError(err).Error("Invalid page_size query params!")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	page, err := cast.ToIntE(c.DefaultQuery("page", common.DefaultPage))
	if err != nil {
		logger.WithError(err).Error("Invalid page query params!")
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	svc, err := db.GetService(uuids[0], true)
	if err != nil {
		logger.WithError(err).Error("get svc from db error")
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	pods, _ := store.GetServicesPods(&model.ServiceListStore{
		UUIDs:     uuids,
		Namespace: "",
		Services:  []*model.ServiceResponse{svc},
		Logger:    logger,
	})
	logger.Debugf("Trying to retrieve pods for service uuids: %+v", uuids)
	for _, pod := range pods {
		logger.Debugf("Get pod: %s/%s", pod.Namespace, pod.Name)
		kubernetes.SetPodStatus(pod)
	}

	result := getPagedResult(pods, pageSize, page)

	c.JSON(http.StatusOK, result)
}

func getPagedResult(pods []*v1.Pod, pageSize int, page int) gin.H {
	count := len(pods)
	start := pageSize * (page - 1)
	end := pageSize * page
	result := pods
	if count > start {
		if end > count {
			end = count
		}
		result = pods[start:end]
	} else {
		result = result[0:0]
	}
	return gin.H{
		"num_pages": page,
		"page_size": pageSize,
		"count":     count,
		"results":   result,
	}
}
