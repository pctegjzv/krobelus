package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/common"
	"krobelus/infra"
	"krobelus/model"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"
	"krobelus/store"

	"github.com/gin-gonic/gin"
	"k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
)

func CreateBrokers(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "Create ClusterServiceBrokers")

	logger.Info("Start")

	var broker model.BrokerRequest
	var err error
	var client *infra.KubeClient
	if err := c.Bind(&broker); err != nil {
		logger.WithError(err).Error("Parse Request Body Error")
		setBadRequestResponse(c, err)
		return
	} else {
		if err := validation.ValidateClusterServiceBrokerCommon(&broker); err != nil {
			setBadRequestResponse(c, err)
			return
		}
		logger.Debugf("Received create broker request: %+v", broker)
		client, err = infra.NewKubeClient(broker.Cluster.UUID)
		if err != nil {
			setRegionErrorResponse(c, err)
			return
		}
		err = transition.TransClusterServiceBrokerRequest(&broker, client)
		if err != nil {
			setBadRequestResponse(c, err)
			return
		}
	}

	cm, err := client.CreateClusterServiceBroker(broker.Kubernetes)
	if err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	// wait for create ready
	var shouldDeleteBroker bool
	var s *store.BrokerStore
	var reb *scV1beta1.ClusterServiceBroker
	brokerName := broker.Resource.Name
	for retryCount := 0; retryCount <= scResultRetryTimes; retryCount++ {
		if retryCount == scResultRetryTimes {
			shouldDeleteBroker = true
			setBrokerTimeoutResponse(c, "Broker Url Timeout")
		}
		scRetrySleep()
		s = getBrokerStoreByUid(string(cm.UID))
		if err := s.LoadBroker(); err == nil {
			reb, err = s.Object.ToBroker()
			if err != nil {
				continue
			}
		} else {
			reb, err = client.GetClusterServiceBroker(brokerName)
			s.Response = model.BrokerToResponse(reb)
		}
		if len(reb.Status.Conditions) == 0 {
			continue
		}
		if reb.Status.Conditions[0].Status == scV1beta1.ConditionTrue {
			break
		}
		if reb.Status.Conditions[0].Status == scV1beta1.ConditionFalse {
			if reb.Status.Conditions[0].Reason == errorSyncingCatalogReason {
				if strings.Contains(reb.Status.Conditions[0].Message, "already exists for Broker") {
					setServiceCatalogServerErrResponse(c, common.BrokerRepeatError, reb.Status.Conditions[0].Message)
				} else {
					setServiceCatalogServerErrResponse(c, errorFetchingCatalogReason, reb.Status.Conditions[0].Message)
				}
			}
			if reb.Status.Conditions[0].Reason == errorAuthCredentialsReason {
				setServiceCatalogClientErrResponse(c, common.BrokerAuthError, reb.Status.Conditions[0].Message)
			}
			if reb.Status.Conditions[0].Reason == errorFetchingCatalogReason {
				setServiceCatalogServerErrResponse(c, common.BrokerConnectionError, reb.Status.Conditions[0].Message)
			}
			shouldDeleteBroker = true
			break
		}
	}

	if shouldDeleteBroker {
		err = client.DeleteClusterServiceBroker(brokerName)
		if reb.Spec.AuthInfo != nil {
			err = client.DeleteV1Secret(common.NamespaceDefault, reb.Spec.AuthInfo.Basic.SecretRef.Name)
			if err != nil {
				logger.Errorf("Delete secrete failed: %+v", err)
			}
		}
		return
	}

	response := s.Response
	response.Cluster = broker.Cluster
	c.JSON(http.StatusCreated, response)
}

func GetBroker(c *gin.Context) {
	s := getBrokerStore(c, "Get")
	if err := s.LoadBroker(); err != nil {
		setErrorResponse(c, err)
	} else {
		client, err := infra.NewKubeClient(s.Response.Cluster.UUID)
		if err != nil {
			setErrorResponse(c, err)
		}
		err = transition.TransClusterServiceBrokerGet(s.Response, client)
		c.JSON(http.StatusOK, s.Response)
	}
}

func ListBrokers(c *gin.Context) {
	logger := common.GetLoggerWithAction(c, "List Brokers")
	request, err := getRequestData(c)
	if len(request.UUIDS) == 0 {
		setEmptyResponse(c)
		return
	}
	if err != nil {
		logger.WithError(err).Error("Parse request body error for Broker list api.")
		setBadRequestResponse(c, err)
		return
	}
	logger.Debugf("Received list Broker request: %+v", request)
	s := store.BrokerListStore{
		ResourceListStore: store.ResourceListStore{
			UUIDs:       request.UUIDS,
			ClusterUUID: request.ClusterUUID,
			Logger:      logger,
		},
	}
	if err = s.LoadBrokers(); err != nil {
		s.Logger.WithError(err).Error("list Broker error")
		setServerUnknownError(c, err)
	} else {
		list, err := transition.TranslateBrokerListResponse(&s)
		if err != nil {
			setServerUnknownError(c, err)
		}
		if len(list) > 0 {
			c.JSON(http.StatusOK, list)
		} else {
			setEmptyListResponse(c)
		}
	}
}

func DeleteBroker(c *gin.Context) {
	s := getBrokerStore(c, "Delete")
	if err := s.LoadBroker(); err != nil {
		setErrorResponse(c, err)
		return
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	brokerName := s.UniqueName.Name
	if brokerName == "" {
		brokerName = s.Response.Resource.Name
	}
	if err = client.DeleteClusterServiceBroker(brokerName); err != nil {
		setK8SErrorResponse(c, err)
		return
	}

	reb, err := s.Object.ToBroker()
	if err != nil {
		setErrorResponse(c, err)
		return
	}
	if reb.Spec.AuthInfo != nil {
		err = client.DeleteV1Secret(common.NamespaceDefault, reb.Spec.AuthInfo.Basic.SecretRef.Name)
		if err != nil {
			s.Logger.Errorf("Delete secrete failed: %+v", err)
		}
	}
	setEmptyResponse(c)
}

func UpdateBroker(c *gin.Context) {
	s := getBrokerStore(c, "Update")
	var update model.BrokerUpdate
	if err := c.Bind(&update); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	s.UpdateRequest = &update
	s.Logger.Debugf("Get update Broker request: %+v", s.UpdateRequest.Kubernetes)
	if err := validation.ValidateClusterServiceBrokerUpdate(s.UpdateRequest); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	if err := s.LoadUpdateObject(); err != nil {
		setErrorResponse(c, err)
		return
	}
	if err := s.LoadUniqueName(); err != nil {
		setErrorResponse(c, err)
		return
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	err = transition.TransClusterServiceBrokerUpdate(s.UpdateRequest, s, client)
	if err != nil {
		setRegionErrorResponse(c, err)
		return
	}
	if err := s.PatchUpdateObject(); err != nil {
		setErrorResponse(c, err)
		return
	}
	if _, err = client.UpdateClusterServiceBroker(s.Object); err != nil {
		setK8SErrorResponse(c, err)
		return
	}
	setEmptyResponse(c)
}

func getBrokerStore(c *gin.Context, action string) *store.BrokerStore {
	return &store.BrokerStore{
		ResourceStore: store.ResourceStore{
			UUID:   getUUID(c),
			Logger: common.GetLoggerWithAction(c, fmt.Sprintf("%s Broker", action)),
		},
	}
}

func getBrokerStoreByUid(uid string) *store.BrokerStore {
	return &store.BrokerStore{
		ResourceStore: store.ResourceStore{
			UUID: uid,
		},
	}
}

// HandleBeforeCSBRequest does some works before executing CSB request.
func HandleBeforeCSBRequest(method string, s *store.KubernetesResourceStore) error {
	var err error
	switch method {
	case common.HttpPost:
		err = handleBeforeCSBCreate(s)
	case common.HttpPut:
		err = handleBeforeCSBUpdate(s)
	case common.HttpDelete:
		err = handleBeforeCSBDelete(s)
	}
	return err
}

func HandleCSBRequestDone(method string, s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	var err error
	var body []byte
	switch method {
	case common.HttpDelete:
		err = handleCSBDeleteDone(s)
	case common.HttpGet:
		if s.UniqueName.Name == "" {
			body, err = handleCSBListDone(s, result)
		}
	}
	return body, err
}

func handleCSBListDone(s *store.KubernetesResourceStore, result *rest.Result) ([]byte, error) {
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return nil, err
	}
	brokerList := &scV1beta1.ClusterServiceBrokerList{}
	brokerList.Items = make([]scV1beta1.ClusterServiceBroker, 0)

	var body []byte
	if result != nil {
		result.Into(brokerList)
		for index, item := range brokerList.Items {
			classes, err := client.GetClusterServiceClasses(&metaV1.ListOptions{
				FieldSelector: fmt.Sprintf("spec.clusterServiceBrokerName=%s", item.ObjectMeta.Name),
			})
			if err != nil {
				return nil, err
			}
			anno := item.GetAnnotations()
			if anno == nil {
				anno = make(map[string]string)
			}
			anno[common.SCBClassesNumKey()] = fmt.Sprintf("%d", len(classes))
			brokerList.Items[index].SetAnnotations(anno)
		}
	} else if s.Objects != nil && len(s.Objects.Items) > 0 {
		for _, item := range s.Objects.Items {
			broker, err := item.ToBroker()
			if err != nil {
				return nil, err
			}
			classes, err := client.GetClusterServiceClasses(&metaV1.ListOptions{
				FieldSelector: fmt.Sprintf("spec.clusterServiceBrokerName=%s", broker.ObjectMeta.Name),
			})

			if err != nil {
				return nil, err
			}
			anno := item.GetAnnotations()
			if anno == nil {
				anno = make(map[string]string)
			}
			anno[common.SCBClassesNumKey()] = fmt.Sprintf("%d", len(classes))
			broker.SetAnnotations(anno)
			brokerList.Items = append(brokerList.Items, *broker)
		}
	}

	body, err = json.Marshal(brokerList)
	if err != nil {
		return nil, err
	}
	return body, err
}

func handleCSBDeleteDone(s *store.KubernetesResourceStore) error {
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return err
	}
	broker, _ := s.Object.ToBroker()
	if broker.Spec.AuthInfo != nil {
		err = client.DeleteV1Secret(common.NamespaceDefault, broker.Spec.AuthInfo.Basic.SecretRef.Name)
		if err != nil {
			return err
		}
	}
	return err
}

func handleBeforeCSBDelete(s *store.KubernetesResourceStore) error {
	if err := s.LoadCacheObject(); err == nil {
		return nil
	}
	if err := s.LoadClient(); err != nil {
		return err
	}
	result := s.Client.Get().Resource(s.UniqueName.Type).Name(s.UniqueName.Name).Do()
	body, _ := result.Raw()
	var res model.KubernetesResource
	json.Unmarshal(body, &res)
	s.Object = res.ToObject()
	s.Logger.Debugf("s.Object is%v+", s.Object)
	return nil
}

func handleBeforeCSBCreate(s *store.KubernetesResourceStore) error {
	authInfo := s.Object.Spec["authInfo"]
	if authInfo == nil {
		return nil
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return err
	}
	authInfoMap := transition.UnknownInterfaceToMap(authInfo)
	if authInfoMap["basicAuthUsernameKey"] != "" && authInfoMap["basicAuthPasswordKey"] != "" {
		client.CreateV1Secret(&v1.Secret{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: "v1",
				Kind:       "Secret",
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name: s.UniqueName.Name + s.UniqueName.Type,
			},
			Type: "Opaque",
			Data: map[string][]byte{
				"username": []byte(authInfoMap["basicAuthUsernameKey"]),
				"password": []byte(authInfoMap["basicAuthPasswordKey"]),
			},
		}, common.NamespaceDefault)
		s.Object.Spec["authInfo"] = scV1beta1.ServiceBrokerAuthInfo{
			Basic: &scV1beta1.BasicAuthConfig{
				SecretRef: &scV1beta1.ObjectReference{
					Namespace: common.NamespaceDefault,
					Name:      s.UniqueName.Name + s.UniqueName.Type,
				},
			},
		}
	} else {
		s.Object.Spec["authInfo"] = nil
	}

	return nil
}

func handleBeforeCSBUpdate(s *store.KubernetesResourceStore) error {
	authInfo := s.Object.Spec["authInfo"]
	if authInfo == nil {
		return nil
	}
	client, err := infra.NewKubeClient(s.UniqueName.ClusterUUID)
	if err != nil {
		return err
	}
	authInfoMap := transition.UnknownInterfaceToMap(authInfo)
	if authInfoMap["basicAuthUsernameKey"] != "" && authInfoMap["basicAuthPasswordKey"] != "" {
		secret := &v1.Secret{
			TypeMeta: metaV1.TypeMeta{
				APIVersion: "v1",
				Kind:       "Secret",
			},
			ObjectMeta: metaV1.ObjectMeta{
				Name: s.UniqueName.Name + s.UniqueName.Type,
			},
			Type: "Opaque",
			Data: map[string][]byte{
				"username": []byte(authInfoMap["basicAuthUsernameKey"]),
				"password": []byte(authInfoMap["basicAuthPasswordKey"]),
			},
		}
		_, err := client.UpdateV1Secret(secret, common.NamespaceDefault)
		if err != nil {
			if k8sErrors.IsNotFound(err) {
				client.CreateV1Secret(secret, common.NamespaceDefault)
			} else {
				return err
			}
		}
		s.Object.Spec["authInfo"] = scV1beta1.ServiceBrokerAuthInfo{
			Basic: &scV1beta1.BasicAuthConfig{
				SecretRef: &scV1beta1.ObjectReference{
					Namespace: common.NamespaceDefault,
					Name:      s.UniqueName.Name + s.UniqueName.Type,
				},
			},
		}
	} else {
		s.Object.Spec["authInfo"] = nil
	}
	return nil
}
