// +build !unit

package handler

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"krobelus/common"
	"krobelus/model"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
	"github.com/stretchr/testify/assert"
)

var (
	router *gin.Engine
)

func init() {
	router = gin.Default()
	AddRoutes(router)
}

func parseResponse(body []byte) (*model.ServiceResponse, error) {
	res := &model.ServiceResponse{}
	if err := json.Unmarshal(body, res); err != nil {
		return res, errors.Annotate(err, "Json unmarshal failed")
	}
	return res, nil
}

func Request(method, url string, body []byte) (*httptest.ResponseRecorder, error) {
	writer := httptest.NewRecorder()
	var err error
	var request *http.Request
	if body != nil {
		request, err = http.NewRequest(method, url, bytes.NewBuffer(body))
	} else {
		request, err = http.NewRequest(method, url, nil)
	}
	if err != nil {
		return writer, errors.Annotate(err, "New request failed")
	}
	request.Header["Content-Type"] = []string{"application/json", "charset=utf-8"}
	router.ServeHTTP(writer, request)
	return writer, nil
}

func testGetService(t *testing.T, uuid string) (*model.ServiceResponse, error) {
	recorder, err := Request(common.HttpGet, "/v3/services/"+uuid, nil)
	if err != nil {
		return nil, errors.Annotate(err, "Request failed")
	}
	assert.Equal(t, 200, recorder.Code)
	return parseResponse(recorder.Body.Bytes())
}

func testCreateService(t *testing.T) (*model.ServiceResponse, error) {
	text, err := ioutil.ReadFile("/go/src/krobelus/run/examples/create-service.json")
	if err != nil {
		return nil, errors.Annotate(err, "Read file failed")
	}
	recorder, err := Request(common.HttpPost, "/v3/services", text)
	if err != nil {
		return nil, errors.Annotate(err, "Request failed")
	}
	assert.Equal(t, 201, recorder.Code)
	return parseResponse(recorder.Body.Bytes())
}

func testDeleteService(t *testing.T, uuid string) (*model.ServiceResponse, error) {
	recorder, err := Request(common.HttpDelete, "/v3/services/"+uuid, nil)
	if err != nil {
		return nil, errors.Annotate(err, "Request failed")
	}
	assert.Equal(t, 204, recorder.Code)
	return nil, nil
}

func TestService(t *testing.T) {
	response, err := testCreateService(t)
	assert.Equal(t, err, nil)
	fmt.Println("Create service done")
	//result, err := json.MarshalIndent(response, "", "  ")
	//fmt.Println(string(result))

	response, err = testGetService(t, response.Resource.UUID)
	assert.Equal(t, err, nil)
	fmt.Println("Get service done")
	//result, err = json.MarshalIndent(response, "", "  ")
	//fmt.Println(string(result))

	time.Sleep(time.Second * 3)
	response, err = testDeleteService(t, response.Resource.UUID)
	assert.Equal(t, err, nil)
	fmt.Println("Delete service done")
}
