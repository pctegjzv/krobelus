package handler

import (
	"fmt"
	"net/http"
	"sort"
	"strings"

	"krobelus/api/transition"
	"krobelus/api/validation"
	"krobelus/common"
	"krobelus/db"
	"krobelus/infra"
	"krobelus/kubernetes"
	"krobelus/model"
	cacheStore "krobelus/store"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
)

func validateAndTrans(store *db.ServiceStore) error {
	if err := validation.ValidateServiceCreate(store); err != nil {
		store.Logger.Errorf("Validate service body error: %s", err.Error())
		return err
	}
	if err := transition.TranslateServiceCreatePayload(store); err != nil {
		store.Logger.Errorf("Translate service payload error: %s", err.Error())
		return err
	}
	return nil
}

func CreateService(c *gin.Context) {
	var service model.ServiceRequest
	parseResources := c.Query("parse_resources") == "true"
	if err := c.Bind(&service); err != nil {
		setBadRequestResponse(c, err)
		return
	}

	store := &db.ServiceStore{
		Request: &service,
		Logger:  common.GetLoggerWithAction(c, "Create Service"),
	}

	store.Logger.Infof("Create service request parsed: %+v", service)
	if err := validateAndTrans(store); err != nil {
		setBadRequestResponse(c, err)
		return
	}
	if parseResources {
		result := store.ParseResourceItems()
		if result == nil || len(result) == 0 {
			setEmptyListResponse(c)
		} else {
			c.JSON(http.StatusOK, result)
		}
		return
	}

	err := store.Create(nil)
	if err != nil {
		store.Logger.Errorf("Service create in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	if err = sendServiceTask(store); err != nil {
		store.Logger.Errorf("Send service create message error: %s", err.Error())
	}

	defer func() {
		// recover from panic if one occurred. Set err to nil otherwise.
		if recover() != nil {
			err = errors.New("Send service create message panic")
		}
	}()

	c.JSON(http.StatusCreated, service)
}

// ListServices list the services in db and trigger the watcher in this region. if not
func ListServices(c *gin.Context) {
	filters := map[string]string{"host_ip": c.Query("host_ip")}

	pvcUUID := c.Query("pvc_uuid")

	logger := common.GetLogger(c)

	list, err := getRequestData(c)
	if err != nil {
		setBadRequestResponse(c, err)
		return
	}

	logger.Infof("List services %v.", list.UUIDS)

	var items []*model.ServiceResponse

	if "" != pvcUUID {
		items, err = db.ListServicesByPVC(list.UUIDS, pvcUUID)
	} else {
		items, err = db.ListServices(list.UUIDS)
	}

	if err != nil {
		logger.Errorf("Service retrieve in db error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}

	store := model.ServiceListStore{
		Services: items,
		Logger:   logger,
		UUIDs:    list.UUIDS,
	}
	err = transition.TranslateServiceListResponse(&store, true, filters)
	if err != nil {
		logger.Errorf("Translate service error: %s", err.Error())
		c.JSON(http.StatusInternalServerError, common.BuildDBUnknownError(err.Error()))
		return
	}
	if len(store.Services) > 0 {
		logger.Debug("Add region for watch when list services")
		infra.AddRegionForWatch(store.Services[0].Cluster.UUID)
		c.JSON(http.StatusOK, store.Services)
	} else {
		setEmptyListResponse(c)
	}
}

// GetService Get service from database and retrieve instances count and status from cache.
// Also it will check reference with other resources.
func GetService(c *gin.Context) {
	store := &db.ServiceStore{
		UUID:   c.Param("uuid"),
		Logger: common.GetLogger(c),
	}

	basic := strings.ToLower(c.Query("basic")) == "true"
	if err := store.GetService(basic); err != nil {
		store.Logger.Errorf("Service retrieve in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	onlyYaml := strings.ToLower(c.Query("only_yaml")) == "true"
	if onlyYaml {
		c.JSON(http.StatusOK, model.KubernetesYaml{Kubernetes: store.Response.Kubernetes, KubernetesBackup: store.Response.KubernetesBackup})
		return
	}

	transition.TranslateServiceDetailResponse(store, true, true, nil)
	store.Logger.Debug("Add region for watch when get service detail")
	infra.AddRegionForWatch(store.Response.Cluster.UUID)
	c.JSON(http.StatusOK, store.Response)
}

func ServiceScale(c *gin.Context) {
	store, err := getServiceStore(c)
	if err != nil {
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	action := common.ScaleUp
	if strings.Contains(c.Request.URL.Path, "scaledown") {
		action = common.ScaleDown
	}
	if err := store.UpdateResourceForScale(action); err != nil {
		store.Logger.Errorf("Update resource for scale error: %s", err.Error())
		setEmptyResponse(c)
	}
	podController := store.FindPodController()
	s := &cacheStore.KubernetesResourceStore{
		ResourceStore: cacheStore.ResourceStore{
			UniqueName: &model.Query{
				ClusterUUID: store.Response.Cluster.UUID,
				Namespace:   store.Response.Namespace.Name,
				Type:        common.GetKubernetesTypeFromKind(podController.Kind),
				Name:        podController.Name,
			},
			Object: podController.ToObject(),
			Logger: store.Logger,
		},
	}
	result, err := s.Request(common.HttpPut, nil)
	if err != nil {
		store.Logger.Error("Do request error: %s", err.Error())
		setEmptyResponse(c)
		return
	}
	if result.Error() != nil {
		setEmptyResponse(c)
		return
	}
	if err := store.UpdateKubernetesFromResponse(); err != nil {
		setDbErrorResponse(c, err, model.AlaudaServiceType)
	} else {
		setEmptyResponse(c)
	}

}

// GetServiceYaml generate yaml for all kubernetes resources in this service.
func GetServiceYaml(c *gin.Context) {
	store := &db.ServiceStore{
		UUID:   c.Param("uuid"),
		Logger: common.GetLogger(c),
	}

	if err := store.GetService(true); err != nil {
		store.Logger.Errorf("Service retrieve in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	bytes, err := transition.KubernetesJsonToYamlBytes(store.Response.Kubernetes)
	if err != nil {
		store.Logger.Errorf("Translate kubernetes json to yaml error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	c.Writer.Header()["Content-Type"] = transition.YamlContentType
	c.Writer.Write(bytes)
	c.Status(http.StatusOK)
}

// GetServiceRevisions fetch revisions for deployment/daemonset from kubernetes cluster, statefulset does not support rollback now.
func GetServiceRevisions(c *gin.Context) {
	store, err := getServiceStore(c)
	if err != nil {
		store.Logger.Errorf("Get service store error: %v", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.Logger.Infof("List versions %v", store.UUID)
	var versions []*model.ServiceRevision
	switch store.Response.Resource.Kind {
	case common.KubernetesKindDeployment:
		versions, err = kubernetes.GetDeploymentRevisions(store.Response)
	case common.KubernetesKindDaemonSet:
		versions, err = kubernetes.GetDaemonsetRevisions(store.Response)
	default:
		setBadRequestResponse(c, fmt.Errorf("%v does not support rollback now", store.Response.Resource.Kind))
		return
	}
	if err != nil {
		setK8SErrorResponse(c, err)
	} else {
		sort.Slice(versions, func(i, j int) bool { return versions[i].Revision < versions[j].Revision })
		c.JSON(http.StatusOK, versions)
	}
}

// Rollback service
func RollbackService(c *gin.Context) {
	store, err := getServiceStore(c)
	if err != nil {
		store.Logger.Errorf("Get service store error: %v", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.Logger.Infof("Rollback service %v", store.UUID)

	store.Request.RollbackTo = c.Query("rollback_to")
	if store.Request.RollbackTo == "" {
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError("Revision for rollback should not be empty"))
		return
	}
	store.Logger.Infof("Rollback service %v", store.UUID)

	store.Logger.Infof("Validate service %v", store.UUID)
	err = validation.ValidateServiceRollback(store)
	if err != nil {
		store.Logger.Errorf("Validate service failed %v", err.Error())
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	store.Logger.Infof("Translate service %v", store.UUID)
	transition.TranslateServiceRollbackPayload(store)

	store.Logger.Infof("Save service %v.", store.UUID)
	if err = db.RollbackService(store); err != nil {
		store.Logger.Errorf("Service retrieve in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	store.Logger.Infof("Send rollback message %v", store.UUID)
	if err = sendServiceTask(store); err != nil {
		store.Logger.Errorf("Send rollback message error: %s", err.Error())
	}
	c.JSON(http.StatusNoContent, nil)
}

// UpdateService handler.
func UpdateService(c *gin.Context) {
	var body model.CommonUpdateRequest

	store, err := getServiceStore(c)
	if err != nil {
		store.Logger.Errorf("Get service store error: %v", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	if err := c.Bind(&body); err != nil {
		store.Logger.Errorf("Bind update service body error: %v", err.Error())
		setBadRequestResponse(c, err)
		return
	}

	store.Logger.Infof("Update service %v with body %+v", store.UUID, body)

	store.Request.Kubernetes = body.Kubernetes
	store.UpdateRequest = &body
	transition.TranslateServiceUpdatePayloadPre(store)

	store.Logger.Infof("Validate service %v", store.UUID)
	err = validation.ValidateServiceUpdate(store)
	if err != nil {
		store.Logger.Errorf("Validate service failed: %v", err.Error())
		setBadRequestResponse(c, err)
		return
	}

	store.Logger.Infof("Translate service %v.", store.UUID)
	if err := transition.TranslateServiceUpdatePayload(store); err != nil {
		store.Logger.Errorf("Translate service error: %v", err.Error())
		setBadRequestResponse(c, err)
		return
	}

	if IsParseResourcesRequest(c) {
		result := store.ParseResourceItems()
		if result == nil || len(result) == 0 {
			setEmptyListResponse(c)
		} else {
			c.JSON(http.StatusOK, result)
		}
		return
	}

	store.Logger.Infof("Save service %v", store.UUID)
	err = store.Update(nil)
	if err != nil {
		store.Logger.Errorf("Save service in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	store.Logger.Infof("Send update message %v.", store.UUID)
	if err = sendServiceTask(store); err != nil {
		store.Logger.Errorf("Send update message error: %s", err.Error())
	}
	c.JSON(http.StatusOK, store.Response)
}

// DeleteService handle.
func DeleteService(c *gin.Context) {
	store, err := getServiceStore(c)
	if err != nil {
		store.Logger.Errorf("Get service store error: %v", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	force := strings.ToLower(c.Query("force")) == "true"
	store.Logger.Infof("Delete service %v", store.GetServiceUUID())
	transition.TranslateServiceDeletePayload(store, force)

	store.Logger.Infof("Validate service %v", store.UUID)
	if err := validation.ValidateServiceDelete(store); err != nil {
		store.Logger.Errorf("Validate service failed: %v", err.Error())
		setBadRequestResponse(c, err)
		return
	}

	if err = store.Delete(nil); err != nil {
		store.Logger.Errorf("Service delete in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	if err = sendServiceTask(store); err != nil {
		store.Logger.Errorf("Send service delete message error: %s", err.Error())
	}

	if force {
		// Set status code to 404, let jakiro delete it in resource table
		c.JSON(http.StatusNotFound, common.BuildDBNotFoundError(fmt.Sprintf("%s not exists", model.AlaudaServiceType)))
	} else {
		c.JSON(http.StatusNoContent, nil)
	}
}

// StartService handler.
func StartService(c *gin.Context) {
	store, err := getServiceStore(c)
	if err != nil {
		store.Logger.Errorf("Get service store error: %v", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.Logger.Infof("Start service %v", store.UUID)

	store.Logger.Infof("Validate service %v", store.UUID)
	err = validation.ValidateServiceStart(store)
	if err != nil {
		store.Logger.Errorf("Validate service failed %v", err.Error())
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	store.Logger.Infof("Translate service %v", store.UUID)
	transition.TranslateServiceStartPayload(store)

	store.Logger.Infof("Save service %v.", store.UUID)
	err = db.StartService(store)
	if err != nil {
		store.Logger.Errorf("Service retrieve in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	store.Logger.Infof("Send start message %v", store.UUID)
	if err = sendServiceTask(store); err != nil {
		store.Logger.Errorf("Send start message error: %s", err.Error())
	}
	c.JSON(http.StatusOK, store.Response)
}

// StopService handler.
func StopService(c *gin.Context) {
	store, err := getServiceStore(c)
	if err != nil {
		store.Logger.Errorf("Get service store error: %v", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.Logger.Infof("Stop service %v", store.UUID)

	store.Logger.Infof("Validate service %v", store.UUID)
	err = validation.ValidateServiceStop(store)
	if err != nil {
		store.Logger.Errorf("Validate service failed %v", err.Error())
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}

	store.Logger.Infof("Translate service %v", store.UUID)
	transition.TranslateServiceStopPayload(store)

	store.Logger.Infof("Save service %v", store.UUID)
	err = db.StopService(store)
	if err != nil {
		store.Logger.Errorf("Service retrieve in db error: %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	store.Logger.Infof("Send stop message %v", store.UUID)
	if err = sendServiceTask(store); err != nil {
		store.Logger.Errorf("Send stop message error: %s", err.Error())
	}
	c.JSON(http.StatusOK, store.Response)
}

// RetryService handler.
func RetryService(c *gin.Context) {
	store, err := getServiceStore(c)
	if err != nil {
		store.Logger.Errorf("Get service store error: %v", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}
	store.Logger.Infof("Retry service %v", store.UUID)

	if err = validation.ValidateServiceRetry(store); err != nil {
		store.Logger.Errorf("Validate service failed  %v", err.Error())
		c.JSON(http.StatusBadRequest, common.BuildInvalidArgsError(err.Error()))
		return
	}
	if status, err := db.GetStatus(store.UUID); err == nil {
		store.Request.RollbackTo = status.Input["rollbackTo"]
	}

	store.Logger.Infof("Translate service %v", store.UUID)
	transition.TranslateServiceRetryPayload(store)

	store.Logger.Infof("Save service %v", store.UUID)
	err = store.Retry()
	if err != nil {
		store.Logger.Errorf("Service retrieve in db error | %s", err.Error())
		setDbErrorResponse(c, err, model.AlaudaServiceType)
		return
	}

	store.Logger.Infof("Send retry message %v", store.UUID)
	if err = sendServiceTask(store); err != nil {
		store.Logger.Errorf("Send retry message error | %s", err.Error())
	}
	c.JSON(http.StatusOK, store.Response)
}

func ListServiceInstances(c *gin.Context) {
	service, err := db.GetService(c.Param("uuid"), true)
	if err != nil {
		setErrorResponse(c, err)
		return
	}

	controller := model.GetKubernetesPodController(service.Kubernetes)

	objects, err := cacheStore.GetServiceInstances(&db.ServiceStore{Response: service, Logger: common.GetLogger(c)})
	if err != nil {
		setK8SErrorResponse(c, err)
	} else {
		c.JSON(http.StatusOK, kubernetes.SetVersionForServicePods(objects, controller))
	}
}

// getServiceStore init a ServiceStore with logger, uuid and response.
func getServiceStore(c *gin.Context) (store *db.ServiceStore, err error) {
	store = &db.ServiceStore{}
	store.UUID = getUUID(c)
	store.Logger = common.GetLogger(c)
	store.Request = &model.ServiceRequest{Resource: model.ServiceResource{UUID: store.UUID}}
	store.Response, err = db.GetService(store.UUID, true)
	return store, err
}
