package kubernetes

import (
	"krobelus/db"
	"krobelus/model"
	kubeStore "krobelus/store"

	"github.com/sirupsen/logrus"
	"k8s.io/api/core/v1"
)

func ParseConfigMapRefFromKubernetesResource(resource *model.KubernetesResource, store *db.ServiceStore) (*db.ConfigMapRef, error) {
	spec, err := resource.GetPodSpec()
	if err != nil {
		return nil, err
	}
	return parseConfigMapRefFromPodSpec(spec, store)
}

// parseConfigMapRefFromPodSpec will parse configmap refs from env and volume from a PodSpec. It can be from a
// deployment,statefulset,daemonset,pod....
// Note: For now, we ignore the optional field of configmap ref.
func parseConfigMapRefFromPodSpec(spec *v1.PodSpec, svcStore *db.ServiceStore) (*db.ConfigMapRef, error) {

	request := svcStore.Request
	var configMapRef db.ConfigMapRef

	// volumes
	for _, v := range spec.Volumes {
		if v.ConfigMap != nil {
			ref := v.ConfigMap
			configmap, err := getConfigMap(svcStore, request.Cluster.UUID, request.Namespace.Name, ref.Name)
			if err != nil {
				return nil, err
			} else {
				if len(ref.Items) == 0 {
					configMapRef.AddConfigMapRefRow(request, configmap, "", "volume", v.Name)
				} else {
					for _, item := range ref.Items {
						configMapRef.AddConfigMapRefRow(request, configmap, item.Key, "volume", v.Name)
					}
				}
			}
		}
	}
	// containers
	for _, c := range spec.Containers {
		for _, envFrom := range c.EnvFrom {
			logrus.Debugf("Parse service containers envFrom: %+v", envFrom)
			if envFrom.ConfigMapRef != nil && (envFrom.ConfigMapRef.Optional == nil || !*envFrom.ConfigMapRef.Optional) {
				ref := envFrom.ConfigMapRef
				configmap, err := getConfigMap(svcStore, request.Cluster.UUID, request.Namespace.Name, ref.Name)
				if err != nil {
					return nil, err
				} else {
					svcStore.Logger.Debugf("Parse configmap response from ref: %+v", configmap)
					configMapRef.AddConfigMapRefRow(request, configmap, "", "env", c.Name)
				}

			}
		}
		for _, env := range c.Env {
			if env.ValueFrom != nil && env.ValueFrom.ConfigMapKeyRef != nil {
				ref := env.ValueFrom.ConfigMapKeyRef
				logrus.Debugf("Parse service containers env key from: %s:%s", ref.Key, ref.Name)
				configmap, err := getConfigMap(svcStore, request.Cluster.UUID, request.Namespace.Name, ref.Name)
				if err != nil {
					return nil, err
				} else {
					configMapRef.AddConfigMapRefRow(request, configmap, ref.Key, "env", c.Name)
				}
			}
		}
	}
	return &configMapRef, nil

}

func getConfigMap(svcStore *db.ServiceStore, cid, namespace, name string) (*model.ConfigMapResponse, error) {
	if svcStore.Parent != nil {
		resources := svcStore.Parent.Others.Request
		resource := model.GetSpecificResource(resources, name, string(model.ConfigMapType))
		if resource != nil {
			svcStore.Logger.Infof("Parse ConfigMap resource from app: %+v", resource.GetAnnotations())
			return resource.ToObject().ToConfigMapResponse(), nil
		}
	}
	return kubeStore.GetConfigMapByNamespace(cid, namespace, name)
}
