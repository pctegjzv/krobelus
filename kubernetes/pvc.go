package kubernetes

import (
	"krobelus/db"
	"krobelus/model"
	kubeStore "krobelus/store"

	"k8s.io/api/core/v1"
)

func ParsePVCRefFromKubernetesResource(resource *model.KubernetesResource, store *db.ServiceStore) (*db.PVCRef, error) {
	spec, err := resource.GetPodSpec()
	if err != nil {
		return nil, err
	}
	return parsePVCRefFromPodSpec(spec, store)
}

// parsePVCRefFromPodSpec will parse PVC refs from a PodSpec. It can be from a statefulset or pod
// Note: For now, we ignore the volumeClaimTemplates declared in statefulset
func parsePVCRefFromPodSpec(spec *v1.PodSpec, store *db.ServiceStore) (*db.PVCRef, error) {
	request := store.Request

	var PVCRef db.PVCRef

	// volumes
	for _, v := range spec.Volumes {
		if nil != v.PersistentVolumeClaim {
			ref := v.PersistentVolumeClaim
			pvc, err := getPVC(store, request.Cluster.UUID, request.Namespace.Name, ref.ClaimName)
			if err != nil {
				return nil, err
			} else {
				PVCRef.AddPVCRefRow(request, pvc)
			}
		}
	}
	return &PVCRef, nil

}

func getPVC(svcStore *db.ServiceStore, cid, namespace, name string) (*model.PVCResponse, error) {
	if svcStore.Parent != nil {
		resources := svcStore.Parent.Others.Request
		resource := model.GetSpecificResource(resources, name, string(model.PersistentVolumeClaimType))
		if resource != nil {
			svcStore.Logger.Infof("Parse PVC resource from app: %s", name)
			return resource.ToObject().ToPVCResponse(), nil
		}
	}
	return kubeStore.GetPVCByNamespace(cid, namespace, name)
}
