package kubernetes

import (
	"fmt"

	"krobelus/common"
	"krobelus/model"

	"github.com/spf13/cast"
	"k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetPodVersion(pod metaV1.Object) int {
	version, ok := pod.GetAnnotations()[common.SvcVersionKey()]
	if !ok {
		return 0
	}
	return cast.ToInt(version)
}

func IsPodRunning(pod *v1.Pod) bool {
	return GetPodStatus(pod) == string(v1.PodRunning)
}

func SetPodStatus(pod *v1.Pod) {
	pod.Status.Phase = v1.PodPhase(GetPodStatus(pod))
}

// Set version for service pods, all pods must belongs to same service
func SetVersionForServicePods(objects []*model.KubernetesObject, controller *model.KubernetesResource) []*v1.Pod {
	var result []*v1.Pod

	if len(objects) < 1 {
		return result
	}
	version, _ := controller.GetPodVersion()

	for _, object := range objects {
		pod, _ := object.ToPod()
		if pod.Annotations == nil {
			pod.Annotations = make(map[string]string)
		}
		if GetPodVersion(pod) == version {
			pod.Annotations[common.SvcVersionKey()] = common.CurrentServiceVersion
		} else {
			pod.Annotations[common.SvcVersionKey()] = common.PreviousServiceVersion
		}
		pod.Status.Phase = v1.PodPhase(GetPodStatus(pod))
		result = append(result, pod)
	}
	return result
}

// GetPodStatus cal pod status
// copy from: https://github.com/hangyan/kubernetes/blob/master/pkg/printers/internalversion/printers.go#L249
func GetPodStatus(pod *v1.Pod) string {
	reason := string(pod.Status.Phase)
	restarts := 0
	readyContainers := 0
	if pod.Status.Reason != "" {
		reason = pod.Status.Reason
	}

	initializing := false
	for i := range pod.Status.InitContainerStatuses {
		container := pod.Status.InitContainerStatuses[i]
		restarts += int(container.RestartCount)
		switch {
		case container.State.Terminated != nil && container.State.Terminated.ExitCode == 0:
			continue
		case container.State.Terminated != nil:
			// initialization is failed
			if len(container.State.Terminated.Reason) == 0 {
				if container.State.Terminated.Signal != 0 {
					reason = fmt.Sprintf("Init:Signal:%d", container.State.Terminated.Signal)
				} else {
					reason = fmt.Sprintf("Init:ExitCode:%d", container.State.Terminated.ExitCode)
				}
			} else {
				reason = "Init:" + container.State.Terminated.Reason
			}
			initializing = true
		case container.State.Waiting != nil && len(container.State.Waiting.Reason) > 0 && container.State.Waiting.Reason != "PodInitializing":
			reason = "Init:" + container.State.Waiting.Reason
			initializing = true
		default:
			reason = fmt.Sprintf("Init:%d/%d", i, len(pod.Spec.InitContainers))
			initializing = true
		}
		break
	}
	if !initializing {
		restarts = 0
		for i := len(pod.Status.ContainerStatuses) - 1; i >= 0; i-- {
			container := pod.Status.ContainerStatuses[i]

			restarts += int(container.RestartCount)
			if container.State.Waiting != nil && container.State.Waiting.Reason != "" {
				reason = container.State.Waiting.Reason
			} else if container.State.Terminated != nil && container.State.Terminated.Reason != "" {
				reason = container.State.Terminated.Reason
			} else if container.State.Terminated != nil && container.State.Terminated.Reason == "" {
				if container.State.Terminated.Signal != 0 {
					reason = fmt.Sprintf("Signal:%d", container.State.Terminated.Signal)
				} else {
					reason = fmt.Sprintf("ExitCode:%d", container.State.Terminated.ExitCode)
				}
			} else if container.Ready && container.State.Running != nil {
				readyContainers++
			}
		}
	}

	if pod.DeletionTimestamp != nil && pod.Status.Reason == "NodeLost" {
		reason = "Unknown"
	} else if pod.DeletionTimestamp != nil {
		reason = "Terminating"
	}
	return string(reason)
}

func GetCountFromPodList(pods []*v1.Pod, controller *model.KubernetesResource) (int, int) {
	version, _ := controller.GetPodVersion()
	desired := 0
	current := 0
	for _, pod := range pods {
		if GetPodVersion(pod) == version {
			desired = desired + 1
			if IsPodRunning(pod) {
				current = current + 1
			}
		}
	}
	return desired, current
}
