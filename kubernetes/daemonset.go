package kubernetes

import (
	"encoding/json"

	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/juju/errors"
	appsV1beta1 "k8s.io/api/apps/v1beta1"
)

func GetDaemonsetRevisions(service *model.ServiceResponse) ([]*model.ServiceRevision, error) {
	kubernetesStore := GetDaemonsetRevisionStore(service)
	selector := map[string]string{"labelSelector": service.GetLabelSelector()}
	list, err := GetControllerRevisions(kubernetesStore, selector)
	if err != nil {
		return nil, err
	}
	versions := make([]*model.ServiceRevision, len(list))
	for idx, item := range list {
		versions[idx] = &model.ServiceRevision{
			Revision:          item.Revision,
			ChangeCause:       item.Annotations[common.ChangeCauseAnnotation],
			CreationTimestamp: item.CreationTimestamp.Time,
		}
	}
	return versions, nil
}

func GetDaemonsetRevisionStore(service *model.ServiceResponse) *store.KubernetesResourceStore {
	return &store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: &model.Query{
				ClusterUUID: service.GetClusterUUID(),
				Namespace:   service.GetNamespace(),
				Type:        common.GetKubernetesTypeFromKind(common.KubernetesControllerRevision),
			},
			Logger: service.GetLogger(),
		},
	}
}

func GetControllerRevisions(kubeStore *store.KubernetesResourceStore, selector map[string]string) ([]appsV1beta1.ControllerRevision, error) {
	result, err := kubeStore.Request(common.HttpGet, selector)
	if err != nil {
		return nil, err
	}
	bytes, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error")
	}
	list := &appsV1beta1.ControllerRevisionList{}
	if err := json.Unmarshal(bytes, list); err != nil {
		return nil, err
	}
	return list.Items, nil
}
