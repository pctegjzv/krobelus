package kubernetes

import (
	"encoding/json"
	"strconv"

	"krobelus/common"
	"krobelus/model"
	"krobelus/store"

	"github.com/juju/errors"
	extV1beta1 "k8s.io/api/extensions/v1beta1"
)

func GetDeploymentRevisions(service *model.ServiceResponse) ([]*model.ServiceRevision, error) {
	kubernetesStore := GetDeploymentRevisionStore(service)
	selector := map[string]string{"labelSelector": service.GetLabelSelector()}
	list, err := GetReplicasets(kubernetesStore, selector)
	if err != nil {
		return nil, err
	}
	versions := make([]*model.ServiceRevision, len(list))
	for idx, item := range list {
		if revision, err := strconv.ParseInt(item.Annotations[common.RevisionAnnotation], 10, 64); err != nil {
			return nil, err
		} else {
			versions[idx] = &model.ServiceRevision{
				Revision:          revision,
				ChangeCause:       item.Annotations[common.ChangeCauseAnnotation],
				CreationTimestamp: item.CreationTimestamp.Time,
			}
		}
	}
	return versions, nil
}

func GetDeploymentRevisionStore(service *model.ServiceResponse) *store.KubernetesResourceStore {
	return &store.KubernetesResourceStore{
		ResourceStore: store.ResourceStore{
			UniqueName: &model.Query{
				ClusterUUID: service.GetClusterUUID(),
				Namespace:   service.GetNamespace(),
				Type:        common.GetKubernetesTypeFromKind(common.KubernetesKindReplicaSet),
			},
			Logger: service.GetLogger(),
		},
	}
}

func GetReplicasets(kubeStore *store.KubernetesResourceStore, selector map[string]string) ([]extV1beta1.ReplicaSet, error) {
	result, err := kubeStore.Request(common.HttpGet, selector)
	if err != nil {
		return nil, err
	}
	bytes, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error")
	}
	list := &extV1beta1.ReplicaSetList{}
	if err := json.Unmarshal(bytes, list); err != nil {
		return nil, err
	}
	return list.Items, nil
}
