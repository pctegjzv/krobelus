# README #

[TOC]

This is a new start.

# Basic information

## Golang tools

* dependency management: dep
* log: logrus
* web framework: gin
* sql generate: goqu
* kubernetes client: client-go
* errors: juju/errors


## Doc
 * [Confluence/Krobelus](http://confluence.alaudatech.com/display/DEV/Krobelus)



## Local

Run `make compose` to start krobelus-api/worker/watcher along with all other deps(db,redis). Modify `run/data/db.json` to add your kubernetes info to a new region,
then the api is ready to go



# API

API has two version:

1. v3, uuid based, different resources have different urls and views
2. v1, name based, common api to support all kinds of kubernetes resoruces


## v1

Due to the restriction of `gin`, the url patten for v1 is a bit of mess. So here is a few example of v1 api:

* `/v1/clusters/47252863-2c8f-4446-b820-3bc9ce7ff88b/namespaces`: list cluster resource namespaces in a cluster
* `/v1/clusters/47252863-2c8f-4446-b820-3bc9ce7ff88b/namespaces`: list configmaps in all namespaces in a cluster
* `/v1/clusters/47252863-2c8f-4446-b820-3bc9ce7ff88b/configmaps/aaaa-namespace/aaaaaa`: get a configmap 
* `/v1/clusters/47252863-2c8f-4446-b820-3bc9ce7ff88b/configmaps/aaaa-namespace/`: list configmap in a namespace
