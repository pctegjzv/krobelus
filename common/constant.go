package common

import (
	"krobelus/config"
)

const (
	Component        = "krobelus"
	QueueTaskPrefix  = "queue:task:"
	QueueGroupPrefix = "queue:group:"
)

func NewGroupUUID() string {
	return QueueGroupPrefix + NewUUID()
}

func NewTaskUUID() string {
	return QueueTaskPrefix + NewUUID()
}

// Cache key prefix
const (
	// RegionWatcherKey is a HashMap, region id is th key, start timestamp is the value
	RegionWatcherKey = "watcher:regions"

	// WatcherInstancesCacheKey stores krobelus-watcher instances info
	// ClusterServiceClassesCachePrefix     = "v1beta1:broker_classes"
	// KubernetesResourceUIDCacheKeyPrefix is the key prefix for kubernetes resource stored by uuid
	// TODO(xxhe): Should be removed if v3 API have migrated to use name as key.
	KubernetesResourceUIDCacheKeyPrefix = "k8s:uid"
	// KubernetesResourceNameCacheKeyPrefix is the key prefix for kubernetes resource stored by name
	KubernetesResourceNameCacheKeyPrefix = "k8s:name"
)

// Default Query Params for paginate
const (
	DefaultPageSize = "20"
	DefaultPage     = "1"
)

// Service Status

type ResourceTargetState string

const (
	// Target states
	TargetStartedState ResourceTargetState = "Started"
	TargetStoppedState ResourceTargetState = "Stopped"
	TargetDeletedState ResourceTargetState = "Deleted"
	// only used by PV now.
	TargetCreatedState ResourceTargetState = "Created"
	// TargetUpdatedState ResourceTargetState = "Updated"
)

type ResourceStatus string

// Common status for apps/services/...
const (
	StatusRunning   ResourceStatus = "Running"
	StatusError     ResourceStatus = "Error"
	StatusDeploying ResourceStatus = "Deploying"
	StatusWarning   ResourceStatus = "Warning"
	StatusStopped   ResourceStatus = "Stopped"
	StatusUnknown   ResourceStatus = "Unknown"

	StatusStarting ResourceStatus = "Starting"
	StatusUpdating ResourceStatus = "Updating"
	StatusStopping ResourceStatus = "Stopping"
	StatusDeleting ResourceStatus = "Deleting"

	// PV status
	StatusAvailable ResourceStatus = "Available"
	StatusBound     ResourceStatus = "Bound"
	// StatusReleased  ResourceStatus = "Released"
	// StatusFailed    ResourceStatus = "Failed"

	//PVC status
	// StatusPending ResourceStatus = "Pending"
	// StatusLost    ResourceStatus = "Lost"
)

var (
	// RunningStatusList   = []KubernetesResourceStatus{CurrentCreatedStatus, CurrentUpdatedStatus}
	ErrorStatusList     = []KubernetesResourceStatus{CurrentCreateErrorStatus, CurrentUpdateErrorStatus, CurrentDeleteErrorStatus}
	DeployingStatusList = []KubernetesResourceStatus{CurrentCreatingStatus, CurrentUpdatingStatus, CurrentDeletingStatus}
	DeployedStatusList  = []KubernetesResourceStatus{CurrentCreatedStatus, CurrentUpdatedStatus, CurrentDeletedStatus}
)

type KubernetesResourceStatus string

const (
	CurrentCreatingStatus    KubernetesResourceStatus = "Creating"
	CurrentCreateErrorStatus KubernetesResourceStatus = "CreateError"
	CurrentCreatedStatus     KubernetesResourceStatus = "Created"
	CurrentUpdatingStatus    KubernetesResourceStatus = "Updating"
	CurrentUpdateErrorStatus KubernetesResourceStatus = "UpdateError"
	CurrentUpdatedStatus     KubernetesResourceStatus = "Updated"
	CurrentDeletingStatus    KubernetesResourceStatus = "Deleting"
	CurrentDeleteErrorStatus KubernetesResourceStatus = "DeleteError"
	CurrentDeletedStatus     KubernetesResourceStatus = "Deleted"
)

func (s KubernetesResourceStatus) IsDeployingStatus() bool {
	for _, item := range DeployingStatusList {
		if s == item {
			return true
		}
	}
	return false
}

func (s KubernetesResourceStatus) IsDeployedStatus() bool {
	for _, item := range DeployedStatusList {
		if s == item {
			return true
		}
	}
	return false
}
func (s KubernetesResourceStatus) IsDeployErrorStatus() bool {
	for _, item := range ErrorStatusList {
		if s == item {
			return true
		}
	}
	return false
}

func (s KubernetesResourceStatus) IsDeleting() bool {
	return s == CurrentDeletingStatus
}

func (target ResourceTargetState) IsDeleted() bool {
	return target == TargetDeletedState
}

func (target ResourceTargetState) IsStopped() bool {
	return target == TargetStoppedState
}

func DeployErrorToDeployingStatus(status string) KubernetesResourceStatus {
	switch KubernetesResourceStatus(status) {
	case CurrentCreateErrorStatus:
		return CurrentCreatingStatus
	case CurrentUpdateErrorStatus:
		return CurrentUpdatingStatus
	case CurrentDeleteErrorStatus:
		return CurrentDeletingStatus
	default:
		return ""
	}
}

// DB drivers

// Alauda Volume Types
const (
	Glusterfs = "glusterfs"
	EBS       = "ebs"
)

func GetGlusterFSEndpoint() string {
	return config.GlobalConfig.Glusterfs.Endpoint
}

const (
	// PV & PVC Resource Type is using for cache type
	PersistentVolumeK8SResType      = "persistentvolumes"
	PersistentVolumeClaimK8SResType = "persistentvolumeclaims"
	ClusterServiceBrokerType        = "clusterservicebrokers"
	ClusterServiceClassType         = "clusterserviceclasses"
	ClusterServicePlanType          = "clusterserviceplans"
	ServiceBindingType              = "servicebindings"
	ServiceInstanceType             = "serviceinstances"
)

const VolumeStateAvailable = "available"

// Envs will be injected into the pod.container spec
const (
	EnvServiceID     = "__ALAUDA_SERVICE_ID__"
	EncServiceName   = "__ALAUDA_SERVICE_NAME__"
	EnvAppName       = "__ALAUDA_APP_NAME__"
	EnvContainerSize = "__ALAUDA_CONTAINER_SIZE__"
)

const (
	HttpGet    = "GET"
	HttpPost   = "POST"
	HttpPut    = "PUT"
	HttpDelete = "DELETE"
	HttpPatch  = "PATCH"
)

type ServiceScaleAction string

const (
	ScaleUp   ServiceScaleAction = "scale-up"
	ScaleDown ServiceScaleAction = "scale-down"
)

// Create method for App/Service. Since UI is a subset of YAML
const (
	CreateThroughYAML = "YAML"
	// CreateThroughUI   = "UI"
)

const (
	NamespaceDefault = "default"
)
