package common

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/pborman/uuid"
)

func ToString(data interface{}) string {
	return spew.Sprintf("%+v", data)
}

// GetTimeDuration generate a time duration of seconds
func GetTimeDuration(t int) time.Duration {
	return time.Duration(t) * time.Second
}

// StringInSlice check whether a string is in an slice.
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// RemoveEmptyString remove empty string for a slice.
func RemoveEmptyString(list []string) []string {
	var result []string
	for _, v := range list {
		if v == "" {
			continue
		} else {
			result = append(result, v)
		}
	}
	return result
}

func MapSubSet(a, b map[string]string) bool {
	for key, value := range a {
		if v, ok := b[key]; !ok || v != value {
			return false
		}
	}
	return true
}

func GetCurrentTimeStamp() int64 {
	return time.Now().Unix()
}

// NewUUID generate a new uuid
func NewUUID() string {
	return uuid.New()
}

// CombineString is a dump string combine function (by : )
func CombineString(items ...string) string {
	str := ""
	for _, item := range items {
		if str == "" {
			str = item
		} else {
			str = fmt.Sprintf("%s:%s", str, item)
		}
	}
	return str
}

// GenerateRandString will return a fixed length random string
func GenerateRandString(n int) string {
	letters := []rune("abcdefghijklmnopqrstuvwxyz0123456789")
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}
