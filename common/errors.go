package common

import (
	"strings"

	"github.com/juju/errors"
)

// internal errors
var (
	ErrCacheMiss = errors.New("cache miss")
	// used for db
	ErrResourceNotExist = errors.New("resource not exist")
	// used for cache
	ErrNotFound = errors.New("resource not found")

	ErrKubernetesResourceExists = errors.New("kubernetes resource already exist")
	ErrInvalidRegionInfo        = errors.New("invalid region info")
	ErrKubernetesConnect        = errors.New("connect kubernetes error")
)

const (
	// NoRowsError is returned by Scan when QueryRow doesn't return a row. In such a case,
	// QueryRow returns a placeholder *Row value that defers this error until a Scan.
	// Note: this may be only used for squirrel package, may be useless for now
	NoRowsError = "sql: no rows in result set"
	// RedisNotFoundError is returned by redis if cache miss.
	RedisNotFoundError = "redis: nil"
)

func IsNotExistError(err error) bool {
	if err == nil {
		return false
	}
	return errors.Cause(err) == ErrResourceNotExist || strings.Contains(err.Error(), NoRowsError) || strings.Contains(err.Error(), RedisNotFoundError)
}

const (
	// Source
	Source         = 1044
	RikiSource     = 1034
	DarchrowSource = 1030
)

const (
	// Code
	CodeInvalidArgs = "invalid_args"
	CodeExist       = "resource_already_exist"

	CodeNotExist     = "resource_not_exist"
	CodeUnknownIssue = "unknown_issue"
	CodeDBError      = "db_unknown_issue"
	CodeTransError   = "resource_trans_error"

	// Get region info error
	CodeRegionError = "region_error"
	// Error about kubernetes
	CodeKubernetesError = "kubernetes_error"

	CodeKubernetesConnectError = "kubernetes_connect_error"

	//Error calling http error
	CodeHttpError = "http_error"

	//Broker Error
	BrokerTimeoutError    = "broker_timeout_error"
	BrokerConnectionError = "broker_connection_error"
	BrokerRepeatError     = "broker_repeat_error"
	BrokerAuthError       = "broker_auth_error"

	//ServiceInstance Error
	ServiceInstanceParamError  = "serviceinstance_param_error"
	ServiceInstanceBrokerError = "serviceinstance_broker_error"
	//Binding Error
	BindingRepeatError = "binding_repeat_error"
	//Namespace Error
	ResourceQuotaCreateError  = "resource_quota_create_error"
	LimitRangeCreateError     = "limit_range_create_error"
	SecretCreateError         = "secret_create_error"
	SAAddSecretError          = "sa_add_secret_error"
	AddSAToSCCError           = "add_sa_to_scc_error"
	JFrogNamespaceCreateError = "jfrog_namespace_create_error"

	// redis error
	CacheError = "cache_error"
)

type KrobelusError struct {
	Source  int                 `json:"source"`
	Code    string              `json:"code"`
	Message string              `json:"message"`
	Detail  []map[string]string `json:"fields,omitempty"`
}

type KrobelusErrors struct {
	Errors []*KrobelusError `json:"errors"`
}

type HttpError KrobelusError

func (es HttpError) Error() string {
	return es.Message
}

// HttpResponseError wraps the error with an http status code.
type HttpResponseError struct {
	Cause      error
	StatusCode int
}

func (he *HttpResponseError) Error() string {
	return he.Cause.Error()
}

type HttpErrorResponse struct {
	Source     int    `json:"source"`
	StatusCode int    `json:"status_code"`
	Message    string `json:"message"`
}

func (he HttpErrorResponse) Error() string {
	return he.Message
}

func (es KrobelusErrors) Error() string {
	var messages []string
	for _, err := range es.Errors {
		messages = append(messages, err.Message)
	}
	return strings.Join(messages, "\n")
}

// Errors for http response

func BuildError(code string, message string) error {
	return KrobelusErrors{
		Errors: []*KrobelusError{
			{
				Source:  Source,
				Code:    code,
				Message: message,
			},
		},
	}
}

func BuildSourceError(source int, code string, message string) error {
	return KrobelusErrors{
		Errors: []*KrobelusError{
			{
				Source:  source,
				Code:    code,
				Message: message,
			},
		},
	}
}

func BuildServerUnknownError(message string) error {
	return BuildError(CodeUnknownIssue, message)
}

func BuildDBUnknownError(message string) error {
	return BuildError(CodeDBError, message)
}

func BuildResourceNotExistError(message string) error {
	return BuildError(CodeNotExist, message)
}

func BuildResourceTransError(message string) error {
	return BuildError(CodeTransError, message)
}

func BuildDBNotFoundError(message string) error {
	return BuildError(CodeNotExist, message)
}

func BuildInvalidArgsError(message string) error {
	return BuildError(CodeInvalidArgs, message)
}

func BuildResourceAlreadyExistError(message string) error {
	return BuildError(CodeExist, message)
}

func BuildKubernetesError(message string) error {
	return BuildError(CodeKubernetesError, message)
}

func BuildRegionError(message string) error {
	return BuildError(CodeRegionError, message)
}

func BuildHttpError(err *HttpErrorResponse) error {
	return HttpError{
		Source:  err.Source,
		Code:    CodeHttpError,
		Message: err.Message,
	}
}

func BuildRikiErrorResponse(statusCode int, message string) *HttpErrorResponse {
	return &HttpErrorResponse{
		Source:     RikiSource,
		StatusCode: statusCode,
		Message:    message,
	}
}

func BuildDarchrowErrorResponse(statusCode int, message string) *HttpErrorResponse {
	return &HttpErrorResponse{
		Source:     DarchrowSource,
		StatusCode: statusCode,
		Message:    message,
	}
}

func BuildBrokerTimeoutError(message string) error {
	return BuildError(BrokerTimeoutError, message)
}

func BuildServiceCatalogError(errorCode string, message string) error {
	return BuildError(errorCode, message)
}

func BuildResourceQuotaCreateError(message string) error {
	return BuildError(ResourceQuotaCreateError, message)
}

func BuildLimitRangeCreateError(message string) error {
	return BuildError(LimitRangeCreateError, message)
}

func BuildSecretCreateError(message string) error {
	return BuildError(SecretCreateError, message)
}

func BuildSAAddSecretCreateError(message string) error {
	return BuildError(SAAddSecretError, message)
}

func BuildAddSAToSCCError(message string) error {
	return BuildError(AddSAToSCCError, message)
}

func BuildJFrogNamespaceCreateError(err error) error {
	httpErr, ok := err.(*HttpErrorResponse)
	if ok {
		return BuildSourceError(httpErr.Source, JFrogNamespaceCreateError, httpErr.Message)
	}
	return BuildError(JFrogNamespaceCreateError, err.Error())
}

// BuildKrobelusErrors builds errors of KrobelusErrors.
// The items of 'errs' array must be 'KrobelusErrors' type.
func BuildKrobelusErrors(errs []error) error {
	var kerrs []*KrobelusError
	for _, err := range errs {
		kerr, ok := err.(KrobelusErrors)
		if ok {
			kerrs = append(kerrs, kerr.Errors[0])
		}
	}
	return KrobelusErrors{Errors: kerrs}
}
