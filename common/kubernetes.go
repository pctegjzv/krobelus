package common

import (
	"strings"

	"krobelus/config"

	"k8s.io/apimachinery/pkg/runtime"
)

// Kubernetes API version
const (
	KubernetesAPIVersionV1                = "v1"
	KubernetesAPIVersionExtensionsV1beta1 = "extensions/v1beta1"
	KubernetesAPIVersionAppV1alpha1       = "app.k8s.io/v1alpha1"
)

// Kubernetes Resources
const (
	KubernetesKindNamespace   = "Namespace"
	KubernetesKindApplication = "Application"
	KubernetesKindService     = "Service"
	KubernetesKindDeployment  = "Deployment"
	KubernetesKindDaemonSet   = "DaemonSet"
	KubernetesKindStatefulSet = "StatefulSet"
	KubernetesKindConfigmap   = "ConfigMap"
	KubernetesKindReplicaSet  = "ReplicaSet"
	KubernetesKindHPA         = "HorizontalPodAutoscaler"
	KubernetesKindPod         = "Pod"
	KubernetesKindSecret      = "Secret"

	KubernetesKindPVC            = "PersistentVolumeClaim"
	KubernetesKindPDB            = "PodDisruptionBudget"
	KubernetesControllerRevision = "ControllerRevision"
	KubernetesKindRoute          = "Route"
	KubernetesKindEvent          = "Event"
	KubernetesKindEndpoints      = "Endpoints"
	KubernetesKindServiceAccount = "ServiceAccount"
)

// Kubernetes Revisions
const (
	RevisionAnnotation    = "deployment.kubernetes.io/revision"
	ChangeCauseAnnotation = "kubernetes.io/change-cause"
)

// Kubernetes Event Reasons
const (
	FailedSync     = "FailedSync"
	ReasonSynced   = "Synced"
	ReasonStarted  = "Started"
	ReasonStopped  = "Stopped"
	ReasonUpdating = "Updating"
)

// kubernetes api params
const (
	ParamLabelSelector = "labelSelector"
)

// kubernetes event
const (
	EventTypeNormal  = "Normal"
	EventTypeWarning = "Warning"
)

// Kubernetes topology reference kinds
const (
	Reference = "Reference"
	Selector  = "Selector"
)

var (
	ClusterScopeResourceKind = map[string]bool{
		KubernetesKindNamespace: true,
		"PersistentVolume":      true,
		"StorageClass":          true,
		"ClusterRole":           true,
		"ClusterRoleBinding":    true,
	}

	AppUnsupportedResourceKind = []string{
		KubernetesKindNamespace,
		"PersistentVolume",
		"StorageClass",
	}
)

// GetKubernetesTypeFromKind get kubernetes type by kind.
// eg: DaemonSet --> daemonsets
// current support:
// 1. Deployment / DaemonSet / StatefulSet
// TODO: handler special case
func GetKubernetesTypeFromKind(kind string) string {
	switch kind {
	case "NetworkPolicy":
		return "networkpolicies"
	case "Ingress":
		return "ingresses"
	case "StorageClass":
		return "storageclasses"
	default:
		return strings.ToLower(kind) + "s"
	}
}

// Kubernetes Deploy Mode. (Pod controllers)
var (
	KubernetesDeployModeKind = map[string]bool{
		KubernetesKindDeployment:  true,
		KubernetesKindDaemonSet:   true,
		KubernetesKindStatefulSet: true,
	}
)

// IsPodController check whether a kubernetes resource kind is a pod controller.
// current support: daemonset/deployment/statefulset
func IsPodController(kind string) bool {
	return KubernetesDeployModeKind[kind]
}

// kubernetes resource name check

var (
	KubernetesResourceTypesMap = map[string]bool{
		KubernetesKindDaemonSet:   true,
		KubernetesKindDeployment:  true,
		KubernetesKindService:     true,
		KubernetesKindStatefulSet: true,
		KubernetesKindHPA:         true,
		KubernetesKindPDB:         true,
	}
)

const (
	KubernetesPodController = "Controller"
	CurrentServiceVersion   = "current"
	PreviousServiceVersion  = "previous"
)

func getKey(resource, attr string) string {
	return resource + "." + config.GlobalConfig.Label.BaseDomain + "/" + attr
}

func ClusterUidKey() string {
	return getKey("cluster", "uuid")
}

func SvcCreateByKey() string {
	return getKey("service", "createby")
}

func SvcUidKey() string {
	return getKey("service", "uuid")
}

func SvcNameKey() string {
	return getKey("service", "name")
}

func AppNameKey() string {
	return getAppKey("name")
}

func AppUidKey() string {
	return getAppKey("uuid")
}

func SvcVersionKey() string {
	return getKey("service", "version")
}

func ResourceUidKey() string {
	return getKey("resource", "uuid")
}

func ResourceDisplayNameKey() string {
	return getKey("resource", "display_name")
}

func ClusterNameKey() string {
	return getKey("cluster", "name")
}

func PVVolumeDriverKey() string {
	return getKey("pv", "volume_driver")
}

func PVVolumeNameKey() string {
	return getKey("pv", "volume_name")
}

func PVVolumeUidKey() string {
	return getKey("pv", "volume_uuid")
}

func ResourceStatusKey() string {
	return getKey("resource", "status")
}

func SCBDisplayNameKey() string {
	return getKey("clusterservicebroker", "display_name")
}

func SCBStatusKey() string {
	return getKey("clusterservicebroker", "status")
}

func SCBClassesNumKey() string {
	return getKey("clusterservicebroker", "classes_num")
}

func SCCNameKey() string {
	return getKey("clusterserviceclass", "name")
}

func SCPNameKey() string {
	return getKey("clusterserviceplan", "name")
}

func SCINameKey() string {
	return getKey("serviceinstance", "name")
}

func SCIAppNumKey() string {
	return getKey("serviceinstance", "app_numbers")
}

func SCCPlanUpdatableKey() string {
	return getKey("clusterserviceclass", "plan_updatable")
}

func ObjRefListKey() string {
	return getKey("refs", "k8s_obj_list")
}

func getAppKey(attr string) string {
	return getKey("app", attr)
}

func AppCreateMethodKey() string {
	return getAppKey("create_method")
}

func AppSourceKey() string {
	return getAppKey("source")
}

func AppDeletedAtKey() string {
	return getAppKey("deleted_at")
}

// start/stop/starting/stopping
// 1. staring/stopping -> handle it
// 2. start/stop -> what can we do(stop or start) on this app.
func AppActionKey() string {
	return getAppKey("action")
}

func AppLastReplicasKey() string {
	return getAppKey("last-replicas")
}

func AppMigrationKey() string {
	return getAppKey("migration")
}

const (
	//TODO: use new name format
	AnnotationsDescription = "description.resource.alauda"
)

const (
	ResourceStatusInitializing = "Initializing"
)

type KubernetesObj struct {
	apiVersion    string
	runtimeObject runtime.Object
}

func IsAppServiceMainResource(kind string) bool {
	return KubernetesResourceTypesMap[kind]
}

var (
	SubResourcesForAlaudaService = []string{
		KubernetesKindDeployment,
		KubernetesKindStatefulSet,
		KubernetesKindDaemonSet,
		KubernetesKindService,
		KubernetesKindHPA,
		KubernetesKindPDB,
	}
)
