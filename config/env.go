package config

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/vrischmann/envconfig"
)

var (
	GlobalConfig Config
)

type Config struct {
	Krobelus struct {
		Debug bool `envconfig:"default=true"`
		Port  int  `envconfig:"default=8080"`
	}

	Log struct {
		// unit: M
		Size   int `envconfig:"default=100"`
		Backup int `envconfig:"default=2"`
	}

	Jakiro struct {
		Endpoint   string `envconfig:"optional"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=3"`
	}

	Furion struct {
		Endpoint   string `envconfig:"default=http://mock-server:80"`
		ApiVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=3"`
	}

	Tiny struct {
		Endpoint   string `envconfig:"default=http://mock-server:80"`
		ApiVersion string `envconfig:"default=v2"`
		Timeout    int    `envconfig:"default=3"`
	}

	Kubernetes struct {
		Timeout                int      `envconfig:"default=3"`
		ApiDiscoveryExpiration int      `envconfig:"default=300"`
		DiscoveryBlackGroups   []string `envconfig:"optional"`
	}

	Pod struct {
		// ResourceLabel is the key for a Pod that indicate what resource this pod belongs to
		// format: <label-key>:namespace-resourceType-resourceName
		ResourceLabel string `envconfig:"optional"`
	}

	Label struct {
		BaseDomain string `envconfig:"default=alauda.io"`
	}

	API struct {
		DeployTimeout int `envconfig:"default=180"`
	}

	DB struct {
		Host        string `envconfig:"default=127.0.0.1"`
		Port        string `envconfig:"default=5432"`
		User        string `envconfig:"default=krobelus"`
		Password    string `envconfig:"default=123456"`
		Name        string `envconfig:"default=krobelusdb"`
		Engine      string `envconfig:"default=postgresql"`
		Timeout     int    `envconfig:"default=0"`
		MaxConn     int    `envconfig:"default=10"`
		MaxIdleConn int    `envconfig:"default=1, DB_MAX_IDLE_CONN"`

		TimeZoneDiff int `envconfig:"default=0"`
	}

	Redis struct {
		//   Prefix string `envconfig:"default=krobelus:"`

		TypeWriter           string `envconfig:"default=normal"`
		TypeReader           string `envconfig:"default=normal"`
		HostWriter           []string
		HostReader           []string
		PortReader           []string
		PortWriter           []string
		DBNameWriter         int    `envconfig:"default=0,REDIS_DB_NAME_WRITER"`
		DBNameReader         int    `envconfig:"default=0,REDIS_DB_NAME_READER"`
		DBPasswordReader     string `envconfig:"optional"`
		DBPasswordWriter     string `envconfig:"optional"`
		MaxConnectionsReader int    `envconfig:"default=32"`
		MaxConnectionsWriter int    `envconfig:"default=32"`
		// KeyPrefix should keep the same, use Reader if not
		KeyPrefixWriter string `envconfig:"default=krobelus:"`
		KeyPrefixReader string `envconfig:"default=krobelus:"`
	}

	Riki struct {
		Endpoint   string `envconfig:"default=127.0.0.1"`
		APIVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=3"`
	}

	Glusterfs struct {
		Endpoint string `envconfig:"default=glusterfs-endpoints"`
	}

	Worker struct {
		Mode           string `envconfig:"default=release"`
		MonitorEnabled bool   `envconfig:"default=true"`
	}

	Darchrow struct {
		Endpoint   string `envconfig:"default=127.0.0.1"`
		APIVersion string `envconfig:"default=v1"`
		Timeout    int    `envconfig:"default=20"`
	}

	CMB struct {
		DefaultContainerCPURequest string `envconfig:"default=0.5"`
		DefaultContainerMEMRequest string `envconfig:"default=256Mi"`
		DefaultContainerCPULimit   string `envconfig:"default=0.5"`
		DefaultContainerMEMLimit   string `envconfig:"default=256Mi"`
	}

	WebSocket struct {
		ReadBufferSize   int `envconfig:"default=4096"`
		WriteBufferSize  int `envconfig:"default=4096"`
		HandShakeTimeOut int `envconfig:"default=5"`
		Port             int `envconfig:"default=6070"`
		PingInterval     int `envconfig:"default=20"`
		IdleTimeout      int `envconfig:"default=120"`
	}
}

func InitConfig() {
	err := GlobalConfig.LoadFromEnv()
	if err != nil {
		log.Errorf("Load config from env error : %s", err.Error())
		os.Exit(1)
	}
}

// LoadFromEnv load env to the Config struct, also check redis env host/port match.
func (conf *Config) LoadFromEnv() error {
	if err := envconfig.Init(conf); err != nil {
		return err
	}

	if conf.Redis.KeyPrefixReader != conf.Redis.KeyPrefixWriter {
		conf.Redis.KeyPrefixWriter = conf.Redis.KeyPrefixReader
	}

	return nil
}
