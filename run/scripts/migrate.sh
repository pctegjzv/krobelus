#!/usr/bin/env sh

MYSQL_ENGINE="mysql"

if [ "$DB_ENGINE" = "$MYSQL_ENGINE" ]; then
    echo "Running mysql migration..."
    migrate -verbose -database "mysql://$DB_USER:$DB_PASSWORD@tcp($DB_HOST:$DB_PORT)/$DB_NAME" -path /krobelus/migrations/mysql up
else
    echo "Running postgres migration..."
    migrate -verbose -database "postgres://$DB_USER:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_NAME?sslmode=disable" -path /krobelus/migrations/psql up
fi
