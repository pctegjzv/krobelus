#!/bin/bash
MYSQL_ENGINE="mysql"
MIGRATIONS_PATH="$GOPATH/src/krobelus/migrations"

if [ "$DB_ENGINE" = "$MYSQL_ENGINE" ]; then
    echo "Running mysql migration..."
    migrate -database "mysql://$DB_USER:$DB_PASSWORD@tcp($DB_HOST:$DB_PORT)/$DB_NAME" -path $MIGRATIONS_PATH/mysql up
else
    echo "Running postgres migration..."
    migrate -verbose -database "postgres://$DB_USER:$DB_PASSWORD@$DB_HOST:$DB_PORT/$DB_NAME?sslmode=disable" -path $MIGRATIONS_PATH/psql up
fi