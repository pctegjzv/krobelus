#!/usr/bin/env sh


if [ "$KROBELUS_COMP" = "all" ]; then
    supervisord --nodaemon --configuration /etc/supervisord_all.conf
else
    supervisord --nodaemon --configuration /etc/supervisord.conf
fi