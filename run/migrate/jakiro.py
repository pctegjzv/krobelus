#!/usr/bin/env python

# usage:
# 1. install packages
# 2. pip install psycopg2-binary(on mac?)

import psycopg2
import os
import json
import click
import uuid
import mysql.connector
from mysql.connector import Error

def connect_psql(host, port, name, user, password):
    return psycopg2.connect(database=name,
                            user=user,
                            password=password,
                            host=host,
                            port=port)


def connect_mysql(host, port, name, user, password):
    """ Connect to MySQL database """
    try:
        conn = mysql.connector.connect(host=host,
                                       database=name,
                                       user=user,
                                       port=port,
                                       password=password)
        if conn.is_connected():
            print('Connected to MySQL database')
        else:
            print('Connect to mysql error')
        return conn
 
    except Error as e:
        print(e)

def migrate_svc(conn):

    with open('data.json') as f:
        data = json.load(f)

    cur = conn.cursor()
    # cur.execute("SELECT * FROM krobelus_services WHERE app_id=''")
    print("The number of services need to migarte: ", len(data))

    for item in data:
        cur.execute("SELECT * FROM resources_resource WHERE uuid='{}'".format(item['svc_id']))
        row = cur.fetchone()
        if row is None:
            print("Ignore: service not found: {}".format(item['svc_id']))
        else:
            print("Process: {}".format(item['svc_id']))
            print(" ---- data:{}".format(row))
            sql = """INSERT INTO resources_resource(type, uuid, namespace, created_by, 
    created_at, name, region_id, space_uuid, project_uuid, namespace_uuid) 
    VALUES (%s, %s, %s, %s, %s ,%s, %s, %s, %s, %s);"""
            cur.execute(sql, ('APPLICATION', item['app_id'], row[3], row[4], 
                row[5], row[6], row[7], row[8], row[9], row[10]))
            
            sql = """INSERT INTO resources_resourcemetadata(resource_uuid, type, key, value, editable,
            resource_id, propagate) VALUES (%s, %s, %s, %s, %s, %s, %s);"""
            cur.execute(sql, (item['svc_id'], 'parent', 'uuid', item['app_id'], True, row[0], False))
    conn.commit()
    cur.close()
    conn.close()


@click.command()
@click.option('--name', default="jakirodb", help='DB name')
@click.option('--user', help='Db user name')
@click.option('--password', help='Db user password')
@click.option('--host', help='DB host')
@click.option('--port', help='DB port')
@click.option('--engine', default='postgresql', help='DB engine, postgresql/mysql')
def migrate(name, user, password, host, port, engine):
    """Migrate exist service to app."""
    conn = None
    if engine == 'postgresql':
        conn = connect_psql(host, port, name, user, password)
    else:
        conn = connect_mysql(host, port, name, user, password)
    migrate_svc(conn)


if __name__ == '__main__':
    migrate()