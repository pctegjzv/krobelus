#!/usr/bin/env python

# usage:
# 1. install packages
# 2. pip install psycopg2-binary (on mac?)

import psycopg2
import os
import json
import click
import uuid
import mysql.connector
from mysql.connector import Error

def connect_psql(host, port, name, user, password):
    return psycopg2.connect(database=name,
                            user=user,
                            password=password,
                            host=host,
                            port=port)


def connect_mysql(host, port, name, user, password):
    """ Connect to MySQL database """
    try:
        conn = mysql.connector.connect(host=host,
                                       database=name,
                                       user=user,
                                       port=port,
                                       password=password)
        if conn.is_connected():
            print('Connected to MySQL database')
        else:
            print('Connect to mysql error')
        return conn
 
    except Error as e:
        print(e)
 

def gen_app_labels(uuid, name=None):
    data =  {
        'app.alauda.io/uuid': uuid
    }
    if name:
        data.update({
            'app.alauda.io/name': name
        })
    return data

def gen_pod_app_env(name):
    return {
        'name': '__ALAUDA_APP_NAME__',
        'value': name,
    }


def update_data(data, name, id):
    data = json.loads(data)
    app_id = id

    for resource in data:
        if resource['kind'] == 'Service':
            resource['metadata']['labels'].update(gen_app_labels(app_id, name))
        else:
            resource['metadata']['labels'].update(gen_app_labels(app_id))

        template_labels = resource.get('spec', {}).get('template', {}).get('metadata', {}).get('labels', {})
        if template_labels:
            template_labels.update(gen_app_labels(app_id, name))
        
        containers = resource.get('spec', {}).get('template', {}).get('spec', {}).get('containers', [])
        if containers:
            for container in containers:
                env = container.get('env', [])
                if env:
                    env.append(gen_pod_app_env(name))
    return data
        

def insert_app_to_psql(cur, row, app_name, app_uuid, data):
    sql = """INSERT INTO krobelus_apps(uuid, name, cluster_id, cluster_name, 
    namespace_id, namespace_name, kubernetes, current_state, target_state, created_at, updated_at,
    create_method, kubernetes_backup) 
    VALUES (%s, %s, %s, %s, %s ,%s, %s, %s, %s, %s, %s, %s, %s);"""
    cur.execute(sql, (app_uuid, app_name, row[2], row[3], row[4], row[5], data, row[10], row[11],
    row[12], row[13], row[14], "[]"))


def update_svc(cur, svc_id, app_id, app_name, data):
    sql = """ UPDATE krobelus_services
                SET app_id = %s, app_name = %s, kubernetes = %s
                WHERE  uuid= %s""" 
    cur.execute(sql, (app_id, app_name, data, svc_id))
    print("Update service parent info: {}".format(svc_id))

def migrate_svc(conn):
    apps = []

    cur = conn.cursor()
    cur.execute("SELECT * FROM krobelus_services WHERE app_id=''")
    print("The number of services need to migarte: ", cur.rowcount)

    output_data = []

    row = cur.fetchone()
    while row is not None:
        name = row[1]
        app_name = name
        app_id = str(uuid.uuid4())
        print("Processing service: {}/{}".format(row[0], name))
        print("App to be inserted: {}/{}".format(app_id, app_name))
        data = row[8]
        data = update_data(data, app_name, app_id)
        data = json.dumps(data)
        apps.append({
            'row': row,
            'name': app_name,
            'uuid': app_id,
            'data': data
        })
        output_data.append({
            'svc_id': row[0],
            'app_id': app_id
        })
        row = cur.fetchone()
    print("{} app to be inserted".format(len(apps)))
    for item in apps:
        insert_app_to_psql(cur, item['row'], item['name'], item['uuid'], item['data'])
    for item in apps:
        update_svc(cur, item['row'][0], item['uuid'], item['name'], item['data'])
    conn.commit()
    cur.close()
    conn.close()

    with open('data.json', 'w') as outfile:
        json.dump(output_data, outfile)


@click.command()
@click.option('--name', default="krobelusdb", help='DB name(default krobelusdb)')
@click.option('--user', help='Db user name')
@click.option('--password', help='Db user password')
@click.option('--host', help='DB host')
@click.option('--port', help='DB port')
@click.option('--engine', default='postgresql', help='DB engine, postgresql/mysql(defult postgresql)')
def migrate(name, user, password, host, port, engine):
    """Migrate exist service to app."""
    conn = None
    if engine == 'postgresql':
        conn = connect_psql(host, port, name, user, password)
    else:
        conn = connect_mysql(host, port, name, user, password)
    migrate_svc(conn)    


if __name__ == '__main__':
    migrate()