OS = Linux

VERSION = 0.0.1

CURDIR = $(shell pwd)
SOURCEDIR = $(CURDIR)
COVER = $($3)

ECHO = echo
RM = rm -rf
MKDIR = mkdir

# If the first argument is "cover"...
ifeq (cover,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (mysql,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (psql,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
ifeq (test, $(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.PHONY: all


.DEFAULT_GOAL := all

test:
	./run/scripts/test.sh $(RUN_ARGS)

unit-test:
	 go test -v -tags unit -cover ./infra ./api/handler ./api/transition ./tests

setup:
	go get -u github.com/golang/dep/cmd/dep

dep:
	dep ensure -v

fmt:
	./run/scripts/check-fmt.sh

cover:
	./scripts/gen-cover.sh $(RUN_ARGS)

bin:
	go build -ldflags "-w -s" -v -o krobelus krobelus

build: bin unit-test

clean:
	./scripts/clean.sh

compile:
	go build -ldflags "-w -s" -v -o $(ALAUDACI_DEST_DIR)/bin/krobelus krobelus

all: setup fmt bin unit-test

gen-mock:
	mockgen -destination ./mock/stores.go -package mock krobelus/domain TemplateStore,ProjectStore
	mockgen -destination ./mock/workflow.go -package mock krobelus/control Workflower
	mockgen -destination ./mock/controllers.go -package mock krobelus/inter TemplateController,ProjectController

gen-default:
	go build -o ./defaulter-gen ./vendor/k8s.io/code-generator/cmd/defaulter-gen
	./defaulter-gen \
		--v 1 --logtostderr \
		--go-header-file "vendor/k8s.io/gengo/boilerplate/boilerplate.go.txt" \
		--input-dirs "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1" \
		--extra-peer-dirs "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1" \
		--output-file-base "zz_generated.defaults"
gen-deepcopy:
	go build -o ./deepcopy-gen ./vendor/k8s.io/code-generator/cmd/deepcopy-gen
	./deepcopy-gen \
		--v 1 --logtostderr \
		--go-header-file "vendor/k8s.io/gengo/boilerplate/boilerplate.go.txt" \
		--input-dirs "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1" \
		--bounding-dirs "krobelus" \
		--output-file-base zz_generated.deepcopy
gen-clientset:
	if [ ! -e "./vendor/k8s.io/code-generator" ]; \
	then dep ensure -add k8s.io/code-generator/cmd/client-gen; \
	fi
	if [ ! -e "./gen-client" ]; \
	then go build -o ./gen-client ./vendor/k8s.io/code-generator/cmd/client-gen; \
	fi
	./gen-client --input-base "krobelus/pkg/servicecatalog/apis" \
			--input "servicecatalog/v1beta1" \
			--clientset-path "krobelus/pkg/servicecatalog/clientset_generated/" \
			--clientset-name "clientset"\
			--go-header-file "vendor/k8s.io/gengo/boilerplate/boilerplate.go.txt"

image:
	docker build -t krobelus .

mysql:
	docker-compose -p kro-mysql -f run/docker/mysql-compose.yaml build --force-rm
	docker-compose -p kro-mysql -f run/docker/mysql-compose.yaml up -d

psql:
	docker-compose -p kro-psql -f run/docker/psql-compose.yaml $(RUN_ARGS)

compose:
	docker-compose -p kro-psql -f run/docker/psql-compose.yaml build --force-rm
	docker-compose -p kro-psql -f run/docker/psql-compose.yaml up -d

compose-linux:
	go build -ldflags "-w -s" -v -o krobelus krobelus
	docker-compose -p kro-psql -f run/docker/psql-compose.linux.yaml build --force-rm
	docker-compose -p kro-psql -f run/docker/psql-compose.linux.yaml up -d


watcher:
	docker-compose -p kro-psql-watcher -f run/docker/psql-compose-watcher.yaml build --force-rm
	docker-compose -p kro-psql-watcher -f run/docker/psql-compose-watcher.yaml up

cloc:
	cloc . --exclude-dir=vendor,.idea

install:
	go install

help:
	@$(ECHO) "Targets:"
	@$(ECHO) "image             - build docker image"
	@$(ECHO) "compose           - docker compose to build and start this project"
	@$(ECHO) "up                - docker compose start service"
	@$(ECHO) "setup             - get dep tool"
	@$(ECHO) "cloc              - cal code lines"
	@$(ECHO) "dep               - make dependencies(vendor) update to date (need http proxy)"
	
