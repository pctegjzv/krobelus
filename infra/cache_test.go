package infra

import (
	"testing"
)

func TestNameCacheKey(t *testing.T) {
	key := "k8s:name:474282bd-1085-4f72-9936-b3c1eb64af15::clusterrolebindings:system:controller:persistent-volume-binder"
	name := "system:controller:persistent-volume-binder"
	query := ParseNameKeyToQuery(key)
	if query.Name != name {
		t.Errorf("Error parse resource name: %s", query.Name)
	}
}

func TestGenNameCacheKey(t *testing.T) {
	target := "k8s:name:fdfcc101-5361-4adf-bf3e-919cdababeb3:kube-system:deployments:kube-dns"
	result := GetKubernetesNameCacheKey("fdfcc101-5361-4adf-bf3e-919cdababeb3", "kube-dns",
		"deployments", "kube-system")
	if result != target {
		t.Errorf("Error gen name key: %s", result)
	}

	target = "k8s:name:fdfcc101-5361-4adf-bf3e-919cdababeb3::clusterroles:system:controller:deployment-controller"
	result = GetKubernetesNameCacheKey("fdfcc101-5361-4adf-bf3e-919cdababeb3",
		"system:controller:deployment-controller", "clusterroles", "")
	if target != result {
		t.Errorf("Error gen name key: %s", result)
	}

}
