package infra

import (
	"errors"
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/config"
	aredis "krobelus/pkg/alauda-redis-client"

	"github.com/alauda/bergamot/diagnose"
	"github.com/sony/gobreaker"
	"gopkg.in/redis.v5"
)

type (
	// RedisCache abstraction used to store a redis connection
	RedisCache struct {
		read  *aredis.RedisClient
		write *aredis.RedisClient
	}

	// RedisOpts abstraction for connection settings
	RedisOpts struct {
		Host     string
		Port     int
		DB       int
		Password string
	}
)

var (
	Redis RedisCache

	RedisCircuitBreaker = gobreaker.NewCircuitBreaker(gobreaker.Settings{
		Name:        "redis",
		MaxRequests: 3,
		Timeout:     common.GetTimeDuration(30),
		Interval:    common.GetTimeDuration(30),
		ReadyToTrip: func(counts gobreaker.Counts) bool {
			return counts.ConsecutiveFailures > 3
		},
	})
)

var (
	ErrUnLockFailed = errors.New("unlock failed")
)

// NewAlaudaRedis constructor based on alauda redis client
func NewAlaudaRedis(opts aredis.RedisClientOptions, writerOpts aredis.RedisClientOptions) (*RedisCache, error) {
	reader := aredis.NewRedisClient(opts)
	writer := reader
	if len(writerOpts.Hosts) > 0 {
		writer = aredis.NewRedisClient(writerOpts)
	}
	client := &RedisCache{
		read:  reader,
		write: writer,
	}
	if err := client.read.Ping().Err(); err != nil {
		return nil, err
	}
	return client, nil
}

// Get gets a value from a key
func (r *RedisCache) Get(key string) (string, error) {
	val, err := r.read.Get(key).Result()
	if err == redis.Nil {
		err = nil
	}
	return val, err
}

// MGet gets multiple values from keys.
func (r *RedisCache) MGet(keys ...string) ([]string, error) {

	var res []string

	if r.read.IsCluster() {
		pipe := r.read.Pipeline()
		for _, k := range keys {
			if err := pipe.Process(r.read.Get(k)); err != nil {
				return nil, err
			}
		}
		cmders, err := pipe.Exec()
		if err != nil {
			return nil, err
		}
		for _, cmder := range cmders {
			result, _ := cmder.(*redis.StringCmd).Result()
			res = append(res, result)
		}
		return res, nil
	}

	vals, err := r.read.MGet(keys...).Result()

	if redis.Nil != err && nil != err {
		return nil, err
	}

	for _, item := range vals {
		res = append(res, fmt.Sprintf("%s", item))
	}

	return res, err
}

// MHSet set a list of HSet
func (r *RedisCache) MHSet(data []map[string]map[string]int, expiration time.Duration) (res []bool, err error) {
	if len(data) == 0 {
		return nil, nil
	}
	pipe := r.write.Pipeline()
	for _, item := range data {
		for key, value := range item {
			for k, v := range value {
				if err = pipe.Process(r.write.HSet(key, k, v)); err != nil {
					return nil, err
				}
			}
			if err = pipe.Process(r.write.Expire(key, expiration)); err != nil {
				return nil, err
			}
		}
	}
	val, err := pipe.Exec()
	if err != nil {
		return nil, err
	}
	for _, item := range val {
		result, _ := item.(*redis.BoolCmd).Result()
		res = append(res, result)
	}
	return res, err
}

// MHVals will get multiple hash values via pipeline.
func (r *RedisCache) MHVals(keys ...string) (res []string, err error) {
	if len(keys) == 0 {
		return
	}
	pipe := r.read.Pipeline()
	for _, key := range keys {
		if err = pipe.Process(r.read.HVals(key)); err != nil {
			return
		}
	}
	val, err := pipe.Exec()
	if err != nil {
		return
	}
	for _, item := range val {
		ss, err := item.(*redis.StringSliceCmd).Result()
		if err != redis.Nil {
			res = append(res, ss...)
		}
	}
	return res, err
}

// Set sets a value to a key
func (r *RedisCache) Set(key string, value interface{}, expiration time.Duration) error {
	return r.write.Set(key, value, expiration).Err()
}

func (r *RedisCache) Lock(key string, value string) (bool, error) {
	result, err := r.write.SetNX(key, value,
		common.GetTimeDuration(config.GlobalConfig.API.DeployTimeout)).Result()
	if err == redis.Nil {
		err = nil
	}
	return result, err
}

func (r *RedisCache) UnLock(key string, value string) error {
	result, err := r.Get(key)
	if err != nil {
		return err
	}
	if result == value {
		_, err := r.Del(key)
		return err
	} else {
		return ErrUnLockFailed
	}
	return nil
}

func (r *RedisCache) Persist(key string) error {
	return r.write.Persist(key).Err()
}

func (r *RedisCache) Exists(key string) (bool, error) {
	result, err := r.read.Exists(key).Result()
	if err == redis.Nil {
		err = nil
	}
	return result, err

}

func (r *RedisCache) Keys(pattern string) ([]string, error) {
	result, err := r.read.Keys(pattern).Result()
	if err == redis.Nil {
		err = nil
	}
	return result, err

}

// HGet will get a field value from a hash key.
func (r *RedisCache) HGet(key string, field string) (string, error) {
	val, err := r.read.HGet(key, field).Result()
	if err == redis.Nil {
		err = nil
	}
	return val, err
}

func (r *RedisCache) HDel(key string, field string) (int64, error) {
	val, err := r.write.HDel(key, field).Result()
	if err == redis.Nil {
		err = nil
	}
	return val, err

}

func (r *RedisCache) HKeys(key string) ([]string, error) {
	ss, err := r.read.HKeys(key).Result()
	if err == redis.Nil {
		err = nil
	}
	return ss, err

}

func (r *RedisCache) HVals(key string) ([]string, error) {
	ss, err := r.read.HVals(key).Result()
	if err == redis.Nil {
		err = nil
	}
	return ss, err

}

func (r *RedisCache) HExists(key string, field string) (bool, error) {
	result, err := r.read.HExists(key, field).Result()
	if err == redis.Nil {
		err = nil
	}
	return result, err

}

// HSet will set a field value to a hash key.
func (r *RedisCache) HSet(key string, field string, value interface{}) error {
	return r.write.HSet(key, field, value).Err()
}

func (r *RedisCache) SAdd(key string, item string) error {
	return r.write.SAdd(key, item).Err()
}

func (r *RedisCache) SMembers(key string) ([]string, error) {
	result, err := r.read.SMembers(key).Result()
	if err == redis.Nil {
		err = nil
	}
	return result, err
}

// Del deletes given number of keys
func (r *RedisCache) Del(key ...string) (int64, error) {
	return r.write.Del(key...).Result()
}

func (r *RedisCache) NoPrefixDel(key ...string) (int64, error) {
	return r.write.NoPrefixDel(key...).Result()
}

// Expire adds an expiration date to a key
func (r *RedisCache) Expire(key string, expiration time.Duration) (bool, error) {
	return r.write.Expire(key, expiration).Result()
}

// Scan scans for keys.
func (r *RedisCache) Scan(cursor uint64, match string, count int64) ([]string, uint64, error) {
	result, rCursor, err := r.read.Scan(cursor, match, count).Result()
	if err == redis.Nil {
		err = nil
	}
	return result, rCursor, err
}

// ScanAll scans for all matched keys.
func (r *RedisCache) ScanAll(match string) ([]string, error) {
	return r.read.ScanAllForEachMaster(match)
}

// Diagnose start diagnose check
// http://confluence.alaudatech.com/pages/viewpage.action?pageId=14123161
func (r *RedisCache) Diagnose() diagnose.ComponentReport {
	var (
		err   error
		start time.Time
	)

	report := diagnose.NewReport("redis")
	start = time.Now()
	err = r.read.Ping().Err()
	report.AddLatency(start)
	report.Check(err, "Redis reader ping failed", "Check environment variables or redis health")
	start = time.Now()
	err = r.write.Ping().Err()
	report.AddLatency(start)
	report.Check(err, "Redis writer ping failed", "Check environment variables or redis health")
	return *report
}

// GetAddr will return a string of the address which is host:port
func (r RedisOpts) GetAddr() string {
	return fmt.Sprintf("%s:%d", r.Host, r.Port)
}

func getRedisReadConfig() aredis.RedisClientOptions {
	clientType := aredis.ClientType(config.GlobalConfig.Redis.TypeReader)
	hosts := make([]string, len(config.GlobalConfig.Redis.HostReader))
	for idx, host := range config.GlobalConfig.Redis.HostReader {
		port := config.GlobalConfig.Redis.PortReader[idx]
		hosts = append(hosts, fmt.Sprintf("%s:%s", host, port))
	}
	//TODO(hang): find out why we need this?
	hosts = common.RemoveEmptyString(hosts)

	return aredis.RedisClientOptions{
		Type:      clientType,
		Hosts:     hosts,
		Database:  config.GlobalConfig.Redis.DBNameReader,
		Password:  config.GlobalConfig.Redis.DBPasswordReader,
		KeyPrefix: config.GlobalConfig.Redis.KeyPrefixReader,
		PoolSize:  config.GlobalConfig.Redis.MaxConnectionsReader,
	}
}

func getRedisWriteConfig() aredis.RedisClientOptions {
	clientType := aredis.ClientType(config.GlobalConfig.Redis.TypeWriter)
	hosts := make([]string, len(config.GlobalConfig.Redis.HostWriter))

	for idx, host := range config.GlobalConfig.Redis.HostWriter {
		port := config.GlobalConfig.Redis.PortWriter[idx]
		hosts = append(hosts, fmt.Sprintf("%s:%s", host, port))
	}
	hosts = common.RemoveEmptyString(hosts)

	return aredis.RedisClientOptions{
		Type:      clientType,
		Hosts:     hosts,
		Database:  config.GlobalConfig.Redis.DBNameWriter,
		Password:  config.GlobalConfig.Redis.DBPasswordWriter,
		KeyPrefix: config.GlobalConfig.Redis.KeyPrefixWriter,
		PoolSize:  config.GlobalConfig.Redis.MaxConnectionsWriter,
	}
}

// GetRedis returns the redis cache instance. a simple singleton.
func GetRedis() (*RedisCache, error) {
	result, err := RedisCircuitBreaker.Execute(func() (interface{}, error) {
		if Redis.read != nil && Redis.write != nil {
			if err := Redis.read.Ping().Err(); err == nil {
				return &Redis, nil
			} else {
				return nil, err
			}

		}
		return NewAlaudaRedis(getRedisReadConfig(), getRedisWriteConfig())
	})
	if err != nil {
		return nil, err
	}

	Redis = *result.(*RedisCache)
	return &Redis, nil
}
