package infra

import (
	"errors"

	log "github.com/sirupsen/logrus"

	"krobelus/common"
	"krobelus/infra/kubernetes"
)

type KubernetesConfig struct {
	Endpoint string
	Token    string
}

type KubeClient struct {
	*kubernetes.KubeResourceClient
	RedisClient *RedisCache
}

// NewKubeClient generate a new KubeClient based on regionID. It need to  retrieve kubernetes cluster
// info from cache or furion.
// also will  add region to watch list
func NewKubeClient(regionID string) (*KubeClient, error) {
	return NewCachedKubeClient(regionID, common.GetLoggerByRegionID(regionID))
}

func NewCachedKubeClient(regionID string, logger common.Log) (*KubeClient, error) {
	if regionID == "" {
		return nil, errors.New("region uuid is empty when generate kube client")
	}
	cfg, err := GetKubernetesConfig(regionID)
	if err != nil {
		return nil, err
	}

	redis, err := GetRedis()
	if err != nil {
		logger.Errorf("Get redis error: %s", err.Error())
	}

	client, err := kubernetes.NewKubeResourceClient(regionID, cfg.Endpoint, cfg.Token, logger)
	if err != nil {
		return nil, err
	}
	AddRegionForWatch(regionID)
	return &KubeClient{
		KubeResourceClient: client,
		RedisClient:        redis,
	}, nil
}

// InvalidateApiDiscoveryCache invalidates the discovery cache of all kubernetes regions.
func InvalidateApiDiscoveryCache() error {
	regions, err := listKubernetesRegionsFromFurion()
	if err != nil {
		return err
	}
	for r, cfg := range regions {
		log.Infof("Updating api discovery cache for region: %v", r)
		c, err := kubernetes.NewKubeResourceClient(r, cfg.Endpoint, cfg.Token, common.GetLoggerByRegionID(r))
		if err != nil {
			log.Errorf("Update api discovery cache for region: %v error %v", r, err)
			continue
		}
		c.InvalidateApiDiscoveryCache()
	}
	return nil
}

//TODO: refactor above to use this function
func InvalidateRegionApiDiscoveryCache(cluster string) error {
	cfg, err := GetKubernetesConfig(cluster)
	if err != nil {
		return err
	}
	c, err := kubernetes.NewKubeResourceClient(cluster, cfg.Endpoint, cfg.Token, common.GetLoggerByRegionID(cluster))
	if err != nil {
		return err
	}
	c.InvalidateApiDiscoveryCache()
	return nil
}
