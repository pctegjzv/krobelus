package kubernetes

import (
	"krobelus/common"
	"krobelus/config"
	appScheme "krobelus/pkg/application/scheme"
	scScheme "krobelus/pkg/servicecatalog/clientset_generated/clientset/scheme"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/watch"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
)

func init() {
	// Aggregate ServiceCatalog scheme to enable decode with ServiceCatalog resources.
	scScheme.AddToScheme(scheme.Scheme)
	appScheme.AddToScheme(scheme.Scheme)
}

type KubeResourceClient struct {
	restMapper *CachedRESTMapper
	Timeout    int
	Endpoint   string
	Token      string
	RegionID   string
	Logger     common.Log
}

const (
	ResourceNamespaceParam = "namespace"
)

// NewKubeClient generate a new KubeClient based on regionID. It need to  retrieve kubernetes cluster
// info from cache or furion.
func NewKubeResourceClient(regionID, endpoint, token string, log common.Log) (*KubeResourceClient, error) {
	cfg := &rest.Config{
		Host:        endpoint,
		BearerToken: token,
		Timeout:     common.GetTimeDuration(config.GlobalConfig.Kubernetes.Timeout),
	}
	cfg.Insecure = true
	resetMapper, err := NewCachedRESTMapper(*cfg, regionID)
	if err != nil {
		return nil, err
	}
	return &KubeResourceClient{
		restMapper: resetMapper,
		Timeout:    config.GlobalConfig.Kubernetes.Timeout,
		Endpoint:   endpoint,
		Token:      token,
		RegionID:   regionID,
		Logger:     log,
	}, nil
}

// ResourceTypeForGVK returns resource type for a gvk.
func (c *KubeResourceClient) ResourceTypeForGVK(gvk schema.GroupVersionKind) (string, error) {
	return c.restMapper.ResourceNameForGVK(gvk)
}

// ServerResources returns server resources list.
func (c *KubeResourceClient) ServerResources() ([]*metaV1.APIResourceList, error) {
	return c.restMapper.ServerResources()
}

// ServerPreferredResources returns server preferred resources list.
func (c *KubeResourceClient) ServerPreferredResources() ([]*metaV1.APIResourceList, error) {
	return c.restMapper.ServerPreferredResources()
}

// ClientForGroupVersionResource returns a REST client by groupVersion and resource.
func (c *KubeResourceClient) ClientForGroupVersionResource(groupVersion string, resourceType string) (rest.Interface, error) {
	gv, err := schema.ParseGroupVersion(groupVersion)
	if err != nil {
		return nil, err
	}
	return c.clientForGroupVersionResource(gv.WithResource(resourceType))
}

// clientForGroupVersionResource returns a REST client by group, version and resource.
func (c *KubeResourceClient) ClientForResource(resourceType string) (rest.Interface, error) {
	return c.clientForGroupVersionResource(schema.GroupVersionResource{Resource: resourceType})
}

func (c *KubeResourceClient) RestClientConfig(resourceType string) (*rest.Config, error) {

	gvr := schema.GroupVersionResource{Resource: resourceType}
	clientCfg, err := c.restMapper.ConfigForResource(gvr)
	if err != nil {
		if err == ErrorResourceKindNotFound {
			c.Logger.Warnf("Found no kind for resource '%v'", gvr)
		}
		return nil, err
	}
	return &clientCfg, nil
}

// clientForGroupVersionResource returns a REST client by group, version and resource.
func (c *KubeResourceClient) clientForGroupVersionResource(gvr schema.GroupVersionResource) (rest.Interface, error) {

	clientCfg, err := c.restMapper.ConfigForResource(gvr)
	if err != nil {
		if err == ErrorResourceKindNotFound {
			c.Logger.Warnf("Found no kind for resource '%v'", gvr)
		}
		return nil, err
	}

	return rest.RESTClientFor(&clientCfg)
}

// extractRequestParams extracts params map to the request.
// The 'params' now supports the following params:
// - namespace: specify the namespace of the request.
func (c *KubeResourceClient) extractRequestParams(request *rest.Request, params map[string]interface{}) *rest.Request {
	for key, obj := range params {
		switch key {
		case "namespace":
			request.Namespace(obj.(string))
			break
		}
	}
	return request
}

// CreateResource creates a k8s resource. The created resource object will be output to the 'outResult' arg.
func (c *KubeResourceClient) CreateResource(resourceName string, resourceObj interface{}, params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Post().
		Resource(resourceName).
		Body(resourceObj)

	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=POST url=%s", request.URL().String())
	return request.Do().Into(outResult)
}

// GetResource gets a k8s resource. The resource object will be output to the 'outResult' arg.
func (c *KubeResourceClient) GetResource(resourceName string, name string, options *metaV1.GetOptions, params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Get().
		Resource(resourceName).
		Name(name).
		VersionedParams(options, scheme.ParameterCodec)

	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=GET url=%s", request.URL().String())
	return request.Do().Into(outResult)
}

// ListResource lists resources by options, the result will be output to the 'outResult' arg.
func (c *KubeResourceClient) ListResource(resourceName string, options *metaV1.ListOptions, params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Get().
		Resource(resourceName).
		VersionedParams(options, scheme.ParameterCodec)
	return c.extractRequestParams(request, params).Do().Into(outResult)
}

// WatchResource watches the resource by options.
func (c *KubeResourceClient) WatchResource(resourceName string, options *metaV1.ListOptions, params map[string]interface{}) (watch.Interface, error) {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return nil, err
	}
	options.Watch = true
	request := client.Get().
		Resource(resourceName).
		VersionedParams(options, scheme.ParameterCodec)
	return c.extractRequestParams(request, params).Watch()
}

// UpdateResource updates the resource by options, and output the updated resource to the 'outResult' arg.
func (c *KubeResourceClient) UpdateResource(resourceName string, name string, resourceObj interface{}, params map[string]interface{}, outResult runtime.Object) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Put().
		Resource(resourceName).
		Name(name).
		Body(resourceObj)
	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=UPDATE url=%s", request.URL().String())
	return request.Do().Into(outResult)
}

// PatchResource patches the resource by options, and output the patched resource to the 'outResult' arg.
func (c *KubeResourceClient) PatchResource(resourceName string, name string, pt types.PatchType, data []byte, params map[string]interface{}, outResult runtime.Object, subresources ...string) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Put().
		Resource(resourceName).
		SubResource(subresources...).
		Name(name).
		Body(data)
	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=PATCH url=%s", request.URL().String())
	return request.Do().Into(outResult)
}

// PatchPatchResource patches the resource by options, it's the real patch, patch patch.
func (c *KubeResourceClient) PatchPatchResource(resourceName string, name string, pt types.PatchType, data []byte, params map[string]interface{}, outResult runtime.Object, subresources ...string) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Patch(pt).
		Resource(resourceName).
		SubResource(subresources...).
		Name(name).
		Body(data)
	request = c.extractRequestParams(request, params)
	c.Logger.Debugf("Request kubernetes: method=PATCH PATCH url=%s data=%s", request.URL().String(), data)
	return request.Do().Into(outResult)
}

// DeleteResource deletes a resource by options.
func (c *KubeResourceClient) DeleteResource(resourceName string, name string, options *metaV1.DeleteOptions, params map[string]interface{}) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Delete().
		Resource(resourceName).
		Name(name).
		Body(options)
	return c.extractRequestParams(request, params).Do().Error()
}

// DeleteResourceCollection deletes resource collection by options.
func (c *KubeResourceClient) DeleteResourceCollection(resourceName string, options *metaV1.DeleteOptions, listOptions *metaV1.ListOptions, params map[string]interface{}) error {
	client, err := c.ClientForResource(resourceName)
	if err != nil {
		return err
	}
	request := client.Delete().
		Resource(resourceName).
		VersionedParams(listOptions, scheme.ParameterCodec).
		Body(options)
	return c.extractRequestParams(request, params).Do().Error()
}

// InvalidateApiDiscoveryCache invalidates the discovery cache.
func (c *KubeResourceClient) InvalidateApiDiscoveryCache() {
	c.restMapper.Reset()
}
