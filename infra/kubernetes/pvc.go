package kubernetes

import (
	"krobelus/model"

	"k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// configmaps
func (c *KubeResourceClient) CreateV1PVC(resource *model.KubernetesObject) (*v1.PersistentVolumeClaim, error) {
	request, err := resource.ToV1PVC()
	if err != nil {
		return nil, err
	}
	result := &v1.PersistentVolumeClaim{}
	err = c.CreateResource("persistentvolumeclaims", request,
		map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	if k8sErrors.IsAlreadyExists(err) {
		c.Logger.Warnf("pvc [%s/%s] already exist, replace it", request.Namespace, request.Name)
		err = c.UpdateResource("persistentvolumeclaims", request.Name, request,
			map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	}
	return result, err
}

func (c *KubeResourceClient) DeleteV1PVC(namespace string, name string) error {
	if err := c.DeleteResource("persistentvolumeclaims", name, &metaV1.DeleteOptions{},
		map[string]interface{}{ResourceNamespaceParam: namespace}); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("pvc [%s/%s] not found when delete, ignore", namespace, name)
		} else {
			return err
		}
	}
	return nil
}

func (c *KubeResourceClient) UpdateV1PVC(resource *model.KubernetesObject) (*v1.PersistentVolumeClaim, error) {
	request, err := resource.ToV1PVC()
	if err != nil {
		return nil, err
	}
	result := &v1.PersistentVolumeClaim{}
	err = c.UpdateResource("persistentvolumeclaims", request.Name, request,
		map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	return result, err
}
