package kubernetes

import (
	"krobelus/model"
	scbetav1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubeResourceClient) GetClusterServiceBroker(name string) (*scbetav1.ClusterServiceBroker, error) {
	result := &scbetav1.ClusterServiceBroker{}
	if err := c.GetResource("clusterservicebrokers", name, &metaV1.GetOptions{}, nil, result); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("broker %s/%s not found", name)
		}
		return nil, err
	}
	return result, nil
}

func (c *KubeResourceClient) CreateClusterServiceBroker(resource *model.KubernetesObject) (*scbetav1.ClusterServiceBroker, error) {
	request, err := resource.ToBroker()
	if err != nil {
		return nil, err
	}

	result := &scbetav1.ClusterServiceBroker{}
	err = c.CreateResource("clusterservicebrokers", request, nil, result)
	if err != nil {
		return nil, err
	}

	if k8sErrors.IsAlreadyExists(err) {
		c.Logger.Warnf("broker %s already exist, replace it", request.Name)

		err = c.UpdateResource("clusterservicebrokers", request.Name, request, nil, result)
	}
	return result, err
}

func (c *KubeResourceClient) UpdateClusterServiceBroker(resource *model.KubernetesObject) (*scbetav1.ClusterServiceBroker, error) {
	request, err := resource.ToBroker()
	if err != nil {
		return nil, err
	}
	result := &scbetav1.ClusterServiceBroker{}
	err = c.UpdateResource("clusterservicebrokers", request.Name, request, nil, result)
	return result, err
}

func (c *KubeResourceClient) DeleteClusterServiceBroker(name string) error {
	if err := c.DeleteResource("clusterservicebrokers", name, &metaV1.DeleteOptions{}, nil); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("broker %s/%s not found when delete, ignore", name)
		} else {
			return err
		}
	}
	return nil
}
