package kubernetes

import (
	scbetav1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubeResourceClient) GetClusterServiceClasses(listOptions *metaV1.ListOptions) ([]*scbetav1.ClusterServiceClass, error) {
	result := &scbetav1.ClusterServiceClassList{}
	err := c.ListResource("clusterserviceClasses", listOptions, nil, result)
	if err != nil {
		return nil, err
	}

	Classes := make([]*scbetav1.ClusterServiceClass, len(result.Items))
	for index, item := range result.Items {
		Classes[index] = &item
	}
	return Classes, nil
}

func (c *KubeResourceClient) GetClusterServiceClass(name string) (*scbetav1.ClusterServiceClass, error) {
	result := &scbetav1.ClusterServiceClass{}
	if err := c.GetResource("clusterserviceClasses", name, &metaV1.GetOptions{}, nil, result); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("broker %s/%s not found", name)
		}
		return nil, err
	}
	return result, nil
}
