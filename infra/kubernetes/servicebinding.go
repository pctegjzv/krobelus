package kubernetes

import (
	"krobelus/model"
	scbetav1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubeResourceClient) GetServiceBindings(listOptions *metaV1.ListOptions, namespace string) ([]scbetav1.ServiceBinding, error) {
	result := &scbetav1.ServiceBindingList{}
	err := c.ListResource("ServiceBindings", listOptions, map[string]interface{}{ResourceNamespaceParam: namespace}, result)
	if err != nil {
		return nil, err
	}
	c.Logger.Debugf("result.Items %v", result.Items)

	return result.Items, nil
}

func (c *KubeResourceClient) GetServiceBinding(name string, namespace string) (*scbetav1.ServiceBinding, error) {
	result := &scbetav1.ServiceBinding{}
	if err := c.GetResource("ServiceBindings", name, &metaV1.GetOptions{}, map[string]interface{}{ResourceNamespaceParam: namespace}, result); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("service-binding %s/%s not found, ignore", name)
		}
		return nil, err
	}
	return result, nil
}

func (c *KubeResourceClient) CreateServiceBinding(resource *model.KubernetesObject) (*scbetav1.ServiceBinding, error) {
	request, err := resource.ToServiceBinding()
	if err != nil {
		return nil, err
	}
	result := &scbetav1.ServiceBinding{}
	err = c.CreateResource("servicebindings", request,
		map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	if k8sErrors.IsAlreadyExists(err) {
		c.Logger.Warnf("ServiceBinding %s already exist, replace it", request.Name)
		c.UpdateResource("servicebindings", request.Name, request,
			map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	}
	return result, err
}

func (c *KubeResourceClient) UpdateServiceBinding(resource *model.KubernetesObject) (*scbetav1.ServiceBinding, error) {
	request, err := resource.ToServiceBinding()
	if err != nil {
		return nil, err
	}
	result := &scbetav1.ServiceBinding{}
	err = c.UpdateResource("servicebindings", request.Name, request,
		map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	return result, err

}

func (c *KubeResourceClient) DeleteServiceBinding(namespace string, name string) error {
	if err := c.DeleteResource("servicebindings", name, &metaV1.DeleteOptions{},
		map[string]interface{}{ResourceNamespaceParam: namespace}); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("ServiceBinding %s/%s not found when delete, ignore", name)
		} else {
			return err
		}
	}
	return nil
}
