package kubernetes

import (
	"krobelus/model"
	scbetav1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubeResourceClient) GetServiceInstances(listOptions *metaV1.ListOptions, namespace string) ([]*scbetav1.ServiceInstance, error) {
	result := &scbetav1.ServiceInstanceList{}
	err := c.ListResource("ServiceInstances", listOptions, map[string]interface{}{ResourceNamespaceParam: namespace}, result)
	if err != nil {
		return nil, err
	}

	bindings := make([]*scbetav1.ServiceInstance, len(result.Items))
	for index, item := range result.Items {
		bindings[index] = &item
	}
	return bindings, nil
}

func (c *KubeResourceClient) GetServiceInstance(name string, namespace string) (*scbetav1.ServiceInstance, error) {
	result := &scbetav1.ServiceInstance{}
	if err := c.GetResource("ServiceInstances", name, &metaV1.GetOptions{}, map[string]interface{}{ResourceNamespaceParam: namespace}, result); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("service-instance %s/%s not found, ignore", name)
		}
		return nil, err
	}
	return result, nil
}

func (c *KubeResourceClient) CreateServiceInstance(resource *model.KubernetesObject) (*scbetav1.ServiceInstance, error) {
	request, err := resource.ToServiceInstance()
	if err != nil {
		return nil, err
	}
	result := &scbetav1.ServiceInstance{}
	err = c.CreateResource("serviceinstances", request,
		map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	if k8sErrors.IsAlreadyExists(err) {
		c.Logger.Warnf("ServiceInstance %s already exist, replace it", request.Name)
		c.UpdateResource("serviceinstances", request.Name, request,
			map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	}
	return result, err
}

func (c *KubeResourceClient) UpdateServiceInstance(resource *model.KubernetesObject) (*scbetav1.ServiceInstance, error) {
	request, err := resource.ToServiceInstance()
	if err != nil {
		return nil, err
	}
	result := &scbetav1.ServiceInstance{}
	c.UpdateResource("serviceinstances", request.Name, request,
		map[string]interface{}{ResourceNamespaceParam: resource.Namespace}, result)
	return result, err

}

func (c *KubeResourceClient) DeleteServiceInstance(namespace string, name string) error {
	c.Logger.Debugf("ServiceInstance %s/%s start delete", namespace, name)
	if err := c.DeleteResource("serviceinstances", name, &metaV1.DeleteOptions{},
		map[string]interface{}{ResourceNamespaceParam: namespace}); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("ServiceInstance %s/%s not found when delete, ignore", name)
		} else {
			return err
		}
	}
	return nil
}
