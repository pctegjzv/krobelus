package kubernetes

import (
	scbetav1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubeResourceClient) GetClusterServicePlans(listOptions *metaV1.ListOptions) ([]scbetav1.ClusterServicePlan, error) {
	result := &scbetav1.ClusterServicePlanList{}
	err := c.ListResource("clusterserviceplans", listOptions, nil, result)
	if err != nil {
		return nil, err
	}
	return result.Items, nil
}

func (c *KubeResourceClient) GetClusterServicePlan(name string) (*scbetav1.ClusterServicePlan, error) {
	result := &scbetav1.ClusterServicePlan{}
	if err := c.GetResource("clusterserviceplans", name, &metaV1.GetOptions{}, nil, result); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("broker %s/%s not found", name)
		}
		return nil, err
	}
	return result, nil
}
