package kubernetes

import (
	// "krobelus/model"
	"k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (c *KubeResourceClient) CreateV1Secret(request *v1.Secret, namespace string) (*v1.Secret, error) {
	result := &v1.Secret{}
	err := c.CreateResource("secrets", request,
		map[string]interface{}{ResourceNamespaceParam: namespace}, result)
	if k8sErrors.IsAlreadyExists(err) {
		c.Logger.Warnf("secret %s/%s already exist, replace it", namespace, request.Name)
		err = c.UpdateResource("secrets", request.Name, request,
			map[string]interface{}{ResourceNamespaceParam: namespace}, result)
	}
	return result, err
}

func (c *KubeResourceClient) UpdateV1Secret(request *v1.Secret, namespace string) (*v1.Secret, error) {
	result := &v1.Secret{}
	err := c.UpdateResource("secrets", request.Name, request,
		map[string]interface{}{ResourceNamespaceParam: namespace}, result)
	if k8sErrors.IsNotFound(err) {
		return nil, err
	}
	return result, err
}

func (c *KubeResourceClient) GetV1Secret(name string, namespace string) (*v1.Secret, error) {
	result := &v1.Secret{}
	err := c.GetResource("secrets", name, &metaV1.GetOptions{},
		map[string]interface{}{ResourceNamespaceParam: namespace}, result)
	if err != nil {
		return nil, err
	}
	return result, err
}

// secrets
// func (c *KubeResourceClient) CreateV1Secret(resource *model.KubernetesObject) (*v1.Secret, error) {
// 	request, err := resource.ToSecret()
// 	if err != nil {
// 		return nil, err
// 	}
// 	result, err := c.Client.CoreV1().Secrets(resource.Namespace).Create(request)
// 	if k8sErrors.IsAlreadyExists(err) {
// 		c.Logger.Warnf("secret %s/%s already exist, replace it", request.Namespace, request.Name)
// 		return c.Client.CoreV1().Secrets(request.Namespace).Update(request)
// 	}
// 	return result, err
// }

func (c *KubeResourceClient) DeleteV1Secret(namespace string, name string) error {
	if err := c.DeleteResource("secrets", name, &metaV1.DeleteOptions{},
		map[string]interface{}{ResourceNamespaceParam: namespace}); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("secret %s/%s not found when delete, ignore", namespace, name)
		} else {
			return err
		}
	}
	return nil
}
