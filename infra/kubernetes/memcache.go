package kubernetes

import (
	"errors"
	"fmt"
	"sync"

	"github.com/googleapis/gnostic/OpenAPIv2"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	"k8s.io/apimachinery/pkg/version"
	"k8s.io/client-go/discovery"
	restclient "k8s.io/client-go/rest"
)

type discoveryMemCache struct {
	sync.RWMutex
	groupToServerResources map[string]*metav1.APIResourceList
	groupList              *metav1.APIGroupList
}

func (d *discoveryMemCache) getResources(groupVersion string) *metav1.APIResourceList {
	d.RLock()
	defer d.RUnlock()
	return d.groupToServerResources[groupVersion]
}

func (d *discoveryMemCache) updateResources(groupVersion string, resources *metav1.APIResourceList) {
	d.Lock()
	defer d.Unlock()
	d.groupToServerResources[groupVersion] = resources
}

type clusterDiscoveryCaches struct {
	sync.RWMutex
	caches map[string]*discoveryMemCache
}

func (c *clusterDiscoveryCaches) updateClusterCache(cluster string, cache *discoveryMemCache) {
	c.Lock()
	defer c.Unlock()
	c.caches[cluster] = cache
}

func (c *clusterDiscoveryCaches) getClusterCache(cluster string) *discoveryMemCache {
	c.RLock()
	defer c.RUnlock()
	return c.caches[cluster]
}

type memCacheClient struct {
	delegate discovery.DiscoveryInterface
	cluster  string
}

var (
	ErrCacheEmpty    = errors.New("the cache has not been filled yet")
	ErrCacheNotFound = errors.New("not found")
)

var clusterCache = &clusterDiscoveryCaches{
	caches: map[string]*discoveryMemCache{},
}
var _ discovery.CachedDiscoveryInterface = &memCacheClient{}

// ServerResourcesForGroupVersion returns the supported resources for a group and version.
func (d *memCacheClient) ServerResourcesForGroupVersion(groupVersion string) (*metav1.APIResourceList, error) {
	getResources := func() (*metav1.APIResourceList, error) {
		c := clusterCache.getClusterCache(d.cluster)
		if c == nil {
			return nil, ErrCacheNotFound
		}
		rl := c.getResources(groupVersion)
		if rl == nil {
			return nil, ErrCacheNotFound
		}
		return rl, nil
	}

	rl, err := getResources()
	if err == nil {
		return rl, nil
	}

	// Try to update the cache
	d.Invalidate()
	return getResources()
}

func (d *memCacheClient) getServerResources(groupVersion string) (*metav1.APIResourceList, error) {
	r, err := d.delegate.ServerResourcesForGroupVersion(groupVersion)
	if err != nil || len(r.APIResources) == 0 {
		return nil, fmt.Errorf("couldn't get resource list for %v in cluster %s: %v", groupVersion, d.cluster, err)
	}
	return r, nil
}

// ServerResources returns the supported resources for all groups and versions.
func (d *memCacheClient) ServerResources() ([]*metav1.APIResourceList, error) {
	apiGroups, err := d.ServerGroups()
	if err != nil {
		return nil, err
	}
	groupVersions := metav1.ExtractGroupVersions(apiGroups)
	result := []*metav1.APIResourceList{}
	for _, groupVersion := range groupVersions {
		resources, err := d.ServerResourcesForGroupVersion(groupVersion)
		if err != nil {
			return nil, err
		}
		result = append(result, resources)
	}
	return result, nil
}

func (d *memCacheClient) ServerGroups() (*metav1.APIGroupList, error) {
	c := clusterCache.getClusterCache(d.cluster)
	if c != nil {
		return c.groupList, nil
	}
	d.Invalidate()
	c = clusterCache.getClusterCache(d.cluster)
	if c != nil {
		return c.groupList, nil
	}
	return nil, fmt.Errorf("couldn't get group list from cache in cluster %s", d.cluster)
}

func (d *memCacheClient) getServerGroups() (*metav1.APIGroupList, error) {
	gl, err := d.delegate.ServerGroups()
	if err != nil || len(gl.Groups) == 0 {
		return nil, fmt.Errorf("couldn't get current server API group list in cluster %s. (%v)", d.cluster, err)
	}
	return gl, nil
}

func (d *memCacheClient) RESTClient() restclient.Interface {
	return d.delegate.RESTClient()
}

// TODO: Should this also be cached? The results seem more likely to be
// inconsistent with ServerGroups and ServerResources given the requirement to
// actively Invalidate.
func (d *memCacheClient) ServerPreferredResources() ([]*metav1.APIResourceList, error) {
	return d.delegate.ServerPreferredResources()
}

// TODO: Should this also be cached? The results seem more likely to be
// inconsistent with ServerGroups and ServerResources given the requirement to
// actively Invalidate.
func (d *memCacheClient) ServerPreferredNamespacedResources() ([]*metav1.APIResourceList, error) {
	return d.delegate.ServerPreferredNamespacedResources()
}

func (d *memCacheClient) ServerVersion() (*version.Info, error) {
	return d.delegate.ServerVersion()
}

func (d *memCacheClient) OpenAPISchema() (*openapi_v2.Document, error) {
	return d.delegate.OpenAPISchema()
}

func (d *memCacheClient) Fresh() bool {
	return true
}

// Invalidate refreshes the cache, blocking calls until the cache has been
// refreshed. It would be trivial to make a version that does this in the
// background while continuing to respond to requests if needed.
func (d *memCacheClient) Invalidate() {
	gl, err := d.delegate.ServerGroups()
	if err != nil || len(gl.Groups) == 0 {
		utilruntime.HandleError(fmt.Errorf("couldn't get current server API group list in cluster %s; will keep using cached value. (%v)", d.cluster, err))
		return
	}

	rl := map[string]*metav1.APIResourceList{}
	for _, g := range gl.Groups {
		for _, v := range g.Versions {
			r, err := d.delegate.ServerResourcesForGroupVersion(v.GroupVersion)
			if err != nil || len(r.APIResources) == 0 {
				utilruntime.HandleError(fmt.Errorf("couldn't get resource list for %v in cluster %s: %v", v.GroupVersion, d.cluster, err))
				continue
			}
			rl[v.GroupVersion] = r
		}
	}

	clusterCache.updateClusterCache(d.cluster, &discoveryMemCache{
		groupToServerResources: rl,
		groupList:              gl,
	})
}

// NewMemCacheClient creates a new CachedDiscoveryInterface which caches
// discovery information in memory and will stay up-to-date if Invalidate is
// called with regularity.
func NewMemCacheClient(delegate discovery.DiscoveryInterface, cluster string) discovery.CachedDiscoveryInterface {
	return &memCacheClient{
		delegate: delegate,
		cluster:  cluster,
	}
}
