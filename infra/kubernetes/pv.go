package kubernetes

import (
	"k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// CreateV1PV ..
func (c *KubeResourceClient) CreateV1PV(resource *v1.PersistentVolume) (*v1.PersistentVolume, error) {
	result := &v1.PersistentVolume{}
	err := c.CreateResource("persistentvolumes", resource, nil, result)
	if k8sErrors.IsAlreadyExists(err) {
		c.Logger.Warnf("pv [%s] already exist, replace it", resource.ObjectMeta.Name)
		err = c.UpdateResource("persistentvolumes", resource.ObjectMeta.Name, resource, nil, result)
	}
	return result, err
}

func (c *KubeResourceClient) UpdateV1PV(resource *v1.PersistentVolume) (*v1.PersistentVolume, error) {
	result := &v1.PersistentVolume{}
	err := c.UpdateResource("persistentvolumes", resource.ObjectMeta.Name, resource, nil, result)
	return result, err
}

// DeleteV1PV ..
func (c *KubeResourceClient) DeleteV1PV(name string) error {
	if err := c.DeleteResource("persistentvolumes", name, &metaV1.DeleteOptions{}, nil); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("pv %s not found when delete, ignore", name)
		} else {
			return err
		}
	}
	return nil
}
