package kubernetes

import (
	"krobelus/model"

	"k8s.io/api/core/v1"
	k8sErrors "k8s.io/apimachinery/pkg/api/errors"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// configmaps
func (c *KubeResourceClient) CreateV1ConfigMap(resource *model.KubernetesObject) (*v1.ConfigMap, error) {
	request, err := resource.ToConfigMap()
	if err != nil {
		return nil, err
	}

	result := &v1.ConfigMap{}
	err = c.CreateResource("configmaps", request,
		map[string]interface{}{
			ResourceNamespaceParam: request.Namespace,
		}, result)
	if err != nil {
		return nil, err
	}

	if k8sErrors.IsAlreadyExists(err) {
		c.Logger.Warnf("configmap %s/%s already exist, replace it", request.Namespace, request.Name)

		err = c.UpdateResource("configmaps", request.Name, request,
			map[string]interface{}{
				ResourceNamespaceParam: request.Namespace,
			}, result)
	}
	return result, err
}

func (c *KubeResourceClient) DeleteV1ConfigMap(namespace string, name string) error {
	if err := c.DeleteResource("configmaps", name, &metaV1.DeleteOptions{},
		map[string]interface{}{ResourceNamespaceParam: namespace}); err != nil {
		if k8sErrors.IsNotFound(err) {
			c.Logger.Warnf("configmap %s/%s not found when delete, ignore", namespace, name)
		} else {
			return err
		}
	}
	return nil
}

func (c *KubeResourceClient) UpdateV1ConfigMap(resource *model.KubernetesObject) (*v1.ConfigMap, error) {
	request, err := resource.ToConfigMap()
	if err != nil {
		return nil, err
	}

	result := &v1.ConfigMap{}
	err = c.UpdateResource("configmaps", request.Name, request,
		map[string]interface{}{
			ResourceNamespaceParam: request.Namespace,
		}, result)
	return result, err
}
