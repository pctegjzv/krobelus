package infra

import (
	"fmt"
	"strings"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/model"

	"github.com/Jeffail/gabs"
	"github.com/juju/errors"
	"github.com/levigross/grequests"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
)

const (
	RegionCacheExpireTime = 5 * 60
	RegionCachePrefix     = "region"
)

// region info stuff
const (
	// RegionPlatformV2 is the oldest platform version, support old k8s
	RegionPlatformV2 = "v2"
	// RegionPlatformV3 support new k8s
	RegionPlatformV3 = "v3"
	// RegionPlatformV4 support new region info and new k8s
	RegionPlatformV4 = "v4"
)

func getRegionCacheKey(regionID string) string {
	return fmt.Sprintf("%s:%s", RegionCachePrefix, regionID)
}

func getRegionInfoFromCache(regionID string) (string, error) {
	key := getRegionCacheKey(regionID)
	redis, err := GetRedis()
	if err != nil {
		return "", err
	}
	regionStr, err := redis.Get(key)
	if err != nil {
		return "", err
	}
	if regionStr == "" {
		return "", errors.New(fmt.Sprintf("Empty region info found from cache: %s", regionID))
	}
	return regionStr, nil

}

func listKubernetesRegionsFromFurion() (map[string]*KubernetesConfig, error) {
	cfg := config.GlobalConfig.Furion
	url := fmt.Sprintf("%s/%s/regions", cfg.Endpoint, cfg.ApiVersion)
	resp, err := grequests.Get(url, &grequests.RequestOptions{
		RequestTimeout: time.Duration(cfg.Timeout) * time.Second})
	if err != nil {
		return nil, err
	}

	regionStr := resp.String()
	if regionStr == "" {
		return nil, errors.New("empty regions found from furion")
	}

	regionListData, err := gabs.ParseJSON([]byte(regionStr))
	if err != nil {
		return nil, err
	}

	children, err := regionListData.Children()
	if err != nil {
		return nil, err
	}

	var regions = map[string]*KubernetesConfig{}

	for _, child := range children {
		data, err := child.ChildrenMap()
		if err != nil {
			return nil, err
		}
		containerManager := data["container_manager"].Data().(string)
		if isKubernetesRegion(containerManager) {
			platformVersion := data["platform_version"].Data().(string)
			if strings.Compare(platformVersion, RegionPlatformV2) == 1 {
				uuid := data["id"].Data().(string)
				kubeConfig, err := getKubernetesConfig(child.String())
				if err != nil {
					logrus.WithError(err).Errorf("parse region info error: %s", uuid)
				} else {
					regions[uuid] = kubeConfig
				}
			}

		}

	}

	return regions, nil
}

func getRegionInfoFromFurion(regionID string) (string, error) {
	cfg := config.GlobalConfig.Furion
	pathPrefix := fmt.Sprintf("%s/%s/regions", cfg.Endpoint, cfg.ApiVersion)
	url := fmt.Sprintf("%s/%s", pathPrefix, regionID)
	resp, err := grequests.Get(url, &grequests.RequestOptions{
		RequestTimeout: time.Duration(cfg.Timeout) * time.Second})
	if err != nil {
		return "", err
	}

	data := resp.String()
	if resp.StatusCode != 200 {
		return "", errors.New(fmt.Sprintf("Retrieve region get: %d %s", resp.StatusCode, data))
	}

	if data == "" {
		return "", errors.New(fmt.Sprintf("Empty region info found from furion: %s", regionID))
	}
	return data, nil
}

func getKubernetesConfig(regionStr string) (*KubernetesConfig, error) {
	platformVersion, err := getPlatformVersion(regionStr)
	if err != nil {
		return nil, err
	}
	var keys []string
	if platformVersion == RegionPlatformV3 {
		keys = []string{
			"features.kubernetes.endpoint",
			"features.kubernetes.token",
		}
	} else if platformVersion == RegionPlatformV4 {
		keys = []string{
			"attr.kubernetes.endpoint",
			"attr.kubernetes.token",
		}
	} else {
		return nil, fmt.Errorf("unsupported platform version for region: %s", platformVersion)
	}
	data, err := getPaths(regionStr, keys)
	if err != nil {
		return nil, err
	}
	return &KubernetesConfig{
		Endpoint: data[0],
		Token:    data[1],
	}, nil

}

func getResourceRatio(region string) *model.ResourceRatio {
	keys := []string{
		"features.kubernetes.docker_resource_ratio.mem",
		"features.kubernetes.docker_resource_ratio.cpu",
	}

	defaultRatio := &model.ResourceRatio{
		Cpu: 1,
		Mem: 1,
	}
	results, err := getPaths(region, keys)
	if err != nil {
		logrus.WithError(err).Errorf("get data error")
		return defaultRatio
	}
	cpu, err := cast.ToIntE(results[1])
	if err != nil {
		logrus.WithError(err).Errorf("Parse cpu error")
		return defaultRatio
	}
	mem, err := cast.ToIntE(results[0])
	if err != nil {
		logrus.WithError(err).Errorf("Parse mem error")
		return defaultRatio
	}
	return &model.ResourceRatio{
		Cpu: cpu,
		Mem: mem,
	}
}

func getPlatformVersion(region string) (string, error) {
	return getPath(region, "platform_version")
}

// getPath retrieve single value from a json string
// support string/number data
func getPath(region string, path string) (string, error) {
	jsonData, err := gabs.ParseJSON([]byte(region))
	if err != nil {
		return "", err
	}
	return getPathFromJson(jsonData, path)
}

func getPathFromJson(jsonData *gabs.Container, path string) (string, error) {
	result := jsonData.Path(path).Data()
	value, ok := result.(string)
	if ok {
		return value, nil
	}
	number, ok := result.(float64)
	if ok {
		return cast.ToString(number), nil
	} else {
		return "", fmt.Errorf("get path %s data for region error", path)
	}
}

func getPaths(region string, paths []string) ([]string, error) {
	jsonData, err := gabs.ParseJSON([]byte(region))
	if err != nil {
		return nil, err
	}

	var results []string
	for _, path := range paths {
		value, err := getPathFromJson(jsonData, path)
		if err != nil {
			return nil, err
		}
		results = append(results, value)
	}
	return results, nil
}

func getKubernetesVersion(region string) (string, error) {
	path := "features.kubernetes.version"
	platformVersion, err := getPlatformVersion(region)
	if err != nil {
		return "", err
	}
	if platformVersion == RegionPlatformV4 {
		path = "attr.kubernetes.version"
	}

	return getPath(region, path)
}

func getRegionInfo(regionID string) (string, error) {
	if regionID == "" {
		return "", errors.New("empty region uuid when retrieve region info")
	}
	logger := logrus.WithField("region_id", regionID)
	regionStr, err := getRegionInfoFromCache(regionID)
	if err != nil {
		logger.Infof("Cache miss for region, err=%s, retrieve from furion", err)
		regionStr, err = getRegionInfoFromFurion(regionID)
		if err != nil {
			return "", errors.Annotate(err, "retrieve region info from furion error.")
		}
		logger.Info("Retrieve region info from furion")
		logger.Debugf("Retrieved region info %+v", regionStr)
		redis, err := GetRedis()
		if err != nil {
			logger.WithError(err).Warning("Set region cache error.")
		} else {
			err = redis.Set(getRegionCacheKey(regionID), regionStr, common.GetTimeDuration(RegionCacheExpireTime))
			if err != nil {
				logger.WithError(err).Warning("Set region cache error.")
			}
		}
	}
	return regionStr, nil
}

// IsKubernetesRegion returns true if the container manager of the region is kubernetes.
func isKubernetesRegion(containerManager string) bool {
	return containerManager == "KUBERNETES"
}

// GetKubernetesConfig get kubernetes config from cache or furion.
func GetKubernetesConfig(regionID string) (*KubernetesConfig, error) {
	regionStr, err := getRegionInfo(regionID)
	if err != nil {
		return nil, err
	}
	return getKubernetesConfig(regionStr)
}

// GetKubernetesVersion get kubernetes version fro region info
func GetKubernetesVersion(regionID string) (string, error) {
	region, err := getRegionInfo(regionID)
	if err != nil {
		return "", err
	}
	return getKubernetesVersion(region)
}

func GetResourceRatio(regionID string) (*model.ResourceRatio, error) {
	region, err := getRegionInfo(regionID)
	if err != nil {
		return nil, err
	}
	return getResourceRatio(region), nil
}
