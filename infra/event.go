package infra

import (
	"errors"
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/db"
	"krobelus/model"

	"github.com/levigross/grequests"
)

var (
	StatusMap = map[common.KubernetesResourceStatus]string{
		common.CurrentCreateErrorStatus: "error",
		common.CurrentUpdateErrorStatus: "error",
		common.CurrentDeleteErrorStatus: "error",
	}

	ResourceTypeToUrlPath = map[model.ResourceType]string{
		model.AlaudaServiceType: "services",
		model.AppType:           "applications",
		model.ApplicationType:   "applications",
	}
)

type Event struct {
	PlatformVersion string `json:"platform_version"`
	Message         string `json:"message"`
}

// SendAlaudaEvent send event to alauda event platform through api portal.
func SendAlaudaEvent(statusStore *db.StatusStore) error {
	status := statusStore.Status
	cfg := config.GlobalConfig.Jakiro
	if cfg.Endpoint == "" {
		statusStore.Logger.Errorf("Jakiro endpoint is empty, please set")
		return nil
	}
	state, ok := StatusMap[status.CurrentState]
	if !ok {
		return nil
	}
	event := Event{
		PlatformVersion: "v3",
		Message:         status.Output,
	}
	pathPrefix := fmt.Sprintf("%s/%s/callback/%s",
		cfg.Endpoint, cfg.ApiVersion, ResourceTypeToUrlPath[status.Type])
	url := fmt.Sprintf("%s/%s/status/%s", pathPrefix, status.UUID, state)
	resp, err := grequests.Post(
		url,
		&grequests.RequestOptions{
			JSON:           event,
			RequestTimeout: time.Duration(cfg.Timeout) * time.Second,
		},
	)
	if err != nil {
		return err
	}
	statusStore.Logger.Infof("Send event to Jakiro: %v, %v, %v", status.UUID, resp.StatusCode, status.Output)
	if resp.StatusCode == 404 {
		statusStore.Logger.Infof("Service resource has been deleted already %v, %v, %v", status.UUID, resp.StatusCode, status.Output)
		return nil
	} else if resp.StatusCode > 300 {
		return errors.New(fmt.Sprintf("Send event to Jakiro error: %v, %v", resp.StatusCode, resp.String()))
	} else {
		return nil
	}
}
