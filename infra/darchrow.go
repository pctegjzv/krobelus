package infra

import (
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/config"

	"github.com/levigross/grequests"
	log "github.com/sirupsen/logrus"
)

// JFrogNamespace describes a struct of jfrog namespace created in darchrow.
type JFrogNamespace struct {
	Name      string `json:"name"`
	Email     string `json:"email"`
	Tenant    string `json:"tenant"`
	Namespace string `json:"namespace"`
	Password  string `json:"password"`
	Account   string `json:"account"`
	Endpoint  string `json:"endpoint"`
}

// CreateJFrogNamespace creates a jfrog namespace in darchrow.
func CreateJFrogNamespace(namespace *JFrogNamespace) error {
	cfg := config.GlobalConfig.Darchrow
	if cfg.Endpoint == "" {
		log.Errorf("Darchrow api endpoint is empty, please set.")
		return common.BuildDarchrowErrorResponse(400, "Darchrow api endpoint is empty, please set.")
	}
	url := fmt.Sprintf("%s/%s/namespaces", cfg.Endpoint, cfg.APIVersion)
	log.Debugf("Try to create jfrog namespace with: %+v", namespace)
	resp, err := grequests.Post(url, &grequests.RequestOptions{
		RequestTimeout: time.Duration(cfg.Timeout) * time.Second,
		JSON:           namespace,
	})
	if err != nil {
		return common.BuildDarchrowErrorResponse(resp.StatusCode, err.Error())
	}
	if resp.StatusCode != 201 {
		log.Errorf("Darchrow response is not ok: %d %s", resp.StatusCode, resp.String())
		return common.BuildDarchrowErrorResponse(resp.StatusCode, resp.String())
	}

	return nil
}
