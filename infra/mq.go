package infra

import (
	"krobelus/common"
	"krobelus/db"
	"krobelus/model"

	"github.com/kelseyhightower/envconfig"
	"github.com/liticer/machinery/v1"
	v1Config "github.com/liticer/machinery/v1/config"
	"github.com/liticer/machinery/v1/log"
	"github.com/liticer/machinery/v1/tasks"
)

var (
	MQServer *machinery.Server
)

var (
	ServiceStateToTaskType = map[common.KubernetesResourceStatus]string{
		common.CurrentCreatingStatus: "service_create",
		common.CurrentDeletingStatus: "service_delete",
		common.CurrentUpdatingStatus: "service_update",
	}

	AppStateToTaskType = map[common.KubernetesResourceStatus]string{
		common.CurrentDeletingStatus: "app_delete",
		common.CurrentCreatingStatus: "app_create",
		common.CurrentUpdatingStatus: "app_update",
	}
)

func InitMQ() error {
	cnf, err := v1Config.NewFromEnvironment(false)
	if err = envconfig.Process("", cnf); err != nil {
		log.ERROR.Printf("New config failed: %s ", err.Error())
		return err
	}

	if cnf.Broker == v1Config.DefaultBroker {
		cnf.AMQP = nil
		rc, err := GetRedis()
		if err != nil {
			log.ERROR.Printf("Get redis failed: %s ", err.Error())
			return err
		}
		if server, err := machinery.NewServerWithRedisClient(cnf, rc.read, rc.write); err != nil {
			log.ERROR.Printf("New server failed: %s", err.Error())
			return err
		} else {
			MQServer = server
		}
	} else {
		if server, err := machinery.NewServer(cnf); err != nil {
			log.ERROR.Printf("New server failed: %s", err.Error())
			return err
		} else {
			MQServer = server
		}
	}
	return nil
}

func GetTaskName(status *model.AlaudaResourceStatus) string {
	switch status.Type {
	case model.AlaudaServiceType:
		if status.TargetState == common.TargetStoppedState {
			return "service_stop"
		} else {
			return ServiceStateToTaskType[status.CurrentState]
		}
	case model.AppType:
		return AppStateToTaskType[status.CurrentState]
	}
	return ""
}

func GenTask(status *model.AlaudaResourceStatus) *tasks.Signature {
	if status == nil || !status.CurrentState.IsDeployingStatus() || GetTaskName(status) == "" {
		return nil
	}
	return &tasks.Signature{
		UUID: status.QueueID,
		Name: GetTaskName(status),
		Args: []tasks.Arg{
			{
				Type:  "string",
				Value: status.UUID,
			},
		},
	}
}

func SendTask(statusStore *db.StatusStore) error {
	if MQServer == nil {
		statusStore.Logger.Errorf("MQServer is nil, try to init it")
		if err := InitMQ(); err != nil {
			statusStore.Logger.Errorf("Init MQ err, %s", err.Error())
			return err
		}
	}
	task := GenTask(statusStore.Status)
	if task == nil {
		statusStore.Logger.Debugf("Task is nil, status %v", statusStore.Status)
		return nil
	}
	if _, err := MQServer.SendTask(task); err != nil {
		statusStore.Logger.Debugf("Send task failed, error %s, status %v", err.Error(), statusStore.Status)
		return err
	}
	return nil
}
