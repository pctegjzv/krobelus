package infra

import "testing"

func TestResourceRatio(t *testing.T) {
	data := `{"resource_actions":["cluster:view","cluster:clear","cluster:create","cluster:delete","cluster:deploy","cluster:update"],"display_name":"htzq","name":"htzq","container_manager":"KUBERNETES","created_at":"2017-12-27T09:14:51Z","namespace":"htzq","updated_at":"2017-12-27T13:37:10Z","platform_version":"v3","state":"RUNNING","features":{"pipeline":{},"lb":{"haproxy":{"lb_tag":"ha","ipaddress":"168.61.5.7","private_ip":"168.61.5.7","cloud_name":"PRIVATE","region_name":"htzq"},"features":["haproxy","alb"]},"kubernetes":{"host_port_range":[60000,62000],"endpoint":"https://168.61.5.6:6443","schedulable_master":true,"version":"1.9.8","docker_resource_ratio":{"mem":2,"cpu":4},"cni":{"type":"macvlan"}},"service":{"volume":{"drivers":[{"disp_name":"glusterfs","driver_name":"glusterfs","desc":"glusterfs"}],"agent":{"endpoint":"http://168.61.5.6:8082/volume_driver"}},"haproxy":{"lb_tag":"ha","ipaddress":"168.61.5.7","private_ip":"168.61.5.7","region_name":"htzq","cloud_name":"PRIVATE"},"manager":{"host_port_range":[60000,62000],"endpoint":"https://168.61.5.6:6443","docker_resource_ratio":{"mem":2,"cpu":4}},"features":["haproxy","raw-container","pipeline","exec","subnet","file-log","alb","volume","application","glusterfs","host-network","registry","mount-points","tunnel","macvlan"],"exec":{"endpoint":"168.61.5.6"}},"tunnel":{"public_key":"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCv14Lmv5cXQd7maCuzcEFoQherdEbP97xApq9/TP2ok2HHf+tBhP1qt9vqSOL8C6eBXq14WbQC7/HyT5aO4fq6LnR1B/Tdhodb2zFuHkcoZSS+dh9k8Jv74P+9HzfqBsOnIck6ir3Um0x/5RYfiGdwLty4JiuQMCXqdFV/tp6fxahr3TJuxHWtCQngrcwG87fp75+wEq5zF0jKvAQcNrn4RD+Rn6cIGSnfQjNCrTb0W2d/6pnU/hCF1dGsm1l1WnS73T1eDQQhDVKZQHs65iObxe7ZnzIvc5dy0K8hhyBn74AqLfjo3VyHKGWb7bfVjBBT/Qu3yI//1phy691kdzK9","private_key":"-----BEGIN RSA PRIVATE KEY-----\nMIIEowIBAAKCAQEAr9eC5r+XF0He5mgrs3BBaEIXq3RGz/e8QKavf0z9qJNhx3/r\nQYT9arfb6kji/AungV6teFm0Au/x8k+WjuH6ui50dQf03YaHW9sxbh5HKGUkvnYf\nZPCb++D/vR836gbDpyHJOoq91JtMf+UWH4hncC7cuCYrkDAl6nRVf7aen8Woa90y\nbsR1rQkJ4K3MBvO36e+fsBKucxdIyrwEHDa5+EQ/kZ+nCBkp30IzQq029Ftnf+qZ\n1P4QhdXRrJtZdVp0u909Xg0EIQ1SmUB7OuYjm8Xu2Z8yL3OXctCvIYcgZ++AKi34\n6N1chyhlm+231YwQU/0Lt8iP/9aYcuvdZHcyvQIDAQABAoIBAFXO2B0cT3HU/h33\npaO52uEyp3ROmKOx49a0wJ1hh8L0sNl2zem6KSQjtphcmw+d7E7QUdySAasRRkMV\nnYX6Nq0WMFRP3qkflfw4OaXf9p96w/SBlg5e7+95O7MhgdqdFGsBrL35qGsQocMj\nmqkmmjt/Usw2LDn/lPEVdgeNqViTf+EMc+MKxGFjWVHYV8R54xXvIZ14k6wkY8Y+\nWkVzwJqDCTAwXfnYisg6LLJGYuvEMGkpoDgst4TzbzuLWcNL7dRknJQzi4mtpI1A\n1ZjfZWqLai71FSdC8oXlV9iSr5PRfj5/HIjqtQBYdhB+lKkWTn2xRig1CmWBNxTk\nKNrC6KECgYEA0BghXshi0mlcgM4qqR/GojQGuqDjDimqJksULT47qN+JaEaCnks1\nXETXkU6dIB+9wjVvzVy5EpoA1ckQ4tj+4sSH6RBig/+JihYKUUKnqGN1SmbHmMXH\ni82AsrmXOrA9RYEvqjjD1JKNylB87iKmFOIcYPMkgm9NLfgC5RsK4+kCgYEA2FKc\nQavZ9jlwJFS45jyIlzR0hGvOuCzdZu8pKwGyn5IyZPWwtFmcWw6ZDYSEFaDrlBoX\nNWG2bf+YmDjJbR61pYsOuDhfVHihCe2fqOdGJ9OyVaWXFoxfMY65Xt+GGvYNe5pP\nULJlKR9s9965DbW5bFHibed1vZvXtKrVItlFN7UCgYANyJtlQNay40G6Um2m+DDb\nC6EqeX+wr3fSw4RZntaqz+u1GX6LZ6XoL7Mrr85ek1upYoq+SpcDsWm+v2u76u2v\nLZ9nA6vS3HlKyTzztvuN/tCFFhvAddFK5FjS/dITlBrq4Ky8lLf7+50iuoQb7fWc\nSoHgthpD/YVaNyRR8qGdCQKBgQC4tGDDGg8zU/vSqZxuygT+xkUuxDZvv68TsuV9\n2SdBZL0uwzGr7E+62A4LIrj9FrfPby+d3t67sWADqYkjaPNEdCWPWmLbZ1xpc8G9\noCug2fsNciQ9loG5eNoCjeydnLYKccmDZ69Em5aZVky/zgiS2p/xo44+5ZAarTrr\nVITreQKBgFBq/S85pkocIfxDHIcdlTQ5T4/H/s4llp/opcLpdbvuhJ7UDWah0dpc\nJBkiI4Qq+Geb60FDF0Fu5YZ2HKWsmVzq+2sKNnDRaoBxq823Ymi0PM0IEzvclZlR\nxgppkB0efNJjHlYe5J2GtbPfd2Kxm1fGxt8lXVjIQV/579bQpSC/\n-----END RSA PRIVATE KEY-----","port_mapping":{"reserved":"168.61.5.15:7055","puck":"168.61.5.15:7051","tiny":"168.61.5.15:7052","ssh":"168.61.5.15:7001","doom":"168.61.5.15:7053","registry":"168.61.5.15:7054"}},"network_modes":{"features":["macvlan"]},"volume":{"drivers":[{"desc":"glusterfs","driver_name":"glusterfs","disp_name":"glusterfs"}],"features":["glusterfs"],"agent":{"endpoint":"http://168.61.5.6:8082/volume_driver"}},"registry":{},"logs":{"storage":{"read_log_source":"default","write_log_source":"default"}}},"env_uuid":"3ea937ca-4d7c-4e0e-9e4a-167c64c2e2cc","type":"CLAAS","id":"4140d2ff-f584-4e3f-ad7f-575ae2b33fd7","attr":{"nic":"ens160","docker":{"path":"/var/lib/docker","version":"1.12.6"},"cloud":{"name":"PRIVATE","region_id":null}}}`
	result := getResourceRatio(data)
	if result.Cpu != 4 || result.Mem != 2 {
		t.Errorf("resource ratio result: %+v", result)
	}

}

func TestSingleRegion(t *testing.T) {
	data := `{
      "id": "9636741d-63bb-465e-8483-af69d0d41228",
      "platform_version": "v3",
      "features": {
        "kubernetes": {
          "endpoint": "https://139.219.56.162:6443",
          "token": "7dd33a.84c08dc99025557f",
          "version": "1.7.3"
        }
      }
    }`
	result, err := getKubernetesConfig(data)
	if err != nil {
		t.Errorf("parse kubernetes config error: %s", err.Error())
	}
	if result.Token != "7dd33a.84c08dc99025557f" {
		t.Errorf("parse kubernetes token error: %s", result.Token)
	}
	if result.Endpoint != "https://139.219.56.162:6443" {
		t.Errorf("parse kubernetes endpoint error: %s", result.Endpoint)
	}
}
