package infra

import (
	"encoding/json"
	"fmt"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/model"

	"github.com/juju/errors"
	"github.com/levigross/grequests"
	log "github.com/sirupsen/logrus"
)

func GetResourceFromJakiro(uuid string) (string, error) {
	cfg := config.GlobalConfig.Jakiro
	if cfg.Endpoint == "" {
		log.Errorf("Jakiro inner api endpoint is empty, please set.")
		return "", nil
	}
	pathPrefix := fmt.Sprintf("%s/%s/inner/resources", cfg.Endpoint, cfg.ApiVersion)
	url := fmt.Sprintf("%s/%s", pathPrefix, uuid)
	resp, err := grequests.Get(url, &grequests.RequestOptions{RequestTimeout: time.Duration(cfg.Timeout) * time.Second})
	if err != nil {
		return "", err
	}
	if resp.StatusCode != 200 {
		message := fmt.Sprintf("Get resource %v from jakiro error: %v, %v.", uuid, resp.StatusCode, resp.String())
		if resp.StatusCode == 404 {
			return "", errors.Annotate(common.ErrResourceNotExist, message)
		} else {
			return "", errors.New(message)
		}
	}
	return resp.String(), nil
}

// ListResourceFromJakiro list resources from jakiro by cluster uuid and type
func ListResourceFromJakiro(cluster string, resourceType string) ([]model.AlaudaInternalResource, error) {
	cfg := config.GlobalConfig.Jakiro
	if cfg.Endpoint == "" {
		return nil, errors.New("Jakiro inner api endpoint is empty, please set.")
	}
	path := fmt.Sprintf("%s/%s/inner/resources", cfg.Endpoint, cfg.ApiVersion)
	resp, err := grequests.Get(path, &grequests.RequestOptions{
		Params: map[string]string{
			"region_uuid":   cluster,
			"resource_type": resourceType,
		},
		RequestTimeout: time.Duration(cfg.Timeout) * time.Second,
	})
	if err != nil {
		return nil, err
	}

	log.Debugf("Get data from jakiro: %s", resp.String())
	var resources []model.AlaudaInternalResource
	if err := json.Unmarshal(resp.Bytes(), &resources); err != nil {
		log.WithField("cluster", cluster).WithError(err).Error("Parse inner resources error")
		return nil, err
	}
	return resources, nil
}

// DeleteResourceFromJakiro delete a a resource record from jakiro by it's uuid
func DeleteResourceFromJakiro(uuid string) error {
	cfg := config.GlobalConfig.Jakiro
	if cfg.Endpoint == "" {
		log.Errorf("Jakiro inner api endpoint is empty, please set.")
		return nil
	}
	pathPrefix := fmt.Sprintf("%s/%s/inner/resources", cfg.Endpoint, cfg.ApiVersion)
	url := fmt.Sprintf("%s/%s", pathPrefix, uuid)
	resp, err := grequests.Delete(url, &grequests.RequestOptions{RequestTimeout: time.Duration(cfg.Timeout) * time.Second})
	if err != nil {
		log.Errorf("Resource %v delete error: %v.", err.Error())
		return err
	}
	if resp.StatusCode < 300 {
		return nil
	} else if resp.StatusCode == 404 {
		log.Infof("Resource %v has been deleted already.", uuid)
		return nil
	} else {
		message := fmt.Sprintf("Resource %v delete error: %v, %v", uuid, resp.StatusCode, resp.String())
		log.Errorf(message)
		return errors.New(message)
	}
}

// GetRikiVolume get volume info by uuid
func GetRikiVolume(uuid string) (*model.VolumeResponse, *common.HttpErrorResponse) {
	cfg := config.GlobalConfig.Riki
	if cfg.Endpoint == "" {
		log.Errorf("Riki api endpoint is empty, please set.")
		return nil, common.BuildRikiErrorResponse(400, "Riki api endpoint is empty, please set.")
	}
	url := fmt.Sprintf("%s/%s/volumes/%s", cfg.Endpoint, cfg.APIVersion, uuid)
	resp, err := grequests.Get(url, &grequests.RequestOptions{RequestTimeout: time.Duration(cfg.Timeout) * time.Second})
	if err != nil {
		return nil, common.BuildRikiErrorResponse(resp.StatusCode, err.Error())
	}
	if resp.StatusCode != 200 {
		log.Errorf("Riki resp is not ok: %d %s", resp.StatusCode, resp.String())
		return nil, common.BuildRikiErrorResponse(resp.StatusCode, resp.String())
	}

	myJSONStruct := &model.VolumeResponse{}
	err = resp.JSON(myJSONStruct)
	if err != nil {
		return nil, common.BuildRikiErrorResponse(
			resp.StatusCode, fmt.Sprintf("GetVolume: Unable to coerce to JSON, %s", err.Error()))
	}

	return myJSONStruct, nil
}

// UpdateRikiVolume get volume info by uuid
func UpdateRikiVolume(uuid string, PVName string, PVUUID string) *common.HttpErrorResponse {
	cfg := config.GlobalConfig.Riki
	if cfg.Endpoint == "" {
		log.Errorf("Riki api endpoint is empty, please set.")
		return common.BuildRikiErrorResponse(400, "Riki api endpoint is empty, please set.")
	}
	url := fmt.Sprintf("%s/%s/volumes/%s", cfg.Endpoint, cfg.APIVersion, uuid)
	resp, err := grequests.Put(url, &grequests.RequestOptions{RequestTimeout: time.Duration(cfg.Timeout) * time.Second, JSON: model.VolumeRequest{PVName: PVName, PVUUID: PVUUID}})
	if err != nil {
		return common.BuildRikiErrorResponse(resp.StatusCode, err.Error())
	}
	if !resp.Ok {
		log.Errorf("Riki resp is not ok: %d %s", resp.StatusCode, resp.String())
		return common.BuildRikiErrorResponse(resp.StatusCode, resp.String())
	}

	return nil
}

func PushTinyMetrics(metrics *model.RegionInstancesCache) {
	logger := common.GetLoggerByRegionID(metrics.RegionID)
	var data []*model.MetricsUnit
	for _, item := range metrics.Apps.Items {
		data = append(data, model.GenMetricsUnit(item.ServiceID, "healthy_instances", item.AvailableReplicas))
		data = append(data, model.GenMetricsUnit(item.ServiceID, "unhealthy_instances", item.UnavailableReplicas))
	}
	logger.Debugf("Trying to send %d data to tiny", len(data))
	tiny := config.GlobalConfig.Tiny
	url := fmt.Sprintf("%s/%s/metrics-data/", tiny.Endpoint, tiny.ApiVersion)
	resp, err := grequests.Post(
		url,
		&grequests.RequestOptions{
			RequestTimeout: time.Duration(tiny.Timeout) * time.Second,
			JSON:           data,
		},
	)
	logger.Debugf("put %d data to tiny", len(data))
	if err != nil {
		logger.Errorf("Put metrics to tiny error: %s", err.Error())
	}
	if !resp.Ok {
		log.Errorf("Tiny resp is not ok: %d %s", resp.StatusCode, resp.String())
	}
}
