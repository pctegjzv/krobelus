package cmd

import (
	"strconv"
	"time"

	"krobelus/common"
	"krobelus/infra"
	"krobelus/worker/apps"
	"krobelus/worker/monitor"
	"krobelus/worker/services"

	"github.com/liticer/machinery/v1/log"
)

var (
	Tasks = map[string]interface{}{
		"app_delete": apps.AppDelete,
		"app_create": apps.AppCreate,
		"app_update": apps.AppUpdate,

		"service_create": services.Create,
		"service_delete": services.Delete,
		"service_update": services.Update,
		"service_stop":   services.Stop,
	}
)

func Worker() {

	err := infra.InitMQ()
	if err != nil {
		panic(err)
	}
	workerName := "krobelus_worker_" + strconv.FormatInt(time.Now().Unix(), 10)
	// Start monitor
	go monitor.Start()

	// Set custom logger
	log.Set(common.GetLoggerByKV("role", "worker"))

	// Start worker
	log.INFO.Printf("Launch server")

	if err := infra.MQServer.RegisterTasks(Tasks); err != nil {
		log.ERROR.Printf("Register task failed!")
		panic(err)
	}
	// The second argument is a consumer tag
	// Ideally, each worker should have a unique tag (worker1, worker2 etc)
	worker := infra.MQServer.NewWorker(workerName, 500)
	if err := worker.Launch(); err != nil {
		log.ERROR.Printf("Launch work failed!")
		panic(err)
	}
}
