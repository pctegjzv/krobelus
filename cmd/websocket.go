package cmd

import (
	"fmt"
	"krobelus/config"
	"krobelus/websocket"

	"log"
	"net/http"
)

// Websocket ..
func Websocket() {

	handler := websocket.WSHandler{
		StopCh: websocket.SetupSignalHandler(),
	}

	addr := fmt.Sprintf(":%d", config.GlobalConfig.WebSocket.Port)

	http.HandleFunc("/api/sock", handler.HandleTerminalSession)

	log.Println("WebsocketServer starting...")

	// Listen and Server in 0.0.0.0:8080
	log.Fatal(http.ListenAndServe(addr, nil))

	log.Println("Krobelus stopped")

}
