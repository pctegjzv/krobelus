package cmd

import (
	"fmt"
	"time"

	"krobelus/api/handler"
	"krobelus/api/schema"
	"krobelus/config"
	"krobelus/db"
	"krobelus/infra"

	"github.com/DeanThompson/ginpprof"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	statusApi "gopkg.in/appleboy/gin-status-api.v1"
)

func API() {
	schema.LoadSchemas()

	err := infra.InitMQ()
	if err != nil {
		log.WithError(err).Error("Init mq error")
	}
	log.Debugf("Current env settings: %+v", config.GlobalConfig)
	gin.SetMode(gin.ReleaseMode)

	r := gin.Default()

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})

	handler.AddRoutes(r)
	// debug apis
	ginpprof.Wrap(r)
	r.GET("/debug/api/status", statusApi.StatusHandler)

	log.Info("Krobelus starting...")

	//TODO: find why
	SetDBTimeZoneDifference()

	// Listen and Server in 0.0.0.0:8080
	addr := fmt.Sprintf(":%d", config.GlobalConfig.Krobelus.Port)
	r.Run(addr)
}

func SetDBTimeZoneDifference() error {
	cur := time.Now().UTC()
	now, err := db.GetDatabaseTime()
	if err != nil {
		log.Errorf("Set DB timezone difference error: %v", err.Error())
		return err
	}

	config.GlobalConfig.DB.TimeZoneDiff = int(cur.Sub(now).Hours())
	log.Debugf("Set DB timezone difference: %v", config.GlobalConfig.DB.TimeZoneDiff)
	return nil
}
