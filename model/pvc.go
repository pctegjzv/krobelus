package model

import (
	"time"

	"k8s.io/api/core/v1"
)

/*

PersistentVolumeClaim Claim Properties {
	Name
	Status
	Volume
	Capacity
	AccessModes
	StorageClass
	Age

	Namespace:
	Cluster:

}

curl 127.0.0.1:8001/api/v1/namespaces/default/PersistentVolumeClaimclaims/pvc0003
{
  "kind": "PersistentVolumeClaimClaim",
  "apiVersion": "v1",
  "metadata": {
    "name": "pvc0003",
    "namespace": "default",
    "selfLink": "/api/v1/namespaces/default/PersistentVolumeClaimclaims/pvc0003",
    "uid": "5a6a2c71-cb70-11e7-9078-000d3a8206af",
    "resourceVersion": "992297",
    "creationTimestamp": "2017-11-17T08:21:47Z",
    "annotations": {
      "pv.kubernetes.io/bound-by-controller": "yes",
      "pv.kubernetes.io/bind-completed": "yes"
    }
  },
  "spec": {
    "accessModes": [
      "ReadWriteOnce"
    ],
    "resources": {
      "requests": {
        "storage": "5Gi"
      }
    },
    "volumeName": "pv0003",
    "storageClassName": "slow"
  },
  "status": {
    "phase": "Bound",
    "accessModes": [
      "ReadWriteOnce"
    ],
    "capacity": {
      "storage": "5Gi"
    }
  }
}
*/

//PersistentVolumeClaimRequest ...
type PVCRequest struct {
	Resource   ResourceNameBase  `json:"resource" binding:"required"`
	Namespace  Namespace         `json:"namespace" binding:"required"`
	Cluster    Cluster           `json:"cluster" binding:"required"`
	Kubernetes *KubernetesObject `json:"kubernetes" binding:"required"`
}

type PVCAllReferencedBy struct {
	UUID string       `json:"uuid"`
	Name string       `json:"name"`
	Type ResourceType `json:"type"`
}

type PVCResource struct {
	UUID        string    `json:"uuid"`
	Name        string    `json:"name"`
	Status      string    `json:"status,omitempty"`
	AccessModes []string  `json:"accessModes"`
	Capacity    string    `json:"capacity,omitempty"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type PVCResponse struct {
	Type             string                `json:"type" binding:"required"`
	Resource         PVCResource           `json:"resource" binding:"required"`
	Namespace        Namespace             `json:"namespace" binding:"required"`
	Cluster          Cluster               `json:"cluster" binding:"required"`
	ReferencedBy     []*PVCAllReferencedBy `json:"referenced_by" binding:"required"`
	PersistentVolume *ResourceNameBase     `json:"persistentvolume" binding:"required"`
	Kubernetes       *KubernetesObject     `json:"kubernetes,omitempty"`
}

func PVCToResponse(pvc *v1.PersistentVolumeClaim) *PVCResponse {
	var object KubernetesObject
	object.ParsePVC(pvc)
	storage, ok := pvc.Spec.Resources.Requests[v1.ResourceStorage]
	capacity := ""
	if ok {
		capacity = storage.String()
	}
	resp := &PVCResponse{
		Resource: PVCResource{
			Name:      pvc.Name,
			UUID:      GetAlaudaUID(&object),
			CreatedAt: pvc.GetCreationTimestamp().Time,
			UpdatedAt: pvc.GetCreationTimestamp().Time,
			Status:    string(pvc.Status.Phase),
			Capacity:  capacity,
		},
		Namespace: Namespace{
			Name: pvc.Namespace,
		},
		ReferencedBy:     []*PVCAllReferencedBy{},
		PersistentVolume: nil,
		Kubernetes:       &object,
		Type:             string(PersistentVolumeClaimType),
	}

	if "" != pvc.Spec.VolumeName {
		resp.PersistentVolume = &ResourceNameBase{Name: pvc.Spec.VolumeName}
	}

	for _, accessMode := range pvc.Spec.AccessModes {
		resp.Resource.AccessModes = append(resp.Resource.AccessModes, string(accessMode))
	}

	return resp

}

func (ks *KubernetesObject) ToPVCResponse() *PVCResponse {
	pvc, _ := ks.ToV1PVC()
	return PVCToResponse(pvc)
}

type PVCUpdate struct {
	Kubernetes *KubernetesObject `json:"kubernetes" binding:"required"`
}
