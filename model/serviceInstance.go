package model

import (
	"krobelus/common"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	"k8s.io/apimachinery/pkg/runtime"
)

type ServiceInstanceRequest struct {
	Resource   ServiceInstanceRequestResource `json:"resource" binding:"required"`
	Cluster    Cluster                        `json:"cluster" binding:"required"`
	Namespace  Namespace                      `json:"namespace" binding:"required"`
	Kubernetes *KubernetesObject              `json:"kubernetes" binding:"required"`
}

type ServiceInstanceRequestResource struct {
	ResourceNameBase
	DisplayName string `json:"display_name"`
}

type ServiceInstanceUpdate struct {
	Kubernetes *KubernetesObject `json:"kubernetes"`
	Resource   ResourceUpdate    `json:"resource"`
}

type ServiceInstanceResource struct {
	ResourceBasic
	BrokerStatus scV1beta1.ConditionStatus `json:"broker_status" binding:"required"`
}

type ServiceInstanceRef struct {
	ResourceBasic
	ClusterServicePlan  string `json:"clusterserviceplan_name" binding:"required"`
	ClusterServiceClass string `json:"clusterserviceclass_name" binding:"required"`
}

type ServiceInstanceResponse struct {
	Resource           ServiceInstanceResource `json:"resource" binding:"required"`
	Namespace          Namespace               `json:"namespace" binding:"required"`
	Cluster            Cluster                 `json:"cluster" binding:"required"`
	Type               ResourceType            `json:"type" binding:"required"`
	Kubernetes         *KubernetesObject       `json:"kubernetes,omitempty"`
	ClusterServicePlan *ServicePlanResponse    `json:"clusterserviceplan,omitempty"`
}

func ServiceInstanceToResponse(si *scV1beta1.ServiceInstance) *ServiceInstanceResponse {
	var object KubernetesObject
	object.ParseServiceInstance(si)
	return &ServiceInstanceResponse{
		Resource: ServiceInstanceResource{
			ResourceBasic: ResourceBasic{
				Name:        si.Name,
				UUID:        string(si.UID),
				Description: si.GetAnnotations()[common.AnnotationsDescription],
				CreatedAt:   si.GetCreationTimestamp().Time,
				UpdatedAt:   si.GetCreationTimestamp().Time,
			},
		},
		Namespace: Namespace{
			Name: si.Namespace,
		},
		Kubernetes: &object,
	}
}

type ServiceInstanceListItemResponse struct {
	ServiceInstanceResource
	Namespace    string                             `json:"namespace" binding:"required"`
	ClassName    string                             `json:"class_name" binding:"required"`
	PlanName     string                             `json:"plan_name" binding:"required"`
	AppNumbers   int                                `json:"app_numbers" binding:"required"`
	Parameters   *runtime.RawExtension              `json:"parameters,omitempty"`
	Cluster      Cluster                            `json:"cluster" binding:"required"`
	Status       scV1beta1.ServiceInstanceCondition `json:"status"`
	BrokerStatus scV1beta1.ConditionStatus          `json:"broker_status"`
}

func ServiceInstanceListItenToResponse(si *scV1beta1.ServiceInstance) *ServiceInstanceListItemResponse {
	var object KubernetesObject
	object.ParseServiceInstance(si)
	return &ServiceInstanceListItemResponse{
		ServiceInstanceResource: ServiceInstanceResource{
			ResourceBasic: ResourceBasic{
				Name:        si.Name,
				UUID:        string(si.UID),
				Description: si.GetAnnotations()[common.AnnotationsDescription],
				CreatedAt:   si.GetCreationTimestamp().Time,
				UpdatedAt:   si.GetCreationTimestamp().Time,
			},
		},
		Namespace:  si.Namespace,
		ClassName:  si.Spec.ClusterServiceClassExternalName,
		PlanName:   si.Spec.ClusterServicePlanExternalName,
		Parameters: si.Spec.Parameters,
		Status:     si.Status.Conditions[0],
	}
}

func (ks *KubernetesObject) ToServiceInstanceResponse() *ServiceInstanceResponse {
	ServiceInstance, _ := ks.ToServiceInstance()
	return ServiceInstanceToResponse(ServiceInstance)
}

func (ks *KubernetesObject) ToServiceInstanceListItenResponse() *ServiceInstanceListItemResponse {
	ServiceInstance, _ := ks.ToServiceInstance()
	return ServiceInstanceListItenToResponse(ServiceInstance)
}
