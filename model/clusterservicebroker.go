package model

import (
	"krobelus/common"

	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"
)

type BrokerRequest struct {
	Resource   BrokerRequestResource `json:"resource" binding:"required"`
	Cluster    Cluster               `json:"cluster" binding:"required"`
	Kubernetes *KubernetesObject     `json:"kubernetes" binding:"required"`
}

type BrokerRequestResource struct {
	ResourceNameBase
	DisplayName string `json:"display_name"`
}

type BrokerUpdate struct {
	Kubernetes *KubernetesObject    `json:"kubernetes"`
	Resource   BrokerUpdateResource `json:"resource"`
}

type BrokerUpdateResource struct {
	DisplayName string `json:"display_name"`
}

type BrokerResource struct {
	ResourceBasic
	DisplayName string                    `json:"display_name"`
	MetaData    BrokerResourceMetaData    `json:"meta_data"`
	Status      scV1beta1.ConditionStatus `json:"status"`
}

type BrokerResourceMetaData struct {
	DocUrl     string `json:"doc_url"`
	SupportUrl string `json:"support_url"`
}

type BrokerResponse struct {
	Resource   BrokerResource    `json:"resource" binding:"required"`
	Cluster    Cluster           `json:"cluster" binding:"required"`
	Type       ResourceType      `json:"type" binding:"required"`
	Kubernetes *KubernetesObject `json:"kubernetes,omitempty"`
}

type BrokerListItemResponse struct {
	BrokerResource
	Cluster  Cluster                          `json:"cluster" binding:"required"`
	Status   scV1beta1.ServiceBrokerCondition `json:"status"`
	ClassNum int                              `json:"classes_num"`
}
type BrokerListStore struct {
	Brokers []*BrokerResponse
	UUIDs   []string
}

func BrokerToResponse(cm *scV1beta1.ClusterServiceBroker) *BrokerResponse {
	var object KubernetesObject
	object.ParseBroker(cm)
	return &BrokerResponse{
		Resource: BrokerResource{
			ResourceBasic: ResourceBasic{
				Name:        cm.Name,
				UUID:        string(cm.UID),
				Description: cm.GetAnnotations()[common.AnnotationsDescription],
				CreatedAt:   cm.GetCreationTimestamp().Time,
				UpdatedAt:   cm.GetCreationTimestamp().Time,
			},
			DisplayName: cm.GetAnnotations()[common.ResourceDisplayNameKey()],
			Status:      GetBrokerStatus(cm),
			MetaData: BrokerResourceMetaData{
				DocUrl:     "test_doc_url",
				SupportUrl: "test_support_url",
			},
		},
		Kubernetes: &object,
		Type:       ClusterServiceBrokerType,
	}
}

func GetBrokerStatus(cm *scV1beta1.ClusterServiceBroker) scV1beta1.ConditionStatus {
	if len(cm.Status.Conditions) > 0 && (&cm.Status.Conditions[0] != nil) {
		return cm.Status.Conditions[0].Status
	} else {
		return scV1beta1.ConditionUnknown
	}
}

func (ks *KubernetesObject) ToBrokerResponse() *BrokerResponse {
	broker, _ := ks.ToBroker()
	return BrokerToResponse(broker)
}
