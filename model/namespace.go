package model

import (
	"krobelus/common"

	"github.com/Jeffail/gabs"
)

var (
	BuiltInNamespaces = []string{common.NamespaceDefault, "kube-public", "kube-system"}
)

type NamespaceResourceRequest struct {
	Name     string `json:"name" binding:"required"`
	UUID     string `json:"uuid,omitempty"`
	Editable bool   `json:"editable,omitempty"`
}

type NamespaceRequest struct {
	Resource   NamespaceResourceRequest `json:"resource" binding:"required"`
	Cluster    Cluster                  `json:"cluster" binding:"required"`
	Kubernetes []*KubernetesResource    `json:"kubernetes,omitempty"`
}

// Secret describes a struct used for CMB namespace.
type Secret struct {
	SecretName string `json:"secret_name" binding:"required"`
	Name       string `json:"name" binding:"required"`
	Email      string `json:"email"`
	Tenant     string `json:"tenant" binding:"required"`
	Namespace  string `json:"namespace" binding:"required"`
	Password   string `json:"password" binding:"required"`
	Account    string `json:"account" binding:"required"`
	Endpoint   string `json:"endpoint" binding:"required"`
}

// DockerConfigs describes a struct for docker configs in ImagePullSecret.
type DockerConfigs struct {
	Auths map[string]*DockerConfig `json:"auths" binding:"required"`
}

// DockerConfig describes a struct for a docker config.
type DockerConfig struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email    string `json:"email" binding:"required"`
	Auth     string `json:"auth" binding:"optional"`
}

// CMBNamespaceRequest describes a struct used for creating a CMB namespace.
type CMBNamespaceRequest struct {
	Namespace     *KubernetesObject `json:"namespace" binding:"required"`
	ResourceQuota *KubernetesObject `json:"resourcequota" binding:"required"`
	SecretData    *Secret           `json:"secret_data" binding:"required"`
}

// CMBNamespaceUpdateRequest describes a struct used for updating a CMB namespace.
type CMBNamespaceUpdateRequest struct {
	ResourceQuota *KubernetesObject `json:"resourcequota" binding:"required"`
}

func IsBuiltInNamespace(name string) bool {
	return common.StringInSlice(name, BuiltInNamespaces)

}

func GetResourceQuotaUpdateJson(quota interface{}) []byte{
	jsonObj := gabs.New()
	jsonObj.SetP(quota, "spec")
	return jsonObj.Bytes()
}
