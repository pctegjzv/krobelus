package model

import (
	"time"

	"krobelus/common"

	"k8s.io/api/core/v1"
)

/*

PV Properties {

	Name: pvc-f726e7dd-c6c0-11e7-9078-000d3a8206af
	Capacity: 9G
	AccessModes: RWO
	ReclaimPolicy: Delete
	Status: Bound
	Claim: default/myclaim
	StorageClasses: slow
	Reason:
	Age:

	Cluster:

}

curl 127.0.0.1:8001/api/v1/PVs/pv0003

{
  "kind": "PV",
  "apiVersion": "v1",
  "metadata": {
    "name": "pv0003",
    "selfLink": "/api/v1/PVs/pv0003",
    "uid": "32975c77-cb70-11e7-9078-000d3a8206af",
    "resourceVersion": "992295",
    "creationTimestamp": "2017-11-17T08:20:40Z",
    "labels": {
      "space": "default"
    },
    "annotations": {
      "pv.kubernetes.io/bound-by-controller": "yes"
    }
  },
  "spec": {
    "capacity": {
      "storage": "5Gi"
    },
    "glusterfs": {
      "endpoints": "glusterfs-cluster",
      "path": "gfs-vol-003"
    },
    "accessModes": [
      "ReadWriteOnce"
    ],
    "claimRef": {
      "kind": "PVClaim",
      "namespace": "default",
      "name": "pvc0003",
      "uid": "5a6a2c71-cb70-11e7-9078-000d3a8206af",
      "apiVersion": "v1",
      "resourceVersion": "992293"
    },
    "PVReclaimPolicy": "Recycle",
    "storageClassName": "slow"
  },
  "status": {
    "phase": "Bound"
  }



PV Claim Properties {
	Name
	Status
	Volume
	Capacity
	AccessModes
	StorageClass
	Age

	Namespace:
	Cluster:

}

curl 127.0.0.1:8001/api/v1/namespaces/default/PVclaims/pvc0003
{
  "kind": "PVClaim",
  "apiVersion": "v1",
  "metadata": {
    "name": "pvc0003",
    "namespace": "default",
    "selfLink": "/api/v1/namespaces/default/PVclaims/pvc0003",
    "uid": "5a6a2c71-cb70-11e7-9078-000d3a8206af",
    "resourceVersion": "992297",
    "creationTimestamp": "2017-11-17T08:21:47Z",
    "annotations": {
      "pv.kubernetes.io/bound-by-controller": "yes",
      "pv.kubernetes.io/bind-completed": "yes"
    }
  },
  "spec": {
    "accessModes": [
      "ReadWriteOnce"
    ],
    "resources": {
      "requests": {
        "storage": "5Gi"
      }
    },
    "volumeName": "pv0003",
    "storageClassName": "slow"
  },
  "status": {
    "phase": "Bound",
    "accessModes": [
      "ReadWriteOnce"
    ],
    "capacity": {
      "storage": "5Gi"
    }
  }
}
*/

// Volume ...
type Volume ResourceUUIDBase

// Driver ..
type Driver ResourceNameBase

// CacheResponse ...
type CacheResponse map[string]*v1.PersistentVolume

// PVRequest ...
type PVRequest struct {
	Resource   ResourceNameBase    `json:"resource" binding:"required"`
	Cluster    Cluster             `json:"cluster" binding:"required"`
	Volume     Volume              `json:"volume" binding:"required"`
	Driver     Driver              `json:"driver" binding:"required"`
	Kubernetes *KubernetesResource `json:"kubernetes" binging:"required"`
}

// PVContext stores context data during processing request
type PVContext struct {
	Resource   ResourceNameBase
	Cluster    Cluster
	PV         *v1.PersistentVolume
	VolumeInfo *VolumeResponse
}

// PVListRequest ...
type PVListRequest struct {
	Cluster Cluster  `json:"cluster" binding:"required"`
	UUIDS   []string `json:"uuids" binding:"required"`
}

// PVUpdateRequest ...
type PVUpdateRequest struct {
	Kubernetes *v1.PersistentVolume `json:"kubernetes" binging:"required"`
}

// PVResource ...
type PVResource struct {
	UUID            string    `json:"uuid"`
	Name            string    `json:"name"`
	Status          string    `json:"status,omitempty"`
	AccessModes     []string  `json:"accessModes"`
	PVReclaimPolicy string    `json:"persistentVolumeReclaimPolicy"`
	TargetStatus    string    `json:"-"`
	Capacity        string    `json:"capacity,omitempty"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
}

// PVResponse ...
type PVResponse struct {
	Type         ResourceType         `json:"type"`
	Resource     PVResource           `json:"resource"`
	Cluster      Cluster              `json:"cluster"`
	Volume       Volume               `json:"volume"`
	Driver       Driver               `json:"driver"`
	ReferencedBy ResourceBaseWithType `json:"referenced_by"`
	Kubernetes   *KubernetesResource  `json:"kubernetes,omitempty"`
}

// VolumeResponse ...
type VolumeResponse struct {
	UUID           string `json:"id" binding:"required"`
	DriverVolumeID string `json:"driver_volume_id" binding:"required"`
	DriverName     string `json:"driver_name" binding:"required"`
	Size           int    `json:"size" binding:"required"`
	Name           string `json:"name" binding:"required"`
	//PVName         string `json:"pv_name" binding:"required"`
	State string `json:"state" binding:"required"`
}

// VolumeRequest ..
type VolumeRequest struct {
	PVName string `json:"pv_name" binding:"required"`
	PVUUID string `json:"pv_uuid" binding:"required"`
}

// GlusterfsSpec ..
type GlusterfsSpec struct {
	Path      string `json:"path"`
	EndPoints string `json:"endpoints"`
	ReadOnly  bool   `json:"readOnly"`
}

// EBSSpec ..
type EBSSpec struct {
	FsType    string `json:"fsType"`
	Partition int    `json:"partition"`
	ReadOnly  bool   `json:"readOnly"`
	VolumeID  string `json:"volumeID"`
}

// GeneratePVLabels ...
func (pv *PVRequest) GeneratePVLabels() map[string]string {
	return map[string]string{
		common.ClusterUidKey(): pv.Cluster.UUID,
	}
}

func (ks *KubernetesObject) ToPVResponse() *PVResponse {
	pv, _ := ks.ToV1PV()
	return PVToResponse(pv)
}

func PVToResponse(pv *v1.PersistentVolume) *PVResponse {
	var resource KubernetesResource
	resource.ParseV1PV(pv)
	annotations := pv.GetAnnotations()
	return &PVResponse{
		Resource: PVResource{
			UUID:         string(pv.ObjectMeta.UID),
			Name:         pv.ObjectMeta.Name,
			Status:       string(pv.Status.Phase),
			TargetStatus: string(common.CurrentCreatedStatus),
			CreatedAt:    pv.ObjectMeta.GetCreationTimestamp().Time,
			UpdatedAt:    pv.ObjectMeta.GetCreationTimestamp().Time,
		},
		Driver: Driver{
			Name: annotations[common.PVVolumeDriverKey()],
		},
		Volume: Volume{
			Name: annotations[common.PVVolumeNameKey()],
			UUID: annotations[common.PVVolumeUidKey()],
		},
		Type:         PersistentVolumeType,
		ReferencedBy: ResourceBaseWithType{},
		Kubernetes:   &resource,
	}
}
