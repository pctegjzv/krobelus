package model

import (
	"krobelus/common"

	"k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/rest"
)

// Query is build from url path param and query params.
// The common format should be :
// 1. /namespaces/<namespace>/<resource_type>/<resource_name>/<sub_resource_type>?cluster_uuid=<cluster_uuid>
// 2. /<resource_type>/<resource_name>?cluster_uud=<cluster_uuid>

type Query struct {
	ClusterUUID string `json:"cluster_uuid"`
	Namespace   string `json:"namespace"`
	Name        string `json:"name"`
	Type        string `json:"type"`

	SubType   string `json:"sub_type"`
	PatchType string `json:"patch_type"`
}

func GetQuery(cluster, kind string, object v1.Object) *Query {
	query := Query{
		ClusterUUID: cluster,
		Namespace:   object.GetNamespace(),
		Name:        object.GetName(),
		Type:        common.GetKubernetesTypeFromKind(kind),
		PatchType:   "application/merge-patch+json",
	}
	return &query
}

type Result struct {
	Body   []byte
	Object runtime.Object
	Code   int
	Error  error

	Extra map[string]interface{}
	// keep a copy of origin data
	Origin *rest.Result
}

func ParseResult(result *rest.Result) *Result {
	if result == nil {
		return &Result{
			Code: 200,
		}
	}

	raw, _ := result.Raw()
	object, _ := result.Get()
	err := result.Error()
	var code int
	result.StatusCode(&code)
	return &Result{
		Body:   raw,
		Object: object,
		Code:   code,
		Error:  err,
		Origin: result,
	}
}
