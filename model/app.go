package model

import (
	"time"

	"krobelus/common"
	"krobelus/pkg/application/v1alpha1"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

type AppListStore struct {
	Apps   []*AppResponse
	Logger common.Log
}

type AppRequest struct {
	Resource   AppResource           `json:"resource" binding:"required"`
	Namespace  Namespace             `json:"namespace" binding:"required"`
	Cluster    Cluster               `json:"cluster" binding:"required"`
	Kubernetes []*KubernetesResource `json:"kubernetes" binding:"required"`
}

type AppServices struct {
	Total     int `json:"total"`
	Running   int `json:"running"`
	Error     int `json:"error"`
	Warning   int `json:"warning"`
	Stopped   int `json:"stopped"`
	Deploying int `json:"deploying"`
	Unknown   int `json:"unknown"`
}

type AppResource struct {
	UUID         string        `json:"uuid" `
	Name         string        `json:"name" binding:"required"`
	Source       string        `json:"source"`
	Category     string        `json:"category"`
	Description  string        `json:"description"`
	CreateMethod string        `json:"create_method"`
	Status       string        `json:"status"`
	State        ResourceState `json:"state"`
	Actions      Actions       `json:"actions"`
	TargetStatus string        `json:"-"`
	Services     *AppServices  `json:"services"`
	CreatedAt    time.Time     `json:"created_at"`
	UpdatedAt    time.Time     `json:"updated_at"`
}

type AppResponse struct {
	Resource         AppResource              `json:"resource"`
	Namespace        Namespace                `json:"namespace"`
	Cluster          Cluster                  `json:"cluster"`
	Services         []*ServiceResponse       `json:"services"`
	Others           []*OtherResourceResponse `json:"others"`
	Type             ResourceType             `json:"type"`
	Kubernetes       []*KubernetesResource    `json:"kubernetes,omitempty"`
	KubernetesBackup []*KubernetesResource    `json:"-"`
}

// ToNewApplicationCrd translate old app to the new CRD format
// Note:
// 1. label selector will be rest to app.uuid
// 2. add annotation about start/stop state
// 3. add sub resources group kind info from kubernetes yaml
func (response *AppResponse) ToNewApplicationCrd() *v1alpha1.Application {
	applicationResource := ApplicationResource{
		Name:         response.Resource.Name,
		Namespace:    response.Namespace.Name,
		Description:  response.Resource.Description,
		CreateMethod: response.Resource.CreateMethod,
		Source:       response.Resource.Source,
		Category:     response.Resource.Category,
	}
	crd := applicationResource.ToAppCrd()
	crd.Spec.Selector = &metav1.LabelSelector{
		MatchLabels: map[string]string{
			common.AppUidKey(): response.Resource.UUID,
		},
	}

	crd.SetLabels(map[string]string{
		common.AppUidKey(): response.Resource.UUID,
	})

	// annotations have been init before
	anno := crd.GetAnnotations()
	if response.Resource.TargetStatus == string(common.TargetStoppedState) {
		anno[common.AppActionKey()] = "stop"
	} else {
		anno[common.AppActionKey()] = "start"
	}
	crd.SetAnnotations(anno)

	for _, resource := range response.Kubernetes {
		groupVersion, _ := schema.ParseGroupVersion(resource.APIVersion)
		crd.Spec.ComponentGroupKinds = append(crd.Spec.ComponentGroupKinds, metav1.GroupKind{
			Kind:  resource.Kind,
			Group: groupVersion.Group,
		})
	}
	return crd
}

func (response *AppResponse) ToNewCommonObjectList() []metav1.Object {
	var result []metav1.Object
	application := *AppCrdToObject(response.ToNewApplicationCrd())
	result = append(result, &application)
	for _, resource := range response.Kubernetes {
		result = append(result, resource)
	}
	return result
}

type AppOtherResourcesUpdate struct {
	DeleteList []*KubernetesResource
	UpdateList []*KubernetesResource
	CreateList []*KubernetesResource
}

type AppOtherResources struct {
	Request  []*KubernetesResource
	Response []*KubernetesResource
	Update   *AppOtherResourcesUpdate
}

type OtherResourceResponse struct {
	Type     ResourceType `json:"type"`
	Resource ResourceBase `json:"resource"`
}

func (ks *KubernetesResource) ToOtherResourceResponse() *OtherResourceResponse {
	return &OtherResourceResponse{
		Type: ResourceType(ks.Kind),
		Resource: ResourceBase{
			Name: ks.Name,
			UUID: GetAlaudaUID(ks),
		},
	}
}

func (ks *KubernetesResource) SetAppIdLabel(uuid string) {
	labels := ks.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	labels[common.AppUidKey()] = uuid
	ks.SetLabels(labels)

}
