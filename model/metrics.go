package model

type InstancesCacheItem struct {
	Kind                string `json:"kind" binding:"required"`
	AvailableReplicas   int    `json:"availableReplicas" binding:"required"`
	ServiceName         string `json:"service_name" binding:"required"`
	ServiceID           string `json:"service_id" binding:"required"`
	UnavailableReplicas int    `json:"unavailableReplicas" binding:"required"`
	StatusReplicas      int    `json:"status_replicas" binding:"required"`
}

type RegionInstancesCacheApps struct {
	Items      []*InstancesCacheItem `json:"items"`
	RegionType string                `json:"region_type"`
}

type RegionInstancesCache struct {
	Apps     RegionInstancesCacheApps `json:"apps" binding:"required"`
	RegionID string                   `json:"region_id" binding:"required"`
}

type Dimensions struct {
	ResourceType string `json:"resource_type"`
	ResourceId   string `json:"resource_id"`
}

type MetricsUnit struct {
	Name       string     `json:"name"`
	Value      int        `json:"value"`
	Dimensions Dimensions `json:"dimensions"`
	Unit       string     `json:"unit"`
}

func GenMetricsUnit(id, name string, value int) *MetricsUnit {
	return &MetricsUnit{
		Name:  name,
		Value: value,
		Unit:  "Count",
		Dimensions: Dimensions{
			ResourceType: "SERVICE",
			ResourceId:   id,
		},
	}
}
