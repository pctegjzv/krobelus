package model

import "encoding/json"

type AllReferencedBy struct {
	UUID   string            `json:"uuid"`
	Name   string            `json:"name"`
	Type   string            `json:"type"`
	Detail *ReferencedDetail `json:"detail,omitempty"`
}

type ReferencedDetail struct {
	// optional configmap key
	Key string `json:"key,omitempty"`
	// env or volume
	Location string `json:"location"`
	//container-name/volume-name
	Value string `json:"value"`
}

type OldReferencedDetail ConfigMapReferencedDetail

// Reference records ref info for service-configmap/pvc...
type Reference struct {
	ReferencedBy    []*AllReferencedBy            `json:"referenced_by,omitempty"`
	KeyReferencedBy map[string][]*AllReferencedBy `json:"key_referenced_by,omitempty"`
}

func (c *ReferencedDetail) Load(content string) {
	var result OldReferencedDetail
	json.Unmarshal([]byte(content), &result)
	c.Key = result.ConfigMapKey
	c.Location = result.Location
	c.Value = result.Value
}
