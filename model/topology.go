package model

import metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"

type TopologyRequest struct {
	Cluster   Cluster
	Namespace Namespace
	Name      string
	Kind      string
}

type TopologyResponse struct {
	Graph *Graph
	Refer *Refer
}

// Graph represents topological relation for kubernetes resources within one namespace
type Graph struct {
	// Nodes represent kubernetes resources
	Nodes map[string]*Node `json:"nodes"`
	// Edges represent relations between these resources
	Edges []*Edge `json:"edges"`
}

// Refer represents topological relation for one kubernetes resource
type Refer struct {
	// Reference represents resources referenced by this resource
	Reference []*ReferNode `json:"reference"`
	// ReferencedBy represents resources which referenced to this resource
	ReferencedBy []*ReferNode `json:"referenced_by"`
}

type ReferNode struct {
	Type string `json:"type"`
	Node *Node  `json:"node"`
}

// Node represents a kubernetes resource with meta data and type data
type Node struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:",inline"`
}

// Edge represents relations between two kubernetes resources
type Edge struct {
	Type string `json:"type"`
	From string `json:"from"`
	To   string `json:"to"`
}
