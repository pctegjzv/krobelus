package model

import (
	"krobelus/common"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"
)

type ServiceBindingRequest struct {
	Resource   ServiceBindingRequestResource `json:"resource" binding:"required"`
	Cluster    Cluster                       `json:"cluster" binding:"required"`
	Namespace  Namespace                     `json:"namespace" binding:"required"`
	Kubernetes *KubernetesObject             `json:"kubernetes" binding:"required"`
}

type ServiceBindingMustQuery struct {
	Cluster   string `form:"cluster" binding:"required`
	Namespace string `form:"namespace" binding:"required`
}

type ServiceBindingRequestResource struct {
	ResourceNameBase
	DisplayName string `json:"display_name"`
}

type ServiceBindingUpdate struct {
	Kubernetes *KubernetesObject `json:"kubernetes"`
	Resource   ResourceUpdate    `json:"resource"`
}

type ServiceBindingResource struct {
	ResourceBasic
}

type ServiceBindingResponse struct {
	Resource           ServiceBindingResource `json:"resource" binding:"required"`
	Namespace          Namespace              `json:"namespace" binding:"required"`
	Cluster            Cluster                `json:"cluster" binding:"required"`
	Type               ResourceType           `json:"type" binding:"required"`
	Kubernetes         *KubernetesObject      `json:"kubernetes,omitempty"`
	ServiceInstanceRef ServiceInstanceRef     `json:"serviceinstance,omitempty"`
	ServiceResource    ServiceResource        `json:"service,omitempty"`
	Credential         map[string]string      `json:"credential"`
}

func ServiceBindingToResponse(sb *scV1beta1.ServiceBinding) *ServiceBindingResponse {
	var object KubernetesObject
	object.ParseServiceBinding(sb)
	return &ServiceBindingResponse{
		Resource: ServiceBindingResource{
			ResourceBasic: ResourceBasic{
				Name:        sb.Name,
				UUID:        string(sb.UID),
				Description: sb.GetAnnotations()[common.AnnotationsDescription],
				CreatedAt:   sb.GetCreationTimestamp().Time,
				UpdatedAt:   sb.GetCreationTimestamp().Time,
			},
		},
		Namespace: Namespace{
			Name: sb.Namespace,
		},
		Kubernetes: &object,
		Type:       common.ServiceBindingType,
		Credential: make(map[string]string),
	}
}

func (ks *KubernetesObject) ToServiceBindingResponse() *ServiceBindingResponse {
	sb, _ := ks.ToServiceBinding()
	return ServiceBindingToResponse(sb)
}
