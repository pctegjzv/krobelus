package model

import (
	"time"
)

// GetUUIDS is used for list api
type GetUUIDS struct {
	UUIDS       []string `json:"uuids"`
	ClusterUUID string   `json:"cluster,omitempty"`
}

type UUIDsWithCluster struct {
	UUIDS       []string `json:"uuids"`
	ClusterUUID string   `json:"cluster"`
}

type PagedResponse struct {
	NumPages int           `json:"num_pages"`
	PageSize int           `json:"page_size"`
	Count    int           `json:"count"`
	Results  []*JsonObject `json:"results"`
}

type ResourceBase struct {
	UUID string `json:"uuid" binding:"required"`
	Name string `json:"name" binding:"required"`
}

// ResourceItem is used when we need to operate on a resource, we need to find out what resources it used.
type ResourceItem struct {
	Name   string       `json:"name"`
	Type   ResourceType `json:"type"`
	Action string       `json:"action"`
}

type ResourceNameBase struct {
	Name string `json:"name" binding:"required"`
	UUID string `json:"uuid,omitempty"`
}

type ResourceUUIDBase struct {
	Name string `json:"name,omitempty"`
	UUID string `json:"uuid" binding:"required"`
}

type ResourceBasic struct {
	UUID        string    `json:"uuid"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

type Namespace ResourceBase

type Cluster ResourceBase

type ResourceBaseWithType struct {
	UUID string `json:"uuid,omitempty"`
	Name string `json:"name,omitempty"`
	Type string `json:"type,omitempty"`
}

type Parent ResourceBaseWithType

type ReferencedResource struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type ReferencedResources map[string][]*ReferencedResource

type JsonObject map[string]interface{}

// alauda resource type
type ResourceType string

const (
	AppType                   ResourceType = "App"
	AlaudaServiceType         ResourceType = "AlaudaService"
	ApplicationType           ResourceType = "Application"
	NamespaceType             ResourceType = "Namespace"
	ConfigMapType             ResourceType = "ConfigMap"
	PersistentVolumeType      ResourceType = "PersistentVolume"
	PersistentVolumeClaimType ResourceType = "PersistentVolumeClaim"
	ClusterServiceBrokerType  ResourceType = "ClusterServiceBrokerType"
	ClusterServiceClassType   ResourceType = "ClusterServiceClassType"
	SecretType                ResourceType = "Secret"
	ResourceQuotaType         ResourceType = "ResourceQuota"
	LimitRange                ResourceType = "LimitRange"
)

type ResourceStatus string

type KubernetesUpdate struct {
	Kubernetes []*KubernetesResource `json:"kubernetes" binding:"required"`
}

// CommonUpdateRequest is a common update model.
// support:
// 1. list of kubernetes resources update
// 2. description update
// 3. create method update
// mainly used for app/service
type CommonUpdateRequest struct {
	Kubernetes []*KubernetesResource `json:"kubernetes"`
	Resource   ResourceUpdate        `json:"resource"`
}
