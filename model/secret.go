package model

import (
	"crypto/sha256"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"strings"

	"krobelus/common"

	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/validation"
)

type SecretGenerator interface {
	GenerateSecret(params map[string]interface{}) (*v1.Secret, error)
}

type Origin map[string]string

type SecretRequest struct {
	Type       string                 `json:"type"`
	Name       string                 `json:"name" binding:"required"`
	Namespace  string                 `json:"namespace" binding:"required"`
	SAName     string                 `json:"service_account"`
	Data       map[string]interface{} `json:"data" binding:"required"`
	AppendHash bool                   `json:"append_hash"`
	Log        common.Log
}

// SecretForOpaque is the default; arbitrary user-defined data
type SecretForOpaque struct{}

// SecretForServiceAccountToken contains a token that identifies a service account to the API
type SecretForServiceAccountToken struct {
	// The name of the ServiceAccount the token identifies
	SAName string `json:"kubernetes.io/service-account.name" binding:"required"`
	// The UID of the ServiceAccount the token identifies
	SAUID string `json:"kubernetes.io/service-account.uid" binding:"required"`
	// A token that identifies the service account to the API
	Token string `json:"token" binding:"required"`
}

// SecretForDockerCfg contains a dockercfg file that follows the same format rules as ~/.dockercfg
type SecretForDockerCfg struct {
	Dockercfg string `json:".dockercfg" binding:"required"`
}

// SecretForBasicAuth contains data needed for basic authentication.
type SecretForBasicAuth struct {
	// Username used for authentication
	Username string `json:"username" binding:"required"`
	// Password or token needed for authentication
	Password string `json:"password" binding:"required"`
}

// SecretForSecretTypeSSHAuth contains data needed for SSH authentication.
type SecretForSecretTypeSSHAuth struct {
	// Private SSH key needed for authentication
	SSHKey string `json:"ssh-privatekey" binding:"required"`
}

// SecretForSecretTypeTLS contains information about a TLS client or server secret. It
// is primarily used with TLS termination of the Ingress resource, but may be
// used in other types.
type SecretForSecretTypeTLS struct {
	// TLS private key.
	Key string `json:"tls.crt" binding:"required"`
	// TLS certificate.
	Cert string `json:"tls.key" binding:"required"`
}

type DockerConfigJson struct {
	Auths DockerCfg `json:"auths"`
}

type DockerCfg map[string]DockerConfigEntry

type DockerConfigEntry struct {
	Username string
	Password string
	Email    string
}

// SecretForDockerConfigJson contains a dockercfg file that follows the same format rules as ~/.docker/config.json
type SecretForDockerConfigJson struct {
	// Username for registry (required)
	Username string `json:"username" binding:"required"`
	// Email for registry (optional)
	Email string `json:"email"`
	// Password for registry (required)
	Password string `json:"password" binding:"required"`
	// Server for registry (required)
	Server string `json:"server" binding:"required"`
}

// AddKeyToSecret add the decoded string to secret's []byte field,
// for all secret data using in a request will be encoded by encoding/json
func AddKeyToSecret(secret *v1.Secret, data map[string]string) error {
	for keyName, val := range data {
		if errs := validation.IsConfigMapKey(keyName); len(errs) != 0 {
			return fmt.Errorf("%q is not a valid key name for a Secret: %s", keyName, strings.Join(errs, ";"))
		}
		if nil == secret.Data {
			secret.Data = map[string][]byte{}
		}
		secret.Data[keyName] = []byte(val)
	}
	return nil
}

// encodeSecret encodes a Secret.
// Data, Kind, Name, and Type are taken into account.
func encodeSecret(sec *v1.Secret) (string, error) {
	// json.Marshal sorts the keys in a stable order in the encoding
	data, err := json.Marshal(map[string]interface{}{"kind": "Secret", "type": sec.Type, "name": sec.Name, "data": sec.Data})
	if err != nil {
		return "", err
	}
	return string(data), nil
}

// hash hashes `data` with sha256 and returns the hex string
func hash(data string) string {
	return fmt.Sprintf("%x", sha256.Sum256([]byte(data)))
}

// SecretHash returns a hash of the Secret.
// The Data, Kind, Name, and Type are taken into account.
func SecretHash(sec *v1.Secret) (string, error) {
	encoded, err := encodeSecret(sec)
	if err != nil {
		return "", err
	}
	h, err := encodeHash(hash(encoded))
	if err != nil {
		return "", err
	}
	return h, nil
}

func encodeHash(hex string) (string, error) {
	if len(hex) < 10 {
		return "", fmt.Errorf("the hex string must contain at least 10 characters")
	}
	enc := []rune(hex[:10])
	for i := range enc {
		switch enc[i] {
		case '0':
			enc[i] = 'g'
		case '1':
			enc[i] = 'h'
		case '3':
			enc[i] = 'k'
		case 'a':
			enc[i] = 'm'
		case 'e':
			enc[i] = 't'
		}
	}
	return string(enc), nil
}

var generators = map[v1.SecretType]SecretGenerator{
	v1.SecretTypeBasicAuth:           SecretForBasicAuth{},
	v1.SecretTypeDockercfg:           SecretForDockerCfg{},
	v1.SecretTypeDockerConfigJson:    SecretForDockerConfigJson{},
	v1.SecretTypeServiceAccountToken: SecretForServiceAccountToken{},
	v1.SecretTypeSSHAuth:             SecretForSecretTypeSSHAuth{},
	v1.SecretTypeTLS:                 SecretForSecretTypeTLS{},
	v1.SecretTypeOpaque:              SecretForOpaque{},
}

func parseToInterface(data map[string]interface{}, i interface{}) error {

	logger := common.GetLoggerByID("")

	bytes, err := json.Marshal(data)

	if err != nil {
		return err
	}

	logger.Debugf("parseToInterface: bytes=%s", string(bytes))

	err = json.Unmarshal(bytes, i)

	if err != nil {
		return err
	}

	logger.Debugf("parseToInterface: interface=%#v", i)

	return nil
}

func (r *SecretRequest) GenerateSecret() (*v1.Secret, error) {

	secret := v1.Secret{
		Type: v1.SecretTypeOpaque,
		ObjectMeta: metav1.ObjectMeta{
			Name:      r.Name,
			Namespace: r.Namespace,
		},
	}

	secret.SetGroupVersionKind(v1.SchemeGroupVersion.WithKind("Secret"))

	sType := v1.SecretType(r.Type)
	if sType == "" {
		sType = v1.SecretTypeOpaque
	}

	generator, found := generators[sType]

	if !found {
		return nil, fmt.Errorf(fmt.Sprintf("invalid secret type: type=%s", sType))
	}

	newSecret, err := generator.GenerateSecret(r.Data)

	if err != nil {
		r.Log.Errorf("generate secret error: err=%s", err)
		return nil, err
	}

	secret.Data = newSecret.Data
	secret.ObjectMeta.Annotations = newSecret.ObjectMeta.Annotations

	if r.AppendHash {
		h, err := SecretHash(&secret)
		if err != nil {
			r.Log.Errorf("secret hash error: err=%s", err)
			return nil, err
		}
		secret.Name = fmt.Sprintf("%s-%s", secret.Name, h)

	}

	r.Log.Debugf("generated secret: secret=%+v", secret)
	return &secret, nil
}

func (s SecretForOpaque) GenerateSecret(params map[string]interface{}) (*v1.Secret, error) {
	secret := &v1.Secret{}
	data := map[string]string{}
	for key, val := range params {
		data[key] = fmt.Sprintf("%s", val)
	}
	if err := AddKeyToSecret(secret, data); err != nil {
		return nil, err
	}
	return secret, nil
}

func (s SecretForBasicAuth) GenerateSecret(params map[string]interface{}) (*v1.Secret, error) {
	if err := parseToInterface(params, &s); err != nil {
		return nil, err
	}
	if "" == s.Password || "" == s.Username {
		return nil, fmt.Errorf("secret data invalid, username and passwod is required, type=%s username=%s password=%s", string(v1.SecretTypeBasicAuth), s.Username, s.Password)
	}
	secret := &v1.Secret{}
	// Put origin data in Data for []byte in json will be encoded by encoding/json
	secret.Data = map[string][]byte{
		v1.BasicAuthUsernameKey: []byte(s.Username),
		v1.BasicAuthPasswordKey: []byte(s.Password),
	}
	return secret, nil
}

func (s SecretForSecretTypeSSHAuth) GenerateSecret(params map[string]interface{}) (*v1.Secret, error) {
	if err := parseToInterface(params, &s); err != nil {
		return nil, err
	}
	if "" == s.SSHKey {
		return nil, fmt.Errorf("secret data invalid, ssh_key is required, type=%s ssh-privatekey=%s", string(v1.SecretTypeSSHAuth), s.SSHKey)
	}
	secret := &v1.Secret{}
	secret.Data = map[string][]byte{
		v1.SSHAuthPrivateKey: []byte(s.SSHKey),
	}
	return secret, nil
}

func (s SecretForSecretTypeTLS) GenerateSecret(params map[string]interface{}) (*v1.Secret, error) {
	if err := parseToInterface(params, &s); err != nil {
		return nil, err
	}
	if "" == s.Key || "" == s.Cert {
		return nil, fmt.Errorf("secret data invalid, tls key and cert is required, type=%s tls.key=%s tls.crt=%s", string(v1.SecretTypeTLS), s.Key, s.Cert)
	}
	secret := &v1.Secret{}
	secret.Data = map[string][]byte{
		v1.TLSCertKey:       []byte(s.Cert),
		v1.TLSPrivateKeyKey: []byte(s.Key),
	}
	return secret, nil
}

func (s SecretForServiceAccountToken) GenerateSecret(params map[string]interface{}) (*v1.Secret, error) {
	if err := parseToInterface(params, &s); err != nil {
		return nil, err
	}
	if "" == s.SAName || "" == s.SAUID || "" == s.Token {
		return nil, fmt.Errorf("secret data invalid, service account name and uid is required, type=%s kubernetes.io/service-account.name=%s kubernetes.io/service-account.uid=%s token=%s", string(v1.SecretTypeServiceAccountToken), s.SAName, s.SAUID, s.Token)
	}
	secret := &v1.Secret{}
	secret.Data = map[string][]byte{
		v1.ServiceAccountTokenKey: []byte(s.Token),
	}
	secret.SetAnnotations(map[string]string{
		v1.ServiceAccountNameKey: s.SAName,
		v1.ServiceAccountUIDKey:  s.SAUID,
	})
	return secret, nil
}

func (s SecretForDockerCfg) GenerateSecret(params map[string]interface{}) (*v1.Secret, error) {
	if err := parseToInterface(params, &s); err != nil {
		return nil, err
	}
	if "" == s.Dockercfg {
		return nil, fmt.Errorf("secret data invalid, dockercfg content can't be empty, type=%s .dockercfg=%s", string(v1.SecretTypeDockercfg), s.Dockercfg)
	}
	secret := &v1.Secret{Data: map[string][]byte{}}
	secret.Data[v1.DockerConfigKey] = []byte(s.Dockercfg)
	return secret, nil
}

// handleDockerCfgJsonContent serializes a ~/.docker/config.json file
func handleDockerCfgJsonContent(username, password, email, server string) ([]byte, error) {
	dockercfgAuth := DockerConfigEntry{
		Username: username,
		Password: password,
		Email:    email,
	}
	dockerCfgJson := DockerConfigJson{
		Auths: map[string]DockerConfigEntry{server: dockercfgAuth},
	}
	return json.Marshal(dockerCfgJson)
}

func (s SecretForDockerConfigJson) GenerateSecret(params map[string]interface{}) (*v1.Secret, error) {
	if err := parseToInterface(params, &s); err != nil {
		return nil, err
	}
	if "" == s.Username || "" == s.Password || "" == s.Server {
		return nil, fmt.Errorf("secret data invalid, username password server can't be empty, type=%s username=%s password=%s server=%s", string(v1.SecretTypeDockerConfigJson), s.Username, s.Password, s.Server)
	}
	dockercfgJsonContent, err := handleDockerCfgJsonContent(s.Username, s.Password, s.Email, s.Server)
	if err != nil {
		return nil, err
	}
	secret := &v1.Secret{Data: map[string][]byte{}}
	secret.Data[v1.DockerConfigJsonKey] = []byte(dockercfgJsonContent)
	return secret, nil
}

func (secret Origin) Parse(data map[string]string) {
	for key, val := range data {
		secret[key] = val
	}
}
func (secret Origin) GetDecodedSecret() Origin {
	newData := Origin{}
	for key, val := range secret {
		if bytes, err := b64.StdEncoding.DecodeString(val); err == nil {
			newData[key] = string(bytes)
		} else {
			newData[key] = val

		}
	}
	return newData
}

func (secret Origin) Encode() Origin {
	newData := Origin{}
	for key, val := range secret {
		bytes := b64.StdEncoding.EncodeToString([]byte(val))
		newData[key] = string(bytes)
	}
	return newData
}
