package model

import (
	"fmt"

	"krobelus/common"

	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

type AlaudaResource interface {
	GetClusterUUID() string
	GetNamespace() string
	GetName() string
	GetLogger() common.Log
	GetUUID() string
	GetType() ResourceType
	// I'm your father!
	IsFamily(object v1.Object) bool
	GetLabelSelector() string
}

func (response *ServiceResponse) GetClusterUUID() string {
	return response.Cluster.UUID
}

func (response *ServiceResponse) GetLabelSelector() string {
	return common.SvcUidKey() + "=" + response.GetUUID()
}

func (response *ServiceResponse) GetNamespace() string {
	return response.Namespace.Name
}

func (response *ServiceResponse) GetName() string {
	return response.Resource.Name
}

func (response *ServiceResponse) GetUUID() string {
	return response.Resource.UUID
}

func (response *ServiceResponse) GetType() ResourceType {
	return AlaudaServiceType
}

func (response *ServiceResponse) IsFamily(object v1.Object) bool {
	for key, value := range object.GetLabels() {
		if key == common.SvcUidKey() {
			return value == response.GetUUID()
		}
	}
	return false
}

func (response *ServiceResponse) GetLogger() common.Log {
	return common.GetLoggerByKV("service_id", response.GetUUID())
}

func (response *AppResponse) GetClusterUUID() string {
	return response.Cluster.UUID
}

func (response *AppResponse) GetNamespace() string {
	return response.Namespace.Name
}

func (response *AppResponse) GetUUID() string {
	return response.Resource.UUID
}

func (response *AppResponse) GetName() string {
	return response.Resource.Name
}

func (response *AppResponse) GetType() ResourceType {
	return AppType
}

func (response *AppResponse) GetLogger() common.Log {
	return common.GetLoggerByKV("app_id", response.GetUUID())
}

func (response *AppResponse) IsFamily(object v1.Object) bool {
	for key, value := range object.GetLabels() {
		if key == common.AppUidKey() {
			return value == response.GetUUID()
		}
	}
	return false
}

func (response *AppResponse) GetLabelSelector() string {
	return common.AppUidKey() + "=" + response.GetUUID()
}

func (application *Application) GetClusterUUID() string {
	return application.Cluster
}

func (application *Application) GetNamespace() string {
	return application.Crd.Namespace
}

func (application *Application) GetName() string {
	return application.Crd.Name
}

func (application *Application) GetUUID() string {
	return GetAppUUID(&application.Crd)
}

func (application *Application) GetType() ResourceType {
	return ApplicationType
}

func (application *Application) GetLogger() common.Log {
	return common.GetLoggerByKV("application",
		fmt.Sprintf("%s:%s", application.GetNamespace(), application.Crd.Name))
}

func (application *Application) IsFamily(object v1.Object) bool {
	for key, value := range object.GetLabels() {
		if key == common.AppUidKey() {
			return value == application.GetUUID()
		}
	}
	return false
}

func (application *Application) GetLabelSelector() string {
	return common.AppUidKey() + "=" + application.GetUUID()
}
