package model

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/pkg/application/v1alpha1"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	"github.com/json-iterator/go"
	"github.com/juju/errors"
	"github.com/spf13/cast"
	appsV1beta1 "k8s.io/api/apps/v1beta1"
	autoScalingV1 "k8s.io/api/autoscaling/v1"
	"k8s.io/api/core/v1"
	policyV1Beta1 "k8s.io/api/policy/v1beta1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
)

type KubernetesSpec JsonObject

type KubernetesStatus JsonObject

// KubernetesResource is a uniformed representation of kubernetes resources. All the kubernetes
// resource object in sdk can be translate to KubernetesResource and vice versa.
// TODO: use KubernetesObject instead
type KubernetesResource struct {
	Kind       string `json:"kind" binding:"required"`
	APIVersion string `json:"apiVersion" binding:"required"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`
	//Metadata   meta_v1.ObjectMeta `json:"metadata" binding:"required"`
	Spec   KubernetesSpec   `json:"spec,omitempty"`
	Status KubernetesStatus `json:"status,omitempty"`
	// For ConfigMap,Secret
	Data map[string]string `json:"data,omitempty"`
	// For ClusterRole/Role
	Rules []map[string]interface{} `json:"rules,omitempty"`
	// For Secret
	StringData map[string]string `json:"stringData,omitempty"`
	// For Secret
	Type string `json:"type,omitempty"`
	//ClusterRoleBinding / RoleBinding
	Subjects []map[string]interface{} `json:"subjects,omitempty"`
	// ClusterRoleBinding / RoleBinding
	RoleRef map[string]interface{} `json:"roleRef,omitempty"`
	// Endpoints
	Subsets []map[string]interface{} `json:"subsets,omitempty"`
	// Revision
	Revision int64 `json:"revision,omitempty"`
	// ServiceAccount
	Secrets                      []map[string]interface{} `json:"secrets,omitempty"`
	ImagePullSecrets             []map[string]interface{} `json:"imagePullSecrets,omitempty"`
	AutomountServiceAccountToken *bool                    `json:"automountServiceAccountToken,omitempty"`
	// StorageClass
	Provisioner          string            `json:"provisioner,omitempty"`
	Parameters           map[string]string `json:"parameters,omitempty"`
	ReclaimPolicy        *string           `json:"reclaimPolicy,omitempty"`
	MountOptions         []string          `json:"mountOptions,omitempty"`
	AllowVolumeExpansion *bool             `json:"allowVolumeExpansion,omitempty"`
	// For Rollback
	RollbackTo map[string]interface{} `json:"rollbackTo,omitempty"`
	// For Event
	Count          int32                  `json:"count,omitempty"`
	Reason         string                 `json:"reason,omitempty"`
	Message        string                 `json:"message,omitempty"`
	Source         map[string]interface{} `json:"source,omitempty"`
	InvolvedObject map[string]interface{} `json:"involvedObject,omitempty"`
	FirstTimestamp metaV1.Time            `json:"firstTimestamp,omitempty"`
	LastTimestamp  metaV1.Time            `json:"lastTimestamp,omitempty"`
}

// Note: use this instead KubernetesResource
// Note: still cannot parse Kind and ApiVersion
type KubernetesObject struct {
	metaV1.TypeMeta                 `json:",inline"`
	metaV1.ObjectMeta               `json:"metadata,omitempty"`
	Spec   map[string]interface{}   `json:"spec,omitempty"`
	Status map[string]interface{}   `json:"status,omitempty"`
	Data   map[string]string        `json:"data,omitempty"`
	Rules  []map[string]interface{} `json:"rules,omitempty"`
	// For Secret
	StringData map[string]string `json:"stringData,omitempty"`
	// For Secret
	Type string `json:"type,omitempty"`
	//ClusterRoleBinding
	Subjects []map[string]interface{} `json:"subjects,omitempty"`
	// ClusterRoleBinding
	RoleRef map[string]interface{} `json:"roleRef,omitempty"`
	// Endpoints
	Subsets []map[string]interface{} `json:"subsets,omitempty"`
	// ServiceAccount
	Secrets                      []map[string]interface{} `json:"secrets,omitempty"`
	ImagePullSecrets             []map[string]interface{} `json:"imagePullSecrets,omitempty"`
	AutomountServiceAccountToken *bool                    `json:"automountServiceAccountToken,omitempty"`
	// StorageClass
	Provisioner          string            `json:"provisioner,omitempty"`
	Parameters           map[string]string `json:"parameters,omitempty"`
	ReclaimPolicy        *string           `json:"reclaimPolicy,omitempty"`
	MountOptions         []string          `json:"mountOptions,omitempty"`
	AllowVolumeExpansion *bool             `json:"allowVolumeExpansion,omitempty"`
	// For Rollback
	RollbackTo map[string]interface{} `json:"rollbackTo,omitempty"`
	// Items. for <Kind>List data
	Items []map[string]interface{} `json:"items,omitempty"`

	// For Event
	Count          int32                  `json:"count,omitempty"`
	Reason         string                 `json:"reason,omitempty"`
	Message        string                 `json:"message,omitempty"`
	Source         map[string]interface{} `json:"source,omitempty"`
	InvolvedObject map[string]interface{} `json:"involvedObject,omitempty"`
	FirstTimestamp metaV1.Time            `json:"firstTimestamp,omitempty"`
	LastTimestamp  metaV1.Time            `json:"lastTimestamp,omitempty"`
}

// KubernetesObjectList describes a struct for kubernetes object list.
type KubernetesObjectList struct {
	Items      []*KubernetesObject `json:"items"`
	ApiVersion string              `json:"apiVersion"`
	Kind       string              `json:"kind"`
}

type Event struct {
	// filled by source
	Type    string
	Reason  string
	Message string

	// filled by AlaudaResource
	Name      string
	Namespace string
	UID       string
}

// Note: if KubernetesObject cannot include all fields, use this
type KubernetesObjectSpecial struct {
	KubernetesObject `json:",inline"`
	Name string      `json:"name,omitempty"`
}

// copy from auto-scaling api group,
type CrossVersionObjectReference struct {
	// Kind of the referent; More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds"
	Kind string `json:"kind" protobuf:"bytes,1,opt,name=kind"`
	// Name of the referent; More info: http://kubernetes.io/docs/user-guide/identifiers#names
	Name string `json:"name" protobuf:"bytes,2,opt,name=name"`
	// API version of the referent
	// +optional
	APIVersion string `json:"apiVersion,omitempty" protobuf:"bytes,3,opt,name=apiVersion"`
}

func UpdateAnnotations(object metaV1.Object, key, value string) {
	anno := object.GetAnnotations()
	if anno == nil {
		anno = make(map[string]string)
	}
	anno[key] = value
	object.SetAnnotations(anno)
}

func (ks *KubernetesObject) GetString() string {
	return ks.getString("/")
}

func (ks *KubernetesObject) getString(split string) string {
	return strings.Join([]string{ks.Namespace, ks.Kind, ks.Name}, split)
}

func (ksl *KubernetesObjectList) String() string {
	var itemsStr []string
	for _, item := range ksl.Items {
		itemsStr = append(itemsStr, fmt.Sprintf("%+v", *item))
	}
	return fmt.Sprintf("Items: [%s]", strings.Join(itemsStr, ","))
}

func (ks *KubernetesResource) ToObject() *KubernetesObject {
	var object KubernetesObject
	bt, _ := json.Marshal(ks)
	_ = json.Unmarshal(bt, &object)
	return &object
}

func (ks *KubernetesResource) SetResourceUidAnnotation(uid string) {
	ano := ks.GetAnnotations()
	if ano == nil {
		ano = make(map[string]string)
	}
	if uid == "" {
		uid = common.NewUUID()
	}
	ano[common.ResourceUidKey()] = uid
	ks.SetAnnotations(ano)
}

func GetResourceUidAnnotation(object metaV1.Object) string {
	return object.GetAnnotations()[common.ResourceUidKey()]
}

func GetAlaudaUID(object metaV1.Object) string {
	uid := GetResourceUidAnnotation(object)
	if uid == "" {
		uid = string(object.GetUID())
	}
	return uid
}

func (ks *KubernetesObject) MatchRequirements(reqs []labels.Requirement) bool {
	set := labels.Set(ks.Labels)
	for _, req := range reqs {
		if !req.Matches(set) {
			return false
		}
	}
	return true
}

func (ks *KubernetesObject) UpdatePodEnv(svcName, svcID, appName string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Update pod template env error")
	}
	var cs []v1.Container
	for _, c := range podTemplateSpec.Spec.Containers {
		c = updateEnv(v1.EnvVar{Name: common.EnvServiceID, Value: svcName}, c)
		c = updateEnv(v1.EnvVar{Name: common.EncServiceName, Value: svcID}, c)
		if appName != "" {
			c = updateEnv(v1.EnvVar{Name: common.EnvAppName, Value: appName}, c)
		}
		cs = append(cs, c)
	}
	podTemplateSpec.Spec.Containers = cs
	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

func (ks *KubernetesResource) UpdatePodEnv(request *ServiceRequest) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Update pod template env error")
	}
	var cs []v1.Container
	for _, c := range podTemplateSpec.Spec.Containers {
		size := "1"
		if !c.Resources.Limits.Cpu().IsZero() {
			size = strconv.FormatFloat(float64(c.Resources.Limits.Cpu().MilliValue())/1024, 'f', 2, 64)
		} else if !c.Resources.Requests.Cpu().IsZero() {
			size = strconv.FormatFloat(float64(c.Resources.Requests.Cpu().MilliValue())/1024, 'f', 2, 64)
		}
		c = updateEnv(v1.EnvVar{Name: common.EnvContainerSize, Value: size}, c)
		c = updateEnv(v1.EnvVar{Name: common.EnvServiceID, Value: request.Resource.UUID}, c)
		c = updateEnv(v1.EnvVar{Name: common.EncServiceName, Value: request.Resource.Name}, c)
		if request.Parent.Name != "" {
			c = updateEnv(v1.EnvVar{Name: common.EnvAppName, Value: request.Parent.Name}, c)
		}
		cs = append(cs, c)
	}
	podTemplateSpec.Spec.Containers = cs
	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

func updateEnv(e v1.EnvVar, c v1.Container) v1.Container {
	found := false
	for i, v := range c.Env {
		if v.Name == e.Name {
			v.Value = e.Value
			c.Env[i] = v
			found = true
			break
		}
	}
	if !found {
		c.Env = append(c.Env, e)
	}
	return c
}

func (ks *KubernetesResource) UpdateSelector(request *ServiceRequest) error {
	/*	selector, ok := ks.Spec["selector"].(map[string]interface{})
		if !ok {
			return nil
		}
		match, ok := selector["matchLabels"].(map[string]interface{})
		if !ok {
			return nil
		}
		if match != nil {
			match[common.SvcUidKey()] = request.Resource.UUID
		}*/
	selector := make(map[string]interface{})
	selector["matchLabels"] = interface{}(map[string]interface{}{
		common.SvcUidKey(): request.Resource.UUID,
	})
	ks.Spec["selector"] = interface{}(selector)
	return nil
}

// UpdatePodTemplateLabels inject alauda metadata info to containers by envs.
func (ks *KubernetesResource) UpdatePodTemplateLabels(request *ServiceRequest) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Update pod template labels error")
	}

	newLabels := request.GenServiceLabels(podTemplateSpec, true)
	if config.GlobalConfig.Pod.ResourceLabel != "" {
		newLabels[config.GlobalConfig.Pod.ResourceLabel] = fmt.Sprintf("%s-%s-%s", ks.Namespace, ks.Kind, ks.Name)
	}
	podTemplateSpec.SetLabels(newLabels)
	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

func (ks *KubernetesObject) AddResourceLabelIfNeeded() {
	if config.GlobalConfig.Pod.ResourceLabel != "" {
		ks.UpdatePodTemplateLabels(map[string]string{
			config.GlobalConfig.Pod.ResourceLabel: ks.getString("-"),
		})
	}
}

func (ks *KubernetesObject) UpdatePodTemplateLabels(labels map[string]string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Update pod template labels error")
	}

	if podTemplateSpec.Labels == nil {
		podTemplateSpec.Labels = make(map[string]string)
	}
	for k, v := range labels {
		podTemplateSpec.Labels[k] = v
	}

	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

func (ks *KubernetesObject) AddPodTemplateLabels(labels map[string]string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Add pod template labels error")
	}

	if podTemplateSpec.Labels == nil {
		podTemplateSpec.Labels = make(map[string]string)
	}
	for k, v := range labels {
		podTemplateSpec.Labels[k] = v
	}

	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

func (ks *KubernetesObject) RemovePodTemplateLabels(keys []string) error {
	podTemplateSpec, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "Remove pod template labels error")
	}

	if podTemplateSpec.Labels == nil {
		return nil
	}
	for _, k := range keys {
		delete(podTemplateSpec.Labels, k)
	}

	result, err := ToInterfaceMap(podTemplateSpec)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil
}

// GetPodTemplatesLabels get pod template labels from pod controller spec
func (ks *KubernetesResource) GetPodTemplatesLabels() (map[string]string, error) {
	template, err := ks.GetPodTemplateSpec()
	if err != nil {
		return nil, errors.Annotate(err, "Get pod template labels error")
	}
	return template.Labels, nil

}

// GetPodVersion retrieve pod version annotation in pod template spec
//  0 means no version found
func (ks *KubernetesResource) GetPodVersion() (int, error) {
	template, err := ks.GetPodTemplateSpec()
	if err != nil {
		return -1, errors.Annotate(err, "get pod version error")
	}
	ano := template.GetAnnotations()
	version, ok := ano[common.SvcVersionKey()]
	if !ok {
		return 0, nil
	}
	return cast.ToIntE(version)
}

func (ks *KubernetesResource) UpdatePodVersionAnnotation(version int) error {
	template, err := ks.GetPodTemplateSpec()
	if err != nil {
		return errors.Annotate(err, "update pod version annotation error")
	}
	ano := template.GetAnnotations()
	if ano == nil {
		ano = make(map[string]string)
	}
	ano[common.SvcVersionKey()] = cast.ToString(version)
	template.SetAnnotations(ano)
	result, err := ToInterfaceMap(template)
	if err != nil {
		return err
	}
	ks.Spec["template"] = result
	return nil

}

func (ks *KubernetesResource) GetScaleTargetRef() (*CrossVersionObjectReference, error) {
	data, ok := ks.Spec["scaleTargetRef"].(map[string]interface{})
	if !ok {
		return nil, errors.New("Parse scaleTargetRef from HPA  spec error")
	}
	return &CrossVersionObjectReference{
		Name:       data["name"].(string),
		APIVersion: data["apiVersion"].(string),
		Kind:       data["kind"].(string),
	}, nil
}

func (ks *KubernetesResource) GetStatefulsetServiceName() (string, error) {
	data, ok := ks.Spec["serviceName"].(string)
	if !ok {
		return "", fmt.Errorf("parse serviceName from statefulset %s error", ks.Name)
	}
	return data, nil
}

func (ks *KubernetesResource) GetPodReplicas() (int, error) {
	if ks.Kind == common.KubernetesKindDaemonSet {
		return 0, nil
	}
	replicas, ok := ks.Spec["replicas"].(float64)
	if !ok {
		return 1, errors.New("Parse replicas from resource spec error")
	}
	return cast.ToIntE(replicas)
}

func (ks *KubernetesObject) GetPodReplicas() (int, error) {
	if ks.Kind == common.KubernetesKindDaemonSet {
		return 0, nil
	}
	replicas, ok := ks.Spec["replicas"].(float64)
	if !ok {
		return 1, errors.New("Parse replicas from resource spec error")
	}
	return cast.ToIntE(replicas)
}

func (ks *KubernetesObject) getInstancesCountByKeys(desiredKey, currentKey string) (*ServiceInstancesCount, error) {
	replicas, ok := ks.Status[desiredKey].(float64)
	if !ok {
		return nil, fmt.Errorf("parse %s from %s error", desiredKey, ks.Kind)
	}
	current, ok := ks.Status[currentKey].(float64)
	if !ok {
		current = 0
	}
	return &ServiceInstancesCount{Desired: cast.ToInt(replicas), Current: cast.ToInt(current)}, nil
}

func (ks *KubernetesObject) GetServicesInstancesCount() (*ServiceInstancesCount, error) {
	if ks.Kind == common.KubernetesKindDeployment {
		return ks.getInstancesCountByKeys("replicas", "availableReplicas")
	}
	if ks.Kind == common.KubernetesKindDaemonSet {
		return ks.getInstancesCountByKeys("desiredNumberScheduled", "numberAvailable")
	}
	if ks.Kind == common.KubernetesKindStatefulSet {
		return ks.getInstancesCountByKeys("replicas", "readyReplicas")
	}
	return nil, fmt.Errorf("unsupported pod controller: %s", ks.Kind)
}

func (ks *KubernetesResource) SetPodReplicas(rs float64) error {
	if ks.Kind == common.KubernetesKindDaemonSet {
		return nil
	}
	ks.Spec["replicas"] = rs
	return nil
}

func (ks *KubernetesObject) SetPodReplicas(rs float64) error {
	if ks.Kind == common.KubernetesKindDaemonSet {
		return nil
	}
	ks.Spec["replicas"] = rs
	return nil
}

func (ks *KubernetesResource) GetPodTemplateSpec() (*v1.PodTemplateSpec, error) {
	data, ok := ks.Spec["template"].(map[string]interface{})
	if !ok {
		return nil, errors.New("No PodTemplateSpec found in resource spec")
	}
	template := KubernetesSpec(data)
	podTemplateSpec, err := template.GetPodTemplateSpec()
	if err != nil {
		return nil, err
	}
	return podTemplateSpec, nil
}

func (ks *KubernetesObject) GetPodTemplateSpec() (*v1.PodTemplateSpec, error) {
	data, ok := ks.Spec["template"].(map[string]interface{})
	if !ok {
		return nil, errors.New("No PodTemplateSpec found in resource spec")
	}
	template := KubernetesSpec(data)
	podTemplateSpec, err := template.GetPodTemplateSpec()
	if err != nil {
		return nil, err
	}
	return podTemplateSpec, nil
}

func (ks *KubernetesObject) GetServiceSpec() (*v1.ServiceSpec, error) {
	data, err := json.Marshal(ks.Spec)
	if err != nil {
		return nil, err
	}
	spec := v1.ServiceSpec{}
	err = json.Unmarshal(data, &spec)
	return &spec, err
}

func (ks *KubernetesObject) GetHPASpec() (*autoScalingV1.HorizontalPodAutoscalerSpec, error) {
	data, err := json.Marshal(ks.Spec)
	if err != nil {
		return nil, err
	}
	spec := autoScalingV1.HorizontalPodAutoscalerSpec{}
	err = json.Unmarshal(data, &spec)
	return &spec, err
}

func (ks *KubernetesResource) UpdatePodTemplateAffinity(uuid string) error {
	template, err := ks.GetPodTemplateSpec()
	if err != nil {
		return err
	}
	affinity := template.Spec.Affinity
	if affinity == nil || affinity.PodAntiAffinity == nil || affinity.PodAntiAffinity.RequiredDuringSchedulingIgnoredDuringExecution == nil {
		return nil
	}
	for _, item := range affinity.PodAntiAffinity.RequiredDuringSchedulingIgnoredDuringExecution {
		for index, expr := range item.LabelSelector.MatchExpressions {
			if expr.Key == common.SvcUidKey() {
				if len(expr.Values) == 0 || (len(expr.Values) == 1 && expr.Values[0] == "") {
					expr.Values = []string{uuid}
					item.LabelSelector.MatchExpressions[index] = expr
				}
			}
		}
	}
	template.Spec.Affinity = affinity
	if result, err := ToInterfaceMap(template); err != nil {
		return err
	} else {
		ks.Spec["template"] = result
		return nil
	}
}

func (ks *KubernetesSpec) GetPodTemplateSpec() (*v1.PodTemplateSpec, error) {
	bt, err := json.Marshal(ks)
	if err != nil {
		return nil, err
	}
	ss := v1.PodTemplateSpec{}
	err = json.Unmarshal(bt, &ss)
	return &ss, err
}

func (object JsonObject) ToV1Affinity() (*v1.Affinity, error) {
	bt, err := json.Marshal(object)
	if err != nil {
		return nil, err
	}
	ss := v1.Affinity{}
	err = json.Unmarshal(bt, &ss)
	return &ss, err
}

func ToInterfaceMap(object interface{}) (result map[string]interface{}, err error) {
	var bt []byte
	if bt, err = json.Marshal(object); err != nil {
		return nil, err
	}
	if err = json.Unmarshal(bt, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (ks *KubernetesObject) GetServiceUUID() string {
	return ks.Labels[common.SvcUidKey()]
}

func (ks *KubernetesResource) ToBytes() ([]byte, error) {
	return json.Marshal(ks)
}

func (ks *KubernetesObject) ToBytes() ([]byte, error) {
	if len(ks.RollbackTo) != 0 {
		return json.Marshal(KubernetesObjectSpecial{KubernetesObject: *ks, Name: ks.Name})
	}
	return json.Marshal(ks)
}

func BytesToKubernetesObject(bytes []byte) (*KubernetesObject, error) {
	var json = jsoniter.ConfigFastest
	var k KubernetesObject
	err := json.Unmarshal(bytes, &k)
	return &k, err
}

func BytesToKubernetesObjectList(bytes []byte) (*KubernetesObjectList, error) {
	var k KubernetesObjectList
	err := json.Unmarshal(bytes, &k)
	return &k, err
}

func (ks *KubernetesResource) ToV1HPA() (*autoScalingV1.HorizontalPodAutoscaler, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, err
	}
	ss := autoScalingV1.HorizontalPodAutoscaler{}
	err = json.Unmarshal(bt, &ss)
	return &ss, err
}

func (ks *KubernetesObject) ToSecret() (*v1.Secret, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	secret := v1.Secret{}
	err = json.Unmarshal(bt, &secret)
	secret.Kind = string(SecretType)
	secret.APIVersion = common.KubernetesAPIVersionV1
	return &secret, err
}
func (ks *KubernetesObject) ToConfigMap() (*v1.ConfigMap, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	configmap := v1.ConfigMap{}
	err = json.Unmarshal(bt, &configmap)
	configmap.Kind = string(ConfigMapType)
	configmap.APIVersion = common.KubernetesAPIVersionV1
	return &configmap, err
}

func (ks *KubernetesObject) ParseConfigMap(cm *v1.ConfigMap) error {
	bt, err := json.Marshal(cm)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	//ks.APIVersion = cm.APIVersion
	//ks.Kind = cm.Kind
	return nil
}

func (ks *KubernetesObject) ToNamespace() (*v1.Namespace, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	ns := v1.Namespace{}
	err = json.Unmarshal(bt, &ns)
	ns.Kind = string(NamespaceType)
	ns.APIVersion = common.KubernetesAPIVersionV1
	return &ns, err
}

func (ks *KubernetesObject) ToPod() (*v1.Pod, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	ns := v1.Pod{}
	err = json.Unmarshal(bt, &ns)
	ns.Kind = string("Pod")
	ns.APIVersion = common.KubernetesAPIVersionV1
	return &ns, err
}

func (ks *KubernetesObject) ToResourceQuota() (*v1.ResourceQuota, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	rq := v1.ResourceQuota{}
	err = json.Unmarshal(bt, &rq)
	rq.Kind = string(ResourceQuotaType)
	rq.APIVersion = common.KubernetesAPIVersionV1
	return &rq, err
}

func (ks *KubernetesObject) ToApplication() (*v1alpha1.Application, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes resource to bytes error")
	}
	obj := v1alpha1.Application{}
	err = json.Unmarshal(bt, &obj)
	obj.Kind = string(ApplicationType)
	obj.APIVersion = "app.k8s.io/v1alpha1"
	return &obj, err
}

// Service
//
//
//
//
func (ks *KubernetesResource) ToV1Service() (*v1.Service, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, err
	}
	service := v1.Service{}
	err = json.Unmarshal(bt, &service)
	return &service, err
}

func (ks *KubernetesResource) ToPDB() (*policyV1Beta1.PodDisruptionBudget, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, err
	}
	pdb := policyV1Beta1.PodDisruptionBudget{}
	err = json.Unmarshal(bt, &pdb)
	return &pdb, err
}

func (ks *KubernetesResource) ParseV1Service(service *v1.Service) error {
	bt, err := json.Marshal(service)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bt, ks)
	if err != nil {
		return err
	}
	return nil
}

// Namespace
//
//
//
//
func (ks *KubernetesResource) ToNamespace() (*v1.Namespace, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, err
	}
	namespace := v1.Namespace{}
	err = json.Unmarshal(bt, &namespace)
	return &namespace, err
}

// GetPodSpec retrieve pod spec from various pod controllers.
func (ks *KubernetesResource) GetPodSpec() (*v1.PodSpec, error) {
	template, err := ks.GetPodTemplateSpec()
	if err != nil {
		return nil, errors.Annotate(err, "Get pod spec error")
	}
	return &template.Spec, nil
}

// Get containers list
func (ks *KubernetesResource) GetContainers(useRequest bool) ([]*ServiceContainer, error) {
	spec, err := ks.GetPodSpec()
	if err != nil {
		return nil, err
	}
	return generateContainerList(spec.Containers, useRequest)
}

func GetKubernetesPodController(resources []*KubernetesResource) *KubernetesResource {
	for _, r := range resources {
		if common.IsPodController(r.Kind) {
			return r
		}
	}
	return nil
}

func generateContainerList(containers []v1.Container, useRequest bool) ([]*ServiceContainer, error) {
	var list []*ServiceContainer
	for _, container := range containers {
		item := &ServiceContainer{
			Image: container.Image,
			Size: ServiceContainerSize{
				CPU: container.Resources.Requests.Cpu().String(),
				MEM: container.Resources.Requests.Memory().String(),
			},
		}
		if !useRequest {
			item.Size = ServiceContainerSize{
				CPU: container.Resources.Limits.Cpu().String(),
				MEM: container.Resources.Limits.Memory().String(),
			}
		}
		list = append(list, item)
	}
	return list, nil
}

func (ks *KubernetesResource) ToV1PV() (*v1.PersistentVolume, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes pv resource to bytes error")
	}
	pv := v1.PersistentVolume{}
	err = json.Unmarshal(bt, &pv)
	return &pv, err
}

func (ks *KubernetesResource) ParseV1PV(pv *v1.PersistentVolume) error {
	bt, err := json.Marshal(pv)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

func (ks *KubernetesObject) ToV1PV() (*v1.PersistentVolume, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes pv resource to bytes error")
	}
	pv := v1.PersistentVolume{}
	err = json.Unmarshal(bt, &pv)
	return &pv, err
}

func (ks *KubernetesObject) ToV1PVC() (*v1.PersistentVolumeClaim, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal kubernetes pvc resource to bytes error")
	}
	pvc := v1.PersistentVolumeClaim{}
	err = json.Unmarshal(bt, &pvc)
	return &pvc, err
}

func (ks *KubernetesObject) ToBroker() (*scV1beta1.ClusterServiceBroker, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal service broker resource to bytes error")
	}
	broker := scV1beta1.ClusterServiceBroker{}
	err = json.Unmarshal(bt, &broker)
	return &broker, err
}

func (ks *KubernetesObject) ToServiceClass() (*scV1beta1.ClusterServiceClass, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal service broker resource to bytes error")
	}
	serviceclass := scV1beta1.ClusterServiceClass{}
	err = json.Unmarshal(bt, &serviceclass)
	return &serviceclass, err
}

func (ks *KubernetesObject) ToServicePlan() (*scV1beta1.ClusterServicePlan, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal service broker resource to bytes error")
	}
	serviceplan := scV1beta1.ClusterServicePlan{}
	err = json.Unmarshal(bt, &serviceplan)
	return &serviceplan, err
}

func (ks *KubernetesObject) ToServiceInstance() (*scV1beta1.ServiceInstance, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal service broker resource to bytes error")
	}
	serviceinstance := scV1beta1.ServiceInstance{}
	err = json.Unmarshal(bt, &serviceinstance)
	return &serviceinstance, err
}

func (ks *KubernetesObject) ToServiceBinding() (*scV1beta1.ServiceBinding, error) {
	bt, err := ks.ToBytes()
	if err != nil {
		return nil, errors.Annotate(err, "marshal service broker resource to bytes error")
	}
	servicebinding := scV1beta1.ServiceBinding{}
	err = json.Unmarshal(bt, &servicebinding)
	return &servicebinding, err
}

// ParsePV parses PV object to KubernetesObject.
func (ks *KubernetesObject) ParsePV(pv *v1.PersistentVolume) error {
	bt, err := json.Marshal(pv)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

func (ks *KubernetesObject) ParsePVC(pvc *v1.PersistentVolumeClaim) error {
	bt, err := json.Marshal(pvc)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

func (ks *KubernetesObject) ToKubernetesResource() (*KubernetesResource, error) {
	var res KubernetesResource
	bt, err := json.Marshal(ks)
	if err != nil {
		return nil, err
	}
	if err = json.Unmarshal(bt, &res); err != nil {
		return nil, err
	}

	return &res, nil
}

func (ks *KubernetesObject) ToResource() *KubernetesResource {
	var res KubernetesResource
	bt, _ := json.Marshal(ks)
	_ = json.Unmarshal(bt, &res)
	return &res
}

func BytesToKubernetesResource(bytes []byte) (*KubernetesResource, error) {
	var json = jsoniter.ConfigFastest
	var res KubernetesResource
	if err := json.Unmarshal(bytes, &res); err != nil {
		return nil, err
	} else {
		return &res, nil
	}
}

func ToKubernetesResource(object interface{}) (*KubernetesResource, error) {
	var res KubernetesResource
	data, err := json.Marshal(object)
	if err != nil {
		return &res, err
	}
	if err := json.Unmarshal(data, &res); err != nil {
		return nil, err
	} else {
		return &res, nil
	}
}

func RestResultToResource(result *rest.Result) (*KubernetesResource, error) {
	bt, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "get raw bytes error for rest client result")
	}
	return BytesToKubernetesResource(bt)
}

// IsKubernetesResourceListEqual compare a list of kubernetes resource by bytes.
// mainly used for app update.
func IsKubernetesResourceListEqual(left, right []*KubernetesResource) (bool, error) {
	l, err := json.Marshal(left)
	if err != nil {
		return false, err
	}
	r, err := json.Marshal(right)
	if err != nil {
		return false, err
	}
	return bytes.Equal(l, r), nil
}

func IsPodTemplateEqual(new, old *KubernetesResource) (bool, error) {
	newSpec, err := new.GetPodTemplateSpec()
	if err != nil {
		return false, err
	}
	oldSpec, err := old.GetPodTemplateSpec()
	if err != nil {
		return false, err
	}

	l, err := json.Marshal(newSpec)
	if err != nil {
		return false, err
	}
	r, err := json.Marshal(oldSpec)
	if err != nil {
		return false, err
	}
	return bytes.Equal(l, r), nil
}

func GetSpecificResource(items []*KubernetesResource, name, kind string) *KubernetesResource {
	for _, item := range items {
		if item.Kind == kind && item.Name == name {
			return item
		}
	}
	return nil
}

func IsKubernetesResourceEqual(left, right *KubernetesResource) (bool, error) {
	a := []*KubernetesResource{left}
	b := []*KubernetesResource{right}
	return IsKubernetesResourceListEqual(a, b)
}

func (ks *KubernetesObject) ParseBroker(broker *scV1beta1.ClusterServiceBroker) error {
	bt, err := json.Marshal(broker)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	//ks.APIVersion = broker.APIVersion
	//ks.Kind = broker.Kind
	return nil
}

func (ks *KubernetesObject) ParseServiceClass(serviceclass *scV1beta1.ClusterServiceClass) error {
	bt, err := json.Marshal(serviceclass)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

func (ks *KubernetesObject) ParseServicePlan(serviceplan *scV1beta1.ClusterServicePlan) error {
	bt, err := json.Marshal(serviceplan)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

func (ks *KubernetesObject) ParseServiceInstance(serviceInstance *scV1beta1.ServiceInstance) error {
	bt, err := json.Marshal(serviceInstance)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

func (ks *KubernetesObject) ParseServiceBinding(serviceBinding *scV1beta1.ServiceBinding) error {
	bt, err := json.Marshal(serviceBinding)
	if err != nil {
		return err
	}
	if err = json.Unmarshal(bt, ks); err != nil {
		return err
	}
	return nil
}

func RestResultToServiceResources(result *rest.Result) ([]*KubernetesResource, error) {
	res, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "Get raw bytes error")
	}
	list := &v1.ServiceList{}
	if err := json.Unmarshal(res, list); err != nil {
		return nil, err
	}
	items := make([]*KubernetesResource, len(list.Items))
	for index, item := range list.Items {
		resource := KubernetesResource{}
		if err := resource.ParseV1Service(&item); err != nil {
			return nil, err
		}
		resource.APIVersion = list.APIVersion
		items[index] = &resource
	}
	return items, nil
}

func FindControllerRevisionFromRestResult(result *rest.Result, revision int64) ([]byte, error) {
	res, err := result.Raw()
	if err != nil {
		return nil, errors.Annotate(err, "Get raw bytes")
	}
	list := &appsV1beta1.ControllerRevisionList{}
	if err := json.Unmarshal(res, list); err != nil {
		return nil, errors.Annotate(err, "To revision list")
	}
	for _, item := range list.Items {
		if item.Revision != revision {
			continue
		}
		return item.Data.MarshalJSON()
	}
	return nil, errors.New("Controller revision not found")
}

func AppCrdToObject(app *v1alpha1.Application) *KubernetesObject {
	bt, _ := json.Marshal(app)
	var obj KubernetesObject
	json.Unmarshal(bt, &obj)
	return &obj
}

func ObjectsToResources(objects []*KubernetesObject) []*KubernetesResource {
	var result []*KubernetesResource
	for _, object := range objects {
		result = append(result, object.ToResource())
	}
	return result
}

// GenKubernetesApplicationEvent generate  a kubernetes application event
// Note: uuid means alauda uuid
func GenKubernetesApplicationEvent(ev *Event) v1.Event {
	event := v1.Event{
		TypeMeta: metaV1.TypeMeta{
			Kind:       common.KubernetesKindEvent,
			APIVersion: common.KubernetesAPIVersionV1,
		},
		ObjectMeta: metaV1.ObjectMeta{
			Name:      ev.Name + "-" + common.GenerateRandString(5),
			Namespace: ev.Namespace,
		},
		InvolvedObject: v1.ObjectReference{
			Kind:      common.KubernetesKindApplication,
			Name:      ev.Name,
			Namespace: ev.Namespace,
			UID:       types.UID(ev.UID),
		},
		Message: ev.Message,
		Reason:  ev.Reason,
		Source: v1.EventSource{
			Component: common.Component,
		},
		Count:          1,
		Type:           ev.Type,
		FirstTimestamp: metaV1.Time{Time: time.Now().UTC()},
		LastTimestamp:  metaV1.Time{Time: time.Now().UTC()},
	}
	return event
}
