package model

import (
	"krobelus/common"
	scV1beta1 "krobelus/pkg/servicecatalog/apis/servicecatalog/v1beta1"

	"k8s.io/apimachinery/pkg/types"
)

type ServiceClassResource struct {
	ResourceBasic
}

type ServiceClassResponse struct {
	ClusterServiceClass *ServiceClassObj       `json:"clusterserviceclass,omitempty"`
	ServiceClassPlans   []*ServicePlanResponse `json:"clusterserviceplans,omitempty"`
	ServiceClassBroker  *BrokerResource        `json:"clusterservicebroker,omitempty"`
}
type ServiceClassObj struct {
	Resource   ServiceClassResource   `json:"resource" binding:"required"`
	Cluster    Cluster                `json:"cluster" binding:"required"`
	Type       ResourceType           `json:"type" binding:"required"`
	Kubernetes map[string]interface{} `json:"kubernetes,omitempty"`
}

type ServiceClassItemResponse struct {
	scV1beta1.ClusterServiceClassSpec
	UUID   types.UID       `json:"uuid" binding:"required"`
	Broker *BrokerResource `json:"broker" binding:"required"`
}

type ServicePlanResponse struct {
	Kubernetes map[string]interface{} `json:"kubernetes,omitempty"`
}

func ServiceClassToResponse(cm *scV1beta1.ClusterServiceClass) *ServiceClassResponse {
	var object KubernetesObject
	object.ParseServiceClass(cm)
	return &ServiceClassResponse{
		ClusterServiceClass: &ServiceClassObj{
			Resource: ServiceClassResource{
				ResourceBasic{
					Name:        cm.Name,
					UUID:        string(cm.UID),
					Description: cm.GetAnnotations()[common.AnnotationsDescription],
					CreatedAt:   cm.GetCreationTimestamp().Time,
					UpdatedAt:   cm.GetCreationTimestamp().Time,
				},
			},
			Kubernetes: object.Spec,
			Type:       ClusterServiceClassType,
		},
	}
}

func (ks *KubernetesObject) ToServiceClassResponse() *ServiceClassResponse {
	scclass, _ := ks.ToServiceClass()
	return ServiceClassToResponse(scclass)
}
