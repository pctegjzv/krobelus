package model

import (
	"encoding/json"
	"fmt"
	"strings"

	"krobelus/common"
	"krobelus/pkg/application/v1alpha1"

	"github.com/Jeffail/gabs"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ApplicationResource struct {
	Name      string `json:"name" binding:"required"`
	Namespace string `json:"namespace" binding:"required"`

	// optional and for compatibility
	Source       string `json:"source"`
	Category     string `json:"category"`
	Description  string `json:"description"`
	CreateMethod string `json:"create_method"`
}

type ApplicationRequest struct {
	Resource   ApplicationResource `json:"resource" binding:"required"`
	Crd        *KubernetesObject   `json:"crd"`
	Kubernetes []*KubernetesObject `json:"kubernetes" binding:"required"`
}

type ApplicationUpdateRequest struct {
	Kubernetes []*KubernetesObject `json:"kubernetes" binding:"required"`
	// Deleted stores the objects that will be deleted after update.
	Deleted KubernetesObjectList
}

type ApplicationImportResourcesRequest struct {
	Kubernetes []*KubernetesObject `json:"kubernetes" binding:"required"`
}

type ApplicationExportResourcesRequest struct {
	Kubernetes []*KubernetesObject `json:"kubernetes" binding:"required"`
}

type Application struct {
	Cluster    string
	Crd        KubernetesObject
	Kubernetes []*KubernetesObject
}

type ApplicationResponse struct {
	Kubernetes []*KubernetesObject `json:"kubernetes"`
}

type ApplicationKubernetesObjectList struct {
	Kubernetes []metav1.Object `json:"kubernetes"`
}

func (res *ApplicationResponse) ToCommonKubernetesObjectList() ApplicationKubernetesObjectList {
	var objects []metav1.Object
	for _, obj := range res.Kubernetes {
		objects = append(objects, obj)
	}
	return ApplicationKubernetesObjectList{
		Kubernetes: objects,
	}
}

func GetAppSelectorName(name, namespace string) string {
	return fmt.Sprintf("%s.%s", name, namespace)
}

// ToAppCrd generate a app crd resource with componentGroups unset and pending phase
func (r ApplicationResource) ToAppCrd() *v1alpha1.Application {
	var app v1alpha1.Application
	app.Kind = "Application"
	app.APIVersion = "app.k8s.io/v1alpha1"
	app.Name = r.Name
	app.Namespace = r.Namespace

	app.Spec.Selector = &metav1.LabelSelector{
		MatchLabels: map[string]string{
			common.AppNameKey(): GetAppSelectorName(r.Name, r.Namespace),
		},
	}

	app.Spec.Descriptor = v1alpha1.Descriptor{
		Description: r.Description,
		Type:        r.Category,
	}

	app.SetAnnotations(map[string]string{
		common.AppCreateMethodKey(): r.CreateMethod,
		common.AppSourceKey():       r.Source,
	})

	app.Spec.AssemblyPhase = v1alpha1.Pending

	return &app
}

// GenSimpleApplicationUpdateData will generate the data needed for patch method
// phase: application phase
// key: application annotation key, if empty, ignore this
// value: application annotation value
func GenSimpleApplicationUpdateData(phase, key, value string) []byte {
	jsonObj := gabs.New()
	jsonObj.SetP(phase, "spec.assemblyPhase")
	if key != "" {
		jsonObj.Set(value, "metadata", "annotations", key)
	}
	return jsonObj.Bytes()
}

func GenAnnotaionPatchData(key, value string) []byte {
	jsonObj := gabs.New()
	jsonObj.Set(value, "metadata", "annotations", key)
	return jsonObj.Bytes()
}

func (r ApplicationResource) ToKubernetesObject() *KubernetesObject {
	crd := r.ToAppCrd()
	return AppCrdToObject(crd)
}

func (ks *KubernetesObject) SetLabel(key, value string) {
	labels := ks.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}
	labels[key] = value
	ks.SetLabels(labels)
}

func (ks *KubernetesObject) RemoveLabel(key string) {
	labels := ks.GetLabels()
	if labels != nil {
		delete(labels, key)
	}
}

func (ks *KubernetesObject) SetAno(key, value string) {
	an := ks.GetAnnotations()
	if an == nil {
		an = map[string]string{}
	}
	an[key] = value
	ks.SetAnnotations(an)
}

func (ks *KubernetesObject) ToAppCrd() *v1alpha1.Application {
	bt, _ := ks.ToBytes()
	var app v1alpha1.Application
	json.Unmarshal(bt, &app)
	return &app

}

func (ks *KubernetesObject) IsAppStoppingAction() bool {
	return ks.GetAnnotations()[common.AppActionKey()] == "stopping"
}

func (ks *KubernetesObject) IsAppStartingAction() bool {
	return ks.GetAnnotations()[common.AppActionKey()] == "starting"
}

func GetAppLabelSelector(app *v1alpha1.Application) string {
	selector := app.Spec.Selector.MatchLabels

	result := ""
	for k, v := range selector {
		result = fmt.Sprintf("%s=%s,", k, v)
	}

	return strings.TrimSuffix(result, ",")
}

func GetAppUUID(object metav1.Object) string {
	return object.GetLabels()[common.AppUidKey()]
}

// InjectApplicationEnv do env inject, different from the old uuid format
// svc_id: name.type.namespace
// svc_name: name.type
// app_name: <app-name>
func (ks *KubernetesObject) InjectApplicationEnv(appName string) {
	svcName := fmt.Sprintf("%s.%s", ks.Name, common.GetKubernetesTypeFromKind(ks.Kind))
	svcId := fmt.Sprintf("%s.%s", svcName, ks.Namespace)
	ks.UpdatePodEnv(svcName, svcId, appName)
}

// SetApplicationNamespace set resource's namespace to application's namespace if not equal
// namespace: application's namespace, should not be ""
func (ks *KubernetesObject) SetApplicationNamespace(namespace string) {
	if ks.Namespace != "" && ks.Namespace != namespace {
		ks.SetNamespace(namespace)
	}
	if !common.ClusterScopeResourceKind[ks.Kind] {
		ks.SetNamespace(namespace)
	}
}
