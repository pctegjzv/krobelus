package model

import (
	"sort"

	"k8s.io/api/core/v1"
)

type KubernetesResourceObject struct {
	UUID        string
	Name        string
	NamespaceID string
	ResourceID  string
	APIVersion  string
	Kind        string
	Kubernetes  string
}

func (r *ConfigMapRequest) ToKubernetesResourceObject(cm *v1.ConfigMap) *KubernetesResourceObject {
	return &KubernetesResourceObject{
		UUID:        string(cm.UID),
		Name:        cm.Name,
		NamespaceID: r.Namespace.UUID,
		ResourceID:  r.Resource.UUID,
		APIVersion:  r.Kubernetes.APIVersion,
		Kind:        r.Kubernetes.Kind,
		Kubernetes:  "",
	}
}

type AlaudaInternalResource struct {
	CreatedAt     string        `json:"created_at,omitempty"`
	CreatedBy     string        `json:"created_by"`
	Members       []interface{} `json:"members,omitempty"`
	Name          string        `json:"name"`
	Namespace     string        `json:"namespace"`
	NamespaceUUID string        `json:"namespace_uuid,omitempty"`
	ProjectUUID   string        `json:"project_uuid,omitempty"`
	RegionID      string        `json:"region_id"`
	SpaceUUID     string        `json:"space_uuid,omitempty"`
	Teams         []interface{} `json:"teams,omitempty"`
	Type          string        `json:"type"`
	UUID          string        `json:"uuid"`
}

// FilterSingleByInternalResources will find the only resource that match the internal resource (by name)
func FilterSingleByInternalResources(internal []AlaudaInternalResource, responses []*AppResponse) (*AppResponse, []*AppResponse) {
	for idx, resp := range responses {
		for _, res := range internal {
			if resp.Resource.UUID == res.UUID {
				return resp, append(responses[:idx], responses[idx+1:]...)
			}
		}
	}
	return nil, responses
}

// IsInInternalResources find if one app is a valid one(sync with jakiro)
func (response *AppResponse) IsInInternalResources(int []AlaudaInternalResource) bool {
	for _, item := range int {
		if item.UUID == response.Resource.UUID {
			return true
		}
	}
	return false
}

var ResourceListKindOrder = []string{
	"Namespace",
	"Secret",
	"ConfigMap",
	"PersistentVolume",
	"ServiceAccount",
	"Service",
	"Pod",
	"ReplicationController",
	"Deployment",
	"DaemonSet",
	"Ingress",
	"Job",
}

var AppKindOrder = []string{
	"Namespace",
	"Secret",
	"ConfigMap",
	"PersistentVolume",
	"ServiceAccount",
	"Pod",
	"ReplicationController",
	"Ingress",
	"Job",
	"PodDisruptionBudget",
	"HorizontalPodAutoscaler",
	"Service",
	"Deployment",
	"DaemonSet",
	"StatefulSet",
}

type ResourceSlice struct {
	Items interface{}
	Order []string
}

func (s ResourceSlice) Len() int {
	res, ok := s.Items.([]*KubernetesResource)
	if ok {
		return len(res)
	}
	obj, ok := s.Items.([]*KubernetesObject)
	if ok {
		return len(obj)
	}
	return 0
}

func (s ResourceSlice) Swap(i, j int) {
	res, ok := s.Items.([]*KubernetesResource)
	if ok {
		res[i], res[j] = res[j], res[i]
		return
	}
	obj, ok := s.Items.([]*KubernetesObject)
	if ok {
		obj[i], obj[j] = obj[j], obj[i]
	}
}

func (s ResourceSlice) Less(i, j int) bool {
	x, y := len(s.Order), len(s.Order)
	var kindI, kindJ string
	res, ok := s.Items.([]*KubernetesResource)
	if ok {
		kindI = res[i].Kind
		kindJ = res[j].Kind
	} else {
		obj, ok := s.Items.([]*KubernetesObject)
		if ok {
			kindI = obj[i].Kind
			kindJ = obj[j].Kind
		}
	}
	for c, k := range s.Order {
		if k == kindI {
			x = c
		}
		if k == kindJ {
			y = c
		}
	}
	return x < y
}

func SortKubernetesObjects(resources interface{}, order []string, reverse bool) {
	if !reverse {
		sort.Sort(ResourceSlice{resources, order})
	} else {
		sort.Sort(sort.Reverse(ResourceSlice{resources, order}))
	}
}

type ResourceRatio struct {
	Cpu int
	Mem int
}

func (r *ResourceRatio) IsDefault() bool {
	return r.Cpu == 1 && r.Mem == 1
}
