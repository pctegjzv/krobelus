package model

import (
	"time"

	"krobelus/common"

	"k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ServiceRequest struct {
	Resource   ServiceResource       `json:"resource" binding:"required"`
	Namespace  Namespace             `json:"namespace" binding:"required"`
	Cluster    Cluster               `json:"cluster" binding:"required"`
	Kubernetes []*KubernetesResource `json:"kubernetes" binding:"required"`
	Parent     Parent                `json:"parent"`
	RollbackTo string                `json:"-"`
}

type ServiceInstancesCount struct {
	Desired int `json:"desired"`
	Current int `json:"current"`
}

type ServiceResource struct {
	UUID         string                 `json:"uuid" `
	Name         string                 `json:"name" binding:"required"`
	State        ResourceState          `json:"state"`
	Actions      Actions                `json:"actions"`
	Status       string                 `json:"status"`
	NewStatus    string                 `json:"new_status"`
	TargetStatus string                 `json:"-"`
	TargetCount  int                    `json:"-"`
	Kind         string                 `json:"kind"`
	CreateMethod string                 `json:"create_method"`
	Instances    *ServiceInstancesCount `json:"instances"`
	Containers   []*ServiceContainer    `json:"containers,omitempty"`
	CreatedAt    time.Time              `json:"created_at"`
	UpdatedAt    time.Time              `json:"updated_at"`
}

type ServiceContainerSize struct {
	CPU string `json:"cpu,omitempty"`
	MEM string `json:"mem,omitempty"`
}

type Actions struct {
	Retry    bool `json:"retry"`
	Rollback bool `json:"rollback"`
}

type ResourceState struct {
	Current string `json:"-"`
	Target  string `json:"-"`
}

type ServiceContainer struct {
	Image string               `json:"image,omitempty"`
	Size  ServiceContainerSize `json:"size,omitempty"`
}

type ServiceResponse struct {
	Resource         ServiceResource       `json:"resource"`
	Namespace        Namespace             `json:"namespace" `
	Cluster          Cluster               `json:"cluster" `
	Kubernetes       []*KubernetesResource `json:"kubernetes,omitempty"`
	KubernetesBackup []*KubernetesResource `json:"-"`
	Type             ResourceType          `json:"type" `
	Parent           Parent                `json:"parent"`
	Referenced       *ReferencedResources  `json:"referenced,omitempty"`
}

type ServiceRevision struct {
	Revision          int64     `json:"revision"`
	ChangeCause       string    `json:"changeCause"`
	CreationTimestamp time.Time `json:"creationTimestamp"`
}

type ServiceListStore struct {
	Services []*ServiceResponse
	Logger   common.Log
	UUIDs    []string
	// If Namespace is not empty, indicate that all the services in this belong to the same namespace
	Namespace string

	// If Selector is not empty we can use this to select all pods for these services
	PodSelector string
}

type KubernetesYaml struct {
	Kubernetes       []*KubernetesResource `json:"kubernetes,omitempty"`
	KubernetesBackup []*KubernetesResource `json:"kubernetes_backup,omitempty"`
}

func (response *ServiceResponse) ToServiceRequest() *ServiceRequest {
	if response == nil {
		return nil
	}
	return &ServiceRequest{
		Resource: ServiceResource{
			UUID: response.Resource.UUID,
			Name: response.Resource.Name,
		},
		Namespace: Namespace{
			UUID: response.Namespace.UUID,
			Name: response.Namespace.Name,
		},
		Cluster: Cluster{
			UUID: response.Cluster.UUID,
			Name: response.Cluster.Name,
		},
		Parent: Parent{
			UUID: response.Parent.UUID,
			Name: response.Parent.Name,
		},
		Kubernetes: response.Kubernetes,
	}
}

// GenServiceLabels append service uuid labels to kubernetes resources
// If withName is true, also append service name label
func (request *ServiceRequest) GenServiceLabels(object v1.Object, withName bool) map[string]string {
	labels := object.GetLabels()
	if labels == nil {
		labels = map[string]string{}
	}

	labels[common.SvcUidKey()] = request.Resource.UUID
	if withName {
		labels[common.SvcNameKey()] = request.Resource.Name
	}
	if request.Parent.UUID != "" {
		labels[common.AppUidKey()] = request.Parent.UUID
	}
	if request.Parent.Name != "" {
		if withName {
			labels[common.AppNameKey()] = request.Parent.Name
		}
	}
	return labels
}

func (store *ServiceListStore) GetServiceUUIDs() []string {
	if len(store.UUIDs) > 0 {
		return store.UUIDs
	}

	var uuids []string

	for _, item := range store.Services {
		uuids = append(uuids, item.Resource.UUID)
	}
	return uuids
}
