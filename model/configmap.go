package model

import (
	"encoding/json"

	"krobelus/common"

	"github.com/sirupsen/logrus"
	"k8s.io/api/core/v1"
)

type ConfigMapResourceRequest struct {
	Name        string `json:"name"`
	UUID        string `json:"uuid,omitempty"`
	Description string `json:"description,omitempty"`
}

type ResourceUpdate struct {
	Description  *string `json:"description,omitempty"`
	CreateMethod *string `json:"create_method,omitempty"`
}

type ConfigMapUpdate struct {
	Kubernetes *KubernetesObject `json:"kubernetes"`
	Resource   ResourceUpdate    `json:"resource"`
}

type ConfigMapRequest struct {
	Resource   ConfigMapResourceRequest `json:"resource" binding:"required"`
	Cluster    Cluster                  `json:"cluster" binding:"required"`
	Namespace  Namespace                `json:"namespace" binding:"required"`
	Kubernetes *KubernetesObject        `json:"kubernetes" binding:"required"`
}

type ConfigMapResource struct {
	ResourceBasic
}

type ConfigMapReferencedDetail struct {
	// optional configmap key
	ConfigMapKey string `json:"configmap_key,omitempty"`
	Location     string `json:"location"`
	Value        string `json:"value"`
}

func (c *ConfigMapReferencedDetail) ToString() string {
	bytes, _ := json.Marshal(c)
	return string(bytes)
}

func (c *ConfigMapReferencedDetail) Load(content string) {
	var result ConfigMapReferencedDetail
	json.Unmarshal([]byte(content), &result)
	c.ConfigMapKey = result.ConfigMapKey
	c.Location = result.Location
	c.Value = result.Value
}

type ConfigMapAllReferencedBy struct {
	UUID   string                     `json:"uuid"`
	Name   string                     `json:"name"`
	Type   ResourceType               `json:"type"`
	Detail *ConfigMapReferencedDetail `json:"detail"`
}

type ConfigMapResponse struct {
	Resource        ConfigMapResource                      `json:"resource" binding:"required"`
	Namespace       Namespace                              `json:"namespace" binding:"required"`
	Cluster         Cluster                                `json:"cluster" binding:"required"`
	Type            ResourceType                           `json:"type" binding:"required"`
	ReferencedBy    []*ConfigMapAllReferencedBy            `json:"referenced_by,omitempty"`
	KeyReferencedBy map[string][]*ConfigMapAllReferencedBy `json:"key_referenced_by,omitempty"`
	Kubernetes      *KubernetesObject                      `json:"kubernetes,omitempty"`
}

func (c *ConfigMapResponse) ToResourceRef() *ReferencedResource {
	return &ReferencedResource{
		UUID: c.Resource.UUID,
		Name: c.Resource.Name,
	}
}

func (c *ConfigMapUpdate) IsOnlyUpdateDB() bool {
	return c.Kubernetes == nil
}

func ConfigMapToResponse(cm *v1.ConfigMap) *ConfigMapResponse {
	var object KubernetesObject
	object.ParseConfigMap(cm)
	logrus.Debugf("[FUCK] %+v, %+v", cm, object)
	return &ConfigMapResponse{
		Resource: ConfigMapResource{
			ResourceBasic{
				Name:        cm.Name,
				UUID:        GetAlaudaUID(&object),
				Description: cm.GetAnnotations()[common.AnnotationsDescription],
				CreatedAt:   cm.GetCreationTimestamp().Time,
				UpdatedAt:   cm.GetCreationTimestamp().Time,
			},
		},
		Namespace: Namespace{
			Name: cm.Namespace,
		},
		Kubernetes: &object,
		Type:       ConfigMapType,
	}
}

func (ks *KubernetesObject) ToConfigMapResponse() *ConfigMapResponse {
	cm, _ := ks.ToConfigMap()
	return ConfigMapToResponse(cm)
}
