package db

import (
	"database/sql"
	"encoding/json"
	"time"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
	"gopkg.in/doug-martin/goqu.v4"
)

const (
	TypeField    = "type"
	QueueIDField = "queue_id"
	OutputField  = "output"
	InputField   = "input"
)

type StatusStore struct {
	Status *model.AlaudaResourceStatus
	Logger common.Log
}

type StatusRow struct {
	UUID         string         `db:"uuid"`
	Type         string         `db:"type"`
	CurrentState string         `db:"current_state"`
	TargetState  string         `db:"target_state"`
	QueueID      string         `db:"queue_id"`
	Output       sql.NullString `db:"output"`
	Input        string         `db:"input"`
	CreatedAt    time.Time      `db:"created_at"`
	UpdatedAt    time.Time      `db:"updated_at"`
}

func (store *StatusStore) ToRecord() goqu.Record {
	params, err := json.Marshal(store.Status.Input)
	if err != nil {
		panic(err.Error())
	}
	return goqu.Record{
		UUIDField:         store.Status.UUID,
		TypeField:         store.Status.Type,
		CurrentStateField: store.Status.CurrentState,
		TargetStateField:  store.Status.TargetState,
		InputField:        string(params),
		QueueIDField:      store.Status.QueueID,
		OutputField:       store.Status.Output,
	}
}

func (row *StatusRow) ToResponse() *model.AlaudaResourceStatus {
	params := map[string]string{}
	if err := json.Unmarshal([]byte(row.Input), &params); err != nil {
		panic(err.Error())
	}
	return &model.AlaudaResourceStatus{
		UUID:         row.UUID,
		Type:         model.ResourceType(row.Type),
		CurrentState: common.KubernetesResourceStatus(row.CurrentState),
		TargetState:  common.ResourceTargetState(row.TargetState),
		QueueID:      row.QueueID,
		Output:       NullStringToDefaultString(row.Output),
		Input:        params,
		CreatedAt:    row.CreatedAt,
		UpdatedAt:    row.UpdatedAt,
	}
}

func (store *StatusStore) Insert(tx *goqu.TxDatabase) error {
	if tx == nil {
		_, err := GoQuDB.From(StatusTable).Insert(store.ToRecord()).Exec()
		return err
	} else {
		_, err := tx.From(StatusTable).Insert(store.ToRecord()).Exec()
		return err
	}
}

func (store *StatusStore) Retrieve(eq goqu.Ex) (*model.AlaudaResourceStatus, error) {
	row := StatusRow{}
	result, err := GoQuDB.From(StatusTable).Where(eq).ScanStruct(&row)
	if err != nil {
		return nil, errors.Annotate(err, "get status")
	}
	if !result {
		return nil, errors.Annotate(common.ErrResourceNotExist, "get status")
	}
	return row.ToResponse(), nil
}

func (store *StatusStore) Set(tx *goqu.TxDatabase) (int64, error) {
	eq := goqu.Ex{UUIDField: store.Status.UUID}
	params, err := json.Marshal(store.Status.Input)
	if err != nil {
		return 0, err
	}
	record := goqu.Record{
		TypeField:         string(store.Status.Type),
		CurrentStateField: store.Status.CurrentState,
		TargetStateField:  store.Status.TargetState,
		QueueIDField:      store.Status.QueueID,
		InputField:        string(params),
		OutputField:       "",
		UpdatedAtField:    goqu.L("now()"),
	}
	if tx != nil {
		if result, err := tx.From(StatusTable).Where(eq).Update(record).Exec(); err != nil {
			return 0, err
		} else {
			return result.RowsAffected()
		}
	} else {
		if result, err := GoQuDB.From(StatusTable).Where(eq).Update(record).Exec(); err != nil {
			return 0, err
		} else {
			return result.RowsAffected()
		}
	}
}

func (store *StatusStore) UpdateStatus() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	_, err = store.Update(tx)
	return err
}

func (store *StatusStore) Update(tx *goqu.TxDatabase) (int64, error) {
	eq := goqu.Ex{UUIDField: store.Status.UUID}
	record := goqu.Record{
		CurrentStateField: store.Status.CurrentState,
		OutputField:       store.Status.Output,
		UpdatedAtField:    goqu.L("now()"),
	}
	if result, err := tx.From(StatusTable).Where(eq).Update(record).Exec(); err != nil {
		return 0, err
	} else {
		return result.RowsAffected()
	}
}

func (store *StatusStore) InsertOrSet(tx *goqu.TxDatabase) error {
	if _, err := store.Retrieve(goqu.Ex{UUIDField: store.Status.UUID}); err != nil {
		if common.IsNotExistError(err) {
			return store.Insert(tx)
		} else {
			return err
		}
	} else {
		_, err := store.Set(tx)
		return err
	}
}

// UpdateServiceStatus will update service's current state both in service table and status table.
// When a service task has finished, service's current state will be changed accordingly.
func (store *StatusStore) UpdateServiceStatus() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	if _, err = store.Update(tx); err != nil {
		tx.Rollback()
		return err
	}

	record := goqu.Record{
		CurrentStateField: store.Status.CurrentState,
		UpdatedAtField:    goqu.L("now()"),
	}
	if _, err = updateService(record, goqu.Ex{UUIDField: store.Status.UUID}, tx); err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

// UpdateAppStatus will update app's current state both in service table and status table.
// When an app task has finished, app's current state will be changed accordingly.
func (store *StatusStore) UpdateAppStatus() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	if _, err = store.Update(tx); err != nil {
		tx.Rollback()
		return err
	}

	record := goqu.Record{
		CurrentStateField: store.Status.CurrentState,
		UpdatedAtField:    goqu.L("now()"),
	}
	if _, err = updateApp(record, goqu.Ex{UUIDField: store.Status.UUID}, tx); err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}

func InsertStatus(status *model.AlaudaResourceStatus) error {
	store := StatusStore{Status: status}
	_, err := GoQuDB.From(StatusTable).Insert(store.ToRecord()).Exec()
	return err
}

// InsertStatusBulk will insert many tasks in bulk.
func InsertStatusBulk(stores []*StatusStore, tx *goqu.TxDatabase) error {
	var records []goqu.Record
	for _, store := range stores {
		records = append(records, store.ToRecord())
	}
	_, err := tx.From(StatusTable).Insert(records).Exec()
	return err
}

// GetDBTime will get time in database.
func GetDBTime() (time.Time, error) {
	result := time.Time{}
	rows, err := GoQuDB.Query("SELECT now()")
	if err != nil {
		return result, err
	}
	defer rows.Close()
	if rows.Next() {
		if err = rows.Scan(&result); err != nil {
			return result, err
		}
	}
	return result, nil
}

func GetStatus(uuid string) (*model.AlaudaResourceStatus, error) {
	store := StatusStore{
		Status: &model.AlaudaResourceStatus{UUID: uuid},
		Logger: common.GetLoggerByServiceID(uuid),
	}
	return store.Retrieve(goqu.Ex{UUIDField: store.Status.UUID})
}

func ListStatus(updatedAt *time.Time, states []common.KubernetesResourceStatus) ([]*model.AlaudaResourceStatus, error) {
	var rows []StatusRow
	var items []*model.AlaudaResourceStatus
	var expressions []goqu.Expression

	if updatedAt != nil {
		expressions = append(expressions, goqu.I(UpdatedAtField).Lt(updatedAt))
	}
	if len(states) != 0 {
		expressions = append(expressions, goqu.I(CurrentStateField).Eq(states))
	}
	if err := GoQuDB.From(StatusTable).Where(expressions...).ScanStructs(&rows); err != nil {
		return items, err
	}
	for _, row := range rows {
		items = append(items, row.ToResponse())
	}
	return items, nil
}

// GetDatabaseTime will get time in database.
// This api is mainly for worker monitor, retry will be triggered if a task always stays in processing status.
func GetDatabaseTime() (time.Time, error) {
	result := time.Time{}
	rows, err := GoQuDB.Query("SELECT now()")
	if err != nil {
		return result, err
	}
	defer rows.Close()
	if rows.Next() {
		if err = rows.Scan(&result); err != nil {
			return result, err
		}
	}
	return result, nil
}
