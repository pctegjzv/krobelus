package db

import (
	"fmt"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
)

type PVCStore struct {
	UUID     string
	Request  *model.PVCRequest
	Response *model.PVCResponse
	Logger   common.Log
}

func AppendRefsToPVCs(pvcs []*model.PVCResponse) ([]*model.PVCResponse, error) {
	var result []*model.PVCResponse
	var errList []string
	for _, item := range pvcs {
		store := PVCStore{
			Response: item,
			UUID:     item.Resource.UUID,
		}
		if err := store.AppendRefsToResponse(); err != nil {
			errList = append(errList, fmt.Sprintf("%s:%s", store.UUID, err.Error()))
		} else {
			result = append(result, store.Response)
		}
	}
	if len(result) > 0 {
		if len(errList) == 0 {
			return result, nil
		} else {
			return result, errors.New(fmt.Sprintf("Append refs to pvc list error: %+v", errList))
		}
	} else {
		return nil, errors.New(fmt.Sprintf("Append refs to pvc list error: %+v", errList))
	}

}
