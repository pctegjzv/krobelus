package db

import (
	"github.com/go-sql-driver/mysql"
	"github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cast"
)

// get db error code from postgres / mysql
func GetDBErrCode(err error) string {
	code := ""
	if IsPostgres() {
		if err, ok := err.(*pq.Error); ok {
			code = err.Code.Name()
		}
	}
	if IsMysql() {
		if err, ok := err.(*mysql.MySQLError); ok {
			code = cast.ToString(err.Number)
		}
	}
	logrus.Debugf("Parsed db error code : %s", code)
	return code
}

func IsUniqueViolationError(err error) bool {
	return GetDBErrCode(err) == "unique_violation"
}
