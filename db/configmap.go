package db

import (
	"fmt"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
)

type ConfigMapStore struct {
	UUID     string
	Request  *model.ConfigMapRequest
	Response *model.ConfigMapResponse
	Logger   common.Log
}

func AppendRefsToConfigMaps(cms []*model.ConfigMapResponse) ([]*model.ConfigMapResponse, error) {
	var result []*model.ConfigMapResponse
	var errList []string
	for _, item := range cms {
		store := ConfigMapStore{
			Response: item,
			UUID:     item.Resource.UUID,
		}
		if err := store.AppendRefsToResponse(); err != nil {
			errList = append(errList, fmt.Sprintf("%s:%s", store.UUID, err.Error()))
		} else {
			result = append(result, store.Response)
		}
	}
	if len(result) > 0 {
		if len(errList) == 0 {
			return result, nil
		} else {
			return result, errors.New(fmt.Sprintf("Append refs to configmap list error: %+v", errList))
		}
	} else {
		return nil, errors.New(fmt.Sprintf("Append refs to configmap list error: %+v", errList))
	}

}
