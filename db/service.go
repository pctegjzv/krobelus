package db

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"time"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
	"github.com/spf13/cast"
	"gopkg.in/doug-martin/goqu.v4"
)

const (
	ServiceColumnAppID            = "app_id"
	ServiceColumnAppName          = "app_name"
	ServiceColumnKubernetesBackup = "kubernetes_backup"
)

type ServiceStore struct {
	// UUID is a shortcut for request/response.resource.UUID
	UUID                 string
	Request              *model.ServiceRequest
	UpdateRequest        *model.CommonUpdateRequest
	Response             *model.ServiceResponse
	Status               *model.AlaudaResourceStatus
	ReferenceObjectsList []ReferencedObjects
	Logger               common.Log
	Parent               *AppStore
}

type ServiceRow struct {
	UUID             string         `db:"uuid"`
	Name             string         `db:"name"`
	ClusterID        string         `db:"cluster_id"`
	ClusterName      string         `db:"cluster_name"`
	NamespaceID      string         `db:"namespace_id"`
	NamespaceName    string         `db:"namespace_name"`
	AppID            sql.NullString `db:"app_id"`
	AppName          sql.NullString `db:"app_name"`
	Kubernetes       string         `db:"kubernetes"`
	CreateMethod     string         `db:"create_method"`
	KubernetesBackup sql.NullString `db:"kubernetes_backup"`
	CurrentState     sql.NullString `db:"current_state"`
	TargetState      sql.NullString `db:"target_state"`
	CreatedAt        time.Time      `db:"created_at"`
	UpdatedAt        time.Time      `db:"updated_at"`
}

func (store *ServiceStore) ToRecord() (goqu.Record, error) {
	req := store.Request
	bytes, err := json.Marshal(req.Kubernetes)
	if err != nil {
		return nil, errors.Annotate(err, "marshal service kubernetes to bytes error")
	}
	if req.Resource.CreateMethod == "" {
		req.Resource.CreateMethod = common.CreateThroughYAML
	}
	return goqu.Record{
		UUIDField:             req.Resource.UUID,
		NameField:             req.Resource.Name,
		ClusterUUIDField:      req.Cluster.UUID,
		ClusterNameField:      req.Cluster.Name,
		NamespaceUUIDField:    req.Namespace.UUID,
		NamespaceNameField:    req.Namespace.Name,
		ServiceColumnAppID:    req.Parent.UUID,
		ServiceColumnAppName:  req.Parent.Name,
		KubernetesField:       string(bytes),
		CreateMethodField:     req.Resource.CreateMethod,
		KubernetesBackupField: "[]",
		CurrentStateField:     store.Status.CurrentState,
		TargetStateField:      store.Status.TargetState,
	}, nil
}

func (row *ServiceRow) ToResponse() (*model.ServiceResponse, error) {
	current := NullStringToDefaultString(row.CurrentState)
	target := NullStringToDefaultString(row.TargetState)
	response := &model.ServiceResponse{
		Resource: model.ServiceResource{
			UUID:         row.UUID,
			Name:         row.Name,
			Status:       current,
			TargetStatus: target,
			State:        model.ResourceState{Current: current, Target: target},
			CreatedAt:    row.CreatedAt,
			UpdatedAt:    row.UpdatedAt,
			CreateMethod: row.CreateMethod,
		},
		Cluster: model.Cluster{
			UUID: row.ClusterID,
			Name: row.ClusterName,
		},
		Namespace: model.Namespace{
			UUID: row.NamespaceID,
			Name: row.NamespaceName,
		},
		Parent: model.Parent{
			UUID: NullStringToDefaultString(row.AppID),
			Name: NullStringToDefaultString(row.AppName),
		},
		Type: model.AlaudaServiceType,
	}
	if err := json.Unmarshal([]byte(row.Kubernetes), &response.Kubernetes); err != nil {
		return nil, err
	}
	kubernetesBackup := SetDefaultForEmptyString(row.KubernetesBackup, "[]")
	if err := json.Unmarshal([]byte(kubernetesBackup), &response.KubernetesBackup); err != nil {
		return nil, err
	}
	response.Resource.Kind = model.GetKubernetesPodController(response.Kubernetes).Kind
	return response, nil
}

// Get service uuid from request and response
//
//
func (store *ServiceStore) GetServiceUUID() string {
	uuid := store.UUID
	if uuid == "" && store.Request != nil {
		uuid = store.Request.Resource.UUID
	}
	if uuid == "" && store.Response != nil {
		uuid = store.Response.Resource.UUID
	}
	return uuid
}

func findHPA(ks []*model.KubernetesResource) *model.KubernetesResource {
	for _, k := range ks {
		if k.Kind == common.KubernetesKindHPA {
			return k
		}
	}
	return nil
}

// FindPodController find pod controller resource from service's kubernetes resource list
// If nothing found, return nil. there should not happen!
func (store *ServiceStore) FindPodController() *model.KubernetesResource {
	return model.GetKubernetesPodController(store.Response.Kubernetes)
}

func (store *ServiceStore) UpdateResourceForScale(action common.ServiceScaleAction) error {
	ks := store.Response.Kubernetes
	podController := store.FindPodController()
	hpaResource := findHPA(ks)
	if podController == nil || hpaResource == nil {
		return fmt.Errorf("missing pod controller or HPA resource in service: %s", store.UUID)
	}
	if podController.Kind == common.KubernetesKindDaemonSet {
		return fmt.Errorf("deamonset does not support auto-scaling. service: %s", store.UUID)
	}
	replicas, err := podController.GetPodReplicas()
	if err != nil {
		return err
	}
	hpa, err := hpaResource.ToV1HPA()
	if err != nil {
		return err
	}
	newReplicas := replicas
	if action == common.ScaleUp {
		if cast.ToInt32(replicas+1) > hpa.Spec.MaxReplicas {
			return fmt.Errorf("max replicas reached, will not scale up: %s %d", store.UUID, hpa.Spec.MaxReplicas)
		}
		newReplicas = replicas + 1
	}
	if action == common.ScaleDown {
		if cast.ToInt32(replicas-1) < *hpa.Spec.MinReplicas {
			return fmt.Errorf("min replicas reached, will not scale up: %s %d", store.UUID, hpa.Spec.MaxReplicas)
		}
		newReplicas = replicas - 1
	}

	for i, k := range store.Response.Kubernetes {
		if common.IsPodController(k.Kind) {
			resource := &store.Response.Kubernetes[i]
			(*resource).Spec["replicas"] = newReplicas
		}
	}
	return nil
}

// Get region uuid from request and response
//
//
func (store *ServiceStore) GetRegionUUID() string {
	uuid := ""
	if store.Request != nil {
		uuid = store.Request.Cluster.UUID
	}
	if uuid == "" && store.Response != nil {
		uuid = store.Response.Cluster.UUID
	}
	return uuid
}

// GetService retrieve service and it's ref data from db. If basic is true,
// only service data will be retrieved
func (store *ServiceStore) GetService(basic bool) error {
	response, err := retrieveService(getEx(store.UUID))
	store.Response = response

	if err != nil {
		return err
	}

	if basic {
		return err
	}

	store.Response = response
	ref, err := store.RetrieveReferences()
	if err != nil {
		return err
	} else {
		store.Response.Referenced = ref
	}
	return nil
}

// Create Service
//
//
func (store *ServiceStore) Create(tx *goqu.TxDatabase) (err error) {
	commit := false
	if tx == nil {
		if tx, err = GoQuDB.Begin(); err != nil {
			return err
		}
		commit = true
	}

	services := []*ServiceStore{store}
	if err = insertServices(services, tx); err != nil {
		tx.Rollback()
		return err
	}

	status := StatusStore{Status: store.Status, Logger: store.Logger}
	if err = status.Insert(tx); err != nil {
		tx.Rollback()
		return err
	}

	for _, item := range store.ReferenceObjectsList {
		if item.IsEmpty() {
			continue
		}
		err = item.InsertReferences(tx)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	if commit {
		tx.Commit()
	}
	return nil
}

// List service will generate service list.
//
//
func ListServices(uuids []string) ([]*model.ServiceResponse, error) {
	if len(uuids) == 0 {
		return []*model.ServiceResponse{}, nil
	}
	return listServices(goqu.Ex{UUIDField: uuids})
}

func ListServicesByPVC(uuids []string, pvcUUID string) ([]*model.ServiceResponse, error) {
	if len(uuids) == 0 || "" == pvcUUID {
		return []*model.ServiceResponse{}, nil
	}

	return listServicesWithRef(goqu.Ex{
		UUIDField:       uuids,
		TargetTypeField: model.PersistentVolumeClaimType,
		TargetUUIDField: pvcUUID,
	})
}

func isServiceDeleted(s *model.ServiceResponse) bool {
	if s.Resource.TargetStatus == string(common.TargetDeletedState) && s.Resource.Status == string(common.CurrentDeletedStatus) {
		return true
	}
	return false
}

// GetService will generate service response.
// If basic is true, it will get simple information from table krobelus_services .
// If basic is false, it will get information from table krobelus_services and
// kubernetes_resources. If failed, synchronous operation to kubernetes will be triggered.
// TODO: use store.GetService instead
func GetService(uuid string, basic bool) (*model.ServiceResponse, error) {
	response, err := retrieveService(goqu.Ex{UUIDField: uuid})

	if err != nil {
		return response, err
	}

	if basic {
		return response, err
	}
	return response, err
}

// Delete will update both service table, status table and reference table.
func (store *ServiceStore) Delete(tx *goqu.TxDatabase) (err error) {
	commit := false
	if tx == nil {
		if tx, err = GoQuDB.Begin(); err != nil {
			return err
		}
		commit = true
	}

	if err = store.updateServiceStatus(tx); err != nil {
		tx.Rollback()
		return err
	}

	if err = store.setResourceStatus(tx); err != nil {
		tx.Rollback()
		return err
	}

	if err := store.DeleteReferences(); err != nil {
		tx.Rollback()
		return err
	}

	if commit {
		tx.Commit()
	}
	return nil
}

// Update will update both service table and status table. Service response will be reload after
// transaction committed.
func (store *ServiceStore) Update(tx *goqu.TxDatabase) (err error) {
	commit := false
	if tx == nil {
		if tx, err = GoQuDB.Begin(); err != nil {
			return err
		}
		commit = true
	}

	if err = store.updateServiceInfo(tx); err != nil {
		tx.Rollback()
		return err
	}

	if err = store.setResourceStatus(tx); err != nil {
		tx.Rollback()
		return err
	}

	if err := store.DeleteReferences(); err != nil {
		tx.Rollback()
		return err
	}

	for _, item := range store.ReferenceObjectsList {
		if item.IsEmpty() {
			continue
		}
		err = item.InsertReferences(tx)
		if err != nil {
			tx.Rollback()
			return err
		}
	}

	if commit {
		tx.Commit()
		return store.reloadService()
	}
	return nil
}

// StartService will update both service table and status table. Service response will be reload after
// transaction committed.
func StartService(store *ServiceStore) error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	err = store.updateServiceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.setResourceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return store.reloadService()
}

// StopService will update both service table and status table. Service response will be reload after
// transaction committed.
func StopService(store *ServiceStore) error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	err = store.updateServiceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.setResourceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return store.reloadService()
}

// Retry will update both service table and status table.
// Service with nil status or not deploying current state will be ignored.
// Service response will be reload after transaction committed.
func (store *ServiceStore) Retry() error {
	if store.Status == nil || !store.Status.CurrentState.IsDeployingStatus() {
		return nil
	}

	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	err = store.updateServiceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.setResourceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return store.reloadService()
}

// RollbackService will update both service table and status table. Service response will be reload after
// transaction committed.
func RollbackService(store *ServiceStore) error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	err = store.updateServiceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.setResourceStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return store.reloadService()
}

func (store *ServiceStore) UpdateKubernetesFromResponse() error {
	data, err := json.Marshal(store.Response.Kubernetes)
	if err != nil {
		return err
	}
	record := goqu.Record{
		KubernetesField: string(data),
		UpdatedAtField:  goqu.L("now()"),
	}
	ex := goqu.Ex{UUIDField: store.Response.Resource.UUID}

	if _, err = GoQuDB.From(ServiceTable).Where(ex).Update(record).Exec(); err != nil {
		return err
	}
	return nil
}

func (store *ServiceStore) getCreateMethodUpdate() string {
	createMethod := store.Response.Resource.CreateMethod
	update := store.UpdateRequest
	if update != nil {
		if update.Resource.CreateMethod != nil {
			createMethod = *update.Resource.CreateMethod
		}
	}
	return createMethod
}

// updateServiceInfo will update service yaml and status in table krobelus_services.
func (store *ServiceStore) updateServiceInfo(tx *goqu.TxDatabase) error {
	previous, err := json.Marshal(store.Response.Kubernetes)
	if err != nil {
		return err
	}
	current, err := json.Marshal(store.Request.Kubernetes)
	if err != nil {
		return err
	}

	rd := goqu.Record{
		KubernetesField:               string(current),
		ServiceColumnKubernetesBackup: string(previous),
		CurrentStateField:             store.Status.CurrentState,
		TargetStateField:              store.Status.TargetState,
		UpdatedAtField:                goqu.L("now()"),
		CreateMethodField:             store.getCreateMethodUpdate(),
	}
	ex := goqu.Ex{UUIDField: store.GetServiceUUID()}
	_, err = updateService(rd, ex, tx)
	return err
}

// updateServiceStatus will update service status in table krobelus_services.
func (store *ServiceStore) updateServiceStatus(tx *goqu.TxDatabase) error {
	rd := goqu.Record{
		CurrentStateField: store.Status.CurrentState,
		TargetStateField:  store.Status.TargetState,
		UpdatedAtField:    goqu.L("now()"),
	}
	ex := goqu.Ex{UUIDField: store.GetServiceUUID()}
	_, err := updateService(rd, ex, tx)
	return err
}

// setResourceStatus will update service status in table krobelus_status.
func (store *ServiceStore) setResourceStatus(tx *goqu.TxDatabase) error {
	status := StatusStore{Status: store.Status}
	_, err := status.Set(tx)
	return err
}

// reloadService will retrieve service in table krobelus_services.
func (store *ServiceStore) reloadService() error {
	res, err := retrieveService(goqu.Ex{UUIDField: store.GetServiceUUID()})
	store.Response = res
	return err
}

func insertServices(stores []*ServiceStore, tx *goqu.TxDatabase) error {
	var records []goqu.Record
	for _, store := range stores {
		if record, err := store.ToRecord(); err != nil {
			return err
		} else {
			records = append(records, record)
		}
	}

	_, err := tx.From(ServiceTable).Insert(records).Exec()
	return err
}

func retrieveService(eq goqu.Ex) (*model.ServiceResponse, error) {
	row := ServiceRow{}
	result, err := GoQuDB.From(ServiceTable).Where(eq).ScanStruct(&row)
	if err != nil {
		return nil, errors.Annotate(err, "get service")
	}
	if !result {
		return nil, errors.Annotate(common.ErrResourceNotExist, "get service")
	}
	response, err := row.ToResponse()
	if err != nil {
		return response, err
	}
	if isServiceDeleted(response) {
		return nil, errors.Annotate(common.ErrResourceNotExist, "get service")
	}
	return response, err
}

func listServices(eq goqu.Ex) ([]*model.ServiceResponse, error) {
	var items []*model.ServiceResponse
	var rows []ServiceRow
	err := GoQuDB.From(ServiceTable).Where(eq).ScanStructs(&rows)
	if err != nil {
		return nil, err
	}
	for _, item := range rows {
		if res, err := item.ToResponse(); err != nil {
			return items, err
		} else {
			if isServiceDeleted(res) {
				continue
			}
			items = append(items, res)
		}
	}
	return items, nil
}

func listServicesWithRef(eq goqu.Ex) ([]*model.ServiceResponse, error) {
	var items []*model.ServiceResponse
	var rows []ServiceRow

	err := GoQuDB.From(goqu.L(ServiceTable).As("s")).
		Select(
			goqu.I("s.*"),
		).
		InnerJoin(
			goqu.I(ReferenceTable).As("ref"),
			goqu.On(
				goqu.I("s.uuid").Eq(goqu.I("ref.source_uuid")),
				goqu.I("ref.source_type").Eq(string(model.AlaudaServiceType)),
				eq,
			),
		).ScanStructs(&rows)

	if err != nil {
		return nil, err
	}
	for _, item := range rows {
		if res, err := item.ToResponse(); err != nil {
			return items, err
		} else {
			if isServiceDeleted(res) {
				continue
			}
			items = append(items, res)
		}
	}
	return items, nil
}

func updateService(record goqu.Record, eq goqu.Ex, tx *goqu.TxDatabase) (int64, error) {
	var result sql.Result
	var err error
	if tx == nil {
		result, err = GoQuDB.From(ServiceTable).Where(eq).Update(record).Exec()
	} else {
		result, err = tx.From(ServiceTable).Where(eq).Update(record).Exec()
	}
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}

func UpdateServiceYaml(uuid string, current []*model.KubernetesResource, previous []*model.KubernetesResource) (int64, error) {
	cu, err := json.Marshal(current)
	if err != nil {
		return 0, errors.Annotate(err, "marshal resources to bytes error")
	}
	pr, err := json.Marshal(previous)
	if err != nil {
		return 0, errors.Annotate(err, "marshal resources to bytes error")
	}
	rd := goqu.Record{
		KubernetesField:       cu,
		KubernetesBackupField: pr,
		UpdatedAtField:        goqu.L("now()"),
	}
	ex := goqu.Ex{UUIDField: uuid}
	return updateService(rd, ex, nil)
}
