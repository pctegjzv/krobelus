package db

import (
	"database/sql"

	"gopkg.in/doug-martin/goqu.v4"
)

// GetUUIDS ..
type GetUUIDS struct {
	UUIDS []string `json:"uuids"`
}

// PagedResponse ..
type PagedResponse struct {
	NumPages int           `json:"num_pages"`
	PageSize int           `json:"page_size"`
	Count    int           `json:"count"`
	Results  []*JsonObject `json:"results"`
}

// ResourceBase ..
type ResourceBase struct {
	UUID string `json:"uuid" binding:"required"`
	Name string `json:"name" binding:"required"`
}

type ResourceNameBase struct {
	Name string `json:"name" binding:"required"`
	UUID string `json:"uuid,omitempty"`
}

type Namespace ResourceBase

type Cluster ResourceBase

type ResourceBaseWithType struct {
	UUID string `json:"uuid,omitempty"`
	Name string `json:"name,omitempty"`
	Type string `json:"type,omitempty"`
}

type Parent ResourceBaseWithType

type ReferencedResource ResourceBaseWithType

type ReferencedResources []ReferencedResource

type JsonObject map[string]interface{}

const (
	AppTable       = "krobelus_apps"
	ServiceTable   = "krobelus_services"
	StatusTable    = "krobelus_status"
	ReferenceTable = "krobelus_reference_info"
)

// common db fields
const (
	DescriptionField = "description"

	UpdatedAtField = "updated_at"
	UUIDField      = "uuid"
	NameField      = "name"

	NamespaceUUIDField = "namespace_id"
	NamespaceNameField = "namespace_name"
	ClusterUUIDField   = "cluster_id"
	ClusterNameField   = "cluster_name"

	KubernetesField       = "kubernetes"
	KubernetesBackupField = "kubernetes_backup"
	CurrentStateField     = "current_state"
	TargetStateField      = "target_state"

	CreateMethodField = "create_method"

	TargetUUIDField = "target_uuid"
	TargetTypeField = "target_type"
)

type ResourceStatus string

const (
	dbDriverPostgres = "postgresql"
	dbDriverMysql    = "mysql"
)

// NullStringToDefaultString translate a sql NullString to a string. If it is NUll, set it to ""
func NullStringToDefaultString(n sql.NullString) string {
	if n.Valid {
		return n.String
	}
	return ""
}

// SetDefaultForEmptyString translate a sql NullString to a string. If it is NUll or empty, set it to d
func SetDefaultForEmptyString(n sql.NullString, d string) string {
	if n.Valid && n.String != "" {
		return n.String
	}
	return d
}

// getEx generate a sql eq with uuid. used to query a
// resource by uuid
func getEx(uuid string) goqu.Ex {
	return goqu.Ex{UUIDField: uuid}
}

// IsAlreadyExist is a interface that used for resource create. when we received a create request,  sometimes
// we need to check the resource is not exist in db. Most of the *Request struct should implements this interface.
// Note: implements the interface in *Store (ensure *Request exist)
type IsAlreadyExist interface {
	// IsAlreadyExist check if a target resource already exist in db
	IsAlreadyExist() (bool, error)
}
