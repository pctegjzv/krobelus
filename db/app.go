package db

import (
	"database/sql"
	"encoding/json"
	"time"

	"krobelus/common"
	"krobelus/model"

	"github.com/juju/errors"
	"gopkg.in/doug-martin/goqu.v4"
)

const (
	AppColumnSource   = "source"
	AppColumnCategory = "category"
)

type AppStore struct {
	UUID          string
	Request       *model.AppRequest
	UpdateRequest *model.CommonUpdateRequest
	Response      *model.AppResponse
	Status        *model.AlaudaResourceStatus
	Services      []*ServiceStore
	Others        *model.AppOtherResources
	Logger        common.Log
}

type AppRow struct {
	UUID             string         `db:"uuid"`
	Name             string         `db:"name"`
	ClusterID        string         `db:"cluster_id"`
	ClusterName      string         `db:"cluster_name"`
	NamespaceID      string         `db:"namespace_id"`
	NamespaceName    string         `db:"namespace_name"`
	Source           sql.NullString `db:"source"`
	Category         sql.NullString `db:"category"`
	Description      sql.NullString `db:"description"`
	CurrentState     sql.NullString `db:"current_state"`
	TargetState      sql.NullString `db:"target_state"`
	Kubernetes       string         `db:"kubernetes"`
	KubernetesBackup sql.NullString `db:"kubernetes_backup"`
	CreateMethod     string         `db:"create_method"`
	CreatedAt        time.Time      `db:"created_at"`
	UpdatedAt        time.Time      `db:"updated_at"`
}

// Create an app:
// 1. Save apps to db.
// 2. Save services to db.
// 3. Save reference resources to db.
// 4. Save status to db.
func (store *AppStore) Create() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	if err = insertApp(store, tx); err != nil {
		tx.Rollback()
		return err
	}

	if err = insertServices(store.Services, tx); err != nil {
		tx.Rollback()
		return err
	}

	var status []*StatusStore
	status = append(status, &StatusStore{Status: store.Status, Logger: store.Logger})
	for _, i := range store.Services {
		status = append(status, &StatusStore{Status: i.Status})
		//TODO：  use bulk create instead
		for _, item := range i.ReferenceObjectsList {
			if item.IsEmpty() {
				continue
			}
			err = item.InsertReferences(tx)
			if err != nil {
				tx.Rollback()
				return err
			}
		}
	}
	if err = InsertStatusBulk(status, tx); err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}

func (store *AppStore) ParseOtherResourcesUpdate() {
	newList := store.Others.Request
	oldList := store.Others.Response

	var createList []*model.KubernetesResource
	var updateList []*model.KubernetesResource
	var deleteList []*model.KubernetesResource

	for _, n := range newList {
		result := model.GetSpecificResource(oldList, n.Name, n.Kind)
		if result == nil {
			store.Logger.Infof("Create plan for %s %s", n.Kind, n.Name)
			createList = append(createList, n)
		} else {
			equal, _ := model.IsKubernetesResourceEqual(n, result)
			if !equal {
				store.Logger.Infof("Update plan for %s %s", n.Kind, n.Name)
				updateList = append(updateList, n)
			}
		}
	}
	for _, o := range oldList {
		result := model.GetSpecificResource(newList, o.Name, o.Kind)
		if result == nil {
			store.Logger.Infof("Delete plan for %s %s", o.Kind, o.Name)
			deleteList = append(deleteList, o)
		}
	}

	store.Others.Update = &model.AppOtherResourcesUpdate{
		DeleteList: deleteList,
		UpdateList: updateList,
		CreateList: createList,
	}
}

// Get an app:
// 1. Resource uuid must be set in request.
// 2. Store response will be set.
func (store *AppStore) Get() error {
	uuid := store.GetAppUUID()
	if store.Response == nil {
		store.Response = &model.AppResponse{Type: model.AppType}
	}

	response, err := retrieveApp(goqu.Ex{UUIDField: uuid})
	if err != nil {
		return err
	}
	store.Response = response

	// parse other resources
	var others []*model.KubernetesResource
	for _, item := range store.Response.Kubernetes {
		if !common.IsAppServiceMainResource(item.Kind) {
			others = append(others, item)
		}
	}
	if store.Others == nil {
		store.Others = &model.AppOtherResources{
			Response: others,
		}
	} else {
		store.Others.Response = others
	}

	services, err := listServices(goqu.Ex{ServiceColumnAppID: uuid})
	if err != nil {
		return err
	}
	store.Response.Services = services

	request := store.Request
	if request == nil {
		request = &model.AppRequest{}
	}
	request.Resource = model.AppResource{
		UUID:         response.Resource.UUID,
		Name:         response.Resource.Name,
		CreateMethod: response.Resource.CreateMethod,
	}
	request.Namespace = model.Namespace{
		UUID: response.Namespace.UUID,
		Name: response.Namespace.Name,
	}
	request.Cluster = model.Cluster{
		UUID: response.Cluster.UUID,
		Name: response.Cluster.Name,
	}
	store.Request = request
	return nil
}

// Start an app:
// 1. Services must be set before start.
// 2. Status for all services must be the same.
func (store *AppStore) Start() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	_, err = store.UpdateApp(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = store.UpdateServices(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.UpdateStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}

// Stop an app:
// 1. Services must be set before stop.
// 2. Status for all services must be the same.
func (store *AppStore) Stop() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	_, err = store.UpdateApp(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = store.UpdateServices(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.UpdateStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}

// Delete an app:
// 1. Services must be set before delete.
// 2. Status for all services must be the same.
func (store *AppStore) Delete() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	_, err = store.UpdateApp(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	_, err = store.UpdateServices(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.UpdateStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()
	return nil
}

// Retry an app:
// 1. Services must be set before retry.
// 2. Only services with deploying status will retry.
func (store *AppStore) Retry() error {
	for _, service := range store.Services {
		service.Retry()
	}
	services := store.Services
	store.Services = nil

	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}

	_, err = store.UpdateApp(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	err = store.UpdateStatus(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	tx.Commit()

	store.Services = services
	return nil
}

// Retry an app:
// 1. Services must be set before retry.
// 2. Only services with deploying status will retry.
func (store *AppStore) Update() error {
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}
	for _, sr := range store.Services {
		switch {
		case sr.Response == nil:
			if err := sr.Create(tx); err != nil {
				return err
			}
		case sr.Request == nil:
			if err := sr.Delete(tx); err != nil {
				return err
			}
		default:
			if sr.Status.CurrentState == common.CurrentUpdatedStatus {
				continue
			}
			if err := sr.Update(tx); err != nil {
				return err
			}
		}
	}
	kubernetes, err := json.Marshal(store.Request.Kubernetes)
	if err != nil {
		tx.Rollback()
		return err
	}
	kubernetesBackup, err := json.Marshal(store.Response.Kubernetes)
	if err != nil {
		tx.Rollback()
		return err
	}
	createMethod := store.Response.Resource.CreateMethod
	if store.UpdateRequest.Resource.CreateMethod != nil {
		createMethod = *store.UpdateRequest.Resource.CreateMethod
	}
	_, err = tx.From(AppTable).Where(goqu.Ex{UUIDField: store.UUID}).Update(
		goqu.Record{
			UpdatedAtField:        goqu.L("now()"),
			KubernetesField:       string(kubernetes),
			KubernetesBackupField: string(kubernetesBackup),
			CreateMethodField:     createMethod,
		}).Exec()
	if err != nil {
		tx.Rollback()
		return err
	}

	statusStore := &StatusStore{Status: store.Status, Logger: store.Logger}
	if err := statusStore.InsertOrSet(tx); err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

func (store *AppStore) UpdateDescription(desc string) error {
	record := goqu.Record{DescriptionField: desc, UpdatedAtField: goqu.L("now()")}
	eq := goqu.Ex{UUIDField: store.UUID}
	_, err := GoQuDB.From(AppTable).Where(eq).Update(record).Exec()
	return err
}

// UpdateApp row for app:
// 1. Update updated_at field.
// 2. Update app current_state/target_state if app will be deleted.
func (store *AppStore) UpdateApp(tx *goqu.TxDatabase) (int64, error) {
	status := store.Status
	record := goqu.Record{UpdatedAtField: goqu.L("now()")}
	if status != nil && status.TargetState.IsDeleted() && status.CurrentState.IsDeleting() {
		record[CurrentStateField] = status.CurrentState
		record[TargetStateField] = status.TargetState
	}
	return updateApp(record, goqu.Ex{UUIDField: store.GetAppUUID()}, tx)
}

// Update services for app:
// 1. Update services' status in bulk.
func (store *AppStore) UpdateServices(tx *goqu.TxDatabase) (int64, error) {
	if len(store.Services) < 1 {
		return 0, nil
	}
	status := store.Services[0].Status
	return updateService(
		goqu.Record{
			CurrentStateField: status.CurrentState,
			TargetStateField:  status.TargetState,
			UpdatedAtField:    goqu.L("now()"),
		},
		goqu.Ex{
			UUIDField: store.GetServiceUUIDS(),
		},
		tx,
	)
}

// Update status for both app and services:
// 1. Update status for app if it will be deleted.
// 2. Update services' status for this app.
func (store *AppStore) UpdateStatus(tx *goqu.TxDatabase) error {
	if store.Status != nil && !store.Status.TargetState.IsStopped() {
		statusStore := &StatusStore{Status: store.Status}
		if err := statusStore.InsertOrSet(tx); err != nil {
			tx.Rollback()
			return err
		}
	}

	for _, service := range store.Services {
		statusStore := &StatusStore{Status: service.Status}
		if _, err := statusStore.Set(tx); err != nil {
			tx.Rollback()
			return err
		}
	}
	return nil
}

// ListApps:
// 1. Retrieve apps from krobelus_apps.
// 2. Retrieve services from krobelus_services.
// 3. Apps status will not be set.
func ListApps(uuids []string) ([]*model.AppResponse, error) {
	if len(uuids) == 0 {
		return []*model.AppResponse{}, nil
	}
	services, err := listServices(goqu.Ex{ServiceColumnAppID: uuids})
	if err != nil {
		return nil, err
	}
	apps, err := listApps(goqu.Ex{UUIDField: uuids})
	if err != nil {
		return nil, err
	}
	for _, service := range services {
		uuid := service.Parent.UUID
		for _, app := range apps {
			if app.Resource.UUID == uuid {
				app.Services = append(app.Services, service)
				break
			}
		}
	}
	return apps, nil
}

// GetAppUUID from app store:
// 1. Get app uuid from app.request or app.response.
// 2. Maybe empty if not found.
func (store *AppStore) GetAppUUID() string {
	uuid := ""
	if store.UUID != "" {
		return store.UUID
	}
	if store.Request != nil {
		uuid = store.Request.Resource.UUID
	}
	if uuid == "" && store.Response != nil {
		uuid = store.Response.Resource.UUID
	}
	return uuid
}

// SetServices for app store:
// 1. Copy services in store.Response to store.Services.
// 2. Service store logger will also be set
func (store *AppStore) SetServices() {
	store.Services = []*ServiceStore{}
	for _, service := range store.Response.Services {
		store.Services = append(store.Services, &ServiceStore{
			Response: service,
			Logger:   store.Logger,
		})
	}
}

// GetServiceUUIDs for this app:
func (store *AppStore) GetServiceUUIDS() []string {
	uuids := make([]string, len(store.Services))
	for i, s := range store.Services {
		uuids[i] = s.GetServiceUUID()
	}
	return uuids
}

// ToRecord is used for transfer app store to db record.
func (store *AppStore) ToRecord() (goqu.Record, error) {
	req := store.Request
	bytes, err := json.Marshal(req.Kubernetes)
	if err != nil {
		return nil, errors.Annotate(err, "marshal app kubernetes to bytes error")
	}
	if req.Resource.CreateMethod == "" {
		req.Resource.CreateMethod = common.CreateThroughYAML
	}
	return goqu.Record{
		UUIDField:             req.Resource.UUID,
		NameField:             req.Resource.Name,
		ClusterUUIDField:      req.Cluster.UUID,
		ClusterNameField:      req.Cluster.Name,
		NamespaceUUIDField:    req.Namespace.UUID,
		NamespaceNameField:    req.Namespace.Name,
		AppColumnSource:       req.Resource.Source,
		AppColumnCategory:     req.Resource.Category,
		DescriptionField:      req.Resource.Description,
		KubernetesField:       string(bytes),
		KubernetesBackupField: "[]",
		CreateMethodField:     req.Resource.CreateMethod,
		CurrentStateField:     store.Status.CurrentState,
		TargetStateField:      store.Status.TargetState,
	}, nil
}

// ToResponse is used for transfer db row to app response.
func (row *AppRow) ToResponse() (*model.AppResponse, error) {
	response := &model.AppResponse{
		Resource: model.AppResource{
			UUID:         row.UUID,
			Name:         row.Name,
			Source:       NullStringToDefaultString(row.Source),
			Category:     NullStringToDefaultString(row.Category),
			Description:  NullStringToDefaultString(row.Description),
			CreateMethod: row.CreateMethod,
			Status:       NullStringToDefaultString(row.CurrentState),
			TargetStatus: NullStringToDefaultString(row.TargetState),
			CreatedAt:    row.CreatedAt,
			UpdatedAt:    row.UpdatedAt,
		},
		Cluster: model.Cluster{
			UUID: row.ClusterID,
			Name: row.ClusterName,
		},
		Namespace: model.Namespace{
			UUID: row.NamespaceID,
			Name: row.NamespaceName,
		},
		Services: []*model.ServiceResponse{},
		Type:     model.AppType,
	}
	if err := json.Unmarshal([]byte(row.Kubernetes), &response.Kubernetes); err != nil {
		return nil, err
	}
	kubernetesBackup := SetDefaultForEmptyString(row.KubernetesBackup, "[]")
	if err := json.Unmarshal([]byte(kubernetesBackup), &response.KubernetesBackup); err != nil {
		return nil, err
	}
	return response, nil
}

// isAppDeleted will ignore deleted app in db.
func isAppDeleted(s *AppRow) bool {
	current, target := NullStringToDefaultString(s.CurrentState), NullStringToDefaultString(s.TargetState)
	if current == string(common.CurrentDeletedStatus) && target == string(common.TargetDeletedState) {
		return true
	}
	return false
}

// insertApp to db.
func insertApp(store *AppStore, tx *goqu.TxDatabase) error {
	record, err := store.ToRecord()
	if err != nil {
		return err
	}
	_, err = tx.From(AppTable).Insert(record).Exec()
	return err
}

// GetAppsByName will retrieve all apps with this name
func GetAppsByName(cluster, namespace, name string) ([]*model.AppResponse, error) {
	eq := goqu.Ex{ClusterUUIDField: cluster, NamespaceNameField: namespace, NameField: name}
	result, err := listApps(eq)
	if len(result) == 0 {
		return nil, common.ErrResourceNotExist
	} else {
		return result, err
	}
}

func DeleteApp(uuid string) error {
	eq := goqu.Ex{UUIDField: uuid}
	tx, err := GoQuDB.Begin()
	if err != nil {
		return err
	}
	record := goqu.Record{
		CurrentStateField: common.CurrentDeletedStatus,
		TargetStateField:  common.TargetDeletedState,
	}
	_, err = tx.From(AppTable).Where(eq).Update(record).Exec()
	if err != nil {
		return err
	}

	_, err = tx.From(ServiceTable).Where(goqu.Ex{ServiceColumnAppID: uuid}).Update(record).Exec()
	if err != nil {
		tx.Rollback()
		return err
	}
	tx.Commit()
	return nil
}

// retrieveApp from db.
func retrieveApp(eq goqu.Ex) (*model.AppResponse, error) {
	row := AppRow{}
	result, err := GoQuDB.From(AppTable).Where(eq).ScanStruct(&row)
	if err != nil {
		return nil, errors.Annotate(err, "get app")
	}
	if !result {
		return nil, errors.Annotate(common.ErrResourceNotExist, "get app")
	}
	if isAppDeleted(&row) {
		return nil, common.ErrResourceNotExist
	}
	return row.ToResponse()
}

func ListAppsInCluster(cluster, namespace string) ([]*model.AppResponse, error) {
	eq := goqu.Ex{ClusterUUIDField: cluster}
	if namespace != "" {
		eq[NamespaceNameField] = namespace
	}
	return listApps(eq)
}

// listApps from db.
func listApps(eq goqu.Ex) ([]*model.AppResponse, error) {
	var items []*model.AppResponse
	var rows []AppRow
	err := GoQuDB.From(AppTable).Where(eq).ScanStructs(&rows)
	if err != nil {
		return nil, err
	}
	for _, item := range rows {
		if isAppDeleted(&item) {
			continue
		}
		if res, err := item.ToResponse(); err != nil {
			return items, err
		} else {
			items = append(items, res)
		}
	}
	return items, nil
}

// updateApp from db.
func updateApp(record goqu.Record, eq goqu.Ex, tx *goqu.TxDatabase) (int64, error) {
	result, err := tx.From(AppTable).Where(eq).Update(record).Exec()
	if err != nil {
		return 0, err
	}
	return result.RowsAffected()
}
