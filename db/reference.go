package db

import (
	"krobelus/common"
	"krobelus/model"

	"gopkg.in/doug-martin/goqu.v4"
)

type ReferenceRow struct {
	SourceUUID string `db:"source_uuid"`
	SourceName string `db:"source_name"`
	SourceType string `db:"source_type"`
	TargetUUID string `db:"target_uuid"`
	TargetName string `db:"target_name"`
	TargetType string `db:"target_type"`
	Detail     string `db:"detail"`
}

func (row *ReferenceRow) ToRecord() *goqu.Record {
	return &goqu.Record{
		"source_uuid": row.SourceUUID,
		"source_name": row.SourceName,
		"source_type": row.SourceType,
		"target_uuid": row.TargetUUID,
		"target_name": row.TargetName,
		"target_type": row.TargetType,
		"detail":      row.Detail,
	}
}

func (row *ReferenceRow) ToConfigMapRefBy() *model.ConfigMapAllReferencedBy {
	var detail model.ConfigMapReferencedDetail
	detail.Load(row.Detail)
	return &model.ConfigMapAllReferencedBy{
		UUID:   row.SourceUUID,
		Name:   row.SourceName,
		Type:   model.ResourceType(row.SourceType),
		Detail: &detail,
	}
}

func (row *ReferenceRow) ToCommonRefBy() *model.AllReferencedBy {
	var detail model.ReferencedDetail
	detail.Load(row.Detail)
	return &model.AllReferencedBy{
		UUID:   row.SourceUUID,
		Name:   row.SourceName,
		Type:   row.SourceType,
		Detail: &detail,
	}
}

func (row *ReferenceRow) ToPVCRefBy() *model.PVCAllReferencedBy {
	return &model.PVCAllReferencedBy{
		UUID: row.SourceUUID,
		Name: row.SourceName,
		Type: model.ResourceType(row.SourceType),
	}
}

type ReferencedObjects interface {
	// ToReferenceRows generate db rows (can use to generate goqu record)
	ToReferenceRows() []*ReferenceRow
	// target resource type
	GetType() model.ResourceType
	// target resource list
	GetResources() []*model.ReferencedResource

	// IsEmpty
	IsEmpty() bool

	// InsertReference insert all the reference in db
	InsertReferences(tx *goqu.TxDatabase) error
}

type ConfigMapRef struct {
	ReferenceRows []*ReferenceRow `json:"reference_rows,omitempty"`
}

func (c *ConfigMapRef) GetType() model.ResourceType {
	return model.ConfigMapType
}

func (c *ConfigMapRef) GetResources() []*model.ReferencedResource {
	var result []*model.ReferencedResource
	for _, item := range c.ReferenceRows {
		result = append(result, &model.ReferencedResource{
			UUID: item.TargetUUID,
			Name: item.TargetName,
		})
	}
	return result
}

func (c *ConfigMapRef) ToReferenceRows() []*ReferenceRow {
	return c.ReferenceRows
}

func (c *ConfigMapRef) IsEmpty() bool {
	return len(c.ReferenceRows) == 0
}

func (c *ConfigMapRef) AddConfigMapRefRow(
	request *model.ServiceRequest,
	configmap *model.ConfigMapResponse,
	key,
	location,
	name string,
) {
	row := &ReferenceRow{
		SourceUUID: request.Resource.UUID,
		SourceName: request.Resource.Name,
		SourceType: string(model.AlaudaServiceType),
		TargetUUID: configmap.Resource.UUID,
		TargetName: configmap.Resource.Name,
		TargetType: string(model.ConfigMapType),
	}
	detail := model.ConfigMapReferencedDetail{
		ConfigMapKey: key,
		Location:     location,
		Value:        name,
	}
	row.Detail = detail.ToString()

	c.ReferenceRows = append(c.ReferenceRows, row)

}

func (c *ConfigMapRef) InsertReferences(tx *goqu.TxDatabase) error {

	var records []*goqu.Record
	for _, item := range c.ToReferenceRows() {
		records = append(records, item.ToRecord())
	}

	_, err := tx.From(ReferenceTable).Insert(records).Exec()

	return err
}

type ReferencesStore interface {
	// RetrieveReferences retrieve a resources's referenced resources. For example, a service may reference multiple
	// ConfigMaps, PVCs.
	RetrieveReferences() (*model.ReferencedResources, error)

	// DeleteReferences delete all the resource's referenced
	DeleteReferences() error
}

func (store *ServiceStore) RetrieveReferences() (*model.ReferencedResources, error) {
	result := make(model.ReferencedResources)
	var rows []*ReferenceRow
	if err := GoQuDB.From(ReferenceTable).Where(goqu.Ex{"source_uuid": store.UUID}).ScanStructs(&rows); err != nil {
		return nil, err
	} else {
		for _, row := range rows {
			result[row.TargetType] = append(result[row.TargetType], &model.ReferencedResource{
				UUID: row.TargetUUID,
				Name: row.TargetName,
			})
		}
	}
	return &result, nil

}

func (store *ServiceStore) DeleteReferences() error {
	if _, err := GoQuDB.From(ReferenceTable).Where(goqu.Ex{"source_uuid": store.UUID}).Delete().Exec(); err != nil {
		return err
	}
	return nil
}

type ReferenceByStore interface {
	RetrieveReferenceByList() ([]*ReferenceRow, error)
	IsReferencedBy() bool
	AppendRefsToResponse() error
}

// RefStore is an abstract store for all kinds of objects(configmap,secret,pvc...)
// For now it has some overlap with the old ConfigMapStore....But we are going to replace them with
// RefStore.
type RefStore struct {
	UUID      string
	Reference *model.Reference
}

func (store *RefStore) RetrieveReferenceByList() ([]*ReferenceRow, error) {
	var rows []*ReferenceRow
	err := GoQuDB.From(ReferenceTable).Where(goqu.Ex{"target_uuid": store.UUID}).ScanStructs(&rows)
	return rows, err
}

func (store *RefStore) AppendRefsToResponse() error {
	rows, err := store.RetrieveReferenceByList()
	if err != nil {
		return err
	} else {
		var ref model.Reference
		for _, row := range rows {
			refBy := row.ToCommonRefBy()
			if refBy.Detail.Key != "" {
				if ref.KeyReferencedBy == nil {
					ref.KeyReferencedBy = make(map[string][]*model.AllReferencedBy)
				}
				items := ref.KeyReferencedBy[refBy.Detail.Key]
				items = append(items, refBy)
				ref.KeyReferencedBy[refBy.Detail.Key] = items
			} else {
				ref.ReferencedBy = append(ref.ReferencedBy, refBy)
			}
		}
		store.Reference = &ref
	}
	return nil
}

// Not implements
func (store *RefStore) IsReferencedBy() bool {
	return true
}

func (store *ConfigMapStore) RetrieveReferenceByList() ([]*ReferenceRow, error) {
	var rows []*ReferenceRow
	err := GoQuDB.From(ReferenceTable).Where(goqu.Ex{"target_uuid": store.UUID}).ScanStructs(&rows)
	return rows, err
}

func (store *ConfigMapStore) IsReferencedBy() bool {
	if len(store.Response.ReferencedBy) != 0 || len(store.Response.KeyReferencedBy) != 0 {
		return true
	}
	return false
}

func (store *ConfigMapStore) AppendRefsToResponse() error {
	rows, err := store.RetrieveReferenceByList()
	if err != nil {
		return err
	} else {
		for _, row := range rows {
			refBy := row.ToConfigMapRefBy()
			if refBy.Detail.ConfigMapKey != "" {
				if store.Response.KeyReferencedBy == nil {
					store.Response.KeyReferencedBy = make(map[string][]*model.ConfigMapAllReferencedBy)
				}
				items := store.Response.KeyReferencedBy[refBy.Detail.ConfigMapKey]
				items = append(items, refBy)
				store.Response.KeyReferencedBy[refBy.Detail.ConfigMapKey] = items
			} else {
				store.Response.ReferencedBy = append(store.Response.ReferencedBy, refBy)
			}
		}
	}
	return nil
}

// ParseContainsResourceItems mainly used by AlaudaService or AlaudaApp. When we create/update these resources,
// we need to find out what other resources it ref in it's yaml. so we can decide whether the use has the permission to
// do the action.
type ParseContainsResourceItems interface {
	// ParseResourceItems will parse a list of referenced resource items.
	ParseResourceItems() []*model.ResourceItem
}

func (store *ServiceStore) ParseResourceItems() []*model.ResourceItem {
	var resources []*model.ResourceItem
	for _, object := range store.ReferenceObjectsList {
		for _, item := range object.GetResources() {
			resources = append(resources, &model.ResourceItem{
				Type:   object.GetType(),
				Name:   item.Name,
				Action: "view",
			})
		}
	}
	return resources

}

// ParseResourceItems will parse the resources and actions need when
// create or update an app.
func (store *AppStore) ParseResourceItems() []*model.ResourceItem {
	var resources []*model.ResourceItem
	for _, service := range store.Services {
		resources = append(resources, &model.ResourceItem{
			Name:   service.Request.Resource.Name,
			Type:   model.AlaudaServiceType,
			Action: "create",
		})
		resources = append(resources, service.ParseResourceItems()...)
	}
	for _, item := range store.Others.Request {
		resources = append(resources, &model.ResourceItem{
			Name:   item.Name,
			Type:   model.ResourceType(item.Kind),
			Action: "create",
		})
	}
	return resources
}

func (store *AppStore) ParseResourceUpdateItems() []*model.ResourceItem {
	var resources []*model.ResourceItem
	for _, service := range store.Services {
		switch {
		case service.Response == nil:
			resources = append(resources, &model.ResourceItem{
				Name:   service.Request.Resource.Name,
				Type:   model.AlaudaServiceType,
				Action: "create",
			})
			resources = append(resources, service.ParseResourceItems()...)
		case service.Request == nil:
			resources = append(resources, &model.ResourceItem{
				Name:   service.Response.Resource.Name,
				Type:   model.AlaudaServiceType,
				Action: "delete",
			})
		default:
			equal, err := model.IsKubernetesResourceListEqual(service.Response.Kubernetes, service.Request.Kubernetes)
			if err != nil {
				store.Logger.Warnf("get error when compare service for app update : %s", err.Error())
			}
			if !equal {
				resources = append(resources, &model.ResourceItem{
					Name:   service.Request.Resource.Name,
					Type:   model.AlaudaServiceType,
					Action: "update",
				})
				resources = append(resources, service.ParseResourceItems()...)
			}
		}
	}

	// other resources
	if store.Others != nil {
		for _, resource := range store.Others.Update.CreateList {
			resources = append(resources, &model.ResourceItem{
				Name:   resource.Name,
				Type:   model.ResourceType(resource.Kind),
				Action: "create",
			})
		}
		for _, resource := range store.Others.Update.DeleteList {
			resources = append(resources, &model.ResourceItem{
				Name:   resource.Name,
				Type:   model.ResourceType(resource.Kind),
				Action: "delete",
			})
		}
		for _, resource := range store.Others.Update.UpdateList {
			resources = append(resources, &model.ResourceItem{
				Name:   resource.Name,
				Type:   model.ResourceType(resource.Kind),
				Action: "update",
			})
		}
	}
	return resources
}

func RetrieveReferenceByList(targetUUID string) ([]*ReferenceRow, error) {

	var rows []*ReferenceRow
	err := GoQuDB.From(ReferenceTable).Where(goqu.Ex{"target_uuid": targetUUID}).ScanStructs(&rows)
	return rows, err
}

func (store *PVCStore) RetrieveReferenceByList() ([]*ReferenceRow, error) {
	return RetrieveReferenceByList(store.UUID)
}

func (store *PVCStore) AppendRefsToResponse() error {
	rows, err := store.RetrieveReferenceByList()

	if err != nil {
		return err
	} else {
		if len(rows) == 0 {
			if string(common.StatusBound) == store.Response.Resource.Status {
				store.Response.Resource.Status = string(common.StatusAvailable)
			}
		} else {
			for _, row := range rows {
				refBy := row.ToPVCRefBy()
				store.Response.ReferencedBy = append(store.Response.ReferencedBy, refBy)
			}
		}
	}
	return nil
}

type PVCRef struct {
	ReferenceRows []*ReferenceRow `json:"reference_rows,omitempty"`
}

func (c *PVCRef) GetType() model.ResourceType {
	return model.PersistentVolumeClaimType
}

func (c *PVCRef) GetResources() []*model.ReferencedResource {
	var result []*model.ReferencedResource
	for _, item := range c.ReferenceRows {
		result = append(result, &model.ReferencedResource{
			UUID: item.TargetUUID,
			Name: item.TargetName,
		})
	}
	return result
}

func (c *PVCRef) ToReferenceRows() []*ReferenceRow {
	return c.ReferenceRows
}

func (c *PVCRef) IsEmpty() bool {
	return len(c.ReferenceRows) == 0
}

func (c *PVCRef) InsertReferences(tx *goqu.TxDatabase) error {

	var records []*goqu.Record
	for _, item := range c.ToReferenceRows() {
		records = append(records, item.ToRecord())
	}

	_, err := tx.From(ReferenceTable).Insert(records).Exec()

	return err
}

func (c *PVCRef) AddPVCRefRow(request *model.ServiceRequest, pvc *model.PVCResponse) {
	row := &ReferenceRow{
		SourceUUID: request.Resource.UUID,
		SourceName: request.Resource.Name,
		SourceType: string(model.AlaudaServiceType),
		TargetUUID: pvc.Resource.UUID,
		TargetName: pvc.Resource.Name,
		TargetType: string(model.PersistentVolumeClaimType),
	}
	c.ReferenceRows = append(c.ReferenceRows, row)
}
