package websocket

import (
	"encoding/json"
	"fmt"
	"io"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"

	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/remotecommand"

	"github.com/gorilla/websocket"
)

// ContainerInfo defines the params used in a session
type ContainerInfo struct {
	Cluster   string `json:"cluster"`
	Namespace string `json:"namespace"`
	Pod       string `json:"pod"`
	Container string `json:"container"`
}

// PtyHandler is what remotecommand expects from a pty
type PtyHandler interface {
	io.Reader
	io.Writer
	remotecommand.TerminalSizeQueue
}

// TerminalSession implements PtyHandler (using a gorilla websocket connection)
type TerminalSession struct {
	*ContainerInfo
	id        string
	Done      chan struct{}
	SizeChan  chan remotecommand.TerminalSize
	WSConn    *websocket.Conn
	Client    rest.Interface
	ClientCfg *rest.Config
	Redis     *infra.RedisCache
	Logger    common.Log
}

// TerminalMessage is the messaging protocol between ShellController and TerminalSession.
//
// OP      DIRECTION  FIELD(S) USED  DESCRIPTION
// ---------------------------------------------------------------------
// bind    fe->be     SessionID      Id sent back from TerminalResponse
// stdin   fe->be     Data           Keystrokes/paste buffer
// resize  fe->be     Rows, Cols     New terminal size
// stdout  be->fe     Data           Output from the process
// toast   be->fe     Data           OOB message to be shown to the user
type TerminalMessage struct {
	Op        string
	Data      string
	SessionID string
	Shell     string
	Rows      uint16
	Cols      uint16
}

// TerminalSize handles pty->process resize events
// Called in a loop from remotecommand as long as the process is running
func (t TerminalSession) Next() *remotecommand.TerminalSize {
	select {
	case size := <-t.SizeChan:
		return &size
	}
}

// Read handles pty->process messages (stdin, resize)
// Called in a loop from remotecommand as long as the process is running
func (t TerminalSession) Read(p []byte) (int, error) {
	mt, message, err := t.WSConn.ReadMessage()
	t.Logger.Debugf("read: mt=%d message=%s", mt, message)
	if err != nil {
		t.Logger.Errorf("read error: err=%+v", err)
		return 0, err
	}

	var msg TerminalMessage
	if err := json.Unmarshal([]byte(message), &msg); err != nil {
		t.Logger.Errorf("read unmarshal error: err=%v msg=%v", err, msg)
		return 0, err
	}

	switch msg.Op {
	case "echo":
		return 0, t.Echo(msg)
	case "stdin":
		return copy(p, msg.Data), nil
	case "resize":
		t.SizeChan <- remotecommand.TerminalSize{msg.Cols, msg.Rows}
		return 0, nil
	default:
		return 0, fmt.Errorf("unknown message type '%s'", msg.Op)
	}
}

// Write handles process->pty stdout
// Called from remotecommand whenever there is any output
func (t TerminalSession) Write(p []byte) (int, error) {
	t.Logger.Debugf("write: receive=%s", string(p))
	msg := TerminalMessage{
		Op:   "stdout",
		Data: string(p),
	}

	if err := t.WSConn.WriteJSON(msg); err != nil {
		t.Logger.Errorf("write error: err=%+v", err)
		return 0, err
	}
	return len(p), nil
}

// Toast can be used to send the user any OOB messages
// hterm puts these in the center of the terminal
func (t TerminalSession) Toast(p string) error {
	msg := TerminalMessage{
		Op:   "toast",
		Data: p,
	}

	if err := t.WSConn.WriteJSON(msg); err != nil {
		return err
	}
	return nil
}

// Close cleanly close the connection by sending a close message and then
// waiting (with timeout) for the server to close the connection.
func (t TerminalSession) Close() {
	t.WSConn.Close()
	close(t.Done)
	t.Logger.Info("Terminal session closed")
}

func (t TerminalSession) Echo(msg TerminalMessage) error {
	err := t.WSConn.WriteJSON(msg)
	if err != nil {
		t.Logger.Errorf("echo error; err=%v", err)
	}
	return err
}

func (t *TerminalSession) Ping(stopCh <-chan struct{}) {

	pingInterval := config.GlobalConfig.WebSocket.PingInterval
	idleTimeout := config.GlobalConfig.WebSocket.IdleTimeout

	ticker := time.NewTicker(time.Duration(pingInterval) * time.Second)
	writeWait := time.Second * 10
	unHealthyStateCnt := 0

	pingLimit := idleTimeout/pingInterval + 1

	defer func() {
		t.Logger.Infof("ping: stopped")
		ticker.Stop()
	}()

	for {
		select {
		case <-ticker.C:
			if err := t.WSConn.WriteControl(websocket.PingMessage, []byte{}, time.Now().Add(writeWait)); err != nil {
				t.Logger.Infof("ping error: err=%v", err)
				unHealthyStateCnt = unHealthyStateCnt + 1
				if unHealthyStateCnt >= pingLimit {
					t.Logger.Infof("ping: reaching idle timeout %d, closing connection", idleTimeout)
					t.Close()
					return
				}
			} else {
				t.Logger.Info("ping success")
				unHealthyStateCnt = 0
			}
		case s := <-stopCh:
			t.Logger.Infof("ping: stop signal received: %v", s)
			return
		case <-t.Done:
			t.Logger.Info("ping: done")
			return
		}
	}
}
