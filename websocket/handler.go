package websocket

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"krobelus/common"
	"krobelus/config"
	"krobelus/infra"

	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/remotecommand"

	"github.com/gorilla/websocket"
	"k8s.io/api/core/v1"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:    config.GlobalConfig.WebSocket.ReadBufferSize,
	WriteBufferSize:   config.GlobalConfig.WebSocket.WriteBufferSize,
	HandshakeTimeout:  time.Duration(config.GlobalConfig.WebSocket.HandShakeTimeOut),
	EnableCompression: true,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type WSHandler struct {
	StopCh chan struct{}
}

func toCurl(r *http.Request) string {
	headers := ""
	for key, values := range r.Header {
		for _, value := range values {
			headers += fmt.Sprintf(` -H %q`, fmt.Sprintf("%s: %s", key, value))
		}
	}
	return fmt.Sprintf("curl -k -v -X%s %s '%s'", r.Method, headers, r.URL.RequestURI())
}

// HandleTerminalSession ..
func (h *WSHandler) HandleTerminalSession(w http.ResponseWriter, r *http.Request) {
	logger := common.GetLoggerByID("")

	logger.Info("Start")

	logger.Infof("Requesting url: %s", toCurl(r))

	conn, err := upgrader.Upgrade(w, r, nil)

	if err != nil {
		logger.Infof("upgrader error: err=%v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	redis, err := infra.GetRedis()
	if err != nil {
		logger.Errorf("Get redis error: %v", err)
		return
	}

	terminalSession := TerminalSession{
		WSConn:   conn,
		Done:     make(chan struct{}),
		SizeChan: make(chan remotecommand.TerminalSize),
		Redis:    redis,
		Logger:   logger,
	}

	defer terminalSession.Close()

	var (
		buf []byte
		msg TerminalMessage
	)

	if _, buf, err = conn.ReadMessage(); err != nil {
		logger.Infof("HandleTerminalSession: can't Recv: %v", err)
		return
	}

	if err = json.Unmarshal([]byte(buf), &msg); err != nil {
		logger.Infof("HandleTerminalSession: can't UnMarshal (%v): %s", err, buf)
		return
	}

	if err = HandleSession(&terminalSession, msg.SessionID); err != nil {
		logger.Errorf("HandleTerminalSession: can't find session '%s'", msg.SessionID)
		return
	}

	if msg.Op != "bind" {
		logger.Infof("HandleTerminalSession: expected 'bind' message, got: %s", msg.Op)
		return
	}

	k8sClient, err := infra.NewKubeClient(terminalSession.Cluster)

	if err != nil {
		logger.Errorf("Get kubernetes client error: err=%v", err)
		return
	}

	terminalSession.Client, err = k8sClient.ClientForGroupVersionResource("", "pods")
	if err != nil {
		logger.Errorf("Get kubernetes native client error: err=%v", err)
		return
	}

	terminalSession.ClientCfg, err = k8sClient.RestClientConfig("pods")
	if err != nil {
		logger.Errorf("Get kubernetes native client config error: err=%v", err)
		return
	}

	go terminalSession.Ping(h.StopCh)

	validShells := []string{"sh", "powershell", "cmd"}

	logger.Infof("Got session and shell: shell=%s sessionID=%s", msg.Shell, msg.SessionID)

	if common.StringInSlice(msg.Shell, validShells) {
		cmd := []string{msg.Shell}
		err = startProcess(&terminalSession, cmd, terminalSession)
	} else {
		// No shell given or it was not valid: try some shells until one succeeds or all fail
		// FIXME: if the first shell fails then the first keyboard event is lost
		for _, testShell := range validShells {
			cmd := []string{testShell}
			if err = startProcess(&terminalSession, cmd, terminalSession); err == nil {
				break
			}
		}
	}

	if err != nil {
		logger.Errorf("Error occured on remote connection: err=%v", err)
	} else {
		logger.Info("Remote connection closed")
	}

	logger.Info("Finish")

}

func startProcess(t *TerminalSession, cmd []string, ptyHandler PtyHandler) error {
	k8sClient := t.Client
	cfg := t.ClientCfg
	containerInfo := t.ContainerInfo

	podName := containerInfo.Pod
	containerName := containerInfo.Container
	namespace := containerInfo.Namespace

	req := k8sClient.Post().
		Resource("pods").
		Name(podName).
		Namespace(namespace).
		SubResource("exec")

	req.VersionedParams(&v1.PodExecOptions{
		Container: containerName,
		Command:   cmd,
		Stdin:     true,
		Stdout:    true,
		Stderr:    true,
		TTY:       true,
	}, scheme.ParameterCodec)

	exec, err := remotecommand.NewSPDYExecutor(cfg, "POST", req.URL())

	t.Logger.Debugf("startProcess: %v", exec)

	if err != nil {
		return err
	}

	return exec.Stream(remotecommand.StreamOptions{
		Stdin:             ptyHandler,
		Stdout:            ptyHandler,
		Stderr:            ptyHandler,
		TerminalSizeQueue: ptyHandler,
		Tty:               true,
	})
}
