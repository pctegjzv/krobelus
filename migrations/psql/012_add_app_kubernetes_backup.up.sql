ALTER TABLE krobelus_apps ADD COLUMN kubernetes_backup TEXT DEFAULT '[]';
ALTER TABLE krobelus_services ALTER COLUMN kubernetes_backup SET DEFAULT '[]';
UPDATE krobelus_services SET kubernetes_backup='[]' WHERE kubernetes_backup IS NULL or kubernetes_backup='';