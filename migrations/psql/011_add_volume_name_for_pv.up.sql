DO $$
BEGIN
  BEGIN
    ALTER TABLE krobelus_persistentvolume ADD COLUMN volume_name varchar(256);
    EXCEPTION
    WHEN duplicate_column THEN RAISE NOTICE 'column <volume_name> already exists in <krobelus_persistentvolume>.';
  END;
END;
$$;
