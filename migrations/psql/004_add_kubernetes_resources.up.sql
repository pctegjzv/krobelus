CREATE TABLE IF NOT EXISTS krobelus_kubernetes_resources (
  uuid         VARCHAR(36)              NOT NULL PRIMARY KEY,
  name         VARCHAR(64)              NOT NULL,
  namespace_id VARCHAR(36)                       DEFAULT '',
  resource_id  VARCHAR(36)              NOT NULL,
  api_version  VARCHAR(36)              NOT NULL,
  kind         VARCHAR(36)              NOT NULL,
  kubernetes   TEXT                              DEFAULT '',
  created_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  CONSTRAINT resource_name_api_kind UNIQUE (resource_id, name, api_version, kind)
);


CREATE INDEX idx_resource_namespace
  ON krobelus_kubernetes_resources (namespace_id);

CREATE  INDEX idx_resource_resource_id
  ON krobelus_kubernetes_resources (resource_id)