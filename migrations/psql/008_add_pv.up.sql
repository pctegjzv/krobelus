CREATE TABLE IF NOT EXISTS krobelus_persistentvolume (
  uuid          VARCHAR(36) NOT NULL PRIMARY KEY,
  type          VARCHAR(16)                       DEFAULT 'PersistentVolume',
  name          VARCHAR(64) NOT NULL,
  cluster_id    VARCHAR(36) NOT NULL,
  cluster_name  VARCHAR(36) NOT NULL,
  driver_name   VARCHAR(36) NOT NULL,
  volume_id     VARCHAR(36) NOT NULL,
  volume_name   VARCHAR(256) NOT NULL,
  kubernetes    TEXT        NOT NULL,
  current_state VARCHAR(16)                       DEFAULT '',
  target_state  VARCHAR(16)                       DEFAULT '',
  created_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

ALTER TABLE krobelus_persistentvolume
  DROP CONSTRAINT IF EXISTS clustered_name;

ALTER TABLE krobelus_persistentvolume
  ADD CONSTRAINT clustered_name_unique UNIQUE (cluster_id, type, name);

DROP INDEX IF EXISTS krobelus_persistentvolume.clustered_name;
CREATE INDEX clustered_name
  ON krobelus_persistentvolume (cluster_id, type, name);

COMMIT;

