CREATE TABLE IF NOT EXISTS krobelus_apps (
  uuid           VARCHAR(36)              NOT NULL PRIMARY KEY,
  name           VARCHAR(64)              NOT NULL,
  cluster_id     VARCHAR(36)              NOT NULL,
  cluster_name   VARCHAR(36)              NOT NULL,
  namespace_id   VARCHAR(36)              NOT NULL,
  namespace_name VARCHAR(64)              NOT NULL,
  source         VARCHAR(32)                       DEFAULT '',
  category       VARCHAR(32)                       DEFAULT '',
  description    VARCHAR(256)                      DEFAULT '',
  current_state  VARCHAR(16)                       DEFAULT '',
  target_state   VARCHAR(16)                       DEFAULT '',
  kubernetes     TEXT                     NOT NULL,
  created_at     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE INDEX  idx_app_namespace
  ON krobelus_apps (namespace_id);