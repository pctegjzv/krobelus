CREATE INDEX idx_service_app_id ON krobelus_services(app_id);

DROP TABLE IF EXISTS krobelus_cluster_resources;
DROP TABLE IF EXISTS krobelus_namespaced_resources;
DROP TABLE IF EXISTS krobelus_kubernetes_resources;