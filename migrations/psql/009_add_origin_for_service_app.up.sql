ALTER TABLE krobelus_apps
    ADD COLUMN create_method VARCHAR(16) DEFAULT 'YAML' NOT NULL;

ALTER TABLE krobelus_services
  ADD COLUMN create_method VARCHAR(16) DEFAULT 'YAML' NOT NULL;

