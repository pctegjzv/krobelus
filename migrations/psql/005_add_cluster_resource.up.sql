CREATE TABLE IF NOT EXISTS krobelus_cluster_resources (
  uuid         VARCHAR(36)              NOT NULL PRIMARY KEY,
  name         VARCHAR(64)              NOT NULL,
  type         VARCHAR(36)              NOT NULL,
  editable     BOOLEAN                           DEFAULT TRUE,
  cluster_id   VARCHAR(36)              NOT NULL,
  cluster_name VARCHAR(36)              NOT NULL,
  kubernetes   TEXT                              DEFAULT '',
  created_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
)