CREATE TABLE IF NOT EXISTS krobelus_status (
  uuid          VARCHAR(36)              NOT NULL PRIMARY KEY,
  type          VARCHAR(16)              NOT NULL,
  current_state VARCHAR(16)              NOT NULL,
  target_state  VARCHAR(16)              NOT NULL,
  queue_id      VARCHAR(64)              NOT NULL,
  message       TEXT                              DEFAULT '',
  created_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);
