CREATE TABLE IF NOT EXISTS krobelus_services (
  uuid              VARCHAR(36)              NOT NULL PRIMARY KEY,
  name              VARCHAR(64)              NOT NULL,
  cluster_id        VARCHAR(36)              NOT NULL,
  cluster_name      VARCHAR(36)              NOT NULL,
  namespace_id      VARCHAR(36)              NOT NULL,
  namespace_name    VARCHAR(64)              NOT NULL,
  app_id            VARCHAR(36)                       DEFAULT '',
  app_name          VARCHAR(64)                       DEFAULT '',
  kubernetes        TEXT                     NOT NULL,
  kubernetes_backup TEXT                              DEFAULT '',
  current_state     VARCHAR(16)                       DEFAULT '',
  target_state      VARCHAR(16)                       DEFAULT '',
  created_at        TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(),
  updated_at        TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE INDEX idx_service_namespace ON krobelus_services(namespace_id);