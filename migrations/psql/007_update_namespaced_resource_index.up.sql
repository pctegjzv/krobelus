BEGIN;
ALTER TABLE krobelus_namespaced_resources
  DROP CONSTRAINT IF EXISTS namespaced_name;
ALTER TABLE krobelus_namespaced_resources
  ADD CONSTRAINT namespaced_resource_unique UNIQUE (cluster_id, namespace_name, type, name);

DROP INDEX IF EXISTS krobelus_namespaced_resources.cluster_namespace_name;
CREATE INDEX namespaced_resource_index
  ON krobelus_namespaced_resources (cluster_id, namespace_name, type, name);

COMMIT;

