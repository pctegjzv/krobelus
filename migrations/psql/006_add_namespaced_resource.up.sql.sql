CREATE TABLE IF NOT EXISTS krobelus_namespaced_resources (
  uuid           VARCHAR(36)              NOT NULL PRIMARY KEY,
  name           VARCHAR(64)              NOT NULL,
  type           VARCHAR(36)              NOT NULL,
  description    VARCHAR(256)             NOT NULL             DEFAULT '',
  namespace_id   VARCHAR(36)              NOT NULL,
  namespace_name VARCHAR(64)              NOT NULL,
  cluster_id     VARCHAR(36)              NOT NULL,
  cluster_name   VARCHAR(36)              NOT NULL,
  kubernetes     TEXT                     NOT NULL             DEFAULT '',
  created_at     TIMESTAMP WITH TIME ZONE NOT NULL             DEFAULT now(),
  updated_at     TIMESTAMP WITH TIME ZONE NOT NULL             DEFAULT now(),
  CONSTRAINT namespaced_name UNIQUE (cluster_id, namespace_name, name)
);

CREATE INDEX cluster_namespace_name
  ON krobelus_namespaced_resources (cluster_id, namespace_name, name);


CREATE TABLE IF NOT EXISTS krobelus_reference_info (
  source_uuid VARCHAR(36)              NOT NULL,
  source_name VARCHAR(64)              NOT NULL,
  source_type VARCHAR(36)                                   DEFAULT 'Service',
  target_uuid VARCHAR(36)              NOT NULL,
  target_name VARCHAR(64)              NOT NULL,
  target_type VARCHAR(36)                                   DEFAULT 'ConfigMap',
  detail      TEXT                     NOT NULL             DEFAULT '',
  created_at  TIMESTAMP WITH TIME ZONE NOT NULL             DEFAULT now(),
  updated_at  TIMESTAMP WITH TIME ZONE NOT NULL             DEFAULT now()
);

CREATE INDEX reference_source_uuid
  ON krobelus_reference_info (source_uuid);

CREATE INDEX reference_target_uuid
  ON krobelus_reference_info (target_uuid);

