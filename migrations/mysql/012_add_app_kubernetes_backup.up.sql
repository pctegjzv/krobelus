ALTER TABLE krobelus_apps ADD COLUMN kubernetes_backup TEXT;
UPDATE krobelus_apps SET kubernetes_backup='[]' WHERE kubernetes_backup IS NULL or kubernetes_backup='';
UPDATE krobelus_services SET kubernetes_backup='[]' WHERE kubernetes_backup IS NULL or kubernetes_backup='';